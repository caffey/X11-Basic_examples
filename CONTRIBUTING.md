Guide to contributing to X11-Basic_examples
===========================================

If you have written a X11-basic program, which you would like to see 
included into this collection, please do not hesitate to contribute
your program. Send me an email, or trigger a merge request on this repro.

best regards
Markus Hoffmann, October 2015

