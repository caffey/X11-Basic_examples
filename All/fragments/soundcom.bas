' *************************************************************************
'                             Soundcomputer
' f�r ATARI ST      ST-BASIC
' *************************************************************************
DEFMOUSE 2
gruen=GET_COLOR(0,65535,0)
rot=GET_COLOR(65535,0,0)
weiss=GET_COLOR(65535,65535,65535)
gelb=GET_COLOR(65535,65535,0)
schwarz=GET_COLOR(0,0,0)
COLOR gruen,gruen
PBOX 1,1,638,398
DIM a(3,7),b(8,2),t$(9)
' Init
FOR ay=0 TO 6
  FOR ax=0 TO 2
    READ a(ax,ay)
  NEXT ax
NEXT ay
FOR bx=0 TO 7
  FOR by=0 TO 1
    READ b(bx,by)
  NEXT by
NEXT bx
FOR i=0 TO 8
  READ t$(i)
NEXT i
' Maske
DEFTEXT ,0,0,26
COLOR schwarz
TEXT 200,35,"Soundcomputer"
DEFTEXT ,0,0,6
TEXT 20,35,"Der mausgesteuerte"
TEXT 425,35,"Version 03 von M.H. 6/1987"
DEFTEXT ,0,0,6
GRAPHMODE 1
FOR i=0 TO 2
  xpos1=10+i*205
  xpos2=xpos1+180
  '
  COLOR schwarz
  PBOX xpos1+3,53,xpos2+3,268
  COLOR rot
  PBOX xpos1,50,xpos2,265
  '
  COLOR schwarz
  PBOX xpos1+20,80,xpos2-110,96
  COLOR weiss
  PBOX xpos1+17,77,xpos2-113,93
  COLOR schwarz
  TEXT xpos1+35,90,"An"
  '
  COLOR schwarz
  PBOX xpos1+120,80,xpos2-10,96
  COLOR weiss
  PBOX xpos1+117,77,xpos2-13,93
  COLOR schwarz
  TEXT xpos1+130,90,"Aus"
NEXT i
COLOR schwarz
PBOX 13,278,603,308
COLOR rot
PBOX 10,275,600,305
COLOR gelb
PBOX 2,370,638,398
DEFTEXT ,0,0,12
COLOR schwarz
TEXT 45,70,"Kanal A"
TEXT 250,70,"Kanal B"
TEXT 450,70,"Kanal C"
DEFTEXT ,0,0,13
FOR i=0 TO 5
  ypos=108+i*30
  LET text$=t$(i)
  FOR k=0 TO 2
    xpos=25+k*205
    TEXT xpos,ypos,text$
  NEXT k
NEXT i
FOR i=0 TO 2
  xpos=25+i*205
  TEXT xpos,298,t$(i+6)
NEXT i
'
COLOR schwarz
PBOX 328,331,523,353
COLOR gelb
PBOX 325,328,520,350
COLOR schwarz
TEXT 355,345,"Ende"
'
COLOR schwarz
PBOX 13,331,118,353
COLOR gelb
PBOX 10,328,115,350
COLOR schwarz
TEXT 25,345,"Speichern"
'
COLOR schwarz
PBOX 128,331,223,353
COLOR gelb
PBOX 125,328,220,350
COLOR schwarz
TEXT 155,345,"AUTO"
'
ausw:
FILESELECT "Ausgabe-File ...","./*.lst","sound.lst",lade$
' lade$="output.ttt"
PRINT lade$
IF lade$=""
  END
ENDIF
IF EXIST(lade$)=-1
  ALERT 1,"Datei existiert schon ! |�berschreiben oder |anf�gen ?",2,"�bersch|anf�gen|Abbruch",balert
  IF balert=3
    GOTO ausw
  ENDIF
  IF balert=1
    OPEN "U",#1,lade$
  ENDIF
  IF balert=2
    OPEN "A",#1,lade$
  ENDIF
ELSE
  OPEN "O",#1,lade$
  CLOSE #1
  OPEN "U",#1,lade$
ENDIF
DEFMOUSE 0
DO
  GOSUB berechnen
  PRINT "wert"
  @werteausgabe
  PRINT "sound"
  @soundausgabe
  VSYNC
  PRINT "maus"
  @mausabfrage
LOOP
PROCEDURE werteausgabe
  FOR i=0 TO 6
    IF i=0 OR i=2
      zs=0
    ELSE
      zs=8
    ENDIF
    IF i=6
      ypos=270
    ELSE
      ypos=80+i*30
    ENDIF
    FOR k=0 TO 2
      xpos=160+zs+k*205
      TEXT xpos,ypos+27,STR$(a(k,i))+" "
    NEXT k
  NEXT i
  LET text$="sound 1,"+STR$(a(0,0))+","+STR$(a(0,2))+","+STR$(a(0,1))+CHR$(13)
  LET text$=text$+"sound 2,"+STR$(a(1,0))+","+STR$(a(1,2))+","+STR$(a(1,1))+CHR$(13)
  LET text$=text$+"sound 3,"+STR$(a(2,0))+","+STR$(a(2,2))+","+STR$(a(2,1))+CHR$(13)
  LET text$=text$+"wave "+STR$(wa1)+","+STR$(wa2)
  LET text$=text$+","+STR$(a(0,6))+","+STR$(a(1,6))+CHR$(13)+"pause "+STR$(a(2,6))
  LET saves$=text$
  TEXT 10,390,text$+" "
RETURN
PROCEDURE berechnen
  PRINT "berechnen"
  wa1$=STR$(a(2,4))+STR$(a(1,4))+STR$(a(0,4))+STR$(a(2,3))+STR$(a(1,3))+STR$(a(0,3))
  wa1=0
  FOR i=6 TO 1 STEP -1
    wa1=wa1+VAL(MID$(wa1$,i,1))*2^(6-i)
  NEXT i
  PRINT "www"
  wa2$=STR$(a(2,5))+STR$(a(1,5))+STR$(a(0,5))
  wa2=0
  FOR i=3 TO 1 STEP -1
    wa2=wa2+VAL(MID$(wa2$,i,1))*2^(3-i)
  NEXT i
  wa3=a(0,6)
  wa4=a(1,6)
RETURN
PROCEDURE soundausgabe
  FOR i=0 TO 2
    SOUND i+1,a(i,0),a(i,2),a(i,1)
  NEXT i
  WAVE wa1,wa2,wa3,wa4
RETURN
PROCEDURE mausabfrage
  mausabfrage:
  DEFMOUSE 0
  VSYNC
  MOUSEEVENT xmais,ymaus,key
  PRINT key
  DEFMOUSE 2
  IF ymaus<305 AND ymaus>93
    ax=-1
    ay=-1
    z=1
    IF xmaus<190
      ax=0
    ENDIF
    IF xmaus>190 AND xmaus<407
      ax=1
    ENDIF
    IF xmaus>407
      ax=2
    ENDIF
    FOR i=0 TO 5
      y=60+i*30
      IF ymaus-28>y AND ymaus-28<y+30
        ay=i
      ENDIF
      z=1
    NEXT i
    IF ymaus>275 AND ymaus<305
      ay=6
    ENDIF
    IF ax=1 AND ay=6
      z=100
    ENDIF
    IF ax=2 AND ay=6
      z=2
    ENDIF
    IF ax=-1 OR ay=-1
      GOTO mausabfrage
    ENDIF
    IF key=1
      x$=STR$(a(ax,ay)-z)
    ENDIF
    IF key=2
      x$=STR$(a(ax,ay)+z)
    ENDIF
    IF LEFT$(x$,1)="-"
      x$=" "+x$
    ENDIF
    a(ax,ay)=VAL(RIGHT$(x$,LEN(x$)))
    @bereich
  ELSE
    IF xmaus>325 AND ymaus>328 AND xmaus<420 AND ymaus<350
      GRAPHMODE 3
      COLOR schwarz
      PBOX 225,328,320,350
      VSYNC
      WAVE 0,0,0,0,0
      CLOSE
      END
    ENDIF
    IF xmaus>125 AND ymaus>328 AND xmaus<220 AND ymaus<350
      GRAPHMODE 3
      COLOR schwarz
      PBOX 225,328,320,350
      WAVE 0,0,0,0,0
      ' auto!=NOT auto!
      VSYNC
    ENDIF
    IF xmaus>10 AND ymaus>328 AND xmaus<115 AND ymaus<350
      GRAPHMODE 3
      COLOR schwarz
      PBOX 10,328,115,350
      VSYNC
      PRINT #1,saves$
      PBOX 10,328,115,350
      VSYNC
      GRAPHMODE 0
    ENDIF
  ENDIF
  IF auto!
    GRAPHMODE 3
    COLOR schwarz
    PBOX 10,328,115,350
    VSYNC
    PRINT #1,saves$
    PBOX 10,328,115,350
    VSYNC
    GRAPHMODE 0
  ENDIF
RETURN
PROCEDURE bereich
  IF z=100
    by=7
  ELSE
    by=ay
  ENDIF
  IF a(ax,ay)<b(by,0)
    a(ax,ay)=b(by,1)
  ENDIF
  IF a(ax,ay)>b(by,1)
    a(ax,ay)=b(by,0)
  ENDIF
RETURN
' --------------------------------------------
' Datas f�r Erstbelegung, Grenzwerte und Texte
' --------------------------------------------
DATA 15,15,15
DATA 3,4,4
DATA 1,5,8
DATA 1,1,1
DATA 0,0,0
DATA 0,0,0
DATA 14,3000,1
'
DATA 0,15
DATA 1,8
DATA 1,12
DATA 0,1
DATA 0,1
DATA 0,1
DATA 0,15
DATA 0,65535
'
DATA "Lautst�rke","Oktave","Note"
DATA "Ton","Rauschen","H�llkurve"
DATA "H�llkurvenform","Periodendauer","Haltedauer"
