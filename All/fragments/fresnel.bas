' Calculates a fresnel table and also plots the values
' Original written in GFA-Basic for ATARI ST
' (c) Markus Hoffmann 20.09.1993
'
PRINT "Abh�ngigkeit der Reflexionskoeffizienten f�r senkrecht und parallel"
PRINT "polarisiertes Licht. n1=1 n2=1.5 (Glas)"
PRINT "Rechnung mit den Fresnelschen Formeln    Markus Hoffmann 20.09.1993"
PRINT
PRINT "Alpha   E/Ee senkrecht       E/Ee parallel"
PRINT STRING$(52,"-")
' Fresnel- Reflexion an transparenten Dielektrika
'
n1=1   ! Vakuum
n2=1.5 ! Glas
'
nr=n2/n1
'
' LINE 0,200,640,200
PLOT 0,200
FOR x=0 TO 90
  a=x
  es=-(SQR(nr^2-(SIN(RAD(a))^2))-COS(RAD(a)))^2/(nr^2-1)
  ep=(nr^2*COS(RAD(a))-SQR(nr^2-SIN(RAD(a))^2))/(nr^2*COS(RAD(a))+SQR(nr^2-SIN(RAD(a))^2))
  PLOT x,400-ABS(es*400)
  PLOT x,400-ABS(ep*400)
  PRINT STR$(a,2)'STR$(es,20,14)'STR$(ep,20,14)
NEXT x
SHOWPAGE
KEYEVENT
QUIT
