' Attempt to emulate 1 and 2-byte Floating point numbers.
'

FOR i=0 TO 255
  PRINT HEX$(i,2),@bin12num(i),HEX$(@num2bin1(@bin12num(i)),2)
NEXT i
QUIT

FOR i=-18 TO 30 STEP 0.25
  PRINT i,HEX$(@num2bin1(i),2),@bin12num(@num2bin1(i))
NEXT i
i=pi*1e3
PRINT i,HEX$(@num2bin1(i),2),@bin12num(@num2bin1(i))
QUIT

FUNCTION bin2num(a)
  LOCAL s,m,e
  IF a=0
    RETURN 0
  ENDIF
  s=BTST(a,15)
  a=a AND 0x7fff
  m=a AND 0x7f
  e=SHR(a,7)
  IF e=255
    RETURN log(0)
  ENDIF
  e=e-127
  m=2^e*(1+m/128)
  IF s
    m=-m
  ENDIF
  RETURN m
ENDFUNC
FUNCTION bin12num(a)
  LOCAL s,m,e
  IF a=0
    RETURN 0
  ENDIF
  s=BTST(a,7)
  a=a AND 0x7f
  m=a AND 0xf
  e=SHR(a,4)
  IF e=7 AND m=0xf
    m=-log(0)
  ELSE
    e=e-3
    m=2^e*(1+m/16)
  ENDIF
  IF s
    m=-m
  ENDIF
  RETURN m
ENDFUNC

' Kodierung der Limitangaben 2 Bytes seee eeee emmm mmmm
FUNCTION num2bin(a)
  LOCAL f,e,b
  f=FALSE
  ret=0
  IF a<0
    a=-a
    f=TRUE
  ENDIF
  IF a=0
    RETURN 0
  ENDIF
  ' e=INT(LOG(a)/LOG(2))
  e=LOGB(a)
  IF e>=127
    ret=0x7F80
  ELSE
    b=a/2^e
    ret=SHL(e+127,7)+(b*128 AND 0x7f)
  ENDIF
  IF f
    ret=(ret OR 0x8000) AND 0xffff
  ENDIF
  RETURN INT(ret)
ENDFUNC
' Kodierung auf 1 Byte   seeemmmm
FUNCTION num2bin1(a)
  LOCAL f,e,b
  f=FALSE
  ret=0
  IF a<0
    a=-a
    f=TRUE
  ENDIF
  IF a=0
    RETURN 0
  ENDIF
  ' e=INT(LOG(a)/LOG(2))
  e=LOGB(a)
  IF e>=7
    ret=0x7f
  ELSE
    b=a/2^e
    ret=SHL(e+3,4)+(b*16 AND 0xf)
  ENDIF
  IF f
    ret=(ret OR 0x80) AND 0xff
  ENDIF
  RETURN INT(ret)
ENDFUNC
