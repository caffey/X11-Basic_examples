
' I found this piece of code somewhere on the internet.
' No Authorship was claimed.
'
' convertet to X11-Basic
'
START:
CLS
variations=1
' RANDOMIZE TIMER

VARIABLES:
A=1
B=1
C=1
D=1
E=1
F=1
G=1
H=1
I=1
J=1
K=1
L=1
M=1
N=1
O=1
P=1
Z=8
A$=""
DIM D$(50000)
cnt=0
col2=GET_COLOR(65535,65535,0)
col0=GET_COLOR(65535,65535,0)
col1=GET_COLOR(10000,10000,10000)
col15=GET_COLOR(0,0,0)
col14=GET_COLOR(0,65535,0)
col13=GET_COLOR(65535,0,0)
COLOR col2,0
SIZEW ,900,700
SHOWPAGE
PAUSE 0.1
CLEARW
BOX 100,100,800,600
LINE 450,100,450,350
LINE 300,350,300,600
LINE 600,350,600,600
LINE 100,350,800,350
AX=275
JX=100
BX=625
CX=100
NX=200
OX=450
PX=700
EX=800
MX=800
FX=200
IX=700
GX=375
HX=525
DX=450
KX=300
LX=600
AY=100
BY=100
FY=350
GY=350
HY=350
IY=350
CY=225
DY=225
EY=225
JY=475
KY=475
LY=475
MY=475
NY=600
OY=600
PY=600
COLOR col0,col15
TEXT AX,AY,"A"
TEXT BX,BY,"B"
TEXT CX,CY,"C"
TEXT DX,DY,"D"
TEXT EX,EY,"E"
TEXT FX,FY,"F"
TEXT GX,GY,"G"
TEXT EX,EY,"E"
TEXT FX,FY,"F"
TEXT GX,GY,"G"
TEXT HX,HY,"H"
TEXT IX,IY,"I"
TEXT JX,JY,"J"
TEXT KX,KY,"K"
TEXT LX,LY,"L"
TEXT MX,MY,"M"
TEXT NX,NY,"N"
TEXT OX,OY,"O"
TEXT PX,PY,"P"
TEXT AX,CY,"Box1"
TEXT BX,EY,"Box2"
TEXT FX,JY,"Box3"
TEXT DX,JY,"Box4"
TEXT IX,JY,"Box5"
REM Each Letter A-P is a Door or Wall which can only be crossed once.
REM Goal is to cross all 16 lines which crossing over twice
MIX=6
GOSUB DOORCHOICE ! Pick a number from 1-6 then find mark center point of that Box as start point
IF CHOICE=1
  B$="Box1"
  COLOR col13
  @pset(AX,CY)
  ZX=AX
  ZY=CY
  GOTO BOX1
ENDIF
IF CHOICE=2
  B$="Box2"
  COLOR col13
  @pset(BX,CY)
  ZX=BX
  ZY=CY
  GOTO BOX2
ENDIF
IF CHOICE=3
  B$="Box3"
  COLOR col13
  @pset(FX,JY)
  ZX=BX
  ZY=JY
  GOTO BOX3
ENDIF
IF CHOICE=4
  B$="Box4"
  COLOR col13
  @pset(OX,JY)
  ZX=OX
  ZY=JY
  GOTO BOX4
ENDIF
IF CHOICE=5
  B$="Box5"
  COLOR col13
  @pset(IX,JY)
  ZX=IX
  ZY=JY
  GOTO BOX5
ENDIF
IF CHOICE=6
  B$="Out."
  COLOR col13
  @pset(OX,650)
  ZX=OX
  ZY=650
  GOTO BOX0
ENDIF
PROCEDURE DOORCHOICE
  l_d:
  CHOICE=INT(RND()*MIX+1)
  IF CHOICE<1 OR CHOICE>MIX
    GOTO l_d
  ENDIF
  COLOR col14,col1
  TEXT 2,10,STR$(LEN(A$))+"  "+B$+"  "+A$
  SHOWPAGE
  PAUSE 0.1
  PRINT choice
RETURN

BOX1:
DOORS=0
MIX=5
IF A=1 OR C=1 OR D=1 OR F=1 OR G=1
  DOORS=1
ENDIF
BOX1OUT:
IF DOORS>0
  GOSUB DOORCHOICE
ELSE
  GOTO DONE
ENDIF
REM arc 300,200,8,rad(10),rad(430):
IF CHOICE=1
  IF A=1
    A=2
    A$=A$+"A"
    Z=1
    COLOR col13
    @LINEto(AX,AY-10)
    GOTO BOX0
  ENDIF
ENDIF
IF CHOICE=2
  IF C=1
    C=2
    A$=A$+"C"
    Z=3
    COLOR col13
    @LINEto(CX-10,CY)
    GOTO BOX0
  ENDIF
ENDIF
IF CHOICE=3
  IF D=1
    D=2
    A$=A$+"D"
    COLOR col13
    @LINEto(DX+50,DY+10)
    GOTO BOX2
  ENDIF
ENDIF
IF CHOICE=4
  IF F=1
    F=2
    A$=A$+"F"
    COLOR col13
    @LINEto(FX-10,FY+50)
    GOTO BOX3
  ENDIF
ENDIF
IF CHOICE=5
  IF G=1
    G=2
    A$=A$+"G"
    COLOR col13
    @LINEto(GX+10,GY+50)
    GOTO BOX4
  ENDIF
ENDIF
GOTO BOX1OUT

BOX2:
DOORS=0
MIX=5
IF B=1 OR D=1 OR H=1 OR I=1 OR E=1
  DOORS=1
ENDIF
BOX2OUT:
IF DOORS>0
  GOSUB DOORCHOICE
ELSE
  GOTO DONE
ENDIF
IF CHOICE=1
  IF B=1
    B=2
    A$=A$+"B"
    Z=2
    COLOR col13
    @lineto(BX,BY-10)
    GOTO BOX0
  ENDIF
ENDIF
IF CHOICE=2
  IF D=1
    D=2
    A$=A$+"D"
    COLOR col13
    @LINEto(DX-50,DY+10)
    GOTO BOX1
  ENDIF
ENDIF
IF CHOICE=3
  IF H=1
    H=2
    A$=A$+"H"
    COLOR col13
    @LINEto(HX-10,HY+50)
    GOTO BOX4
  ENDIF
ENDIF
IF CHOICE=4
  IF I=1 THEN I=2
    A$=A$+"I"
    COLOR col13
    @lineto(IX-10,IY+50)
    GOTO BOX5
  ENDIF
ENDIF
IF CHOICE=5
  IF E=1
    E=2
    A$=A$+"E"
    Z=4
    COLOR col13
    @lineto(EX+10,EY)
    GOTO BOX0
  ENDIF
ENDIF
GOTO BOX2OUT

BOX3:
DOORS=0
MIX=4
IF F=1 OR J=1 OR K=1 OR N=1
  DOORS=1
ENDIF
BOX3OUT:
IF DOORS>0
  GOSUB DOORCHOICE
ELSE
  GOTO DONE
ENDIF
IF CHOICE=1
  IF F=1
    F=2
    A$=A$+"F"
    COLOR col13
    @LINEto(FX+10,FY-50)
    GOTO BOX1
  ENDIF
ENDIF
IF CHOICE=2
  IF J=1
    J=2
    A$=A$+"J"
    Z=5
    COLOR col13
    @LINEto(JX-10,JY)
    GOTO BOX0
  ENDIF
ENDIF
IF CHOICE=3
  IF K=1
    K=2
    A$=A$+"K"
    COLOR col13
    @LINEto(KX+50,KY+10)
    GOTO BOX4
  ENDIF
ENDIF
IF CHOICE=4
  IF N=1
    N=2
    A$=A$+"N"
    Z=7
    COLOR col13
    @LINEto(NX,NY+10)
    GOTO BOX0
  ENDIF
ENDIF
GOTO BOX3OUT

BOX4:
DOORS=0
MIX=5
IF G=1 OR H=1 OR K=1 OR L=1 OR O=1
  DOORS=1
ENDIF
BOX4OUT:
IF DOORS>0
  GOSUB DOORCHOICE
ELSE
  GOTO DONE
ENDIF
IF CHOICE=1
  IF G=1
    G=2
    A$=A$+"G"
    COLOR col13
    @LINEto(GX,GY-50)
    GOTO BOX1
  ENDIF
ENDIF
IF CHOICE=2
  IF H=1
    H=2
    A$=A$+"H"
    COLOR col13
    @lineto(HX,HY-50)
    GOTO BOX2
  ENDIF
ENDIF
IF CHOICE=3
  IF K=1
    K=2
    A$=A$+"K"
    COLOR col13
    @lineto(KX-50,KY)
    GOTO BOX3
  ENDIF
ENDIF
IF CHOICE=4
  IF L=1
    L=2
    A$=A$+"L"
    COLOR col13
    @lineto(LX+50,LY+10)
    GOTO BOX5
  ENDIF
ENDIF
IF CHOICE=5
  IF O=1
    O=2
    A$=A$+"O"
    Z=8
    @lineto(OX,OY+10)
    GOTO BOX0
  ENDIF
ENDIF
GOTO BOX4OUT

BOX5:
DOORS=0
MIX=4
IF I=1 OR L=1 OR M=1 OR P=1
  DOORS=1
ENDIF
BOX5OUT:
IF DOORS>0
  GOSUB DOORCHOICE
ELSE
  GOTO DONE
ENDIF
IF CHOICE=1
  IF I=1
    I=2
    A$=A$+"I"
    COLOR col13
    @lineto(IX,IY-50)
    GOTO BOX2
  ENDIF
ENDIF
IF CHOICE=2
  IF L=1
    L=2
    A$=A$+"L"
    @lineto(LX-50,LY+10)
    GOTO BOX4
  ENDIF
ENDIF
IF CHOICE=3
  IF M=1
    M=2
    A$=A$+"M"
    Z=6
    @lineto(MX+10,MY)
    GOTO BOX0
  ENDIF
ENDIF
IF CHOICE=4
  IF P=1
    P=2
    A$=A$+"P"
    Z=9
    @lineto(PX,PY+10)
    GOTO BOX0
  ENDIF
ENDIF
GOTO BOX5OUT

BOX0:
DOORS=0
MIX=9
IF A=1 OR B=1 OR C=1 OR E=1 OR J=1 OR M=1 OR N=1 OR O=1 OR P=1
  DOORS=1
ENDIF
INTO_A_BOX:
IF DOORS>0
  GOSUB DOORCHOICE
ELSE
  GOTO DONE
ENDIF
IF CHOICE=1
  IF A=1
    A=2
    A$=A$+"A"
    GOSUB DOOR_A
    COLOR col13
    @LINEto(AX,AY+50)
    GOTO BOX1
  ENDIF
ENDIF
IF CHOICE=2
  IF B=1
    B=2
    A$=A$+"B"
    GOSUB DOOR_B
    COLOR col13
    @LINEto(BX,BY+50)
    GOTO BOX2
  ENDIF
ENDIF
IF CHOICE=3
  IF C=1
    C=2
    A$=A$+"C"
    GOSUB DOOR_C
    COLOR col13
    @LINEto(CX+50,CY)
    GOTO BOX1
  ENDIF
ENDIF
IF CHOICE=4
  IF E=1
    E=2
    A$=A$+"E"
    GOSUB DOOR_E
    @lineto(EX-50,EY)
    GOTO BOX2
  ENDIF
ENDIF
IF CHOICE=5
  IF J=1
    J=2
    A$=A$+"J"
    GOSUB DOOR_J
    @lineto(JX+50,JY)
    GOTO BOX3
  ENDIF
ENDIF
IF CHOICE=6
  IF M=1
    M=2
    A$=A$+"M"
    GOSUB DOOR_M
    @lineto(MX-50,MY)
    GOTO BOX5
  ENDIF
ENDIF
IF CHOICE=7
  IF N=1
    N=2
    A$=A$+"N"
    GOSUB DOOR_N
    @lineto(NX,NY-50)
    GOTO BOX3
  ENDIF
ENDIF
IF CHOICE=8
  IF O=1
    O=2
    A$=A$+"O"
    GOSUB DOOR_O
    @lineto(OX,OY-50)
    GOTO BOX4
  ENDIF
ENDIF
IF CHOICE=9
  IF P=1
    P=2
    A$=A$+"P"
    GOSUB DOOR_P
    @lineto(PX,PY-50)
    GOTO BOX5
  ENDIF
ENDIF
GOTO INTO_A_BOX

DONE:
REM if len(A$)>14 then gosub Addchg:rem to count variations of 15 (it''s well over 40,000 - rem out cls)
REM IF LEN(A$)>14 THEN
GOSUB missedline
COLOR col14
TEXT 2,10,STR$(LEN(A$))+"  "+B$+"  "+A$
PAUSE 1.000 !REM Pause to show Picture.
IF LEN(A$)=16
  PRINT "EUREKA!"
  PRINT LEN(A$),B$,A$
  END
ENDIF
A$=""
CLS
REM rem cls if counting variations.
GOTO VARIABLES

PROCEDURE Addchg
  newone=1
  FOR iter=1 TO variations
    IF A$=D$(iter)
      newone=0
    ENDIF
  NEXT iter
  IF newone=1
    D$(variations+1)=A$
    variations=variations+1
    COLOR col14
    TEXT 3,10,STR$(variations)
  ENDIF
RETURN

PROCEDURE missedline
  IF A=1
    PBOX CX,AY,DX,AY+5
  ENDIF
  IF B=1
    PBOX DX,BY,EX,BY+5
  ENDIF
  IF C=1
    PBOX CX,AY,CX+5,FY
  ENDIF
  IF D=1
    PBOX DX,AY,DX+5,FY
  ENDIF
  IF E=1
    PBOX EX,AY,EX+5,FY
  ENDIF
  IF F=1
    PBOX CX,FY,KX,FY+5
  ENDIF
  IF G=1
    PBOX KX,GY,DX,GY+5
  ENDIF
  IF H=1
    PBOX DX,HY,LX,HY+5
  ENDIF
  IF I=1
    PBOX LX,IY,EX,IY+5
  ENDIF
  IF J=1
    PBOX JX,FY,JX+5,NY
  ENDIF
  IF K=1
    PBOX KX,FY,KX+5,NY
  ENDIF
  IF L=1
    PBOX LX,FY,LX+5,NY
  ENDIF
  IF M=1
    PBOX MX,FY,MX+5,NY
  ENDIF
  IF N=1
    PBOX JX,NY,KX,NY+5
  ENDIF
  IF O=1
    PBOX KX,NY,LX,NY+5
  ENDIF
  IF P=1
    PBOX LX,PY,MX,PY+5
  ENDIF
  SHOWPAGE
RETURN

REM These are subs to walk lines outside of large box (Box0) to go into chosen door
PROCEDURE DOOR_A
  IF Z=2
    COLOR col13
    @LINEto(AX,AY-25)
  ENDIF
  IF Z=3 OR Z=5
    COLOR col13
    @LINEto(CX-20,AY-30)
    @LINEto(AX,AY-25)
  ENDIF
  IF Z=4 OR Z=6
    COLOR col13
    @LINEto(EX+25,AY-35)
    @LINEto(AX,AY-25)
  ENDIF
  IF Z=7 OR Z=8 OR Z=9
    COLOR col13
    @LINEto(JX-40,NY+45)
    @LINEto(JX-10,AY-45)
    @LINEto(AX,AY-25)
  ENDIF
RETURN

PROCEDURE DOOR_B
  IF Z=1
    COLOR col13
    @lineto(BX,BY-15)
  ENDIF
  IF Z=3 OR Z=5
    COLOR col13
    @lineto(CX-20,BY-34)
    @lineto(BX,BY-25)
  ENDIF
  IF Z=4 OR Z=6
    COLOR col13
    @lineto(EX+30,BY-30)
    @lineto(BX,BY-25)
  ENDIF
  IF Z=7 OR Z=8 OR Z=9
    COLOR col13
    @lineto(MX+24,NY+24)
    @lineto(MX+35,BY-35)
    @lineto(BX,BY-25)
  ENDIF
RETURN

PROCEDURE DOOR_C
  COLOR col13
  IF Z=1 OR Z=2
    @lineto(CX-25,AY-25)
  ENDIF
  IF Z=5
    @lineto(CX-25,CY)
  ENDIF
  IF Z=4 OR Z=6
    @lineto(EX+30,AY-45)
    @lineto(CX-25,AY-35)
    @lineto(CX-25,CY)
  ENDIF
  IF Z=7 OR Z=8 OR Z=9
    @lineto(CX-25,NY+30)
    @lineto(CX-25,CY)
  ENDIF
RETURN

PROCEDURE DOOR_E
  COLOR col13
  IF Z=1 OR Z=2
    @lineto(EX+10,AY-20)
    @lineto(EX+20,EY)
  ENDIF
  IF Z=3 OR Z=5
    @lineto(CX-10,AY-20)
    @lineto(EX+10,AY-20)
    @lineto(EX+13,EY)
  ENDIF
  IF Z=6
    @lineto(EX+10,EY)
  ENDIF
  IF Z=7 OR Z=8 OR Z=9
    @lineto(EX+10,NY+10)
    @lineto(EX+10,EY)
  ENDIF
RETURN

PROCEDURE DOOR_J
  IF Z=1 OR Z=2
    @lineto(JX-25,AY-20)
    @lineto(JX-25,JY)
  ENDIF
  IF Z=3
    @lineto(JX-10,JY)
  ENDIF
  IF Z=4 OR Z=6
    @lineto(EX+10,PY+25)
    @lineto(JX-30,PY+25)
    @lineto(JX-25,JY)
  ENDIF
  IF Z=7 OR Z=8 OR Z=9
    @lineto(JX-10,NY+30)
    @lineto(JX-20,JY)
  ENDIF
RETURN

PROCEDURE DOOR_M
  IF Z=1 OR Z=2
    @lineto(MX+25,AY-25)
    @lineto(MX+25,MY)
  ENDIF
  IF Z=3 OR Z=5
    @lineto(CX-10,AY-20)
    @lineto(MX+48,AY-25)
    @lineto(MX+25,MY)
  ENDIF
  IF Z=4
    @lineto(MX+25,MY)
  ENDIF
  IF Z=7 OR Z=8 OR Z=9
    @lineto(MX+25,PY+35)
    @lineto(MX+25,MY)
  ENDIF
RETURN

PROCEDURE DOOR_N
  IF Z=1 OR Z=2
    @lineto(CX-30,AY-20)
    @lineto(CX-30,NY+10)
    @lineto(NX,NY+25)
  ENDIF
  IF Z=3 OR Z=5
    @lineto(CX-14,NY+25)
    @lineto(NX,NY+15)
  ENDIF
  IF Z=4 OR Z=6
    @lineto(EX+10,NY+12)
    @lineto(NX,NY+25)
  ENDIF
  IF Z=8 OR Z=9
    @lineto(NX,NY+10)
  ENDIF
RETURN

PROCEDURE DOOR_O
  IF Z=1 OR Z=2
    @lineto(EX+40,AY-35)
    @lineto(EX+40,OY+34)
    @lineto(OX,OY+25)
  ENDIF
  IF Z=3 OR Z=5
    @lineto(CX-10,OY+52)
    @lineto(OX,OY+25)
  ENDIF
  IF Z=4 OR Z=6
    @lineto(EX+10,OY+52)
    @lineto(OX,OY+25)
  ENDIF
  IF Z=7 OR Z=9
    @lineto(OX,NY+10)
  ENDIF
RETURN

PROCEDURE DOOR_P
  IF Z=1 OR Z=2
    COLOR col13
    @LINEto(EX+35,AY-25)
    @LINEto(EX+35,PY+12)
    @LINEto(PX,PY+25)
  ENDIF
  IF Z=3 OR Z=5
    COLOR col13
    @lineto(CX-32,PY+42)
    @LINEto(PX,PY+25)
  ENDIF
  IF Z=4 OR Z=6
    COLOR col13
    @LINEto(EX+10,PY+10)
    @LINEto(PX,PY+26)
  ENDIF
  IF Z=7 OR Z=8
    COLOR col13
    @LINEto(PX,NY+10)
  ENDIF
RETURN
PROCEDURE pset(nx,ny)
  PLOT nx,ny
  ox=nx
  oy=ny
RETURN
PROCEDURE lineto(nx,ny)
  LINE ox,oy,nx,ny
  ox=nx
  oy=ny
  SHOWPAGE
RETURN
