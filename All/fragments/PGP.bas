' Implementation of PGP (decodes PGP messages) written in X11-Basic
' (funktioniert noch nicht ....)
'
'
t$="-----BEGIN PGP MESSAGE-----"+CHR$(10)
t$=t$+"Version: GnuPG v2.0.16 (GNU/Linux)"+CHR$(10)
t$=t$+""+CHR$(10)
t$=t$+"hQEMA1PUVhZb8UnsAQf+KS9PNvkWYFONnoStveMc4KwvGT7WlRFv/ZACvdyFsKDO"+CHR$(10)
t$=t$+"icurhL57uh56KCof1m5drfftwjDQWgNyMy0cixqV/2WzeQgjZILE0Z1FDg7cgAbs"+CHR$(10)
t$=t$+"UZvy2hmaJf0dhHEUziALotfUMhoSeHeObxmomzb7vovJv5tWDtQ9W+p2tbQ4tiin"+CHR$(10)
t$=t$+"LAsJtwQhEVPNltootBteC0dTgOdISe6kfqUSoN3A22SiSUihmjxMPiiO6iZB8gBS"+CHR$(10)
t$=t$+"hhfiSPa4khNwODncRe2BjqW+YQHf7L6CfLjx2S1BCSr+KWLmUnVdWSUonhHPF9mI"+CHR$(10)
t$=t$+"E/q7t2uoBWg0iQgCjQubgYeqSUYN/xWpqAUX9O71zdKUAbVjjLVT0qTjNLLvms2H"+CHR$(10)
t$=t$+"s4BDzHEqKeuGuMAWFzyfuW+VNofTxtcHhzrdjPuYi7sRL3YNUvqUpcGeKGyTApW2"+CHR$(10)
t$=t$+"k/fd7U32av7Pq63NoKK2g3RFcyBUiSdNlNhW8TYS1NdMSMXNw1R9dWVgFmsLj2vs"+CHR$(10)
t$=t$+"Rv89ufRiPbNLDXcx7CkRrTf13q0miy1850d6k5nt8qUFrnh4xQ=="+CHR$(10)
t$=t$+"=z6Xk"+CHR$(10)
t$=t$+"-----END PGP MESSAGE-----"+CHR$(10)

SPLIT t$,CHR$(10),0,a$,t$
SPLIT t$,CHR$(10),0,a$,t$
SPLIT t$,CHR$(10),0,a$,t$
t$=REPLACE$(t$,CHR$(10),"")
y$=@base64$(t$)
MEMDUMP varptr(y$),LEN(y$)
QUIT
s$=HASH$(t$)+MKL$(1)+CHR$(2)+CHR$(2)
MEMDUMP varptr(s$),LEN(s$)
f$=@radix64$(s$)
PRINT enclose$(f$)
y$=@base64$(f$)
MEMDUMP varptr(y$),LEN(y$)
QUIT
FUNCTION decode(a)
  IF a>=ASC("A") AND a<=ASC("Z")
    RETURN a-ASC("A")
  ELSE if a>=ASC("a") AND a<=ASC("z")
    RETURN a-ASC("a")+26
  ELSE if a>=ASC("0") AND a<=ASC("9")
    RETURN a-ASC("0")+52
  ELSE if a=ASC("+")
    RETURN 62
  ELSE if a=ASC("/")
    RETURN 63
  ELSE if a=ASC("=")
    RETURN 0
  ENDIF
  PRINT "ERROR: a=";a
  RETURN -1
ENDFUNCTION
FUNCTION encode(a)
  IF a<=25
    RETURN asc("A")+a
  ELSE if a<=51
    RETURN asc("a")+a-26
  ELSE if a<=61
    RETURN asc("0")+a-52
  ELSE if a=62
    RETURN asc("+")
  ELSE if a=63
    RETURN asc("/")
  ENDIF
  PRINT "ERROR: a=";a
  RETURN -1
ENDFUNCTION
FUNCTION base64$(a$)
  LOCAL r$,i
  r$=""
  FOR i=0 TO LEN(a$)/4-1
    a=PEEK(VARPTR(a$)+4*i) and 255
    b=PEEK(VARPTR(a$)+4*i+1) and 255
    c=PEEK(VARPTR(a$)+4*i+2) and 255
    d=PEEK(VARPTR(a$)+4*i+3) and 255
    r$=r$+CHR$(@decode(a)*4+(SHR(@decode(b),4) AND 0x3))
    IF d<>asc("=") OR c<>asc("=")
      r$=r$+CHR$(@decode(b)*16+(SHR(@decode(c),2) AND 0xf))
    ENDIF
    IF d<>asc("=")
      r$=r$+CHR$(@decode(c)*64+@decode(d))
    ENDIF
  NEXT i
  RETURN r$
ENDFUNCTION
FUNCTION radix64$(g$)
  LOCAL l,pointer,t$
  t$=""
  l=LEN(g$)
  pointer=0
  WHILE l>=3
    t$=t$+CHR$(@encode((PEEK(VARPTR(g$)+pointer*3) AND 0xfc)/4))
    t$=t$+CHR$(@encode((PEEK(VARPTR(g$)+pointer*3) AND 0x3)*16+(PEEK(VARPTR(g$)+pointer*3+1) AND 0xf0)/16))
    t$=t$+CHR$(@encode((PEEK(VARPTR(g$)+pointer*3+1) AND 0xf)*4+(PEEK(VARPTR(g$)+pointer*3+2) AND 0xc0)/64))
    t$=t$+CHR$(@encode((PEEK(VARPTR(g$)+pointer*3+2) AND 0x3f)))
    SUB l,3
    INC pointer
  WEND
  IF l ! handle the last 1 or 2 bytes
    t$=t$+CHR$(@encode((PEEK(VARPTR(g$)+pointer*3) AND 0xfc)/4))
    IF l=1
      t$=t$+CHR$(@encode((PEEK(VARPTR(g$)+pointer*3) AND 0x3)*16))+"=="
    ELSE
      t$=t$+CHR$(@encode((PEEK(VARPTR(g$)+pointer*3) AND 0x3)*16+(PEEK(VARPTR(g$)+pointer*3+1) AND 0xf0)/16))
      IF l=2
        t$=t$+CHR$(@encode((PEEK(VARPTR(g$)+pointer*3+1) AND 0xf)*4))+"="
      ELSE
        t$=t$+CHR$(@encode((PEEK(VARPTR(g$)+pointer*3+1) AND 0xf)*4+(PEEK(VARPTR(g$)+pointer*3+2) AND 0xc0)/64))
        t$=t$+CHR$(@encode((PEEK(VARPTR(g$)+pointer*3+2) AND 0x3f)))
      ENDIF
    ENDIF
  ENDIF
  RETURN t$
ENDFUNCTION
