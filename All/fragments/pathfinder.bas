
' INTENTION as defined    (c) Markus Hoffmann 2006

name$="global"
commandparm$="global.cmd_dc"
statusparm$="global.state_dm"

' Here the Transitionmatrix is defined.
' "nop" means: no action
' "-" means: forbidden or impossible transition
' procedure names starting with "i" are internal
' procedure names starting with "p" are derived from other intentions

n=4 ! Number of States

t$()=["nop","p1","-","-";"p2","nop","p3","-";"p4","p5","nop","p6";"p7","p8","-","nop"]

' Here the inverse Lengthes of internal procedures is defined

in=0 ! number of internal procedures
ilen()=[]

' Here the actions for the p-Functions are defined:

pn=8
paction$()=["nop","vacuum(5);timing(4);rf(3)","","","","","",""]

' they are dined as a ";" separated list of other intentions, like
' vacuum(5) means the vacuum subsystem should be in state 5
depth=0

PRINT "von 0 nach 1:";@path$(0,1)
PRINT "von 0 nach 2:";@path$(0,2)
PRINT "von 0 nach 3:";@path$(0,3)
PRINT "von 3 nach 0:";@path$(3,0)
QUIT

FUNCTION path$(a,b)
  LOCAL i,z,c$,g$,a$,b$
  z=0
  c$=""
  IF a=b
    RETURN "(nop)"
  ENDIF
  IF depth>2*n+1
    ' "Recursion depth too high. There must be a loop!"
    RETURN "(end)"
  ENDIF
  INC depth

  FOR i=0 TO n-1
    IF t$(a,i)<>"-" AND a<>i
      IF i<>b
        g$=@path$(i,b)
        WHILE len(g$)
          SPLIT g$,CHR$(10),0,a$,g$
          b$="("+STR$(a)+"--"+t$(a,i)+"-->"+STR$(i)+")"+a$
          IF @checkloop(b$)=0
            IF len(c$)
              c$=c$+CHR$(10)
            ENDIF
            c$=c$+b$
            INC z
          ENDIF
        WEND
      ELSE if i=b
        IF len(c$)
          c$=c$+CHR$(10)
        ENDIF
        c$=c$+"("+STR$(a)+"--"+t$(a,i)+"-->"+STR$(i)+")"
        INC z
      ENDIF
    ENDIF
  NEXT i
  DEC depth
  RETURN c$
ENDFUNCTION

FUNCTION checkloop(t$)
  LOCAL a$,b$,i,f,loo,count
  DIM cl(n)
  ARRAYFILL cl(),0
  loo=0
  count=0
  t$=LEFT$(t$,LEN(t$)-1)
  t$=RIGHT$(t$,LEN(t$)-1)
  WHILE len(t$)
    SPLIT t$,")(",0,a$,t$
    IF a$<>"end"
      SPLIT a$,"--",0,b$,a$
      i=VAL(b$)
      SPLIT a$,"-->",0,b$,a$
      f=VAL(a$)
      IF count=0
        cl(i)=1
      ENDIF
      IF cl(f)=1
        loo=1
      ENDIF
      cl(f)=1
      INC count
    ENDIF
  WEND
  RETURN loo
ENDFUNCTION

FUNCTION lengthofpath(t$)
  RETURN 0
ENDFUNCTION

FUNCTION lengthoftransition()
ENDFUNCTION

FUNCTION lengthofexternalaction
ENDFUNCTION

