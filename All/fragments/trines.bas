' Programmiert in X11-Basic Markus Hoffmann 2008
'
epsilon=-0.5

genauigkeit=1e-10    ! Geforderte Genauigkeit fuer theta

' for epsilon=-0.005 to -1.5 step -0.05
FOR x=0.0001 TO 10 STEP 0.1
  y=@theta(x)
  PRINT "tau=";x;" eps=";epsilon;" theta=";y;" +/- ";genauigkeit;" Abweichung(tau)=";x-@tau(y)
NEXT x
' next epsilon
QUIT

' Dies ist die Funktion tau(theta)
FUNCTION tau(theta)
  IF theta>=1
    RETURN -9999999
  ELSE
    RETURN theta+epsilon*ln(1-theta)
  ENDIF
ENDFUNC

' Dies berechnet die Funktion theta(tau)
FUNCTION theta(tau)
  a=tau/(1-epsilon) ! Startwert ergibt sich aus der linearen Näherung
  increment=a/100   ! Anfangsintervall für die Schachtelung
  IF a>=1           ! Vorsicht: Die lin Näherung ergibt Werte >1
    a=1-increment*3
  ENDIF
  fehler=tau-@tau(a)
  WHILE abs(fehler)>genauigkeit AND increment>1e-16
    WHILE fehler>0 AND abs(fehler)>genauigkeit AND a+increment<1
      ADD a,increment
      fehler=tau-@tau(a)
    WEND
    increment=increment/2
    WHILE fehler<0 AND abs(fehler)>genauigkeit
      SUB a,increment
      fehler=tau-@tau(a)
    WEND
    increment=increment/2
    fehler=tau-@tau(a)
  WEND
  RETURN a
ENDFUNCTION
