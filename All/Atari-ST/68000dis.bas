' Disassembler for the M68000 Processor by Markus Hoffmann 1999
' (maybe also useful for the coldfire architecture)
'
'
' still buggy and unfinished.
' Last modification 2015-04-06 X11-Basic 1.23

inputfile$="/work/CDs/ATARI-ST_v_2/atari/disks/Projekte/sp_sek/sp-sek.q/rdskatom.b"

DIM bef$(400),mask%(400),typ$(400),txt$(400),cmp%(400)
DIM symbol%(1000)
CLR anzsymbol%
ARRAYFILL mask%(),0
ARRAYFILL cmp%(),0
CLR i%
RESTORE mc68000_data
DO
  READ bef$(i%)
  EXIT IF bef$(i%)="***"
  FOR gu=0 TO LEN(bef$(i%))-1
    IF PEEK(VARPTR(bef$(i%))+gu)=ASC("0")
      mask%(i%)=BSET(mask%(i%),15-gu)
      cmp%(i%)=BCLR(cmp%(i%),15-gu)
    ELSE IF PEEK(VARPTR(bef$(i%))+gu)=ASC("1")
      mask%(i%)=BSET(mask%(i%),15-gu)
      cmp%(i%)=BSET(cmp%(i%),15-gu)
    ELSE
      cmp%(i%)=BCLR(cmp%(i%),15-gu)
      mask%(i%)=BCLR(mask%(i%),15-gu)
    ENDIF
  NEXT gu
  READ typ$(i%)
  READ txt$(i%)
  INC i%
LOOP
cond$()=["F","SR","HI","LS","CC","CS","NE","EQ","VC","VS","PL","MI","GE","LT","GT","LE"]
size$()=["size0","b","l","w"]

PRINT "read ";i%;" commands."
anzu=i%
'
' Read some 68000 binary file
OPEN "I",#1,inputfile$
memory$=INPUT$(#1,LOF(#1))
CLOSE #1

pcl=VARPTR(memory$) !+28
memlen=LEN(memory$)
MEMDUMP pcl,memlen
'
PRINT "; output of 68000dis.bas"
PRINT "; File: "+inputfile$
DO
  pcsl=pcl
  ccc%=@DPEEK(pcl)
  ' print hex$(ccc%)
  ADD pcl,2
  bll=2
  CLR flb
  eacount=0
  FOR iu=0 TO anzu-1
    IF (ccc% AND mask%(iu))=cmp%(iu)
      ' PRINT BIN$(ccc%,16)''BIN$(mask%(iu),16)''
      t$=txt$(iu)
      posl=INSTR(t$,"$")
      WHILE posl>0
        car$=MID$(t$,posl+1,1)
        al=@wert(car$,iu)
        tf$=@typ$(car$,iu)
        IF tf$="Cond"
          replace$=cond$(al)
        ELSE IF tf$="Areg"
          replace$=@areg$(al)
        ELSE IF tf$="Dreg"
          replace$="d"+STR$(al)
        ELSE IF tf$="Ea(AM_DA)"
          replace$=@ea$(al)
        ELSE IF tf$="Ea(AM_D)"
          replace$=@ea$(al)
        ELSE IF tf$="IEa(AM_DA)"
          replace$=@ea$(al)
        ELSE IF tf$="Ea(AM_MA)"
          replace$=@ea$(al)
        ELSE IF tf$="Ea(AM_DB)"
          replace$=@ea$(al)
        ELSE IF tf$="Ea(AM_C)"
          replace$=@ea$(al)
        ELSE IF tf$="Ea(AM_DNI)"
          replace$=@ea$(al)
        ELSE IF tf$="Ea(*)"
          replace$=@ea$(al)
        ELSE IF tf$="Size"
          replace$=size$(al)
        ELSE IF tf$="Size1"
          replace$=size$(ABS(al=1)*2+0)
        ELSE IF tf$="PCrel"
          replace$="L"+HEX$(pcl+al,8)
        ELSE IF tf$="S8"
          IF ABS(BYTE(al))>15
            replace$="$"+HEX$(BYTE(al),2)
          ELSE
            replace$=STR$(BYTE(al))
          ENDIF
        ELSE IF tf$="SI"
          replace$="$"+HEX$(@DPEEK(pcl),4)
          ADD pcl,2
          ADD bll,2
        ELSE IF tf$="SI32"
          replace$=@value$(@LPEEK(pcl))
          ADD pcl,4
          ADD bll,4
        ELSE IF tf$="SI16" OR tf$="SI8"
          replace$=HEX$(@DPEEK(pcl),4)
          ADD pcl,2
          ADD bll,2
        ELSE IF tf$="PCrel16"
          replace$="L"+HEX$(pcl+@DPEEK(pcl),8)
          ADD pcl,2
          ADD bll,2
        ELSE IF tf$="Q3"
          replace$=STR$(al)
        ELSE IF tf$="U4"
          replace$=STR$(al)
        ELSE IF tf$="U12"
          replace$="$"+HEX$(al,4)
        ELSE
          PRINT car$''tf$''al
          replace$="EEE"+STR$(al)
        ENDIF
        t$=LEFT$(t$,posl-1)+replace$+RIGHT$(t$,LEN(t$)-posl-1)
        posl=INSTR(t$,"$",posl+LEN(replace$))
        ' print posl,t$
      WEND
      PRINT "$"+HEX$(pcl,8)''
      IF @is_symbol(pcl)
        PRINT "L"+HEX$(pcl,8)+": ";
      ELSE
        PRINT space$(10);
      ENDIF
      FOR hl=pcsl TO pcsl+bll-2 STEP 2
        PRINT HEX$(@DPEEK(hl),4);
      NEXT hl
      PRINT SPACE$(2*(10-bll))+t$
      flb=TRUE
      EXIT IF TRUE
    ENDIF
  NEXT iu
  IF NOT flb
    CLR flb
    PRINT "$"+HEX$(pcl,8)''
    IF @is_symbol(pcl)
      PRINT "L"+HEX$(pcl,8)+": ";
    ELSE
      PRINT space$(10);
    ENDIF
    FOR hl=pcsl TO pcsl+bll-2 STEP 2
      PRINT HEX$(@DPEEK(hl),4);
    NEXT hl
    PRINT ,"DC.W $"+HEX$(ccc%,4)
  ENDIF
  EXIT if pcl-varptr(memory$)>memlen
LOOP
'
PRINT "Symbols: ";anzsymbol%

QUIT
'
'
FUNCTION wert(t$,i%)
  LOCAL pos%,w%
  CLR w%
  ' print "WERT: ";bef$(i%);" ";t$;" ";
  pos%=INSTR(bef$(i%),t$)
  WHILE pos%>0
    w%=SHL(w%,1)
    w%=w% OR ABS(BTST(ccc%,16-pos%))
    pos%=INSTR(bef$(i%),t$,pos%+1)
  WEND
  ' print "--> ";bin$(w%,tally(bef$(i%),t$))
  RETURN w%
ENDFUNC
FUNCTION typ$(t$,iu)
  LOCAL posl,w$
  w$="undef"
  FOR posl=1 TO LEN(typ$(iu))
    IF MID$(typ$(iu),posl,1)=t$
      posl=INSTR(typ$(iu),":",posl)
      IF posl=0
        RETURN w$+"1"
      ENDIF
      IF posl=LEN(typ$(iu))
        RETURN w$+"2"
      ENDIF
      RETURN MID$(typ$(iu),posl+1,INSTR(typ$(iu),";",posl)-posl-1)
    ELSE IF MID$(typ$(iu),posl,1)=":"
      posl=INSTR(typ$(iu),";",posl)
      IF posl=0
        RETURN w$+"3"
      ENDIF
      IF posl=LEN(typ$(iu))
        RETURN w$+"4"
      ENDIF
    ELSE IF MID$(typ$(iu),posl,1)=";"
      RETURN w$+"5"
    ENDIF
  NEXT posl
  RETURN w$+"6"
ENDFUNC
'
PROCEDURE bitschiebe
  ' ASL/ASR LSL/LSR ROXL/ROXR ROL/ROR (ea) (Dn,Dn) (#k,ea)
  ' 1110 kkky bbef ghhh
  IF @bits(cu,3,2)=0
    dis$="AS"
  ELSE IF @bits(cu,3,2)=1
    dis$="LS"
  ELSE IF @bits(cu,3,2)=2
    dis$="ROX"
  ELSE IF @bits(cu,3,2)=3
    dis$="RO"
  ENDIF
  IF BTST(8,cu)
    dis$=dis$+"L"
  ELSE
    dis$=dis$+"R"
  ENDIF
  IF @bits(cu,6,2)=&X11
    dis$=dis$+" "+@ea$
  ELSE
    dis$=dis$+@bwl$+" "
    IF BTST(cu,5)
      @dndn
    ELSE
      @kkea
    ENDIF
  ENDIF
RETURN
PROCEDURE linef
  dis$="DC.W $"+HEX$(cu,4)+"    ; LINE-F"
RETURN
PROCEDURE linea
  dis$="DC.W $"+HEX$(cu,4)+"    ; LINE-A"
RETURN
'
'
PROCEDURE kkea
  dis$=dis$+" #"
  IF @bits(cu,9,3)=0
    dis$=dis$+"8"
  ELSE
    dis$=dis$+STR$(@bits(cu,9,3))
  ENDIF
  dis$=dis$+","+@ea$
RETURN
PROCEDURE eadn
  dis$=dis$+" "+@ea$+",D"+STR$(@bits(cu,9,3))
RETURN
PROCEDURE dndn
  dis$=dis$+" D"+STR$(@bits(cu,9,3))+",D"+STR$(@bits(cu,0,3))
RETURN
PROCEDURE dnea
  dis$=dis$+" D"+STR$(@bits(cu,9,3))+","+@ea$
RETURN
FUNCTION bits(cu,au,su)
  RETURN SHR(cu,au) AND (2^su)-1
ENDFUNC
PROCEDURE 1ea(t$,cu,flb)
  dis$=t$+@bwl$+" "+@ea$
RETURN

FUNCTION ea$(wu)
  IF tf$="IEa(AM_DA)"
    wu=SHL(@bits(wu,0,3),3) OR @bits(wu,3,3)
  ENDIF
  PRINT "EA:"'BIN$(wu,6)''
  SELECT wu
  CASE 0,1,2,3,4,5,6,7              ! Dn
    RETURN "D"+STR$(wu AND 7)
  CASE 8,9,10,11,12,13,14,15        ! An
    RETURN "A"+STR$(wu AND 7)
  CASE 0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17                 ! (An)
    RETURN "(A"+STR$(wu AND 7)+")"
  CASE 0x18,0x19,0x1a,0x1b,0x1c,0x1d,0x1e             ! (An)+
    RETURN "(A"+STR$(wu AND 7)+")+"
  CASE 0x1F                         ! (SP)+
    RETURN "(SP)+"
  CASE 0x20,0x21,0x22,0x23,0x24,0x25,0x26                 ! -(An)
    RETURN "-(A"+STR$(wu AND 7)+")"
  CASE 0x27
    RETURN "-(SP)"
  CASE 0x28,0x29,0x2a,0x2b,0x2c,0x2d,0x2e,0x2F
    ADD bll,2
    ADD pcl,2
    IF ABS(@DPEEK(pcl-2))<256
      RETURN STR$(@DPEEK(pcl-2))+"(A"+STR$(wu AND 7)+")"
    ELSE
      RETURN "$"+HEX$(@DPEEK(pcl-2),4)+"(A"+STR$(wu AND 7)+")"
    ENDIF
  CASE 0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37
    ADD bll,2
    ADD pcl,2
    RETURN STR$(@dpeek(pcl-2) AND 0xFFF)+"(A"+STR$(wu AND 7)+","+CHR$(ASC("A")+(ASC("D")-ASC("A"))*ABS(NOT BTST(15,@dpeek(pcl-2))))+STR$(SHR(@dpeek(pcl-2) AND 0xF000,12) AND 7)+".W)"
  CASE 0x38
    ADD bll,2
    ADD pcl,2
    @add_symbol(@dpeek(pcl-2))
    RETURN "$"+HEX$(@dpeek(pcl-2),4)+".s"
  CASE 0x39
    ADD bll,4
    ADD pcl,4
    @add_symbol(@lpeek(pcl-4))
    RETURN "$"+HEX$(@lpeek(pcl-4),8)
  CASE 0x3a
    ADD bll,2
    ADD pcl,2
    @add_symbol(pcl-2+@dpeek(pcl-2))
    RETURN "L"+HEX$(pcl-2+@dpeek(pcl-2),8)+"(pc)"
  CASE 0x3b
    ADD bll,2
    ADD pcl,2
    RETURN STR$(@dpeek(pcl-2) AND 0xFFF)+"(pc,"+CHR$(ASC("A")+ABS(NOT BTST(@dpeek(pcl-2),15))*(ASC("D")-ASC("A")))+".W)"
  CASE 0x3c
    IF tf$="Ea(AM_D)" OR tf$="Ea(*)"
      ADD pcl,4
      ADD bll,4
      RETURN @value$(@lPEEK(pcl-4))
    ELSE
      ADD pcl,2
      ADD bll,2
      RETURN @value$(@DPEEK(pcl-2))
    ENDIF
  DEFAULT
    RETURN "ERROR"
  ENDSELECT
ENDFUNC
FUNCTION bwl$
  wu=SHR(wu AND 0xc0,6)
  IF flb=0
    RETURN ""
  ENDIF
  SELECT wu
  CASE 0
    RETURN ".B"
  CASE 1
    RETURN ""       !".W"
  CASE 2
    RETURN ".L"
  DEFAULT
    RETURN "-BWL-ERROR-"
  ENDSELECT
ENDFUNC
FUNCTION bwl2$
  wu=@bits(cu,12,2)
  SELECT wu
  CASE 1
    RETURN ".B"
  CASE 3
    RETURN ""       !".W"
  CASE 2
    RETURN ".L"
  DEFAULT
    RETURN "-BWL2-ERROR-"
  ENDSELECT
ENDFUNC
FUNCTION reg$
  LOCAL ru,r$
  CLR r$,db
  cb=TRUE
  ru=@DPEEK(pcl)
  db=0
  FOR iu=0 TO 15
    reg$=CHR$(ASC("A")+ABS(NOT BTST(iu,3))*3)+STR$(iu AND 7)
    IF BTST(ru,iu)<>db
      r$=r$+reg$
      IF db AND iu<15
        r$=r$+"/"
      ENDIF
      db=NOT db
      CLR cb
    ELSE
      IF cb=0
        r$=r$+"-"
        cb=TRUE
      ENDIF
    ENDIF
  NEXT iu
  IF db
    r$=r$+reg$
  ENDIF
  ADD offsu,2
  RETURN r$
ENDFUNC
FUNCTION areg$(i%)
  IF i%=7
    RETURN "sp"
  ELSE
    RETURN "a"+STR$(i%)
  ENDIF
ENDFUNC
'
FUNCTION dpeek(adr%)
  RETURN (PEEK(adr%) AND 0xff)*256+(PEEK(adr%+1) AND 0xff)
ENDFUNCTION
FUNCTION lpeek(adr%)
  RETURN (PEEK(adr%) AND 255)*256*256*256+(PEEK(adr%+1) AND 255)*256*256+(PEEK(adr%+2) AND 255)*256+(PEEK(adr%+3) AND 255)
ENDFUNCTION

PROCEDURE add_symbol(adr%)
  symbol%(anzsymbol%)=adr%
  INC anzsymbol%
RETURN
FUNCTION is_symbol(adr%)
  LOCAL i
  IF anzsymbol%=0
    RETURN 0
  ENDIF
  FOR i=0 TO anzsymbol%-1
    IF symbol%(i)=adr%
      RETURN true
    ENDIF
  NEXT i
  RETURN false
ENDFUNCTION
FUNCTION value$(val%)
  LOCAL t$,s$,i,a,fl
  fl=0
  s$=""
  FOR i=0 TO 3
    a=SHR(val% AND SHL(0xff,(3-i)*8),(3-i)*8)
    IF a>=ASC(" ") AND a<127
      s$=s$+CHR$(a)
    ELSE
      IF a<>0 OR LEN(s$)>0
        fl=1
      ENDIF
    ENDIF
  NEXT i
  IF fl=0
    RETURN "#'"+s$+"'"
  ENDIF
  IF val%>0 AND val%<32
    RETURN "#"+STR$(val%)
  ENDIF
  IF val%>0 AND val%<1024
    RETURN "#"+STR$(val%)
  ENDIF
  IF val%>0 AND val%<=0xffff
    RETURN "#$"+HEX$(val%,4)
  ENDIF
  RETURN "#$"+HEX$(val%,8)
ENDFUNCTION

'
mc68000_data:
DATA "0000000000111100","i:SI8;","ORI.B $i,ccr"
DATA "0000000001111100","i:SI16;","ORI.W $i,sr"
DATA "00000000sseeeeee","s:Size;e:Ea(AM_DA);i:SI;","ORI.$s $i,$e"
DATA "0000001000111100","i:SI8;","ANDI_CCR $i"
DATA "0000001001111100","i:SI16;","ANDI.W $i,sr"
DATA "00000010sseeeeee","s:Size;e:Ea(AM_DA);i:SI;","ANDI.$s $i $e"
DATA "00000100sseeeeee","s:Size;e:Ea(AM_DA);i:SI;","SUBI.$s $i,$e"
DATA "00000110sseeeeee","s:Size;e:Ea(AM_DA);i:SI;","ADDI.$s $i,$e"
DATA "0000100000000rrr","r:Dreg;i:SI8;","BTST.L_SR $i,$r"
DATA "0000100000eeeeee","e:Ea(AM_M);i:SI8;","BTST.B_SM $i,$e"
DATA "0000100001000rrr","r:Dreg;i:SI8;","BCHG_SR.L $i,$r"
DATA "0000100001eeeeee","e:Ea(AM_MA);i:SI8;","BCHG_SM.B $i,$e"
DATA "0000100010000rrr","r:Dreg;i:SI8;","BCLR $i,$r"
DATA "0000100010eeeeee","e:Ea(AM_MA);i:SI8;","BCLR_SM.B $i,$e"
DATA "0000100011000rrr","r:Dreg;i:SI8;","BSET.L_SR $i,$r"
DATA "0000100011eeeeee","e:Ea(AM_MA);i:SI8;","BSET_SM.B $i,$e"
DATA "0000101000111100","i:SI8;","EORI.B _CCR $i"
DATA "0000101001111100","i:SI16;","EORI.W $i,sr"
DATA "00001010sseeeeee","s:Size;e:Ea(AM_DA);i:SI;","EORI.$s $i,$e"
DATA "00001100sseeeeee","s:Size;i:SI32;e:Ea(AM_D);","CMPI.$s $i,$e"
DATA "0000rrr100000ddd","rd:Dreg;","BTST.l $r,$d"
DATA "0000rrr100eeeeee","r:Dreg;e:Ea(AM_DB);","BTST.b $r,$e"
DATA "0000rrr101000ddd","rd:Dreg;","BCHG.l $r,$d"
DATA "0000ddd10s001aaa","d:Dreg;a:Areg;s:Size1;i:SI16;","MOVEP_MR.$s $i $a,$d"
DATA "0000rrr101eeeeee","r:Dreg;e:Ea(AM_MA);","BCHG.b $r,$e"
DATA "0000rrr110000ddd","rd:Dreg;","BCLR.l $r,$d"
DATA "0000rrr110eeeeee","r:Dreg;e:Ea(AM_MA);","BCLR.b $r,$e"
DATA "0000rrr111000ddd","rd:Dreg;","BSET.l $r,$d"
DATA "0000ddd11s001aaa","d:Dreg;a:Areg;s:Size1;i:SI16;","MOVEP_RM.$s $i,$d,$a"
DATA "0000rrr111eeeeee","r:Dreg;e:Ea(AM_MA);","BSET.b $r,$e"
'
DATA "0001rrr001eeeeee","r:Areg;e:Ea(*);","ILLEGAL"
DATA "00ssrrr001eeeeee","s:Size;r:Areg;e:Ea(*);","MOVEA.$s $e,$r"
DATA "00sseeeeeeffffff","s:Size;e:IEa(AM_DA);f:Ea(*);","MOVE.$s $f,$e"
'
DATA "0100000011eeeeee","e:Ea(AM_DA);","MOVE.w sr,$e"
DATA "01000000sseeeeee","s:Size;e:Ea(AM_DA);","NEGX.$s $e"
DATA "01000010sseeeeee","s:Size;e:Ea(AM_DA);","CLR.$s $e"
DATA "0100010011eeeeee","e:Ea(AM_D);","MOVE.W $e,ccr"
DATA "01000100sseeeeee","s:Size;e:Ea(AM_DA);","NEG.$s $e"
DATA "0100011011eeeeee","e:Ea(AM_D);","MOVE.W $e,sr"
DATA "01000110sseeeeee","s:Size;e:Ea(AM_DA);","NOT.$s $e"

DATA "0100100000eeeeee","e:Ea(AM_DA);","NBCD $e"
DATA "0100100001000rrr","r:Dreg;","SWAP $r"
DATA "0100100001eeeeee","e:Ea(AM_C);","PEA $e"
DATA "010010001s000rrr","s:Size1;r:Dreg;","EXT.$s $r"
DATA "010010001s100rrr","s:Size1;r:Areg;i:SI16;","MOVEM_RM_PD.$s $i,$r"
DATA "010010001seeeeee","s:Size1;e:Ea(AM_CA);i:SI16;","MOVEM_RM_CA.$s $i,$e"
DATA "0100101011111100",";","Illegal"
DATA "010011100100vvvv","v:U4;","TRAP #$v"
DATA "0100111001010rrr","r:Areg;i:SI16;","LINK.W $i,$r"
DATA "0100111001011rrr","r:Areg;","UNLK $r"
DATA "0100111001100rrr","r:Areg;","MOVE.L $r,usp"
DATA "0100111001101rrr","r:Areg;","MOVE.L usp,$r"
DATA "0100111001110000",";","RESET"
DATA "0100111001110001",";","NOP"
DATA "0100111001110010","i:SI16;","STOP $i"
DATA "0100111001110011",";","RTE"
DATA "0100111001110101",";","RTS"
DATA "0100111001110110",";","TRAPV"
DATA "0100111001110111",";","RTR"
DATA "0100111010eeeeee","e:Ea(AM_C);","JSR $e"
DATA "0100111011eeeeee","e:Ea(AM_C);","JMP $e"
DATA "0100rrr110eeeeee","r:Dreg;e:Ea(AM_D);","CHK.W $e,$r"
DATA "0100rrr111eeeeee","r:Areg;e:Ea(AM_C);","LEA $e,$r"
'
DATA "010011001seeeeee","s:Size1;e:Ea(AM_C);i:SI16;",":MOVEM_MR_C($s,$i,$e)"
DATA "010011001s011rrr","s:Size1;r:Areg;i:SI16;",":MOVEM_MR_PI($s,$i,$r)"

DATA "0100101011eeeeee","e:Ea(AM_DA);","TAS.b $e"
DATA "01001010sseeeeee","s:Size;e:Ea(AM_DNI);","TST.$s $e"
DATA "0101ddd1ss001rrr","d:Q3;s:Size;r:Areg;","SUBQ.$s #$d,$r"
DATA "0101ddd1sseeeeee","d:Q3;s:Size;e:Ea(AM_DA);","SUBQ.$s #$d,$e"
'
DATA "0101ddd000001rrr","d:Q3;r:Areg;","Illegal"
DATA "0101ddd0ss001rrr","d:Q3;s:Size;r:Areg;","ADDQ.$s #$d,$r"
DATA "0101ddd0sseeeeee","d:Q3;s:Size;e:Ea(AM_DA);","ADDQ.$s #$d,$e"
DATA "0101ddd100001rrr","d:Q3;r:Areg;","Illegal"
DATA "0101cccc11001rrr","c:CondAll;r:Dreg;i:PCrel16;","DB$c $r,$i"
DATA "0101cccc11eeeeee","c:CondAll;e:Ea(AM_DA);","S$c.b $e"
'
'
DATA "0110000000000000","i:PCrel16;","BRA $i"
DATA "01100000dddddddd","d:PCrel;","BRA.S $d"
DATA "0110000100000000","i:PCrel16;","BSR $i"
DATA "01100001dddddddd","d:PCrel;","BSR.S $d"
DATA "0110cccc00000000","c:Cond;i:PCrel16;","B$c $i"
DATA "0110ccccdddddddd","c:Cond;d:PCrel;","B$c.S $d"
'
DATA "0111rrr0dddddddd","r:Dreg;d:S8;","MOVEQ #$d,$r"
'
DATA "1000rrr011eeeeee","r:Dreg;e:Ea(AM_D);","DIVU $e,$r"
DATA "1000rrr0sseeeeee","r:Dreg;s:Size;e:Ea(AM_D);","OR_EA.$s $e,$r"
DATA "1000yyy100000xxx","xy:Dreg;","SBCD_REG.B $x,$y"
DATA "1000yyy100001xxx","xy:Areg;","SBCD_MEM.B $x,$y"
DATA "1000rrr111eeeeee","r:Dreg;e:Ea(AM_D);","DIVS $e,$r"
DATA "1000rrr1sseeeeee","r:Dreg;s:Size;e:Ea(AM_MA);","OR.$s $r,$e"
'
DATA "1001rrrs11eeeeee","r:Areg;s:Size1;e:Ea(*);","SUBA.$s $e,$r"
DATA "1001rrr0sseeeeee","r:Dreg;s:Size;e:Ea(*);","SUB.$s $e,$r"
DATA "1001yyy1ss000xxx","xy:Dreg;s:Size;","SUBX_DREG.$s $x,$y"
DATA "1001yyy1ss001xxx","xy:Areg;s:Size;","SUBX_PD.$s $x,$y"
DATA "1001rrr1sseeeeee","r:Dreg;s:Size;e:Ea(AM_MA);","SUB_REG.$s $r,$e"
'
DATA "1010000011111111","i:SI32;","NATIVE $i"
DATA "1010xxxxxxxxxxxx","x:U12;","dc.w A$x ; LINEA $x"
'
DATA "1011rrr0sseeeeee","r:Dreg;s:Size;e:Ea(*);","CMP.$s $e,$r"
DATA "1011rrrs11eeeeee","r:Areg;s:Size1;e:Ea(*);","CMPA.$s $e,$r"
DATA "1011xxx1ss001yyy","xy:Areg;s:Size;","CMPM.$s $y,$x"
DATA "1011rrr1sseeeeee","r:Dreg;s:Size;e:Ea(AM_DA);","EOR.$s $r,$e"
'
DATA "1101rrr0sseeeeee","r:Dreg;s:Size;e:Ea(*);",":ADD.$s $e,$r"
DATA "1101rrr1sseeeeee","r:Dreg;s:Size;e:Ea(AM_MA);",":ADD_REG($s,$r,$e)"
DATA "1101rrrs11eeeeee","r:Areg;s:Size1;e:Ea(*);",":ADDA($s,$e,$r)"
DATA "1101xxx1ss000yyy","xy:Dreg;s:Size;",":ADDX_DREG($s,$y,$x)"
DATA "1101xxx1ss001yyy","xy:Areg;s:Size;",":ADDX_PD($s,$y,$x)"
'
DATA "1100xxx100001yyy","xy:Areg;","B:ABCD_MEM($x,$y)"
DATA "1100xxx100000yyy","xy:Dreg;","B:ABCD_REG($x,$y)"
DATA "1100rrr0sseeeeee","r:Dreg;s:Size;e:Ea(AM_D);","AND.$s $e,$r"
DATA "1100rrr1sseeeeee","r:Dreg;s:Size;e:Ea(AM_MA);",":AND_REG($s,$r,$e)"
DATA "1100rrr011eeeeee","r:Dreg;e:Ea(AM_D);","MULU  $e,$r"
DATA "1100xxx101000yyy","xy:Dreg;","L:EXG_DD $x,$y"
DATA "1100xxx101001yyy","xy:Areg;","L:EXG_AA $x,$y"
DATA "1100xxx110001yyy","x:Dreg;y:Areg;","L:EXG_DA $x,$y"
DATA "1100rrr111eeeeee","r:Dreg;e:Ea(AM_D);","MULS  $e,$r"
'
DATA "1110ccc0ss000rrr","c:Q3;s:Size;r:Dreg;",":ASR_IMM($s,$c,$r)"
DATA "1110ccc0ss100rrr","c:Dreg;s:Size;r:Dreg;",":ASR_DREG($s,$c,$r)"
DATA "1110000011eeeeee","e:Ea(AM_MA);","W:ASR_MEM($e)"
DATA "1110ccc0ss001rrr","c:Q3;s:Size;r:Dreg;",":LSR_IMM($s,$c,$r)"
DATA "1110ccc0ss101rrr","c:Dreg;s:Size;r:Dreg;",":LSR_DREG($s,$c,$r)"
DATA "1110001011eeeeee","e:Ea(AM_MA);","W:LSR_MEM($e)"
DATA "1110ccc0ss011rrr","c:Q3;s:Size;r:Dreg;",":ROR_IMM($s,$c,$r)"
DATA "1110ccc0ss111rrr","c:Dreg;s:Size;r:Dreg;",":ROR_DREG($s,$c,$r)"
DATA "1110011011eeeeee","e:Ea(AM_MA);","W:ROR_MEM($e)"
DATA "1110ccc0ss010rrr","c:Q3;s:Size;r:Dreg;",":ROXR_IMM($s,$c,$r)"
DATA "1110ccc0ss110rrr","c:Dreg;s:Size;r:Dreg;",":ROXR_DREG($s,$c,$r)"
DATA "1110010011eeeeee","e:Ea(AM_MA);","W:ROXR_MEM($e)"
DATA "1110ccc1ss000rrr","c:Q3;s:Size;r:Dreg;",":ASL_IMM($s,$c,$r)"
DATA "1110ccc1ss100rrr","c:Dreg;s:Size;r:Dreg;",":ASL_DREG($s,$c,$r)"
DATA "1110000111eeeeee","e:Ea(AM_MA);","W:ASL_MEM($e)"
DATA "1110ccc1ss001rrr","c:Q3;s:Size;r:Dreg;",":LSL_IMM($s,$c,$r)"
DATA "1110ccc1ss101rrr","c:Dreg;s:Size;r:Dreg;",":LSL_DREG($s,$c,$r)"
DATA "1110001111eeeeee","e:Ea(AM_MA);","W:LSL_MEM($e)"
DATA "1110ccc1ss011rrr","c:Q3;s:Size;r:Dreg;",":ROL_IMM($s,$c,$r)"
DATA "1110ccc1ss111rrr","c:Dreg;s:Size;r:Dreg;",":ROL_DREG($s,$c,$r)"
DATA "1110011111eeeeee","e:Ea(AM_MA);","W:ROL_MEM($e)"
DATA "1110ccc1ss010rrr","c:Q3;s:Size;r:Dreg;",":ROXL_IMM($s,$c,$r)"
DATA "1110ccc1ss110rrr","c:Dreg;s:Size;r:Dreg;",":ROXL_DREG($s,$c,$r)"
DATA "1110010111eeeeee","e:Ea(AM_MA);","W:ROXL_MEM($e)"
'
DATA "1111xxxxxxxxxxxx","x:U12;","dc.w F$x ; LINEF $x"
'
DATA "***"
