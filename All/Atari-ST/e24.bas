' Read and decode a SIGNUM2 font
' originally written in GFA-BASIC 1989
'
DIM width(128)
f$="grotlt.e24"
f$="normande.e24"
OPEN "I",#1,f$
d=10

CLEARW
SEEK #1,0
header$=INPUT$(#1,140)
FOR i=0 TO 128-1
  width(i)=CVI(REVERSE$(INPUT$(#1,4)))
NEXT i
MEMDUMP VARPTR(header$),140
data$=INPUT$(#1,LOF(#1)-140-512)
wx=0
FOR c=1 TO 63
  @disp(wx,100,c)
NEXT c
wx=0
FOR c=64 TO 127
  @disp(wx,200,c)
NEXT c
KEYEVENT
QUIT

PROCEDURE disp(x,y,c)
  LOCAL o,w,h,d
  o=width(c)
  h=(width(c+1)-o)/2
  d=PEEK(VARPTR(data$)+o)
  w=PEEK(VARPTR(data$)+o+1)
  w=PEEK(VARPTR(data$)+o+2)
  TEXT x,y+50,STR$(w)
  PRINT PEEK(VARPTR(data$)+o+1),PEEK(VARPTR(data$)+o+3),w
  PRINT "char ";c;" offs=";d
  ADD y,d
  ADD o,4
  SUB h,2
  FOR j=0 TO h-1
    a=PEEK(VARPTR(data$)+o+j*2)*256+(PEEK(VARPTR(data$)+o+1+j*2) AND 255)
    FOR i=0 TO 15
      IF BTST(a,i)
        PLOT x+15-i,y
      ENDIF
    NEXT i
    INC y
  NEXT j
  SHOWPAGE
  ADD wx,w
RETURN
