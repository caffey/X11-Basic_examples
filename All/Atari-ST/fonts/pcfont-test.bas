' Displays 8*16 fixed Fonts (c) Markus Hoffmann
scale=1
text$="This example demonstrates the use of the PC (console) fonts!"+" We used "
COLOR get_color(65535,65535,0)

FILESELECT "load PC-FONT","/usr/share/kbd/*.fnt","",f$
IF len(f$)
  IF exist(f$)
    OPEN "I",#1,f$
    text$=text$+f$
    f$=input$(#1,4096)
    CLOSE #1
    FOR i=0 TO 256
      text$=text$+CHR$(i)
    NEXT i
  ENDIF
  @text(text$)
ENDIF
PAUSE 10
QUIT
PROCEDURE text(t$)
  LOCAL i
  FOR i=0 TO LEN(t$)-1
    char=PEEK(VARPTR(t$)+i) and 255
    @char(x,y,char)
    VSYNC
    ADD x,8*scale
    IF x>=640
      x=0
      ADD y,16*scale
    ENDIF
  NEXT i
RETURN
PROCEDURE char(x,y,c)
  LOCAL i,j,t$
  PRINT c
  '  for i=0 to 15
  '    for j=0 to 7
  '      if btst(peek(varptr(f$)+i+c*16),7-j)=0
  '        pbox x+j*scale,y+i*scale,x+j*scale+scale,y+i*scale+scale-1
  '      endif
  '    next j
  '  next i
  t$=MID$(f$,c*16+1,16*2)
  PUT_BITMAP t$,x,y,8,16
RETURN
