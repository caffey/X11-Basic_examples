' Displays Atari-ST 8*16 fixed Fonts (c) Markus Hoffmann
scale=2
text$="This example demonstrates the use of the old Atari-ST fonts!"+CHR$(14)+CHR$(15)+" We used "
COLOR get_color(65535,65535,0)

FILESELECT "FONT-Laden","./*.fnt","",f$
IF len(f$)
  IF exist(f$)
    OPEN "I",#1,f$
    text$=text$+f$
    f$=input$(#1,4096)
    CLOSE #1
    FOR i=0 TO 256
      text$=text$+CHR$(i)
    NEXT i
  ENDIF
  @text(text$)
ENDIF
ENDIF
OPEN "O",#1,"out.fnt"
FOR i=0 TO 255
  FOR j=0 TO 15
    PRINT #1,CHR$(PEEK(VARPTR(f$)+i+j*256));
  NEXT j
NEXT i
CLOSE
QUIT
PROCEDURE text(t$)
  LOCAL i
  FOR i=0 TO LEN(t$)-1
    char=PEEK(VARPTR(t$)+i)
    @char(x,y,char)
    VSYNC

    ADD x,8*scale
    IF x>=640
      x=0
      ADD y,16*scale
    ENDIF
  NEXT i
RETURN
PROCEDURE char(x,y,c)
  LOCAL i,j
  FOR i=0 TO 15
    FOR j=0 TO 7
      IF btst(PEEK(VARPTR(f$)+c+i*256),7-j)=0
        PBOX x+j*scale,y+i*scale,x+j*scale+scale,y+i*scale+scale-1
      ENDIF
    NEXT j
  NEXT i
RETURN
