' Displays Atari-ST 8*16 fixed Fonts (c) Markus Hoffmann
scale=2
COLOR get_color(65535,65535,0)
SIZEW ,scale*16*16+32,scale*16*16
f1$=PARAM$(2)
f2$=PARAM$(3)
PRINT "font1: ";f1$
PRINT "font2: ";f2$

IF exist(f1$) AND EXIST(f2$)
  OPEN "I",#1,f1$
  OPEN "I",#2,f2$
  ff1$=input$(#1,4096)
  ff2$=input$(#2,4096)
  CLOSE #1,#2
ENDIF
FOR i=0 TO 15
  FOR j=0 TO 15
    @char1(j*scale*8,i*scale*16,i*16+j)
    @char2(scale*8*16+30+j*scale*8,i*scale*16,i*16+j)
    VSYNC
  NEXT j
NEXT i

STOP

OPEN "O",#1,"out.fnt"
FOR i=0 TO 255
  FOR j=0 TO 15
    PRINT #1,CHR$(PEEK(VARPTR(f$)+i+j*256));
  NEXT j
NEXT i
CLOSE
QUIT
PROCEDURE text(t$)
  LOCAL i
  FOR i=0 TO LEN(t$)-1
    char=PEEK(VARPTR(t$)+i)
    @char(x,y,char)
    VSYNC

    ADD x,8*scale
    IF x>=640
      x=0
      ADD y,16*scale
    ENDIF
  NEXT i
RETURN
PROCEDURE char1(x,y,c)
  LOCAL i,j
  FOR i=0 TO 15
    FOR j=0 TO 7
      IF btst(PEEK(VARPTR(ff1$)+i+c*16),7-j)=0
        PBOX x+j*scale,y+i*scale,x+j*scale+scale,y+i*scale+scale-1
      ENDIF
    NEXT j
  NEXT i
RETURN
PROCEDURE char2(x,y,c)
  LOCAL i,j
  FOR i=0 TO 15
    FOR j=0 TO 7
      IF btst(PEEK(VARPTR(ff2$)+i+c*16),7-j)=0
        PBOX x+j*scale,y+i*scale,x+j*scale+scale,y+i*scale+scale-1
      ENDIF
    NEXT j
  NEXT i
RETURN
