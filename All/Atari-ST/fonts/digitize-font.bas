f$="ST-fonts/spat-a.fnt"

OPEN "I",#1,f$
text$=text$+f$
f$=INPUT$(#1,4096)
CLOSE #1

scalex=31
scaley=22

a=ASC("s")
DO
  CLEARW
  @char(0,0,a)
  COLOR GET_COLOR(65535,0,0)
  FOR i=0 TO 10
    LINE 10+0,50+i*20,10+10*20,50+i*20
  NEXT i
  FOR i=0 TO 10
    LINE 10+i*20,50+0,10+i*20,50+200
  NEXT i
  DEFTEXT ,2,2
  DEFLINE ,3
  LTEXT 10,50,CHR$(a)
  DEFLINE ,1
  TEXT 300,200,STR$(a)
  VSYNC
  KEYEVENT
  INC a
LOOP
PAUSE 10
QUIT

PROCEDURE char(x,y,c)
  LOCAL i,j
  COLOR get_color(65535,65535,0)
  FOR i=0 TO 15
    FOR j=0 TO 7
      IF BTST(PEEK(VARPTR(f$)+c+i*256),7-j)=0
        PBOX x+j*scalex,y+i*scaley,x+j*scalex+scalex-1,y+i*scaley+scaley-1
      ENDIF
    NEXT j
  NEXT i
RETURN
