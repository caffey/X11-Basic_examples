' Utility to convert ATARI ST RSC Files to GUI Files
' written in X11-Basic
' The GUI file format is a complete ASCII representation of the
' RSC files.
' (c) Markus Hoffmann 2003 (letzte Bearbeitung: 10.08.2003)
'                                               27.01.2005
'
i=1
outputfilename$="b.gui"
WHILE LEN(PARAM$(i))
  IF LEFT$(PARAM$(i))="-"
    IF PARAM$(i)="--help" OR PARAM$(i)="-h"
      @intro
      @using
    ELSE IF PARAM$(i)="--version"
      @intro
      QUIT
    ELSE IF PARAM$(i)="-o"
      INC i
      IF LEN(PARAM$(i))
        outputfilename$=PARAM$(i)
      ENDIF
    ENDIF
  ELSE
    inputfile$=PARAM$(i)
    IF NOT EXIST(inputfile$)
      PRINT "rsc2gui: "+inputfile$+": file or path not found"
      CLR inputfile$
    ENDIF
  ENDIF
  INC i
WEND
IF LEN(inputfile$)
  rumpf$=inputfile$
  WHILE LEN(rumpf$)
    SPLIT rumpf$,"/",1,a$,rumpf$
  WEND
  f$=a$
  rumpf$=a$
  SPLIT a$,".",1,rumpf$,typ$
  IF typ$="rsc"
    IF EXIST(outputfilename$)
      PRINT "rsc2gui: Outputfilename already exists: ";outputfilename$
    ELSE
      @convert
    ENDIF
  ELSE
    PRINT f$+": file not recognized: File format not recognized"
  ENDIF
ELSE
  PRINT "rsc2gui: No input files"
ENDIF
QUIT
PROCEDURE intro
  PRINT "GEM RSC to GUI Converter V.1.09 (c) Markus Hoffmann 2003-2005"
  VERSION
RETURN
PROCEDURE using
  PRINT "Usage: rsc2gui [options] file..."
  PRINT "Options:"
  PRINT "  -h, --help		    Display this information"
  PRINT "  -o <file>		    Place the output into <file>"
RETURN
PROCEDURE convert
  OPEN "I",#1,inputfile$
  rsc$=INPUT$(#1,LOF(#1))
  CLOSE #1

  colors$()=["WHITE","BLACK","RED","GREEN","BLUE","CYAN","YELLOW","MAGENTA","LWHITE","LBLACK","LRED","LGREEN","LBLUE","LCYAN","LYELLOW","LMAGENTA"]

  OPEN "O",#2,outputfilename$
  PRINT #2,"' rsc2gui V.1.09 ("+f$+")"
  PRINT #2,"'	   (c) Markus Hoffmann "+DATE$+" "+TIME$
  spaces=0
  @header(VARPTR(rsc$))
  IF ntree>0
    FOR i=0 TO ntree-1
      PRINT #2,"TREE_"+STR$(i)+": TREE {"
      INC spaces
      @dotree(VARPTR(rsc$)+@lswap(LPEEK(VARPTR(rsc$)+treeadr+4*i)))
      DEC spaces
      PRINT #2,"}"
    NEXT i
  ENDIF
  IF nfrstr>0
    FOR i=0 TO nfrstr-1
      text$=@getchar$(VARPTR(rsc$)+@lswap(LPEEK(VARPTR(rsc$)+frstradr+4*i)))
      PRINT #2,"FREESTR_"+STR$(i)+": FREESTR STRING="+ENCLOSE$(text$)
    NEXT i
  ENDIF
  PRINT #2,"' ----- End of INPUT -----"
  CLOSE #2
RETURN
PROCEDURE header(a)
  rscver=@dswap(DPEEK(a))
  PRINT #2,"RSC {"
  PRINT #2,"  Version=";rscver
  IF rscver=0
    objadr=@dswap(DPEEK(a+2))
    tedinfoadr=@dswap(DPEEK(a+4))
    iconblkadr=@dswap(DPEEK(a+6))
    bitblkadr=@dswap(DPEEK(a+6))
    frstradr=@dswap(DPEEK(a+10))
    stringadr=@dswap(DPEEK(a+12))
    imdataadr=@dswap(DPEEK(a+14))
    frimgadr=@dswap(DPEEK(a+16))
    treeadr=@dswap(DPEEK(a+18))
    nobj=@dswap(DPEEK(a+20))
    ntree=@dswap(DPEEK(a+22))
    ntedinfo=@dswap(DPEEK(a+24))
    nib=@dswap(DPEEK(a+26))
    nbb=@dswap(DPEEK(a+28))
    nfrstr=@dswap(DPEEK(a+30))
    nimage=@dswap(DPEEK(a+32))
    size=@dswap(DPEEK(a+34))
  ELSE IF rscver=3
    objadr=@lswap(LPEEK(a+2))
    tedinfoadr=@lswap(LPEEK(a+6))
    iconblkadr=@lswap(LPEEK(a+10))
    bitblkadr=@lswap(LPEEK(a+14))
    frstradr=@lswap(LPEEK(a+18))
    stringadr=@lswap(LPEEK(a+22))
    imdataadr=@lswap(LPEEK(a+26))
    frimgadr=@lswap(LPEEK(a+30))
    treeadr=@lswap(LPEEK(a+34))
    nobj=@dswap(DPEEK(a+38))
    ntree=@dswap(DPEEK(a+40))
    ntedinfo=@dswap(DPEEK(a+42))
    nib=@dswap(DPEEK(a+44))
    nbb=@dswap(DPEEK(a+46))
    nfrstr=@dswap(DPEEK(a+48))
    nimage=@dswap(DPEEK(a+50))
    size=@lswap(LPEEK(a+52))
  ELSE
    PRINT "ERROR: Unknown RSC Version ";rscver
  ENDIF
  PRINT #2,"  Objadr	=$";HEX$(objadr,8);" # Adresse Object-Array"
  PRINT #2,"  Tedinfoadr=$";HEX$(tedinfoadr,8);" # Adresse TEDINFO-Array"
  PRINT #2,"  Iconblkadr=$";HEX$(iconblkadr,8);" # Adresse ICONBLK-Array"
  PRINT #2,"  Bitblkadr =$";HEX$(bitblkadr,8);" # Adresse BITBLK-Array"
  PRINT #2,"  Frstradr  =$";HEX$(frstradr,8);" # Adresse FreeString-Table"
  PRINT #2,"  Imdataadr =$";HEX$(imdataadr,8);" # Adresse Imagedata"
  PRINT #2,"  Frimgadr  =$";HEX$(frimgadr,8);" # Adresse Freeimage-Table"
  PRINT #2,"  Treeadr	=$";HEX$(treeadr,8);" # Adresse Tree-Table"
  PRINT #2,"  nObj	 =";nobj
  PRINT #2,"  nTree	 =";ntree
  PRINT #2,"  nTed	 =";ntedinfo
  PRINT #2,"  niconblk   =";nib
  PRINT #2,"  nbitblk	 =";nbb
  PRINT #2,"  nFreestring=";nfrstr
  PRINT #2,"  nImage	 =";nimage
  PRINT #2,"  RSCsize	 =";size
  PRINT #2,"}"
  FLUSH #2
RETURN
PROCEDURE dotree(a)
  treestart=a
  @doobj(0,65535)
  FLUSH #2
RETURN
PROCEDURE doobj(idx,parent)
  LOCAL obnext,obhead,obtail
  PRINT "OB >",idx
  WHILE idx<>parent
    obnext=@dswap(DPEEK(treestart+idx*24+0))

    obhead=@dswap(DPEEK(treestart+idx*24+2))
    obtail=@dswap(DPEEK(treestart+idx*24+4))
    obtype=@dswap(DPEEK(treestart+idx*24+6))
    obx=@dswap(DPEEK(treestart+idx*24+16))
    oby=@dswap(DPEEK(treestart+idx*24+18))
    obw=@dswap(DPEEK(treestart+idx*24+20))
    obh=@dswap(DPEEK(treestart+idx*24+22))
    obspec=@lswap(LPEEK(treestart+idx*24+12))
    obflags=@dswap(DPEEK(treestart+idx*24+8))
    obstate=@dswap(DPEEK(treestart+idx*24+10))
    PRINT #2,SPACE$(spaces*2);"OB_"+STR$(idx)+": ";
    IF obtype=20
      typ$="BOX"
    ELSE if obtype=21
      typ$="TEXT"
    ELSE if obtype=22
      typ$="BOXTEXT"
    ELSE if obtype=23
      typ$="IMAGE"
    ELSE if obtype=24
      typ$="USERDEF"
    ELSE if obtype=25
      typ$="IBOX"
    ELSE if obtype=26
      typ$="BUTTON"
    ELSE if obtype=27
      typ$="BOXCHAR"
    ELSE if obtype=28
      typ$="STRING"
    ELSE if obtype=29
      typ$="FTEXT"
    ELSE if obtype=30
      typ$="FBOXTEXT"
    ELSE if obtype=31
      typ$="ICON"
    ELSE if obtype=32
      typ$="TITLE"
    ELSE if obtype=33
      typ$="CICON"
    ELSE if obtype=42
      typ$="ALERTTYP"
    ELSE
      typ$="UNKNOWN"
    ENDIF
    PRINT #2,typ$;"(";
    PRINT #2,"X=";obx;",Y=";oby;",W=";obw;",H=";obh;
    ' print #2,obtype,obnext,obhead,obtail;
    IF obtype=28 OR obtype=32 OR obtype=26
      text$=@getchar$(VARPTR(rsc$)+obspec)
      PRINT #2,", TEXT="+ENCLOSE$(text$);
    ELSE if obtype=21 OR obtype=22 OR obtype=29 OR obtype=30
      tedinfo=VARPTR(rsc$)+obspec
      text$=@getchar$(VARPTR(rsc$)+@lswap(LPEEK(tedinfo)))
      ptmp$=@getchar$(VARPTR(rsc$)+@lswap(LPEEK(tedinfo+4)))
      pvalid$=@getchar$(VARPTR(rsc$)+@lswap(LPEEK(tedinfo+8)))
      font=@dswap(DPEEK(tedinfo+12))
      just=@dswap(DPEEK(tedinfo+16))
      color=@dswap(DPEEK(tedinfo+18))
      border=@dswap(DPEEK(tedinfo+22))

      PRINT #2,", TEXT="+ENCLOSE$(text$);
      IF len(ptmp$)
        PRINT #2,", PTMP="+ENCLOSE$(ptmp$);
      ENDIF
      IF len(pvalid$)
        PRINT #2,", PVALID="+ENCLOSE$(pvalid$);
      ENDIF
      PRINT #2,", FONT=";font;
      PRINT #2,", JUST=";just;
      PRINT #2,", COLOR=";color;
      PRINT #2,", BORDER=";border;
    ELSE if obtype=23
      bitblk=VARPTR(rsc$)+obspec
      iw=@dswap(DPEEK(bitblk+4))
      ih=@dswap(DPEEK(bitblk+6))
      ix=@dswap(DPEEK(bitblk+8))
      iy=@dswap(DPEEK(bitblk+10))
      ic=@dswap(DPEEK(bitblk+12))
      data$=@inlineconv$(VARPTR(rsc$)+@lswap(LPEEK(bitblk)),ih*iw)
      PRINT #2,", IW=";STR$(iw);
      PRINT #2,", IH=";STR$(ih);
      PRINT #2,", IX=";STR$(ix);
      PRINT #2,", IY=";STR$(iy);
      PRINT #2,", IC=";STR$(ic);
      PRINT #2,", DATA="+ENCLOSE$(data$);

    ELSE if obtype=31 OR obtype=33
      iconblk=VARPTR(rsc$)+obspec
      text$=@getchar$(VARPTR(rsc$)+@lswap(LPEEK(iconblk+8)))
      char=@dswap(DPEEK(iconblk+12))
      xchar=@dswap(DPEEK(iconblk+14))
      ychar=@dswap(DPEEK(iconblk+16))
      IF char AND 255
        PRINT #2,", CHAR='";CHR$(char);"'";
        PRINT #2,", XCHAR=";STR$(xchar);
        PRINT #2,", YCHAR=";STR$(ychar);
      ENDIF
      PRINT #2,", TEXT="+ENCLOSE$(text$);
      xicon=@dswap(DPEEK(iconblk+18))
      yicon=@dswap(DPEEK(iconblk+20))
      wicon=@dswap(DPEEK(iconblk+22))
      hicon=@dswap(DPEEK(iconblk+24))
      PRINT #2,", XICON=";STR$(xicon);
      PRINT #2,", YICON=";STR$(yicon);
      PRINT #2,", WICON=";STR$(wicon);
      PRINT #2,", HICON=";STR$(hicon);
      xtext=@dswap(DPEEK(iconblk+26))
      ytext=@dswap(DPEEK(iconblk+28))
      wtext=@dswap(DPEEK(iconblk+30))
      htext=@dswap(DPEEK(iconblk+32))
      PRINT #2,", XTEXT=";STR$(xtext);
      PRINT #2,", YTEXT=";STR$(ytext);
      PRINT #2,", WTEXT=";STR$(wtext);
      PRINT #2,", HTEXT=";STR$(htext);
      mask$=@inlineconv$(VARPTR(rsc$)+@lswap(LPEEK(iconblk)),hicon*wicon/16*2)
      data$=@inlineconv$(VARPTR(rsc$)+@lswap(LPEEK(iconblk+4)),hicon*wicon/16*2)
      PRINT #2,", DATA="+ENCLOSE$(data$);
      PRINT #2,", MASK="+ENCLOSE$(mask$);

    ELSE
      char=ASC(RIGHT$(MKL$(obspec)))
      framesize=ASC(MID$(MKL$(obspec),3,1))
      color=ASC(MID$(MKL$(obspec),2,1))
      rest=ASC(MID$(MKL$(obspec),1,1))
      framecol=color AND (255 XOR 15)/16
      textcol=color AND 15
      pattern=rest AND (255 XOR 15)/16
      textmode=(pattern AND 8)/8
      pattern=pattern AND 7
      bgcol=rest AND 15
      IF char<>0 OR obtype=27
        PRINT #2,", CHAR='";CHR$(char);"'";
      ENDIF
      PRINT #2,", FRAME=";framesize;
      PRINT #2,", FRAMECOL=";framecol;
      PRINT #2,", TEXTCOL=";textcol;
      PRINT #2,", BGCOL=";bgcol;
      PRINT #2,", PATTERN=";pattern;
      PRINT #2,", TEXTMODE=";textmode;
    ENDIF
    IF obflags<>0
      flagtxt$=""
      IF obflags AND 1
        flagtxt$=flagtxt$+"SELECTABLE+"
      ENDIF
      IF (obflags AND 2)<>0
        flagtxt$=flagtxt$+"DEFAULT+"
      ENDIF
      IF obflags AND 4
        flagtxt$=flagtxt$+"EXIT+"
      ENDIF
      IF obflags AND 8
        flagtxt$=flagtxt$+"EDITABLE+"
      ENDIF
      IF obflags AND 16
        flagtxt$=flagtxt$+"RADIOBUTTON+"
      ENDIF
      IF obflags AND 32
        flagtxt$=flagtxt$+"LASTOB+"
      ENDIF
      IF obflags AND 64
        flagtxt$=flagtxt$+"TOUCHEXIT+"
      ENDIF
      IF obflags AND 128
        flagtxt$=flagtxt$+"HIDETREE+"
      ENDIF
      IF obflags AND 256
        flagtxt$=flagtxt$+"INDIRECT+"
      ENDIF
      IF obflags AND 0x0200
        flagtxt$=flagtxt$+"FL3DIND+"
      ENDIF
      IF obflags AND 0x0400
        flagtxt$=flagtxt$+"FL3DACT+"
      ENDIF
      IF obflags AND 0x0800
        flagtxt$=flagtxt$+"SUBMENU+"
      ENDIF
      IF obflags AND (65535 XOR 0Xfff)
        flagtxt$=flagtxt$+"MOREFLAGS(%";BIN$(obflags,16,16,1);")"
      ENDIF
      IF RIGHT$(flagtxt$)="+"
        flagtxt$=LEFT$(flagtxt$,LEN(flagtxt$)-1)
      ENDIF
      PRINT #2,", FLAGS=";flagtxt$;
    ENDIF
    IF obstate<>0
      statetxt$=""
      IF obstate AND 1
        statetxt$=statetxt$+"SELECTED+"
      ENDIF
      IF obstate AND 2
        statetxt$=statetxt$+"CROSSED+"
      ENDIF
      IF obstate AND 4
        statetxt$=statetxt$+"CHECKED+"
      ENDIF
      IF obstate AND 8
        statetxt$=statetxt$+"DISABLED+"
      ENDIF
      IF obstate AND 16
        statetxt$=statetxt$+"OUTLINED+"
      ENDIF
      IF obstate AND 32
        statetxt$=statetxt$+"SHADOWED+"
      ENDIF
      IF obstate AND 64
        statetxt$=statetxt$+"WHITEBACK+"
      ENDIF
      IF obstate AND 128
        statetxt$=statetxt$+"DRAW3D+"
      ENDIF
      IF obstate AND (65535 XOR 255)
        statetxt$=statetxt$+"MORESTATE(%";BIN$(obstate,16,16,1);")"
      ENDIF
      IF RIGHT$(statetxt$)="+"
        statetxt$=LEFT$(statetxt$,LEN(statetxt$)-1)
      ENDIF
      PRINT #2,", STATE=";statetxt$;
    ENDIF
    PRINT #2,")";
    IF obhead<>65535
      PRINT #2," {"
      INC spaces
      sss=idx
      @doobj(obhead,sss)
      DEC spaces
      PRINT #2,SPACE$(spaces*2);"}"
    ELSE
      PRINT #2
    ENDIF
    FLUSH #2
    idx=obnext
  WEND
RETURN

FUNCTION getchar$(a)
  LOCAL t$,s
  t$=""
  DO
    s=PEEK(a)
    EXIT IF s=0
    t$=t$+CHR$(s)
    INC a
  LOOP
  RETURN t$
ENDFUNC
DEFFN dswap(v)=(v AND 255)*256+(v AND (65535 XOR 255))/256
DEFFN lswap(v)=CVL(REVERSE$(MKL$(v)))
FUNCTION inlineconv$(adr,l)
  LOCAL t$,a$,ll
  ll=l
  WHILE l>3
    a$=CHR$(PEEK(adr+ll-l))+CHR$(PEEK(adr+ll-l+1))+CHR$(PEEK(adr+ll-l+2))
    t$=t$+CHR$(36+(PEEK(VARPTR(a$)) AND 0xfc)/4)
    t$=t$+CHR$(36+(PEEK(VARPTR(a$)) AND 0x3)*16+(PEEK(VARPTR(a$)+1) AND 0xf0)/16)
    t$=t$+CHR$(36+(PEEK(VARPTR(a$)+1) AND 0xf)*4+(PEEK(VARPTR(a$)+2) AND 0xc0)/64)
    t$=t$+CHR$(36+(PEEK(VARPTR(a$)+2) AND 0x3f))
    SUB l,3
  WEND
  IF l
    a$=CHR$(PEEK(adr+ll-l))
    IF l>1
      a$=a$+CHR$(PEEK(adr+ll-l+1))
    ENDIF
    IF l>2
      a$=a$+CHR$(PEEK(adr+ll-l+2))
    ENDIF
    a$=a$+mkl$(0)
    t$=t$+CHR$(36+(PEEK(VARPTR(a$)) AND 0xfc)/4)
    t$=t$+CHR$(36+(PEEK(VARPTR(a$)) AND 0x3)*16+(PEEK(VARPTR(a$)+1) AND 0xf0)/16)
    t$=t$+CHR$(36+(PEEK(VARPTR(a$)+1) AND 0xf)*4+(PEEK(VARPTR(a$)+2) AND 0xc0)/64)
    t$=t$+CHR$(36+(PEEK(VARPTR(a$)+2) AND 0x3f))
  ENDIF
  RETURN t$
ENDFUNC
