' Example program how to use the GUI objects. All who are familiar with the
' GEM AES definitions of ATARI ST will find it similar.
' (c) Markus Hoffmann 2002 2002-05-05

strings$="Please select from given choices:"+CHR$(0)
btext1$="OK"+CHR$(0)
btext2$="CANCEL"+CHR$(0)
x=100
y=100
w=320
h=200
sel=9
s$=""
FOR i=0 TO sel-1
  s$=s$+STR$(i+1)+CHR$(0)
NEXT i

name$="Itsme"+STRING$(30,CHR$(0))
buf$=SPACE$(20)+CHR$(0)
ted$=MKL$(VARPTR(name$))+MKL$(VARPTR(buf$))+MKL$(VARPTR(buf$))
ted$=ted$+MKI$(3)+MKI$(0)+MKI$(0)+MKI$(0x100)+MKI$(0)+MKI$(1)+MKI$(LEN(name$)-1)+MKI$(LEN(buf$)-1)

ob$=MKI$(-1)+MKI$(1)+MKI$(5+sel)+MKI$(20)+MKI$(0)+MKI$(16)
ob$=ob$+MKL$(0x21100)+MKI$(x)+MKI$(y)+MKI$(w)+MKI$(h)

ob$=ob$+MKI$(2)+MKI$(-1)+MKI$(-1)+MKI$(28)+MKI$(0)+MKI$(0)
ob$=ob$+MKL$(VARPTR(strings$))+MKI$(16)+MKI$(20)+MKI$(200)+MKI$(20)

ob$=ob$+MKI$(3)+MKI$(-1)+MKI$(-1)+MKI$(26)+MKI$(1 OR 4 OR 2)+MKI$(0)
ob$=ob$+MKL$(VARPTR(btext1$))+MKI$(16)+MKI$(160)+MKI$(16*8)+MKI$(16)

ob$=ob$+MKI$(4)+MKI$(-1)+MKI$(-1)+MKI$(26)+MKI$(1 OR 4)+MKI$(0)
ob$=ob$+MKL$(VARPTR(btext2$))+MKI$(170)+MKI$(160)+MKI$(16*8)+MKI$(16)

ob$=ob$+MKI$(5+sel)+MKI$(5)+MKI$(5+sel-1)+MKI$(20)+MKI$(0)+MKI$(32)
ob$=ob$+MKL$(-1)+MKI$(50)+MKI$(50)+MKI$(200)+MKI$(50)

FOR i=0 TO sel-2
  ob$=ob$+MKI$(5+i+1)+MKI$(-1)+MKI$(-1)+MKI$(26)+MKI$(1 OR 16)+MKI$(0)
  ob$=ob$+MKL$(VARPTR(s$)+2*i)+MKI$(10+i*20)+MKI$(16)+MKI$(16)+MKI$(16)
NEXT i
ob$=ob$+MKI$(4)+MKI$(-1)+MKI$(-1)+MKI$(26)+MKI$(1 OR 16)+MKI$(0)
ob$=ob$+MKL$(VARPTR(s$)+2*(sel-1))+MKI$(10+(sel-1)*20)+MKI$(16)+MKI$(16)+MKI$(16)

ob$=ob$+MKI$(0)+MKI$(-1)+MKI$(-1)+MKI$(29)+MKI$(8 OR 32)+MKI$(0)
ob$=ob$+MKL$(VARPTR(ted$))+MKI$(10)+MKI$(110)+MKI$(16*10)+MKI$(16)

~FORM_DIAL(0,0,0,0,0,x,y,w,h)
~FORM_DIAL(1,0,0,0,0,x,y,w,h)
~OBJC_DRAW(VARPTR(ob$),0,-1,0,0,w,h)
SHOWPAGE
PRINT "Move the mouse and this tells you the OBJ-#:"
PRINT "click the mouse to do the form."
WHILE MOUSEK=0
  IF MOUSEX<>omx OR MOUSEY<>omy
    MOUSE omx,omy
    PRINT OBJC_FIND(VARPTR(ob$),MOUSEX,MOUSEY)
  ENDIF
  PAUSE 0.1
WEND
~FORM_DIAL(2,0,0,0,0,x,y,w,h)
~FORM_DIAL(3,0,0,0,0,x,y,w,h)

SHOWPAGE
~FORM_DIAL(0,0,0,0,0,x,y,w,h)
~FORM_DIAL(1,0,0,0,0,x,y,w,h)
~OBJC_DRAW(VARPTR(ob$),0,-1,0,0,w,h)
nochmal:
PRINT "Now, fill out the form..."
ret=FORM_DO(VARPTR(ob$))
PRINT "RET:";ret
IF ret=2 ! OK
  gedr=-1
  FOR i=0 TO sel-1
    IF DPEEK(VARPTR(ob$)+(5+i)*24+10)=1
      gedr=i
      PRINT "You selected nr.";gedr+1;"."
    ENDIF
  NEXT i
  IF gedr=-1
    ~FORM_ALERT(1,"[3][You have not selected anything !][OH]")
    DPOKE VARPTR(ob$)+(ret)*24+10,0
    GOTO nochmal
  ENDIF
ENDIF

~FORM_DIAL(2,0,0,0,0,x,y,w,h)
~FORM_DIAL(3,0,0,0,0,x,y,w,h)
SHOWPAGE
PRINT "Your text input was: ",name$
QUIT
