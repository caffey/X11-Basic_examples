outsx=0
outsy=0
SETFONT "-*-courier-medium-r-*-*-14-*"
CLEARW
@gprint("Hallo, das ist ein Text....")
@gprint(" (c) Markus Hoffmann 2001")
@gprint("-----------------------------")
FILESELECT "Text laden ...","./*.txt","diffglei.txt",f$
IF not EXIST(f$)
  QUIT
ENDIF
OPEN "I",#1,f$
WHILE not eof(#1)
  s=INP(#1)
  @outs(s)
WEND
CLOSE
END

PROCEDURE gprint(a$)
  LOCAL i
  FOR i=0 TO LEN(a$)-1
    @outs(peek(varptr(a$)+i))
  NEXT i
  @outs(10)
RETURN

PROCEDURE outs(a)
  IF a=13
    outsx=0
    VSYNC
  ELSE if a=8
    outsx=MAX(0,outsx-1)
  ELSE if a=9
    outsx=MIN(79,8*((outsx+8) DIV 8))
  ELSE if a=10
    INC outsy
    CLR outsx
    VSYNC
    IF outsy>24
      outsy=24
      COPYAREA 0,16,640,400-16,0,0
      COLOR get_color(65535,65535,65535)
      PBOX 0,400-16,640,400
      COLOR get_color(0,0,0)
    ENDIF
  ELSE
    TEXT outsx*8,outsy*16+13,CHR$(a)
    INC outsx
    IF outsx=80
      CLR outsx
      @outs(10)
    ENDIF
  ENDIF
RETURN
