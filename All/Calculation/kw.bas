' calculates the nuber of the week of the year
' written in X11-Basic by Markus Hoffmann 2010-11-26
'
' demonstrates the use of JULIAN
'
a$="26.11.2010"
i=1
' get the date from the commandline
WHILE PARAM$(i)<>""
  a$=PARAM$(i)
  INC i
WEND

IF JULIAN(a$)<0
  PRINT "Usage: xbasic kw.bas dd.mm.yyyy"
  QUIT
ENDIF
PRINT a$,
PRINT @kw(a$)
QUIT

' Gibt Kalenderwoche zu Datum (c) Markus Hoffmann
FUNCTION kw(d$)
  LOCAL j,j2,wt,wt2,a,b
  j=JULIAN(d$)
  wt=(j+7) MOD 7
  a=j-wt+3            ! Donnerstag der Woche
  j2=JULIAN("04.01."+RIGHT$(d$,4))
  wt2=(j2+7) MOD 7
  b=j2-wt2+3          ! Erster Donnerstag im Jahr
  RETURN (a-b)/7+1
ENDFUNCTION

