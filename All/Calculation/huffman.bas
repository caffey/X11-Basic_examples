' ######################################################################
' Erstellt eine Huffman tabelle (binaerbaum) fuer ein Alphabet und eine
' gegebene Haeufigkeitsverteilung. Das Alphabet und die
' Haeufigkeitsverteilung wird aus einem Eingangsfile bestimmt.
' (c) Markus Hoffmann 2010-02-05
'
' Eigentlich ist es Shannon-Fano-Kodierung Binaerbaum
'
' Letzte Bearbeitung 03.02.2010
'
DIM h(256),a%(256)
DIM t$(256)
ARRAYFILL h(),0
h(0)=1 ! Das Ende Zeichen
count=0
gh=0
f$="huffman.bas" ! This is the input file
IF EXIST(f$)
  OPEN "I",#1,f$
  content$=INPUT$(#1,LOF(#1))
  l=LEN(content$)
  FOR i=0 TO l-1
    a=PEEK(VARPTR(content$)+i) AND 0xff
    h(a)=h(a)+1
  NEXT i
ENDIF
FOR i=0 TO 255
  IF h(i)>0
    INC count
    gh=gh+h(i)
  ENDIF
  a%(i)=i
NEXT i
FOR i=0 TO 255
  PRINT i,h(i),a%(i)
NEXT i
SORT h(),256,a%()
'for i=0 to 255
' print i,h(i),a(i)
'next i
PRINT "Das Alphabet besteht aus ";count;" Zeichen."
PRINT "Gesamthaeufigkeit=";gh;"= Dateilaenge=";LOF(#1)
PRINT "char alphabet[";count;"]={";
flag=0
FOR i=255 DOWNTO 0
  IF h(i)>0
    IF flag
      PRINT ",";
    ENDIF
    a=a%(i)
    IF a>=32 AND a<ASC("z") AND a<>34 AND a<>ASC("'")
      PRINT "'"+CHR$(a)+"'";
    ELSE
      PRINT "0x"+HEX$(a,2,2);
    ENDIF
    flag=1
  ENDIF
NEXT i
PRINT "};"
PRINT "char haufig[";count;"]={";
flag=0
FOR i=255 DOWNTO 0
  IF h(i)>0
    IF flag
      PRINT ",";
    ENDIF
    PRINT h(i);
    flag=1
  ENDIF
NEXT i
PRINT "};"

@doit(0,count-1)
PRINT "char *hufftable[]={"

FOR i=255 DOWNTO 255-count+1
  PRINT ENCLOSE$(t$(i))+",",
  a=a%(i) AND 0xff
  IF (a>=32 AND a<=ASC("z") AND a<>34) OR a=ASC("{") OR a=ASC("}")
    PRINT ENCLOSE$(CHR$(a));
  ELSE IF a=0
    PRINT "NULL";
  ELSE
    PRINT ENCLOSE$("\"+OCT$(a,3,3));
  ENDIF
  IF i>255-count+1
    PRINT ",";
  ENDIF
  PRINT ,"/* ";STR$(h(i)/gh*100,2,2);" % */"
NEXT i
PRINT "};"

' Jetzt die codierung
PRINT "char code_data[]={ "
SEEK #1,0
kod$=""
kodcount=0
FOR j=0 TO l-1
  a=PEEK(VARPTR(content$)+j) AND 0xff
  FOR i=0 TO count-1
    IF a%(255-i)=a
      kod$=kod$+t$(255-i)
      WHILE LEN(kod$)>=8
        kkk$=LEFT$(kod$,8)
        @output(kkk$)
        INC kodcount
        kod$=RIGHT$(kod$,LEN(kod$)-8)
      WEND
      BREAK
    ENDIF
  NEXT i
  IF i=count
    PRINT "ERROR"
  ENDIF
NEXT j
a=0
FOR i=0 TO count-1
  IF a%(255-i)=a
    kod$=kod$+t$(255-i)
    WHILE LEN(kod$)
      kkk$=LEFT$(kod$,8)
      @output(kkk$)
      INC kodcount
      kod$=RIGHT$(kod$,LEN(kod$)-8)
    WEND
    BREAK
  ENDIF
NEXT i
IF i=count
  PRINT "ERROR"
ENDIF
PRINT "};"

PRINT "New Size:    ";kodcount
PRINT "Compression: ";kodcount/LOF(#1)
PRINT "Compression: ";8*kodcount/LOF(#1);" Bits pro Zeichen. Soll:";LOG(count)/LOG(2)

CLOSE
QUIT

PROCEDURE output(b$)
  LOCAL i
  LOCAL a
  a=0
  FOR i=0 TO 7
    IF PEEK(VARPTR(b$)+i)-ASC("0")=1
      a=BSET(a,i)
    ELSE
    ENDIF
  NEXT i
  PRINT "0x";HEX$(a,2,2);", ";
  FLUSH
  INC ocount
  IF ocount>=8
    CLR ocount
    PRINT
  ENDIF
RETURN

PROCEDURE doit(sta,sto)
  LOCAL oo,x1,x2,i,hh,gh
  x1=sta
  x2=sto
  CLR gh,oo
  IF x2-x1=1
    t$(255-x1)=t$(255-x1)+"1"
    t$(255-x2)=t$(255-x2)+"0"
    RETURN
  ENDIF
  FOR i=255-sta DOWNTO 255-sto
    gh=gh+h(i)
  NEXT i
  ' print "gh=";gh
  oo=gh
  FOR i=255-sta DOWNTO 255-sto
    oo=oo-h(i)
    EXIT IF oo<gh/2
  NEXT i
  hh=255-i
  ' print "Halbierung bei ";x1;":";hh;":";x2
  FOR i=x1 TO hh
    t$(255-i)=t$(255-i)+"1"
  NEXT i
  FOR i=hh+1 TO x2
    t$(255-i)=t$(255-i)+"0"
  NEXT i
  IF hh>x1
    @doit(x1,hh)
  ENDIF
  IF hh+1<x2
    @doit(hh+1,x2)
  ENDIF
RETURN
