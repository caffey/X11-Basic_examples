' Mandelbrotfraktale (c) Markus Hoffmann
' Simple and direct drawing algorithm using complex math in X11-Basic V.1.23
'
bx%=0
by%=0
bw%=256*2
bh%=256*2

sx=-2.2
sy=-1.7
sw=3.4
sh=3.4

SIZEW ,bw%,bh%
CLEARW
t=TIMER
FOR x%=bx% TO bx%+bw%
  FOR y%=by% TO by%+bh%
    g#=(x%-bx%)*sw/bw%+sx+1i*((y%-by%)*sh/bh%+sy)
    z#=g#
    FOR c%=0 TO 255
      z#=z#*z#+g#
      EXIT IF ABS(z#)>4
    NEXT c%
    COLOR c%*256+c%*64
    PLOT x%,y%
  NEXT y%
  SHOWPAGE
NEXT x%
PRINT TIMER-t;" Sekunden."
END
DATA "Benchmark fot the virtual machine by Markus Hoffmann 2014"
