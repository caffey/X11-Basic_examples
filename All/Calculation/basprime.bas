' This program counts how many primes are smaller than 5000000
'

apc%=1
n%=1
lim%=500000
WHILE n%<lim%
  k%=3
  p%=1
  n%=n%+2
  WHILE k%*k%<=n% AND p%
    p%=(FRAC(n%/k%)<>0)
    k%=k%+2
  WEND
  IF p%
    INC apc%
  ENDIF
WEND
PRINT apc%'" primes smaller than ";lim%;" found "
END
