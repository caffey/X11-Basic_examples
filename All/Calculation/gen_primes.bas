' Copyright Nick Warne 2014: nick@ukfsn.org
' Licence: Public Domain
' A small X11Basic program to produce prime numbers.
' Using xbc to build a binary, this is _extremely_ fast beyond belief:
'   xbc -s -virtualm gen_primes.bas -o gen_primes
' Usage:  start=3; finish=100 will produce all primes in that range (etc.)
' or perhaps start=100001 finish=100301 produce primes in that range.

CLS
again:
PRINT "Input number to start (must be odd and >2 (2 is the odd prime!))"
INPUT s$
' test if a valid number
IF VAL?(s$)<>LEN(s$)
  PRINT "Not a number!"
  GOTO again
ENDIF
s=VAL(s$)
IF MOD(s,2)=0 OR s<3
  PRINT "Not an odd number or smaller than 3!"
  GOTO again
ENDIF
again1:
PRINT "Input number to finish"
INPUT f$
IF VAL?(f$)<>LEN(f$)
  PRINT "Not a number!"
  GOTO again1
ENDIF
f=VAL(f$)
FOR b=s TO f STEP 2
  ' no need to check odd numbers ending in 5
  IF b>5 AND FLOOR(b/5)=(b/5)
    c=1
    BREAK
  ENDIF
  c=0
  ' only need to test the sqr of number
  FOR a=2 TO FLOOR(SQR(b))
    IF FLOOR(b/a)=(b/a)
      c=1
      BREAK
    ENDIF
  NEXT a
  IF c=0
    PRINT b
  ENDIF
NEXT b
GOTO again
