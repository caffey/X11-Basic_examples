' Zu Spektrum der Wissenschaft
' There is a scaling law on the first digit of random numbers
' programmed in X11-Basic by Markus Hoffmann 2011-10-03 
'
SIZEW ,11*10,400
black=COLOR_RGB(0,0,0)
red=COLOR_RGB(1,0,0)
DIM zl(10)
CLR zl()
CLEARW
FOR I=0 TO 9
  TEXT i*10,10,STR$(i)
NEXT i
DO
  INC c%
  z%=RANDOM(RANDOM(10000))
  ziff%=VAL(LEFT$(STR$(z%)))
  INC zl(ziff%)
  ' print ziff%,zl(ziff%),zl(10)
  COLOR black
  LINE ziff%*10,400,ziff%*10,16
  COLOR red
  LINE ziff%*10,zl(ziff%)/c%*600+16,ziff%*10,16
  TEXT 50,380,STR$(c%)
  SHOWPAGE
  PAUSE 0.01
LOOP
QUIT
