'
' Finanzierung.bas   (c) Markus Hoffmann 2007-2010 2011-07-23
'
' berechnet bei gegebenem Zinssatz und gewuenschter
' monatlicher Zahlung (Tilgunk+Zinsen) und optional jaerhlichen
' Extratilgungen die Kreditlaufzeit in Abhaengigekeit von der
' Kreditsumme.
'
' needs gnuplot
'

' Effektiver Jahreszinssatz f"uer den Kredit
zinssatz=5.4/100

' Monatliche Rueckzahlung
belastung=1100

' Extra-Tilgung am Jahresende
extratilgung=00

tmp$="/tmp/f"+STR$(TIMER)+".dat"
tmp2$="/tmp/f"+STR$(TIMER)+".gnu"

OPEN "O",#1,tmp$
PRINT #1,"# Kreditsumme   Laufzeit/Jahren"
FOR j=1000 TO 500000 STEP 1000
  t=@laufzeit(j)
  PRINT #1,j;" ";t/12
  PRINT j;" EURO --> ";INT(t/12);" Jahre";
  IF (t MOD 12)
    PRINT " und ";(t MOD 12);" Monate."
  ELSE
    PRINT "."
  ENDIF
  EXIT IF t=0
NEXT j
CLOSE #1
OPEN "O",#1,tmp2$
PRINT #1,"set grid"
PRINT #1,"set xlabel 'Kreditsumme'"
PRINT #1,"set ylabel 'Laufzeit / Jahre'"
PRINT #1,"plot [][:30] "+ENCLOSE$(tmp$)+" u 1:2 w st t 'mon. Belastung: "+STR$(belastung)+"+"+STR$(extratilgung)+"'"
PRINT #1,"pause -1"
CLOSE #1
SYSTEM "gnuplot "+tmp2$
KILL tmp$
KILL tmp2$
QUIT

FUNCTION laufzeit(gesamt)
  LOCAL i
  i=0
  DO
    zins=gesamt*zinssatz/12
    EXIT IF 12*zins>12*belastung+extratilgung
    ' print i,int(i/12),gesamt,zins,(belastung-zins)
    SUB gesamt,(belastung-zins)
    INC i
    EXIT IF gesamt<1000
    IF (i MOD 12)=0
      SUB gesamt,extratilgung
    ENDIF
  LOOP
  RETURN i
ENDFUNCTION
