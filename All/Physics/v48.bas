' Versuch 48 Hysterese (c) Markus Hoffmann 1995
'
' This program demonstrates the use of DATA statements.
' It reads a set of data valaues to draw a plot.
' Originally written in GFA-Basic
'
' DEFLINE 1,0,0,1
grau=COLOR_RGB(0.3,0.3,0.3)
weiss=COLOR_RGB(1,1,1)
COLOR grau
CLEARW
SHOWPAGE
' get the screen size. This defaults to 640x400
GET_GEOMETRY 1,bx,by,bw,bh
LINE bw/2,by+bh,bw/2,by
LINE bx,bh/2,bx+bw,bh/2
TEXT bw/2+2,16,"H/(A/m)"
' DEFLINE 1,0,0,0
FOR j=-1000 TO 1000 STEP 100
  FOR i=-1000 TO 1000 STEP 100
    x=bw/2+i/700*320
    y=bh/2-j/0.5*200/1000
    IF x>bx AND x<bx+bw AND y>by AND y<by+bh
      LINE x+2,y,x-2,y
      LINE x,y+2,x,y-2
    ENDIF
  NEXT i
NEXT j
SHOWPAGE
COLOR weiss
l=0.667
dl=0.001
n=1500
d=0.007
dd=1E-05
u=1.25664E-06
di=0.01
dy=1
cf=1.6
dcf=0.04
'
' PRINT " I/Ampere Y/Skt   B/mT           H/(A/m)"
' PRINT STRING$(79,"-")
DO
  READ i,y
  EXIT IF i=4711
  b=cf*y/1000
  IF y<>0
    db=ABS(b*SQR((dcf/cf)^2+(dy/y)^2))
  ENDIF
  h=n*i/l-d*b/(u*l)
  dh=SQR((di*n/l)^2+(dd*b/u/l)^2+(dl/l^2*(n*i-d/u*b))^2+(d*db/(u*l))^2)
  ' PRINT 'STR$(i,5,2)''STR$(y,5,1)''STR$(b*1000,6,1);"+-";STR$(db*1000,4,1)''STR$(h,6,1);"+-";STR$(dh,5,2)
  hmu=bw/2+h/700*320
  bmu=bh/2-b/0.5*200
  hwu=dh/700*320
  bwu=db/0.5*200
  CIRCLE hmu,bmu,bwu+3
  ' PCIRCLE hm&,bm&,bw&
  LINE hmu-hwu,bmu,hmu+hwu,bmu
  LINE hmu,bmu-bwu,hmu,bmu+bwu
LOOP
'
SHOWPAGE
KEYEVENT
QUIT
'
'
'
DATA 0 ,1
DATA 0.05,6.5
DATA 0.10,9.8
DATA 0.14,16.5
DATA 0.20,23.5
DATA 0.27,36.5
DATA 0.30,41
DATA 0.34,49
DATA 0.40,58
DATA 0.45,67.5
DATA 0.51,79
DATA 0.55,84
DATA 0.61,94.5
DATA 0.65,101.5
DATA 0.71,111.5
DATA 0.75,119.5
DATA 0.81,129.5
DATA 0.86,136.5
DATA 0.90,141.5
DATA 0.95,150.5
DATA 1.00,159.5
DATA 1.05,166
DATA 1.11,174
DATA 1.15,180.5
DATA 1.20,186.5
DATA 1.26,196.5
DATA 1.32,203
DATA 1.35,208
DATA 1.42,216
DATA 1.49,223.5
DATA 1.54,229.5
DATA 1.59,236.5
DATA 1.64,241.5
DATA 1.70,246.5
DATA 1.75,250.5
'
DATA 1.50,251
DATA 1.46,248
DATA 1.39,241.5
DATA 1.34,235.5
DATA 1.29,230.5
DATA 1.25,226
DATA 1.20,218.5
DATA 1.14,212.5
DATA 1.10,205.5
DATA 1.03,198.5
DATA 1.00,192.5
DATA 0.95,185
DATA 0.90,177
DATA 0.84,169
DATA 0.80,162.5
DATA 0.75,154
DATA 0.70,145.5
DATA 0.65,135.5
DATA 0.59,125.5
DATA 0.55,117.5
DATA 0.50,109.5
DATA 0.45,99
DATA 0.39,88.5
DATA 0.35,80.5
DATA 0.30,72
DATA 0.23,57.5
DATA 0.20,52.5
DATA 0.15,44.5
DATA 0.10,35.5
DATA 0.05,24.5
DATA 0.01,19
DATA -0.01,17.5
DATA -0.04,12
DATA -0.11,0
DATA -0.16,-9
DATA -0.20,-17
DATA -0.25,-25
DATA -0.32,-40
DATA -0.36,-46
DATA -0.40,-52
DATA -0.45,-61
DATA -0.52,-76
DATA -0.55,-81
DATA -0.60,-91
DATA -0.66,-99
DATA -0.71,-108
DATA -0.75,-116
DATA -0.82,-126
DATA -0.87,-135
DATA -0.90,-139
DATA -0.95,-149
DATA -1.02,-159
DATA -1.05,-164
DATA -1.11,-173
DATA -1.16,-181
DATA -1.27,-196
DATA -1.31,-200
DATA -1.35,-207
DATA -1.39,-212
DATA -1.45,-220
DATA -1.51,-225
DATA -1.55,-231
DATA -1.60,-236
DATA -1.65,-243
DATA -1.70,-248
DATA -1.75,-253
'
DATA -1.50,-255
DATA -1.45,-249
DATA -1.40,-244
DATA -1.31,-235
DATA -1.20,-222
DATA -1.10,-209
DATA -1.00,-195
DATA -0.91,-180
DATA -0.80,-165
DATA -0.70,-147
DATA -0.60,-130
DATA -0.50,-111
DATA -0.40,-92
DATA -0.30,-71
DATA -0.20,-53
DATA -0.16,-45
DATA -0.10,-36
DATA -0.05,-25
DATA -0.01,-19
DATA 0.01,-18
DATA 0.06,-9.5
DATA 0.10,-3
DATA 0.14,7
DATA 0.20,18
DATA 0.256,28
DATA 0.30,36
DATA 0.41,55
DATA 0.50,72
DATA 0.60,90
DATA 0.71,108
DATA 0.81,125
DATA 0.91,142
DATA 1.00,157
DATA 1.11,173
DATA 1.20,188
DATA 1.34,206
DATA 1.41,216
DATA 1.50,228
DATA 1.60,239
DATA 1.70,250
DATA 4711,0
