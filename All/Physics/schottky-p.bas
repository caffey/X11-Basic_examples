'
' Program for simulating the phase space schottky spectrum
' in Proton HF of accelerator HERA-p          (c) Markus Hoffmann 03/2004
'
' make the fast tracking routine with:
' gcc -c -O2 trackit.c
' gcc -o trackit.so -shared trackit.o
'

view=01

n=10000      ! number of particles to be tracked
umlaufe=8*1024

dt=0.021    ! time resolution
bx=16     ! Picture area location
by=16
bw=5*128      ! and dimension
bh=3*128
a1=1.2875
a2=4.1201
DIM y(n),x(n)
DIM datax(umlaufe)
PRINT "Initializing particle distribution ..."
' @init_gauss(n)
@init_random(n)
@init_gauss(n/2)
' @init_line
ext$="-"+STR$(n)+"-"+STR$(umlaufe)+"-"
'x(0)=-pi/3.6
'y(0)=2
'x(1)=pi/3.6
'y(1)=-1.999
IF view
  SIZEW ,bw+2*bx,bh+2*by
  ' boundary 0
  schwarz=GET_COLOR(0,0,0)
  weiss=GET_COLOR(65535,65535,65535)
  grau=GET_COLOR(20000,20000,20000)
  rot=GET_COLOR(65535,0,0)
  gruen=GET_COLOR(0,65535,0)
  gelb=GET_COLOR(65535,65535,0)
  COLOR schwarz
  PBOX bx,by,bx+bw,by+bh
  COLOR grau
  BOX bx-1,by-1,bx+bw+1,by+bh+1
ENDIF
IF NOT EXIST("./trackit.so")
  SYSTEM "gcc -O3 -shared -o trackit.so trackit.c"
ENDIF
LINK #11,"./trackit.so"

FOR mega=0 TO 4
  IF view=0
    t=ctimer
    adr=CALL(sym_adr(#11,"trackit"),L:n,L:umlaufe,L:varptr(x(0)),L:varptr(y(0)),L:varptr(datax(0)))
    PRINT "Time: ";ctimer-t;" sek."
  ELSE
    CLR count
    DO
      IF view
        COLOR schwarz
        PBOX bx,by,bx+bw,by+bh
        COLOR rot
        TEXT bx+10,by+10,STR$(count)
        TEXT bx+bw-80,by+bh-10,"(c) 2004 MH"
        COLOR weiss
      ENDIF
      meanx=0
      ~CALL(sym_adr(#11,"trackit"),L:n,L:1,L:varptr(x(0)),L:varptr(y(0)),L:varptr(meanx))
      IF view
        ' for i=0 to n-1
        '   plot y(i)*bh/7,x(i)*bw/5
        ' next i
        SCOPE y(),x(),1,bh/7,by+bh/2,bw/5,bx+bw/2,1
      ENDIF
      SHOWPAGE
      PAUSE 0.02
      datax(count)=meanx
      INC count
      EXIT if count>=umlaufe
    LOOP
  ENDIF
  IF 0
    PRINT "saving data ..."
    PRINT "schottkydata"+ext$+STR$(mega)+".dat: [";
    FLUSH
    OPEN "O",#1,"schottkydata"+ext$+STR$(mega)+".dat"
    FOR i=0 TO umlaufe-1
      PRINT #1,i;" ";datax(i)
    NEXT i
    CLOSE #1
    PRINT "]"
    PRINT "fft"+ext$+STR$(mega)+".dat: [";
    FLUSH
    FFT datax()
    OPEN "O",#1,"fft"+ext$+STR$(mega)+".dat"
    FOR i=1 TO umlaufe-1 STEP 2
      PRINT #1,i/dt/umlaufe;" ";sqrt(datax(i)*datax(i)+datax(i+1)*datax(i+1))
    NEXT i
    CLOSE #1
    PRINT "]"
  ENDIF
NEXT mega
ENDIF
UNLINK #11
SYSTEM "rm -f trackit.so"
QUIT
PROCEDURE init_gauss(n)
  ' initializes phase space with 2D gaussian distribution
  LOCAL i
  FOR i=0 TO n-1
    y(i)=gasdev()/6
    x(i)=gasdev()/6
  NEXT i
RETURN
PROCEDURE init_random(n)
  ' initializes phase space with 2D gaussian distribution
  LOCAL i,x,y
  FOR i=0 TO n-1
    x=(RND()-0.5)
    y=(RND()-0.5)
    WHILE SQRT(x*x+y*y)>0.5
      x=(rnd()-0.5)
      y=(rnd()-0.5)
    WEND
    x(i)=x*2
    y(i)=y*7
  NEXT i
RETURN
PROCEDURE init_line
  ' initializes phase space with a simple line distribution
  LOCAL i
  FOR i=0 TO n-1
    y(i)=(i+n/20)/(0.43*n+n/20)
    x(i)=0
  NEXT i
RETURN
