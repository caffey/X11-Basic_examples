' Demonstration der HF-Anpassung bei zwei Beschleunigern
' mit unterschiedlichem Umfang und leicht unterschiedlichen
' Frequenzen.   (c) Markus Hoffmann DESY 2004

v=11/4
r=50
dt=4*r/180*pi
h1=20
h2=v*h1
CLR count
bx=0
by=0
bw=160
bh=160

SIZEW ,bw,bh
schwarz=GET_COLOR(0,0,0)
weiss=GET_COLOR(65535,65535,65535)
rot=GET_COLOR(65535,0,0)

DO
  COLOR schwarz
  PBOX 0,0,bw,bh
  f1=1/2/pi/r*h1
  f2=1/2/pi/r/v*h2+f1/11/9

  @drawzak(100-r-10,100,r,h1,-t,0,0)
  ahit=hit
  @drawzak(100+v*r+10,100,r*v,h2,t,-10*v,f1*t-f2*t)

  bhit=hit
  TEXT 20,180,STR$(INT((f1*t-f2*t)*180/pi MOD 360))
  IF ahit=1 AND bhit=1
    PCIRCLE 25,15,5
  ELSE
    CIRCLE 25,15,5
  ENDIF
  IF abs(sin(f1*t-f2*t))<0.1 AND cos(f1*t-f2*t)<0
    PCIRCLE 40,15,5
    armin=1
  ELSE
    armin=0
    CIRCLE 40,15,5
  ENDIF
  IF ahit=1 AND bhit=1 AND armin=1
    TEXT 100,80,"** TRANSFER **"
    EXIT if true
  ENDIF
  VSYNC
  'get bx,by,bw,by+bh,t$
  'bsave "b"+str$(count,3,3,1)+".xpm",varptr(t$),len(t$)
  'get 0,0,60,40,t$
  'bsave "s"+str$(count,3,3,1)+".xpm",varptr(t$),len(t$)
  'get 0,40,100,120,t$
  'bsave "p"+str$(count,3,3,1)+".xpm",varptr(t$),len(t$)
  'get 100,0,bw-100,bh,t$
  'bsave "h"+str$(count,3,3,1)+".xpm",varptr(t$),len(t$)
  ' exit if count>=89
  ' exit if t/r/v>2*pi
  ADD t,dt
  INC count
  EXIT if count>=248*4
LOOP
END
PROCEDURE drawzak(x0,y0,r0,n,t,marker,df)
  LOCAL i,p
  COLOR weiss
  FOR i=0 TO n
    p=i/n*2*pi+t/r0+df/n
    LINE x0+r0*cos(p),y0+r0*sin(p),x0+(r0+10)*cos(p+pi/n),y0+(r0+10)*sin(p+pi/n)
    LINE x0+r0*cos(p),y0+r0*sin(p),x0+(r0+10)*cos(p-pi/n),y0+(r0+10)*sin(p-pi/n)
  NEXT i
  COLOR rot
  p2=marker/n*2*pi+t/r0
  LINE x0+r0*cos(p2),y0+r0*sin(p2),x0+(r0-10)*cos(p2),y0+(r0-10)*sin(p2)
  ' print abs((t/r0 mod 2*pi))
  IF abs((t/r0 MOD 2*pi))<0.1
    PCIRCLE 10,r0/5,5
    hit=1
  ELSE
    CIRCLE 10,r0/5,5
    hit=0
  ENDIF
RETURN
