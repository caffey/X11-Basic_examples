' Abbildungen an der Linse
' geschrieben von Markus Hoffmann für  Atari nach der Idee von ... Hock
'       15.6.1986
'
' originally written in GFA-Basic
'

weiss=COLOR_RGB(1,1,1)
schwarz=COLOR_RGB(0,0,0)
rot=COLOR_RGB(1,0,0)
orange=COLOR_RGB(1,0.5,0)

o$="Fehler"

DEFTEXT 1,0.1,0.2,0
DEFMOUSE 2
CLEARW
COLOR weiss
TEXT 0,20,"  Menuleiste noch nicht erstellt...    |  Maus noch nicht aktiv  | Markus Hoffmann 1986"
COLOR rot
LTEXT 160,40,"Optik"
COLOR weiss
DEFTEXT 0,0.05,0.1,0
LTEXT 100,80,"Dieses Programm berechnet Werte"
LTEXT 100,100," 	aus der Optik nach der"
DEFTEXT 0,0.05,0.1,1
LTEXT 145,125," 	      1   1   1"
LTEXT 100,140,"LINSENFORMEL : - + - = -"
LTEXT 145,155," 	      g   b   f"
DEFTEXT 0,0.05,0.1,0
LTEXT 10,200," Es ergeben sich für die einzelnen Werte"
LTEXT 10,220," nach Umformung die folgenden Formeln"
LTEXT 10,260,"   1.	      Brennweite f=(g*b)/(g+b)"
LTEXT 10,280,"   2.	Gegenstandsweite g=(b*f)/(b-f)"
LTEXT 10,300,"   3.	       Bildweite b=(g*f)/(g-f)"
LTEXT 310,180,"   Ansonsten gelten folgende Zeichen"
LTEXT 310,220,"     G =  Gegenstandsgröße"
LTEXT 310,240,"     B =  Bildgröße"
LTEXT 310,260,"     A =  Abbildungsmaßstab"
LTEXT 310,300,"    Der Wert > G < muß vorliegen !"
LTEXT 310,320,"    > A < wird immer mitberechnet"
LTEXT 310,380,"    > Weiter, Taste drücken"
SHOWPAGE
KEYEVENT a
DO
  COLOR orange
  PBOX 10,120,629,389
  DEFMOUSE 0
  t$=" Was soll berechnet werden ? ||"
  t$=t$+"  Bildweite b und Bildgr��e B = < 1 >||"
  t$=t$+"           Gegenstandsweite g = < 2 >||"
  t$=t$+"                 Brennweite f = < 3 >|"

  ALERT 0,t$,4,"1|2|3|QUIT",n
  IF n=4
    QUIT
  ENDIF
  760:
  ON n GOSUB bildweite,gegenstandsweite,brennweite

  fac=0
  IF gg>55
    fac=2
    gg=gg/2
    bb=bb/2
  ENDIF
  IF gg<15
    fac=0.5
    gg=gg*2
    bb=bb*2
  ENDIF
  SHOWPAGE

  COLOR weiss
  PBOX 0,0,639,399
  COLOR orange
  PBOX 10,10,609,179
  COLOR rot
  PBOX 20,20,599,169

  COLOR schwarz
  LINE 30,85,590,85
  LINE 310,30,310,140
  LINE 310+f,88,310+f,82
  LINE 310-f,88,310-f,82
  TEXT 310-f,93,"f"
  TEXT 310+f,93,"f"
  FOR x=-7 TO 7
    LINE 310-g+x,85,310-g,85-gg
  NEXT x

  LINE 310-g,85-gg,310+b,85+bb
  LINE 310-g,85-gg,310,85-gg
  LINE 310,85-gg,310+b,85+bb

  FOR x=-7 TO 7
    LINE 310+b+x,85,310+b,85+bb
  NEXT x

  ' DEFTEXT 1,0,0,13
  TEXT 318-g,90-gg/2,"G"
  TEXT 318+b,90+bb/2,"B"

  ' DEFLINE 1,0,1,1
  LINE 310-g,140,310,140
  LINE 310,140,310+b,140
  TEXT 310-g/2,155,"g"
  TEXT 310+b/2,155,"b"
  IF fac=2
    gg=gg*2
    bb=bb*2
  ENDIF
  IF fac=0.5
    gg=gg/2
    bb=bb/2
  ENDIF
  COLOR schwarz
  LTEXT 20,300,"Gegenstandsgr��e   G = "+STR$(gg)
  LTEXT 20,320,"Gegenstandsweite   g = "+STR$(g)
  LTEXT 20,340,"Brennweite	 f = "+STR$(f)
  LTEXT 320,300,"Bildweite	  b = "+STR$(b)
  LTEXT 320,320,"Bildgr��e	  B = "+STR$(bb)
  LTEXT 320,340,"Abbildungsma�stab A = "+STR$(aa)+" zu 1"
  DEFMOUSE 0
  ALERT 0,"Noch eine Berechnung der |"+o$,2,"Ja|Nein",balert
  IF balert=1
    GOTO 760
  ENDIF
LOOP

PROCEDURE brennweite
  LOCAL t$,gg$,b$,g$,a,ret$
  ' ------------------
  o$="Brennweite "
  ' ------------------
  gg$="100"
  g$="110"
  b$="100"
  t$="Berechnung der "+o$+":|"
  t$=t$+" Gegenstandsgr��e G  (0-110) "+CHR$(27)+gg$+"|"
  t$=t$+" Gegenstandsweite g  (0-250) "+CHR$(27)+g$+"|"
  t$=t$+" Bildweite b         (0-250) "+CHR$(27)+b$+"|"
  ALERT 0,t$,1," OK ",a,ret$
  SPLIT ret$,CHR$(13),0,gg$,ret$
  SPLIT ret$,CHR$(13),0,g$,ret$
  SPLIT ret$,CHR$(13),0,b$,ret$
  gg=VAL(gg$)
  b=VAL(b$)
  g=VAL(g$)
  f=(b*g)/(b+g)
  aa=b/g
  bb=(b/g)*gg
RETURN
PROCEDURE gegenstandsweite
  LOCAL t$,gg$,b$,f$,a,ret$
  ' ----------------------
  o$="Gegenstandsweite"
  ' ----------------------
  gg$="100"
  b$="110"
  f$="-100"
  t$="Berechnung der "+o$+":|"
  t$=t$+" Gegenstandsgr��e G  (0-110) "+CHR$(27)+gg$+"|"
  t$=t$+" Brennweite       f  ((-200)-200) "+CHR$(27)+f$+"|"
  t$=t$+" Bildweite b         (0-250) "+CHR$(27)+b$+"|"
  ALERT 0,t$,1," OK ",a,ret$
  SPLIT ret$,CHR$(13),0,gg$,ret$
  SPLIT ret$,CHR$(13),0,f$,ret$
  SPLIT ret$,CHR$(13),0,b$,ret$
  gg=VAL(gg$)
  b=VAL(b$)
  f=VAL(f$)
  g=(b*f)/(b-f)
  aa=b/g
  bb=aa*gg
RETURN
PROCEDURE bildweite
  LOCAL t$,gg$,g$,f$,a,ret$
  ' ---------------------------
  o$="Bildweite und Bildgr��e"
  ' ---------------------------
  gg$="100"
  g$="100"
  f$="-100"
  t$="Berechnung der "+o$+":|"
  t$=t$+" Gegenstandsgr��e G  (0-110) "+CHR$(27)+gg$+"|"
  t$=t$+" Gegenstandsweite g  (0-250) "+CHR$(27)+g$+"|"
  t$=t$+" Brennweite       f  ((-200)-200) "+CHR$(27)+f$+"|"
  ALERT 0,t$,1," OK ",a,ret$
  SPLIT ret$,CHR$(13),0,gg$,ret$
  SPLIT ret$,CHR$(13),0,g$,ret$
  SPLIT ret$,CHR$(13),0,f$,ret$
  gg=VAL(gg$)
  g=VAL(g$)
  f=VAL(f$)
  b=g*f/(g-f)
  aa=b/g
  bb=aa*gg
RETURN
