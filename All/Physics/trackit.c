#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#define PI       3.141592653589793
#define C0       2.9979E8         /* Lichtgeschwindigkeit*/
#define m0       1022000           /* Masse Proton in eV */
#define dt 0.021
#define a1 1.2875
#define a2 4.1201
#define h  1

/* This file is part of  the fast Schottky tracking
   (schottky-p.bas)     (c) Markus Hoffmann 2004       */
/*  schnelle Trackingroutine fuer den longitudinalen Phasenraum
   mit doppel-HF. 
   n= anzahl der Teilchen
   umlaufe= anzahl der Umlaufe*/

void trackit(int n,int umlaufe,double *x,double *y,double *data) {
  int i,ucount=0;
  double meanx;
  if(umlaufe>1) {
  printf("Trackit: n=%d umlaufe=%d\n",n,umlaufe);
  printf("[");
  }
  while(ucount<umlaufe) {
    meanx=0;
    for(i=0;i<n;i++) {
//printf("%d: %g %g\n",i,x[i],y[i]);
      y[i]-=dt*(a1*sin(x[i])+a2*sin(4*x[i]));
      x[i]+=y[i]*dt;
      if(x[i]>PI) x[i]-=2*PI;
      if(x[i]<-PI) x[i]+=2*PI;
      meanx+=sin(h*x[i]);
    }
    meanx/=n;
    data[ucount++]=meanx;
    if((ucount % 100)==0) {
      printf(".");
      fflush(stdout);
    }
  }
  if(umlaufe>1) printf("]%d\n",ucount);
}
#undef dt
#define dt parms[0]
#define t0 0.000021

#define alpha 1.2896e-3      /* Momentum compaction factor (Lumi)*/
#define e (920e9)     /* Energie des Strahls in eV  */

#define gamma (e/m0)
#define beta (1-1/gamma/gamma)
#define amg (alpha-1/gamma/gamma)

#define uhf1 parms[2]       /* 52 MHz Cavity voltage [V]  */
#define uhf2 parms[3]       /* 208 MHz Cavity voltage [V] */


#define de parms[1]     /* rel Energieverlust pro x Umlaufe  */
#define q  1100  /* Harmonischenzahl    */

void trackit2(int n,int umlaufe,double *x,double *y,double *data, double *parms) {
  int i,ucount=0;
  double meanx;
  if(umlaufe>1) {
  printf("** Trackit: n=%d umlaufe=%d\n",n,umlaufe);
  printf("** dt=%g s     de=%g/(%g Umlaufe)\n",dt,de,dt/t0);
  printf("** U(52MHz)=%g kV   U(208MHz)=%g kV\n",uhf1/1000,uhf2/1000);
  printf("** [");
  }
  while(ucount<umlaufe) {
    meanx=0;
    for(i=0;i<n;i++) {
      
      y[i]+=-dt/t0/e*(uhf1*sin(x[i])+uhf2*sin(4*x[i]))-de;
      x[i]+=2*PI*q*amg/beta/beta/t0*y[i]*dt;
      if(x[i]>PI) x[i]-=2*PI;
      if(x[i]<-PI) x[i]+=2*PI;
      meanx+=sin(x[i]);
    }
    meanx/=n;
    data[ucount++]=meanx;
    if((ucount % 100)==0) {
      printf(".");
      fflush(stdout);
    }
  }
  if(umlaufe>1) printf("]%d\n",ucount);
}

void binit(int n,int dn,double *x,double *data) {
  int i,j;
  int over=0,under=0;
    for(i=0;i<n;i++) {
      j=(int)((x[i]+PI)/2/PI*dn);
      if(j<0) under++;
      else if(j>=dn) over++;
      else data[j]++;
    }  
}
