' Original (c) 1995 Markus Hoffmann, Uni Bonn
'
' converted from GFA-Basic to X11-Basic
'

grau=COLOR_RGB(0.3,0.3,0.3)
rot=COLOR_RGB(1,0.3,0.3)
weiss=COLOR_RGB(1,1,1)
COLOR grau
CLEARW
DIM yu(2048)
'
cx%=8     !  cx%=DPEEK(L~A-2)
bh%=400   !  bh%=DPEEK(L~A-4)
bwu=640   !  bwu=DPEEK(L~A-12)
'
'
'
f$="b7coge"
@load_spec(f$)
start%=1000
endu=1800
s=(bwu-10)/(endu-start%)
'
'
'
TEXT 200,20,"Impulsh�henspektrum: "+f$
LINE 9,0,9,bh%
LINE 0,bh%-9,bwu,bh%-9
ymaxu=0
FOR iu=start% TO endu
  ymaxu=MAX(ymaxu,yu(iu))
NEXT iu
yf=(bh%-20)/ymaxu
DEFTEXT ,0,0,4
FOR iu=0 TO ymaxu STEP MAX(INT(ymaxu/10/5+0.5)*5,1)
  LINE 5,bh%-10-iu*yf,10,bh%-10-iu*yf
  TEXT 15,bh%-10-iu*yf,STR$(iu)
NEXT iu
FOR iu=start% TO endu STEP MAX(INT((endu-start%)/10/5)*5,1)
  LINE 10+(iu-start%)*s,390,10+(iu-start%)*s,395
  TEXT 10+(iu-start%)*s-3*LEN(STR$(iu)),bh%-1,STR$(iu)
NEXT iu
x=10
COLOR rot
PLOT x,400
FOR iu=start% TO endu
  DRAW TO x,390-yu(iu)*yf
  DRAW TO x+s,390-yu(iu)*yf
  ADD x,s
  EXIT IF x>640
NEXT iu
f$=HEX$(s*10,2)+RIGHT$(f$,LEN(f$)-2)
SHOWPAGE
KEYEVENT
QUIT
'
' @save_img("C:\ABLAGE\"+f$+".IMG",XBIOS(3),80,640,400,TRUE)
'
PROCEDURE load_spec(f$)
  LOCAL i
  ' OPEN "I",#1,"C:\GFABASIC\PHYSIK\FP\K121\"+f$+".dat"
  OPEN "I",#1,f$+".dat"
  SEEK #1,1024
  FOR i=0 TO 2048-1
    ~INP(#1)
    ~INP(#1)
    yu(i)=INP(#1)+256*INP(#1)
  NEXT i
  CLOSE #1
RETURN

' All the following procedures are not useful in X11-basic
' They have worked in GFA-BASIC
'

PROCEDURE save_img(f$,adrl,awu,bwu,bh%,flb)
  LOCAL iu,anzu,azeileu,vzeileu,patlenu
  ' Nur monochrome (1 Plane)
  '
  ' f$ dateiname
  ' adr% adresse der Bitmap
  ' aw&  Breite einer Zeile in Bytes
  ' bw&  Breite in Pixeln
  ' bh&  H�he in Zeilen
  ' fl!=true : gepackt  0= ungepackt
  '
  '
  '
  patlenu=2
  OPEN "O",#1,f$
  PRINT #1,MKI$(1);MKI$(8);MKI$(1);MKI$(patlenu);MKI$(200);MKI$(200);
  PRINT #1,MKI$(bwu);MKI$(bh%);
  '
  IF flb
    vzeile$=SPACE$(awu)
    anzu=1
    CLR azeile$
    iu=0
    WHILE iu<bh%
      LINE 0,iu-1,640,iu-1
      '      PRINT AT(1,1);"Zeile"'i&
      BMOVE adrl+iu*awu,V:vzeile$,awu
      IF vzeile$=azeile$
        INC anzu
      ELSE
        IF LEN(azeile$)        !  (1 Zeile hat keinen Vorgaenger)
          IF anzu>1
            PRINT #1,CHR$(0);CHR$(0);CHR$(&HFF);CHR$(anzu);
          ENDIF
          PRINT #1,@pack_zeile$(azeile$);
          ~FRE(0)
        ENDIF
        azeile$=vzeile$
        anzu=1
      ENDIF
      INC iu
    WEND
    ' nun die letzte zeile speichern :
    IF LEN(azeile$)
      IF anzu>1
        PRINT #1,CHR$(0);CHR$(0);CHR$(&HFF);CHR$(anzu);
      ENDIF
      PRINT #1,@pack_zeile$(azeile$);
    ENDIF
    ~FRE(0)
  ELSE
    '
    ' Ungepackt speichern (sehr Speicherintensiv !)
    '
    '
    FOR iu=0 TO bh%-1
      PRINT #1,CHR$(&H80);CHR$(awu);
      BPUT #1,adrl+iu*awu,awu
    NEXT iu
  ENDIF
  CLOSE #1
RETURN
FUNCTION pack_zeile$(z$)
  LOCAL c$
  CLR c$,bu$
  '
  ' Diese Prozedur ist zeitkritisch, deshalb gut optimieren !!!
  '
  WHILE LEN(z$)
    CLR ffu
    nullu=@nulltest(z$)
    IF nullu=0
      ffu=@fftest(z$)
    ENDIF
    patu=@pattest(z$)
    vpat=patu*patlenu/(2+patlenu)
    IF ffu AND ffu>=vpat
      IF LEN(bu$) AND ffu=1
        bu$=bu$+LEFT$(z$)
        z$=RIGHT$(z$,LEN(z$)-1)
      ELSE
        IF LEN(bu$)
          c$=c$+CHR$(&H80)+CHR$(LEN(bu$))+bu$
          CLR bu$
        ENDIF
        c$=c$+CHR$(ffu OR &X10000000)
        z$=RIGHT$(z$,LEN(z$)-ffu)
      ENDIF
    ELSE IF nullu AND nullu>=vpat
      IF LEN(bu$) AND nullu=1
        bu$=bu$+LEFT$(z$)
        z$=RIGHT$(z$,LEN(z$)-1)
      ELSE
        IF LEN(bu$)
          c$=c$+CHR$(&H80)+CHR$(LEN(bu$))+bu$
          CLR bu$
        ENDIF
        c$=c$+CHR$(nullu OR &X0)
        z$=RIGHT$(z$,LEN(z$)-nullu)
      ENDIF
    ELSE IF vpat>ffu AND vpat>nullu AND patu>1
      IF LEN(bu$)
        c$=c$+CHR$(&H80)+CHR$(LEN(bu$))+bu$
        CLR bu$
      ENDIF
      c$=c$+CHR$(0)+CHR$(patu)+LEFT$(z$,2)
      z$=RIGHT$(z$,LEN(z$)-patu*patlenu)
    ELSE
      bu$=bu$+LEFT$(z$)
      z$=RIGHT$(z$,LEN(z$)-1)
    ENDIF
  WEND
  IF LEN(bu$)
    c$=c$+CHR$(&H80)+CHR$(LEN(bu$))+bu$
    CLR bu$
  ENDIF
  RETURN c$
ENDFUNC
FUNCTION nulltest(z$)
  LOCAL cu,iu
  IF LEN(z$)
    cu=0
    FOR iu=0 TO MIN(LEN(z$),127)-1
      IF PEEK(V:z$+iu)=0
        INC cu
      ELSE
        RETURN cu
      ENDIF
    NEXT iu
    RETURN cu
  ELSE
    RETURN 0
  ENDIF
ENDFUNC
FUNCTION fftest(z$)
  LOCAL cu,iu
  IF LEN(z$)
    cu=0
    FOR iu=0 TO MIN(LEN(z$),127)-1
      IF PEEK(V:z$+iu)=&HFF
        INC cu
      ELSE
        RETURN cu
      ENDIF
    NEXT iu
    RETURN cu
  ELSE
    RETURN 0
  ENDIF
ENDFUNC
FUNCTION pattest(z$)
  LOCAL cu,iu,ju
  ' Prozedur benoetigt die meiste Zeit !
  '
  IF LEN(z$)>=patlenu
    cu=1
    IF LEN(z$)>=2*patlenu
      pat$=LEFT$(z$,patlenu)
      FOR iu=1 TO INT(LEN(z$)/patlenu)-1
        FOR ju=0 TO patlenu-1
          IF PEEK(V:z$+iu*patlenu+ju)<>PEEK(V:pat$+ju)
            RETURN cu
          ENDIF
        NEXT ju
        INC cu
      NEXT iu
      RETURN cu
    ELSE
      RETURN 1
    ENDIF
  ELSE
    RETURN 0
  ENDIF
ENDFUNC
