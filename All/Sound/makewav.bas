ofilename$="output.wav"

t=10
sample_rate=44100
block_align=4
bits_per_sample=16
num_channels=2        ! stereo
audio_format=1        ! no compression
byte_rate=176400

OPEN "O",#1,ofilename$
PRINT #1,"RIFF";
sz=t*block_align*sample_rate+36 ! file size in bytes - 8
PRINT #1,mkl$(sz)+"WAVEfmt ";
sz=16
PRINT #1,mkl$(sz);mki$(audio_format);mki$(num_channels);
PRINT #1,mkl$(sample_rate);mkl$(byte_rate);mki$(block_align);mki$(bits_per_sample);
sz=t*block_align*sample_rate
PRINT #1,"data";mkl$(sz);
FOR i=0 TO t*sample_rate
  a=0
  c=gasdev()*400
  b=b+c+sin(800*i/sample_rate)*1000
  b=0.9*b
  PRINT #1,mki$(a);mki$(b);
NEXT i
CLOSE
QUIT
