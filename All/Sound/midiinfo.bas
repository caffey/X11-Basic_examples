#!/usr/bin/xbasic
' Midi File information viewer ... (c) Markus Hoffmann 1989 - 2006
'
' Letzte Bearbeitung Jan. 2007 / 2011
' Experimental function: Play midi file through the console speaker.
'
'
verbose=0
DIM chunk$(100),chunktyp$(100)
anzchunks=0
defaulttrack=1

WAVE 0,4,50,100,0.9,200
WAVE 1,2,50,100,0.9,200
WAVE 2,2,50,100,0.9,200
WAVE 3,2,50,100,0.9,200
WAVE 4,2,50,100,0.9,200
WAVE 5,2,50,100,0.9,200
WAVE 6,2,50,100,0.9,200
WAVE 7,2,50,100,0.9,200
WAVE 8,2,50,100,0.9,200
WAVE 9,4,50,100,0.9,200

i=1
CLR docomment,dotitle,dostrip
CLR inputfile$,xflag,pflag,outputexpected,title$,comment$,trackselected
outputfile$="a.mid"

WHILE LEN(PARAM$(i))
  IF LEFT$(PARAM$(i))="-"
    IF param$(i)="--help" OR PARAM$(i)="-h"
      @intro
      @using
    ELSE IF PARAM$(i)="--version"
      @intro
      QUIT
    ELSE IF PARAM$(i)="-c"
      INC i
      IF LEN(PARAM$(i))
        comment$=PARAM$(i)
        docomment=true
      ENDIF
    ELSE IF PARAM$(i)="-t"
      INC i
      IF LEN(PARAM$(i))
        title$=PARAM$(i)
        dotitle=true
      ENDIF
    ELSE IF PARAM$(i)="-q"
      DEC verbose
    ELSE IF PARAM$(i)="-v"
      INC verbose
    ELSE IF PARAM$(i)="--strip"
      dostrip=true
    ELSE IF PARAM$(i)="--track"
      INC i
      defaulttrack=VAL(PARAM$(i))
      trackselected=true
    ELSE IF PARAM$(i)="-x"
      xflag=true
    ELSE IF PARAM$(i)="-p"
      pflag=true
    ELSE IF PARAM$(i)="-o"
      INC i
      IF LEN(PARAM$(i))
        outputfilename$=PARAM$(i)
        outputexpected=true
      ENDIF
    ELSE
      collect$=collect$+PARAM$(i)+" "
    ENDIF
  ELSE
    inputfile$=PARAM$(i)
    IF NOT EXIST(inputfile$)
      PRINT "midiinfo: "+inputfile$+": file or path not found"
      CLR inputfile$
    ENDIF
  ENDIF
  INC i
WEND
IF dostrip AND (not outputexpected)
  PRINT "ERROR: In this experimental version you need to specify an"
  PRINT "output filemane with -o"
  QUIT
ENDIF
IF LEN(inputfile$)
  rumpf$=inputfile$
  WHILE len(rumpf$)
    SPLIT rumpf$,"/",1,a$,rumpf$
  WEND
  rumpf$=a$

  PRINT "File: ";inputfile$;"  ";
  OPEN "I",#1,inputfile$
  PRINT lof(#1);" Bytes."
  ' Test if it is in Midi format
  h$=input$(#1,4)
  IF h$<>"MThd"
    PRINT "ERROR : File format error. Not a midi file ?"
    QUIT
  ENDIF
  CLOSE #1
  OPEN "I",#1,inputfile$
  ' Read in all chunks
  WHILE not eof(#1)
    h$=input$(#1,4)
    l$=input$(#1,4)
    len=CVL(REVERSE$(l$))
    IF verbose>10 OR xflag
      PRINT "#";anzchunks;" Chunk: "+h$+" len="+STR$(len)
    ENDIF
    chunk$(anzchunks)=input$(#1,len)
    chunktyp$(anzchunks)=h$
    INC anzchunks
    @compute_chunk(h$,anzchunks-1)
  WEND
  CLOSE #1
  ' Now produce output
  IF outputexpected
    OPEN "O",#1,outputfile$
    IF trackselected
      PRINT "saving Track ";defaulttrack;" to file ";outputfile$
      header$=MKI$(0)+REVERSE$(MKI$(1))+REVERSE$(MKI$(timebase))
      PRINT #1,"MThd"+REVERSE$(MKL$(LEN(header$)))+header$;
      @savechunk(defaulttrack)
    ELSE
      FOR i=0 TO anzchunks-1
        PRINT "saving chunk #";i;" to file ";outputfile$
        @savechunk(i)
      NEXT i
    ENDIF
    CLOSE #1
  ENDIF
ELSE
  PRINT "midiinfo: No input files"
ENDIF

ENDIF
QUIT

PROCEDURE savechunk(id)
  LOCAL chunk$
  chunk$=chunk$(id)
  IF chunktyp$(id)="MTrk"
    IF dostrip
      chunk$=@strip$(chunk$)
    ENDIF
    IF id=defaulttrack
      x$=err$(100)
      chunk$=CHR$(0)+CHR$(255)+CHR$(1)+CHR$(LEN(x$))+x$+chunk$
      x$="created by midiinfo (c) Markus Hoffmann 1989-2007"
      chunk$=CHR$(0)+CHR$(255)+CHR$(1)+CHR$(LEN(x$))+x$+chunk$
    ENDIF
    IF id=defaulttrack
      IF docomment
        comment$=LEFT$(comment$,MIN(LEN(comment$),125))
        chunk$=CHR$(0)+CHR$(255)+CHR$(1)+CHR$(LEN(comment$))+comment$+chunk$
      ENDIF
      IF dotitle
        title$=LEFT$(title$,MIN(LEN(title$),125))
        chunk$=CHR$(0)+CHR$(255)+CHR$(3)+CHR$(LEN(title$))+title$+chunk$
      ENDIF
    ENDIF
  ENDIF
  PRINT #1,chunktyp$(id)+REVERSE$(MKL$(LEN(chunk$)))+chunk$;
RETURN

PROCEDURE intro
  PRINT "midiinfo V.1.00 Copyright (C) 1989-2006 Markus Hoffmann"
  PRINT
  PRINT "  MP3Info comes with ABSOLUTELY NO WARRANTY.  This is free software, and"
  PRINT "  you are welcome to redistribute it under certain conditions."
  PRINT "  See the file 'COPYING' for more information."
  PRINT "
  PRINT "Use 'midiinfo -h' for a usage summary or see the midiinfo man page for a"
  PRINT "complete description."
  VERSION
RETURN
PROCEDURE using
  PRINT "Usage: midiinfo [options] file "
  PRINT "Options:"
  PRINT "  -c <title>     specify comment"
  PRINT "  -h, --help     Display this information"
  PRINT "  --merge <file> merge file"
  PRINT "  -o <file>      place output to file ["+outputfile$+"]"
  PRINT "  -p             play channel 1 via the console speaker (experimental)"
  PRINT "  -q             be more quiet"
  PRINT "  --strip        strip off any text-messages"
  PRINT "  -t <title>     specify title"
  PRINT "  --track n      select track n only"
  PRINT "  -v             be more verbose"
  PRINT "  -x             Display technical attributes of the MID file"
RETURN

FUNCTION gl(adr)
  RETURN shl(PEEK(adr) AND 255,24)+shl(PEEK(adr+1) AND 255,16)+shl(PEEK(adr+2) AND 255,8)+(PEEK(adr+3) AND 255)
ENDFUNCTION
FUNCTION gi(adr)
  RETURN shl(PEEK(adr) AND 255,8)+(PEEK(adr+1) AND 255)
ENDFUNCTION

PROCEDURE compute_chunk(typ$,id)
  LOCAL c$
  c$=chunk$(id)
  IF typ$="MThd"
    fileformat=@gi(VARPTR(c$))
    numtracks=@gi(VARPTR(c$)+2)
    timebase=@gi(VARPTR(c$)+4)
    IF verbose>0 OR xflag
      PRINT "   Format: ";fileformat
      PRINT "   Tracks: ";numtracks
      PRINT "   Timebase: ";timebase
    ENDIF
    IF trackselected AND (defaulttrack<1 OR defaulttrack>=numtracks)
      PRINT "ERROR: The track number must be [1;";numtracks;"["
      QUIT
    ENDIF
  ELSE if typ$="MTrk"
    IF verbose>1
      PRINT "   Track:"
    ENDIF
    @compute_track(c$,(not trackselected and pflag) or (trackselected and id=defaulttrack and pflag))
  ELSE
    IF verbose>10
      PRINT "Chunk Typ: "+CHR$(34)+typ$+CHR$(34)+" ignored."
    ENDIF
  ENDIF
RETURN
FUNCTION strip$(tr$)
  LOCAL s$,ssp,dt,c,d,ast,l
  CLR ssp,ast,s$
  WHILE ssp<len(tr$)
    assp=ssp
    dt=@getvarlen
    c=PEEK(VARPTR(tr$)+ssp) and 255
    INC ssp
    IF c=0xf0
      ADD ssp,@getvarlen
    ELSE if c=0xff
      d=PEEK(VARPTR(tr$)+ssp) and 255
      INC ssp
      l=@getvarlen
      ADD ssp,l
      IF d>0 AND d<10
        s$=s$+MID$(tr$,ast+1,assp-ast)
        ast=ssp
        PRINT "Strip: ";"META(";d;")[";l;"] ";CHR$(34);MID$(tr$,ssp+1-l,l);CHR$(34)
      ENDIF
    ELSE if c>=0x80 AND c<0xc0
      ADD ssp,2
    ELSE if (c>=0xc0 AND c<0xe0) OR c=10
      ADD ssp,1
    ELSE if c>=0xe0 AND c<0xf0
      ADD ssp,2
    ELSE
      ' Ignorieren, also uebernehmen
    ENDIF
  WEND
  s$=s$+MID$(tr$,ast+1,ssp-ast)
  RETURN s$
ENDFUNCTION

PROCEDURE compute_track(tr$,pflag)
  LOCAL ssp,adt,c,dt
  PRINT pflag
  CLR ssp,adt
  ' memdump varptr(tr$),64
  WHILE ssp<len(tr$)
    ' get delta time
    dt=@getvarlen
    c=PEEK(VARPTR(tr$)+ssp) and 255
    INC ssp
    IF verbose>5
      PRINT "dt=";STR$(dt,6,6);" Status=";c;" ";
    ENDIF
    IF pflag AND dt>0
      adt=MIN(adt+dt,2000)
    ENDIF
    IF c=0xf0
      l=@getvarlen
      IF verbose>6
        PRINT "SYSEX[";l;"] "
      ENDIF
      ADD ssp,l
    ELSE if c=0xff
      d=PEEK(VARPTR(tr$)+ssp) and 255
      INC ssp
      l=@getvarlen
      IF verbose>6
        PRINT "META(";d;")[";l;"] ";
      ENDIF
      IF d=1
        PRINT "Text:       ";CHR$(34);MID$(tr$,ssp+1,l);CHR$(34)
      ELSE if d=2
        PRINT "Copyright:  ";CHR$(34);MID$(tr$,ssp+1,l);CHR$(34)
      ELSE if d=3
        PRINT "Track Name: ";CHR$(34);MID$(tr$,ssp+1,l);CHR$(34)
      ELSE if d=4
        PRINT "Instrument: ";CHR$(34);MID$(tr$,ssp+1,l);CHR$(34)
      ELSE if d=5
        PRINT "Lyrik:      ";CHR$(34);MID$(tr$,ssp+1,l);CHR$(34)
      ELSE if d=6
        PRINT "Marker:     ";CHR$(34);MID$(tr$,ssp+1,l);CHR$(34)
      ELSE if d=7
        PRINT "Cue:        ";CHR$(34);MID$(tr$,ssp+1,l);CHR$(34)
      ELSE if d=8
        PRINT "Program Name: ";CHR$(34);MID$(tr$,ssp+1,l);CHR$(34)
      ELSE if d=9
        PRINT "Device Name:  ";CHR$(34);MID$(tr$,ssp+1,l);CHR$(34)
      ELSE if d=0x2f AND verbose>5
        PRINT "EOT."
      ELSE if d=0x51 AND verbose>5
        PRINT "Tempo: ";shl(PEEK(VARPTR(tr$)+ssp) AND 255,16)+shl(PEEK(VARPTR(tr$)+ssp+1) AND 255,8)+(PEEK(VARPTR(tr$)+ssp+2) AND 255)
      ELSE if d=0x54 AND verbose>5
        PRINT "SMPTE OFFSET"
      ELSE if d=0x58 AND verbose>5
        PRINT "Time Signature"
      ELSE if d=0x59 AND verbose>5
        PRINT "Key Signature"
      ELSE if d=0x7f
        IF verbose>5
          PRINT "USR defined: "
          MEMDUMP varptr(tr$)+ssp,l
        ENDIF
      ELSE
        IF verbose>6
          PRINT
        ENDIF
      ENDIF
      ADD ssp,l
    ELSE if c>=0x80 AND c<0x90
      f=PEEK(VARPTR(tr$)+ssp) and 255
      a=PEEK(VARPTR(tr$)+ssp+1) and 255
      IF verbose>5
        PRINT "NOTE OFF ("+HEX$(c AND 15,1,1)+")";f;" ";a
      ENDIF
      ADD ssp,2
      IF pflag
        PAUSE adt/1000*timebase/384
        CLR adt
        SOUND c-0x90,0
      ENDIF
    ELSE if c>=0x90 AND c<0xa0
      f=PEEK(VARPTR(tr$)+ssp) and 255
      a=PEEK(VARPTR(tr$)+ssp+1) and 255
      IF verbose>5
        PRINT "NOTE ON ("+HEX$(c AND 15,1,1)+") ";f;" ";a
      ENDIF
      ADD ssp,2
      IF pflag
        PAUSE adt/1000*timebase/384
        CLR adt
        SOUND c-0x90,300*2^((f-60)/12),a/128
      ENDIF
    ELSE if c>=0xa0 AND c<0xb0
      f=PEEK(VARPTR(tr$)+ssp) and 255
      a=PEEK(VARPTR(tr$)+ssp+1) and 255
      IF verbose>5
        PRINT "AFTERTOUCH ("+HEX$(c AND 15,1,1)+") ";f;" ";a
      ENDIF
      ADD ssp,2
    ELSE if c>=0xb0 AND c<0xc0
      f=PEEK(VARPTR(tr$)+ssp) and 255
      a=PEEK(VARPTR(tr$)+ssp+1) and 255
      IF verbose>5
        PRINT "CONTROLLER ("+HEX$(c AND 15,1,1)+") ";f;" ";a
      ENDIF
      ADD ssp,2
    ELSE if c>=0xc0 AND c<0xd0
      f=PEEK(VARPTR(tr$)+ssp) and 255
      IF verbose>5
        PRINT "PROGRAM ("+HEX$(c AND 15,1,1)+") ";f
      ENDIF
      ADD ssp,1
    ELSE if c>=0xd0 AND c<0xe0
      f=PEEK(VARPTR(tr$)+ssp) and 255
      IF verbose>5
        PRINT "CHANNEL PRESSURE ("+HEX$(c AND 15,1,1)+") ";f
      ENDIF
      ADD ssp,1
    ELSE if c>=0xe0 AND c<0xf0
      f=PEEK(VARPTR(tr$)+ssp) and 255
      a=PEEK(VARPTR(tr$)+ssp+1) and 255

      IF verbose>5
        PRINT "PITCH WHEEL ("+HEX$(c AND 15,1,1)+") ";f+shl(a,7)
      ENDIF
      ADD ssp,2
    ELSE if c=0x23
      IF verbose>5
        PRINT "REP:";oc
      ENDIF
    ELSE if c>=0xf8 AND c<=0xff
    ELSE if c=10
      f=PEEK(VARPTR(tr$)+ssp) and 255
      IF verbose>5
        PRINT "??? ("+HEX$(c AND 15,1,1)+") ";f
      ENDIF
      ADD ssp,1
    ELSE
      IF verbose>5
        PRINT "Unknown: ";HEX$(c,2,2)
      ENDIF
      ' stop
    ENDIF
    oc=c
  WEND
RETURN
FUNCTION getvarlen
  LOCAL a,c
  a=PEEK(VARPTR(tr$)+ssp)
  INC ssp
  IF btst(a,7)
    a=a and 0x7f
    REPEAT
      c=PEEK(VARPTR(tr$)+ssp)
      INC ssp
      a=shl(a,7)+(c AND 0x7F)
    UNTIL btst(c,7)=0
  ENDIF
  RETURN a
ENDFUNCTION
