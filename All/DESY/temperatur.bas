' ############################################################
' ## Dynamische Temperatur-Ueberwachung der Messfuehler an  ##
' ## den Absorberflanschen der Wechselwirkungszonen Nord    ##
' ## und Sued bei HERA.                                     ##
' ## 							    ##
' ## Projekt von Anju Stroemer und Markus Hoffmann          ##
' ## Letzte Bearbeitung: 03.11.2003                         ##
' ############################################################

DIM tempsn(6,1),tempss(6,1)
DIM timesn(6,1),timess(6,1)
DIM taun(6),taus(6),texte$(6)
DIM warnn(6),dumpn(6)
DIM warns(6),dumps(6)

' Zeitkonstanten:
' Ermittelt aus den Abkuehlkurven bei Strahldump oder -verlust
' des Positronenstrahls. Verhalten etwa E-Funktion
' Bei hohen Temperatuen allerdings staerkere Abkuehlung und
' kleineres Tau,   Einheit: Sekunden

taun(0)=440
taun(1)=500
taun(2)=370
taun(3)=500
taun(4)=500
taun(5)=888
taun(6)=5040

taus(0)=500
taus(1)=500
taus(2)=1900
taus(3)=486
taus(4)=500
taus(5)=2900

taus(6)=60

dumpn(0)=50
dumpn(1)=50
dumpn(2)=50
dumpn(3)=50
dumpn(4)=50
dumpn(5)=50
dumpn(6)=50

dumps(0)=50
dumps(1)=50
dumps(2)=50
dumps(3)=50
dumps(4)=50
dumps(5)=50
dumps(6)=200

texte$(0)="11m e innen"
texte$(1)="11m p innen"
texte$(2)="11m p aussen"
texte$(3)="11m innen"
texte$(4)="11m aussen"
texte$(5)="14m"
texte$(6)="H1 C4a / SR 19 m"

SIZEW ,640,200
TITLEW 1,"Temperaturueberwachung 11m + 14 m"
dt=8
schwarz=GET_COLOR(0,0,0)

weiss=GET_COLOR(65535,65535,65535)
rot=GET_COLOR(65535,0,0)
gruen=GET_COLOR(0,65535,0)
gelb=GET_COLOR(65535,65535,0)
orange=GET_COLOR(65535,32000,0)
blau=GET_COLOR(0,0,65535)

CLEARW 1
@button(370,185,"QUIT",0)
@button(320,185,"HELP",0)
COLOR weiss
TEXT 400,190,"(c) Anju Stroemer & Markus Hoffmann"
FOR i=0 TO 6
  TEXT 500,i*20+20,texte$(i)
NEXT i
SHOWPAGE

DO
  sdt=timer
  @mausfrage
  COLOR schwarz
  PBOX 0,0,500,175
  @drawabs(0,0)
  TEXT 94,16,"N"
  TEXT 370,12,"N"
  TEXT 400,12,"S"
  TEXT 94,116,"S"
  @drawabs(0,100)
  FOR i=0 TO 6
    IF abs(tempsn(i,1)-tempsn(i,0))>0.21
      tempsn(i,0)=tempsn(i,1)
      timesn(i,0)=timesn(i,1)
    ENDIF
    IF abs(tempss(i,1)-tempss(i,0))>0.21
      tempss(i,0)=tempss(i,1)
      timess(i,0)=timess(i,1)
    ENDIF
    timesn(i,1)=timer
    timess(i,1)=timer
  NEXT i
  tempsn(0,1)=tineget("HERA/HERADATA/NR11ei/[ABSTEMPNRD1]")
  tempsn(1,1)=tineget("HERA/HERADATA/NR11ea/[ABSTEMPNRD1]")
  tempsn(2,1)=tineget("HERA/HERADATA/NR11pa/[ABSTEMPNRD1]")
  tempsn(3,1)=tineget("HERA/HERADATA/NR11i/[ABSTEMPNRD1]")
  tempsn(4,1)=tineget("HERA/HERADATA/NR11a/[ABSTEMPNRD1]")
  tempsn(5,1)=tineget("HERA/HERADATA/NR14i/[ABSTEMPNRD1]")
  tempsn(6,1)=tineget("HISTORY/C4a-R-Innen/[H1TEMP-HV]")
  ' tempsn(6,1)=tineget("HISTORY/#0/[H1TEMP-HV]")
  @mausfrage
  tempss(0,1)=tineget("HERA/HERADATA/SR11ei/[ABSTEMPSUD1]")
  tempss(1,1)=tineget("HERA/HERADATA/SR11ea/[ABSTEMPSUD1]")
  tempss(2,1)=tineget("HERA/HERADATA/SR11pa/[ABSTEMPSUD1]")
  tempss(3,1)=tineget("HERA/HERADATA/SR11i/[ABSTEMPSUD1]")
  tempss(4,1)=tineget("HERA/HERADATA/SR11a/[ABSTEMPSUD1]")
  tempss(5,1)=tineget("HERA/HERADATA/SR14i/[ABSTEMPSUD1]")
  tempss(6,1)=tineget("HERA/HERADATA/SR19a/[ABSTEMPSUD1]")
  @mausfrage
  ecur=tineget("HERA/HERADATA/#0[HEDCCur]")
  pcur=tineget("HERA/HERADATA/#0[HPDCCur]")
  ' for i=0 to 6
  '   print tempsn(i,1)-tempsn(i,0),(timesn(i,1)-timesn(i,0))
  ' next i
  ' for i=0 to 6
  '   print tempss(i,0),tempss(i,1),tempss(i,1)-tempss(i,0),(timess(i,1)-timess(i,0))
  ' next i
  COLOR gruen
  TEXT 420,160,"p:"
  COLOR rot
  TEXT 320,160,"e+:"
  COLOR gelb
  TEXT 350,160,STR$(INT(ecur*10)/10)+" mA"
  TEXT 450,160,STR$(INT(pcur*10)/10)+" mA"
  @dispn(155,44,0)
  @dispn(155,67,1)
  @dispn(158,28,2)
  @dispn(233,38,3)
  @dispn(30,71,4)
  @dispn(15,8,5)
  @disps(155,144,0)
  @disps(155,167,1)
  @disps(158,128,2)
  @disps(233,138,3)
  @disps(30,171,4)
  @disps(15,108,5)
  ' print mousex,mousey
  FOR i=0 TO 6
    a=tempsn(i,1)+taun(i)*(tempsn(i,1)-tempsn(i,0))/(timesn(i,1)-timesn(i,0))
    COLOR orange
    TEXT 330,i*20+20,STR$(INT(a*10)/10)
    COLOR gelb
    TEXT 300,i*20+20,STR$(INT(tempsn(i,1)*10)/10)
    IF tempsn(i,1)-tempsn(i,0)>0.11
      COLOR rot
      LINE 290,i*20+20,290,i*20+2
    ELSE if tempss(i,1)-tempss(i,0)<-0.11
      COLOR blau
      LINE 290,i*20+20,290,i*20+2
    ENDIF
    IF a>dumpn(i)
      warnn(i)=2
      COLOR rot
      PCIRCLE 370,i*20+20,6
    ELSE if a>0.8*(dumpn(i)-20)+20
      warnn(i)=1
      COLOR gelb
      PCIRCLE 370,i*20+20,6
    ELSE
      warnn(i)=0
    ENDIF
    COLOR weiss
    CIRCLE 370,i*20+20,6
  NEXT i
  FOR i=0 TO 6
    a=tempss(i,1)+taus(i)*(tempss(i,1)-tempss(i,0))/(timess(i,1)-timess(i,0))
    COLOR orange
    TEXT 420,i*20+20,STR$(INT(a*10)/10)
    COLOR gelb
    TEXT 450,i*20+20,STR$(INT(tempss(i,1)*10)/10)
    IF tempss(i,1)-tempss(i,0)>0.11
      COLOR rot
      LINE 480,i*20+20,480,i*20+2
    ELSE if tempss(i,1)-tempss(i,0)<-0.11
      COLOR blau
      LINE 480,i*20+20,480,i*20+2
    ENDIF
    warns(i)=INT(a/50)
    IF a>dumps(i)
      COLOR rot
      PCIRCLE 400,i*20+20,6
      warns(i)=2
    ELSE if a>0.8*(dumps(i)-20)+20
      COLOR gelb
      PCIRCLE 400,i*20+20,6
      warns(i)=1
    ELSE
      warns(i)=0
    ENDIF
    COLOR weiss
    CIRCLE 400,i*20+20,6
  NEXT i
  SHOWPAGE
  a=MAX(0.5,dt-(timer-sdt))
  WHILE a>0.5
    @mausfrage
    PAUSE 0.5
    SUB a,0.5
  WEND
  PAUSE a
LOOP
END
PROCEDURE mausfrage
  LOCAL t$,mouse_x,mouse_y,mouse_k
  MOUSE mouse_x,mouse_y,mouse_k
  IF mouse_k=1
    IF @inbutton(370,185,"QUIT",mouse_x,mouse_y)
      @button(370,185,"QUIT",TRUE)
      VSYNC
      QUIT
    ENDIF
    IF @inbutton(320,185,"HELP",mouse_x,mouse_y)
      t$="Temperaturueberwachung der Flansche in der Naehe von Absorber 4||"
      t$=t$+"Gelbe Punkte: Temperatur kann bis 80% der Dumpschwelle ansteigen|"
      t$=t$+"Rote Punkte: Temperaturanstieg fuehrt wahrscheinlich zum Dump|"
      t$=t$+"Gelbe Zahlen: Aktuelle Temperatur-Messwerte|"
      t$=t$+"Orange Zahlen: Temperaturanstieg geht wahrscheinlich bis...|"
      IF form_alert(1,"[1]["+t$+"][ OK | mehr ...]")=2
        t$="Temperaturueberwachung der Flansche in der Naehe von Absorber 4||"
        t$=t$+"Fuer weitere Fragen bitte an Markus Hoffmann wenden|"
        ~form_alert(1,"[1]["+t$+"][ OK ]")
      ENDIF
    ENDIF
  ENDIF
RETURN
PROCEDURE drawabs(x,y)
  COLOR weiss
  BOX x+50,y,x+50+100,y+60
  LINE x,y+10,x+50,y+10
  LINE x,y+20+30,x+50,y+20+30
  LINE x+150,y+6,x+200,y+6
  LINE x+150,y+16,x+200,y+16
  LINE x+150,y+26,x+200,y+26
  LINE x+150,y+36,x+200,y+36
  LINE x+150,y+46,x+200,y+46
  LINE x+150,y+56,x+200,y+56

  BOX x+35,y,x+45,y+60
  BOX x+130+35,y+22,x+130+40,y+40
  BOX x+120+35,y+42,x+120+40,y+60
  BOX x+140+35,y+2,x+140+40,y+20
  TEXT x+80,y+40,"Abs 4"

  BOX x+200+35,y+42,x+200+40,y+60
  LINE x+225,y+46,x+250,y+46
  LINE x+225,y+56,x+250,y+56

RETURN
PROCEDURE button(button_x,button_y,button_t$,sel)
  LOCAL x,y,w,h
  DEFLINE ,1
  DEFTEXT 1,0.05,0.1,0
  button_l=ltextlen(button_t$)

  x=button_x-button_l/2-10
  y=button_y-10
  w=button_l+20
  h=20
  COLOR grau
  PBOX x+5,y+5,x+w+5,y+h+5
  COLOR abs(sel)*schwarz+abs(not sel)*weiss
  PBOX x,y,x+w,y+h
  IF sel=-1
    COLOR weiss
  ELSE
    COLOR schwarz
  ENDIF
  BOX x,y,x+w,y+h

  LTEXT button_x-button_l/2,button_y-5,button_t$
RETURN
FUNCTION inbutton(button_x,button_y,button_t$,mx,my)
  LOCAL x,y,w,h

  DEFTEXT 1,0.05,0.1,0
  button_l=ltextlen(button_t$)

  x=button_x-button_l/2-10
  y=button_y-10
  w=button_l+20
  h=20
  IF mx>=x AND my>=y AND mx<=x+w AND my<=y+h
    RETURN TRUE
  ELSE
    RETURN FALSE
  ENDIF
ENDFUNC
PROCEDURE dispn(x,y,oi)
  IF warnn(oi)=2
    COLOR rot
  ELSE if warnn(oi)
    COLOR rot
  ELSE
    COLOR gelb
  ENDIF
  TEXT x,y,STR$(INT(tempsn(oi,1)*10)/10)
RETURN
PROCEDURE disps(x,y,oi)
  IF warns(oi)=2
    COLOR rot
  ELSE if warnn(oi)
    COLOR rot
  ELSE
    COLOR gelb
  ENDIF
  TEXT x,y,STR$(INT(tempss(oi,1)*10)/10)
RETURN

'
' Daten von Anju Stromer und Markus Hoffmann
'
'
'Parameter (Archiv)    Name            Dumpschwelle  [C]         Tau [s]
'
'ABSTEMPNRD1[42]      NR11a                 50            1000 +/- 800
'ABSTEMPNRD1[83]      NR11i                 55            1000 +/- 500
'ABSTEMPNRD1[48]      NR11ei                50           1500 bzw. 440
'ABSTEMPNRD1[43]      NR11pi                50           300+/-115 bzw. 1860+/-11
'ABSTEMPNRD1[57]      NR11pa                50               370 bzw 1700
'ABSTEMPNRD1[54]      NR14i                 50           470  bzw  1380 (892+/-22)
'
'Parameter            Name            Dumpschwelle  [C]         Tau [s]
'
'ABSTEMPSUD1[57]	   SR11a		 50
'ABSTEMPSUD1[81]	   SR11i		 55
'ABSTEMPSUD1[48]	   SR11ei		 50
'ABSTEMPSUD1[55]	   SR11pi		 50
'ABSTEMPSUD1[58]	   SR11pa		 50
'ABSTEMPSUD1[54]	   SR14i		 50
'
'Parameter            Name            Dumpschwelle  [C]         Tau [s]
'
'H1TEMP-HV[0]	   C4a-R-Innen		 ?            (6800+/-400) --->(5040+/-80) bzw (7200 +/- 90)
'H1TEMP-HV[1]	   C4b-R-Innen	         ?            (6600+/-780)
'H1TEMP-HV[2]	   C5a-R-Innen		 ?            (2430+/-140)
'H1TEMP-HV[3]	   C5b-R-Innen		 ?            (7160+/-370)
'H1TEMP-HV[4]	   GO-Flange		 ?            (5920+/-120)
'H1TEMP-HV[5]	   GO-Flange		 ?                ----
'H1TEMP-HV[6]	   key-3570-Oben		 ?    (3130+/-150)
'H1TEMP-HV[7]	   GG-TrFlug-Ri		 ?            (2590+/-50)
'H1TEMP-HV[8]	   AlBe-Metall-Ob		 ?    (2700+/-100)
'H1TEMP-HV[9]	   GO-PU-Balg-Ob-1		 ?    (5870+/-100)
'H1TEMP-HV[10]	   GG-PU-Balg-Ob-2		 ?    (7470+/-400)

