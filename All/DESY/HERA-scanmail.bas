OPEN "I",#1,"mail/HERA"
PRINT "<HTML> <HEAD> <TITLE> HERA Beam Dynamics Meeting</TITLE></HEAD>"
PRINT "<BODY  bgcolor=#ffffff link=2200aa vlink=008800>"
PRINT "MARKUS HOFFMANN         1/1 2003<hr>"
PRINT "scanmail V.1.01 Markus Hoffmann's at DESY "
i=0
WHILE not eof(#1)
  LINEINPUT #1,t$
  IF left$(t$,16)="Subject: HERA Be"
    WORT_SEP t$,":",1,a$,subject$
    PRINT "<hr>"
    PRINT "<H3>"
    PRINT subject$
    PRINT "</H3>"
    PRINT "<A HREF="+CHR$(34)+"minutes/"+CHR$(34)+"> MINUTES </A>"
    FLUSH
    WORT_SEP subject$," : ",1,a$,sdate$
    a=20
  ENDIF
  IF a
    IF left$(t$,5)="From:" OR LEFT$(t$,5)="Recei"
      IF flag
        PRINT "</UL>"
      ENDIF
      a=0
      flag=0
    ELSE
      IF instr(t$,"schedule") AND flag=0
        PRINT "<UL>"
        flag=1
      ENDIF
      IF instr(t$,"regards") OR instr(t$,"Barber")
        IF flag
          PRINT "</UL>"
        ENDIF
        flag=0
      ENDIF
    ENDIF
  ENDIF
  IF flag
    t$=TRIM$(t$)
    IF glob(LEFT$(t$,3),"*)*")
      PRINT "<li> ";
      flag2=1
    ELSE
      IF flag2 AND LEN(t$)
        PRINT "<p>"
        flag2=0
      ENDIF
    ENDIF
    IF glob(t$,"*http:/*")
      WORT_SEP t$,"http:/",1,a$,b$
      WORT_SEP b$," ",1,b$,c$
      PRINT a$+"<A HREF="+CHR$(34)+"http:/"+b$+CHR$(34)+"> http:/"+b$+"</a> "+c$
    ELSE
      PRINT t$
    ENDIF
  ENDIF
WEND
CLOSE
IF flag
  PRINT "</UL>"
ENDIF
PRINT "<HR><br><I>Kommentare oder Anregungen zu dieser WWW-Seite bitte"
PRINT "<A HREF=mailto:hoffmann@mpyldhoff.desy.de>hierhin</A>.</I><P>"
PRINT "<FONT FACE="+CHR$(34)+"ARIAL,HELVETICA"+CHR$(34)+" SIZE=1>"
PRINT "<SCRIPT Language="+CHR$(34)+"JavaScript"+CHR$(34)+">"
PRINT "<!--"
PRINT "  document.write('Letzte Bearbeitung: '+document.lastModified);"
PRINT " //-->"
PRINT "</SCRIPT></FONT></BODY></HTML>"
QUIT
