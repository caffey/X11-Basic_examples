' tine server. Sample how to export variables.
' The time between two tinecycles must not exceed 50ms
' (c) Markus Hoffmann 2005-2006

TINESERVER "TINEGATE"

t$="Hallo"
a=3
b=5
c=7

TINEEXPORT a,b,c,t$

DO
  PRINT a,b,c
  FOR i=0 TO 30
    PAUSE 0.01
    TINECYCLE
  NEXT i
LOOP
