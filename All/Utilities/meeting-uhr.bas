' Die Meeting Uhr
'
' Zeigt die Kosten, die Durch ein Meeting entstehen.
' (c) Markus Hoffmann
'
' Mit den Maustasten kann man die Anzahl der Teilnehmer
' erhoehen oder erniedrigen
'

red=GET_COLOR(65535,0,0)
black=GET_COLOR(0,0,0)
green=GET_COLOR(0,65535,0)

bx=0
by=32
bw=480*2
bh=320*2
SIZEW ,bw,bh
SHOWPAGE

npeople=10
stundensatz=4600/4/40
PRINT "Stundensatz: ";stundensatz
t=timer
ot=t
cost=0
DEFLINE ,3,2,1
DEFTEXT 0.9,0.1,0.1
LTEXT bx+bw/2-ltextlen("-  /  +")/2,by-30,"-  /  +"
DEFLINE ,bw/50,2,1
DEFTEXT 0.9,bw/100/10,bh/100/3*0.8
DO
  COLOR black
  PBOX bx,by,bx+bw,by+bh
  COLOR red
  TEXT 50,50,STR$(npeople)

  gs=(timer-ot)
  gm=gs div 60
  gh=hm div 60
  gm=gm mod 60
  gs=gs mod 60
  t$=STR$(gh,2,2,1)+":"+STR$(gm,2,2,1)
  ' ltext 100,70,str$(gh,2,2,1)+":"+str$(gm,2,2,1)+":"+str$(int(gs),2,2,1)
  LTEXT bx+bw/2-ltextlen(t$)/2,by+bh/10,t$
  COLOR green
  LTEXT 70,by+bh/10*6,STR$(INT(cost),4,4)+" C"
  LTEXT 70,by+bh/10*6,STR$(INT(cost),4,4)+" -"
  SHOWPAGE
  FOR i=0 TO 50
    MOUSE x,y,k
    IF k
      IF x<bx+bw/2
        npeople=MAX(1,npeople-1)
      ELSE
        INC npeople
      ENDIF
      COLOR black
      PBOX 50,30,50+100,30+30
      COLOR red
      TEXT 50,50,STR$(npeople)
      SHOWPAGE
    ENDIF
    PAUSE 0.1
  NEXT i
  tt=timer
  ADD cost,(tt-t)*stundensatz/3600*npeople
  t=tt
LOOP
QUIT
