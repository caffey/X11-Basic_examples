i=1
WHILE LEN(PARAM$(i))
  IF LEFT$(PARAM$(i))="-"
    IF param$(i)="--help" OR PARAM$(i)="-h"
      @intro
      @using
    ELSE IF PARAM$(i)="--version"
      @intro
      QUIT
    ELSE IF PARAM$(i)="-o"
      INC i
      IF LEN(PARAM$(i))
        outputfilename$=PARAM$(i)
      ENDIF
    ELSE
      collect$=collect$+PARAM$(i)+" "
    ENDIF
  ELSE
    inputfile$=PARAM$(i)
    IF NOT EXIST(inputfile$)
      PRINT param$(0)+": "+inputfile$+": file or path not found"
      CLR inputfile$
    ENDIF
  ENDIF
  INC i
WEND

@getid3info(inputfile$)
QUIT

PROCEDURE getid3info(if$)
  LOCAL t$,a
  OPEN "I",#12,if$
  t$=input$(#12,lof(#12))
  CLOSE #12

  a=instr(t$,"ID3")
  WHILE a
    ADD a,2
    PRINT "ID3 Version:"+STR$(PEEK(VARPTR(t$)+a))+":"+STR$(PEEK(VARPTR(t$)+1+a))
    flags=PEEK(VARPTR(t$)+2+a)
    PRINT "Flags: "+bin$(flags,8)
    CLR size
    size=shl(PEEK(VARPTR(t$)+3+a),21)
    size=size+shl(PEEK(VARPTR(t$)+4+a),14)
    size=size+shl(PEEK(VARPTR(t$)+5+a),7)
    size=size+PEEK(VARPTR(t$)+6+a)
    PRINT "Size: ",size
    ADD a,7
    MEMDUMP varptr(t$)+a,size
    goon=1
    WHILE goon
      tag$=mkl$(LPEEK(VARPTR(t$)+a))
      CLR s
      s=shl(PEEK(VARPTR(t$)+4+a),21)
      s=s+shl(PEEK(VARPTR(t$)+5+a),14)
      s=s+shl(PEEK(VARPTR(t$)+6+a),7)
      s=s+PEEK(VARPTR(t$)+7+a)
      f=dpeek(VARPTR(t$)+a+8)
      PRINT tag$,s,bin$(f,8)
      ADD a,10
      IF tag$=mkl$(0) OR a>=size-7
        goon=0
      ELSE if tag$="TALB"
        talb$=MID$(t$,a+1,s)
        PRINT "Album: ";talb$
      ELSE if tag$="TPE1"
        tpe1$=MID$(t$,a+1,s)
        PRINT tpe1$,LEN(tpe1$)
      ELSE if tag$="TIT2"
        tit2$=MID$(t$,a+1,s)
        PRINT tit2$,LEN(tit2$)
      ELSE if tag$="TOFN"
        tofn$=MID$(t$,a+1,s)
        PRINT tofn$,LEN(tofn$)
      ELSE if tag$="WCOP"
        wcop$=MID$(t$,a+1,s)
        PRINT wcop$,LEN(wcop$)
      ELSE if tag$="WORS"
        wors$=MID$(t$,a+1,s)
        PRINT wors$,LEN(wors$)
      ELSE
        PRINT "Unknown Tag:"
        MEMDUMP varptr(t$+a),s
      ENDIF
      ADD a,s
    WEND
    a=instr(t$,"ID3",a+1)
  WEND
RETURN
