'	Program OSTERN	(c) Markus Hoffmann  V.1.00 (Fortranversion 1993)

IF LEN(parm$)
  jahr=VAL(parm$)
ELSE
  INPUT "Jahreszahl (vierstellig): ",jahr
ENDIF
xjahr=VAL(RIGHT$(DATE$,4))
q=jahr DIV 4
a=jahr MOD 19
b=(204-11*a) MOD 30
IF b=28 OR b=28
  DEC b
ENDIF
i=b
j=jahr+q+i-13
c=j-(j DIV 7)*7
o=28+i-c
PRINT "Ostersonntag ";jahr'
IF jahr<xjahr
  PRINT "war ";
ELSE if jahr=xjahr
  PRINT "ist ";
ELSE
  PRINT "wird sein ";
ENDIF
PRINT "am ";
IF o<=31
  PRINT o;"ten Maerz."
ELSE
  PRINT o-31;"-ten April."
ENDIF
END
