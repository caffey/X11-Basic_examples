' beatify a textfile by removing unnecessary spaces at the end of
' each line. (c) Markus Hoffmann 2005
'
' Uage: xbasic beautify.bas input.txt
'
' The original File will be replaced by the beautified version.
i=1
WHILE len(PARAM$(i))
  inputfile$=PARAM$(i)
  INC i
WEND

PRINT inputfile$
OPEN "I",#1,inputfile$
OPEN "O",#2,inputfile$+".beau"

WHILE not eof(#1)
  LINEINPUT #1,t$
  WHILE len(t$) AND RIGHT$(t$)=" "
    t$=LEFT$(t$,LEN(t$)-1)
  WEND
  PRINT #2,t$
WEND
IF loc(#1)<>lof(#1)
  PRINT "ERROR"
  QUIT
ENDIF
PRINT loc(#2)/loc(#1)*100;"%"
CLOSE #1
CLOSE #2
SYSTEM "mv "+inputfile$+".beau"+" "+inputfile$
QUIT
