' stepdir.bas     X11-Basic V.1.22     example program
'                                      by Markus Hoffmann 2014-02-11
'
' Go recursively through a filesystem and list all files.
'
' Demonstrates the use of FSFIRST(), FSNEXT() and GLOB().
'
DIM files$(100000)
DIM dirs$(10000)
anzfiles=0
IF WIN32?
  root$="C:"
  mask$="*.exe" ! list only .exe files
ELSE IF ANDROID?
  root$="."
  mask$="*.bas" ! list only .bas files
ELSE
  root$="/home"
  mask$="*.pdf" ! list only .pdf files
ENDIF

anzdir=1
dirs$(0)=root$ ! This is the starting directory
dirpointer=0
WHILE dirpointer<anzdir
  ON ERROR CONT            ! Skip any error like permission denied or so....
  a$=FSFIRST$(dirs$(dirpointer),"*")
  WHILE len(a$)
    SPLIT a$," ",0,typ$,name$
    IF typ$="d"                ! Is it a directory?
      IF name$<>"." AND name$<>".."
        dirs$(anzdir)=dirs$(dirpointer)+"/"+name$
        INC anzdir
      ENDIF
    ELSE
      IF GLOB(name$,mask$)            ! Check if the filename matches the pattern...
        files$(anzfiles)=dirs$(dirpointer)+"/"+name$
        INC anzfiles
      ENDIF
    ENDIF
    ON ERROR CONT          ! Skip any error like permission denied or so....
    a$=FSNEXT$()
  WEND
  INC dirpointer
WEND
' Now list all files found (with full path name)
IF anzfiles>0
  FOR i=0 TO anzfiles-1
    PRINT files$(i)
  NEXT i
ENDIF
PRINT "Found ";anzfiles;" files in ";anzdir;" directories."
END
