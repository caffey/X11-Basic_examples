' creates ov2 files from ascii input
' (c) Markus Hoffmann 2008
'
' written in X11-basic
'
' Version 1.02 now does the map-Patitioning (new optimized algorithm)
' The algorithm is not yet heavily tested. (c) Markus Hoffmann
' latest modification 27.03.2008
' latest modification 27.01.2010
' latest modification 01.02.2010
' latest modification 01.07.2011
'

DIM skipper(400)

maxpoiinarea=16
minpoiinarea=maxpoiinarea/4

maxlevel=0

' For drawing

bx1=-510000
bx2=811000
by1=3450000
by2=5110000
bw=700
bh=700

i=1
outputfilename$="a.ov2"

SIZEW ,bw,bh

WHILE LEN(PARAM$(i))
  IF LEFT$(PARAM$(i))="-"
    IF PARAM$(i)="--help" OR PARAM$(i)="-h"
      @intro
      @using
    ELSE IF PARAM$(i)="--version"
      @intro
      QUIT
    ELSE IF PARAM$(i)="-o"
      INC i
      IF LEN(PARAM$(i))
        outputfilename$=PARAM$(i)
      ENDIF
    ENDIF
  ELSE
    inputfile$=PARAM$(i)
    IF NOT EXIST(inputfile$)
      PRINT "a2ov2: "+inputfile$+": file or path not found"
      CLR inputfile$
    ENDIF
  ENDIF
  INC i
WEND
CLEARW 1
SHOWPAGE
IF LEN(inputfile$)
  IF EXIST(outputfilename$)
    PRINT "a2ov2: Outputfilename already exists: ";outputfilename$
  ELSE
    @convert
  ENDIF
ELSE
  PRINT "a2ov2: No input files"
ENDIF
QUIT

PROCEDURE intro
  PRINT "ASCII POINT OF INTEREST to ov2 file Converter V.1.10 (c) Markus Hoffmann 2008-2008"
  VERSION
RETURN
PROCEDURE using
  PRINT "Usage: a2ov2 [options] file..."
  PRINT "Options:"
  PRINT "  -h, --help		    Display this information"
  PRINT "  -o <file>		    Place the output into <file>"
RETURN

PROCEDURE convert
  LOCAL t$
  anzzeilen=0
  PRINT "PASS 0"
  OPEN "I",#1,inputfile$
  WHILE NOT Eof(#1)
    LINEINPUT #1,t$
    t$=XTRIM$(t$)
    IF LEN(t$)
      IF LEFT$(t$)<>"#" AND t$<>"}"
        INC anzzeilen
      ENDIF
    ENDIF
  WEND
  CLOSE #1
  PRINT "numlines=";anzzeilen
  DIM zeilen$(anzzeilen)
  DIM ztyp(anzzeilen)
  DIM zx(anzzeilen)
  DIM zy(anzzeilen)
  PRINT "PASS 1"
  anzzeilen=0
  OPEN "I",#1,inputfile$
  WHILE NOT EOF(#1)
    LINEINPUT #1,t$
    t$=XTRIM$(t$)
    IF LEFT$(t$,6)="HEADER"
      WHILE t$<>"}"
        LINEINPUT #1,t$
        t$=xtrim$(t$)
      WEND
    ENDIF
    IF LEN(t$)
      IF left$(t$)<>"#" AND t$<>"}"
        zeilen$(anzzeilen)=t$
        IF left$(t$,3)="POI"
          ztyp(anzzeilen)=2
        ELSE IF LEFT$(t$,4)="AREA"
          ztyp(anzzeilen)=1
        ELSE
          ztyp(anzzeilen)=VAL(@getval$(t$,"TYP"))
        ENDIF
        IF ztyp(anzzeilen)<>1
          zx(anzzeilen)=round(VAL(@getval$(t$,"X"))*100000)
          zy(anzzeilen)=round(VAL(@getval$(t$,"Y"))*100000)
        ENDIF
        IF ztyp(anzzeilen)=0 AND zx(anzzeilen)=0
          PRINT "ERROR: ";zeilen$(anzzeilen)
          ztyp(anzzeilen)=1
        ELSE if ztyp(anzzeilen)<>1 AND zx(anzzeilen)=0 AND zy(anzzeilen)=0
          PRINT "ERROR?: ";zeilen$(anzzeilen)
        ENDIF
        INC anzzeilen
      ENDIF
    ENDIF
    IF (anzzeilen MOD 1000)=0
      PRINT chr$(13);"#";anzzeilen;
      FLUSH
    ENDIF
  WEND
  CLOSE #1
  PRINT ":"
  pointarray$=SPACE$(4*anzzeilen)
  FOR i=0 TO anzzeilen-1
    LPOKE varptr(pointarray$)+4*i,i
  NEXT i
  PRINT "PASS 2"
  OPEN "O",#2,outputfilename$
  @doit(pointarray$)
  CLOSE #2
  PRINT poicount;" POIs in ";areacount;" areas."
  PRINT "Deepest level: ";maxlevel
  ~INP(-2)
RETURN

PROCEDURE doit(array$)
  LOCAL area$,area1$,area2$,area3$,area4$
  LOCAL ar1$,ar2$,ar3$,ar4$
  LOCAL x1,x2,y1,y2
  LOCAL i,j
  IF len(array$)/4<=minpoiinarea
    @writepois(array$)
  ELSE
    area$=@makearea$(array$)
    x1=cvl(MID$(area$,0*4+1,4))
    y1=cvl(MID$(area$,1*4+1,4))
    x2=cvl(MID$(area$,2*4+1,4))
    y2=cvl(MID$(area$,3*4+1,4))
    IF x2-x1>3 AND y2-y1>3
      @openskipper(area$)
      ' print x1/100000,y1/100000,x2/100000,y2/100000
      area1$=mkl$(x1)+mkl$(y1)+mkl$((x2-x1)/2+x1)+mkl$((y2-y1)/2+y1)
      area2$=mkl$((x2-x1)/2+x1)+mkl$(y1)+mkl$(x2)+mkl$((y2-y1)/2+y1)
      area3$=mkl$(x1)+mkl$((y2-y1)/2+y1)+mkl$((x2-x1)/2+x1)+mkl$(y2)
      area4$=mkl$((x2-x1)/2+x1)+mkl$((y2-y1)/2+y1)+mkl$(x2)+mkl$(y2)
      ar1$=@selectpois$(area1$,array$)
      ar2$=@selectpois$(area2$,array$)
      ar3$=@selectpois$(area3$,array$)
      ar4$=@selectpois$(area4$,array$)
      @doit(ar1$)
      @doit(ar2$)
      @doit(ar3$)
      @doit(ar4$)
      @closeskipper
    ELSE
      PRINT "Something is wrong: ";LEN(array$)/4;" identical points?"
      FOR i=0 TO LEN(array$)/4-1
        j=LPEEK(VARPTR(array$)+i*4)
        PRINT i,j,zeilen$(j)
      NEXT i
      @writepois(RIGHT$(array$,4)) ! take only one out of all identical
    ENDIF
  ENDIF
RETURN

PROCEDURE openskipper(a$)
  PRINT "{"
  skipper(level)=LOC(#2)
  INC level
  maxlevel=MAX(maxlevel,level)
  INC areacount
  PRINT #2,CHR$(1);mkl$(0xdeadface);RIGHT$(a$,8);LEFT$(a$,8);
RETURN

PROCEDURE closeskipper
  DEC level
  tt=loc(#2)
  SEEK #2,skipper(level)+1
  PRINT #2,mkl$(tt-loc(#2)+1);
  SEEK #2,tt
  PRINT space$(level*2);"}"
RETURN

FUNCTION selectpois$(aa$,idx$)
  LOCAL oo$,x1,x2,y1,y2,i,t$,typ,x,y,j
  LOCAL flag
  LOCAL eee
  LOCAL ptr
  LOCAL anzsel
  anzsel=0
  flag=1
  eee=LEN(idx$)/4-1
  ptr=VARPTR(idx$)
  ' print "SELECTPOIS-";
  oo$=idx$
  x1=cvl(MID$(aa$,0*4+1,4))
  y1=cvl(MID$(aa$,1*4+1,4))
  x2=cvl(MID$(aa$,2*4+1,4))
  y2=cvl(MID$(aa$,3*4+1,4))
  '  print x1/100000,y1/100000,x2/100000,y2/100000
  FOR i=0 TO eee
    ' j=cvl(mid$(idx$,i*4+1,4))
    j=LPEEK(ptr+i*4)
    typ=ztyp(j)
    IF typ<>1
      ' t$=zeilen$(j)
      x=zx(j)
      y=zy(j)
      ' x=round(val(@getval$(t$,"X"))*100000) ! there is a strange roundoff error without round...
      ' y=round(val(@getval$(t$,"Y"))*100000)
      IF x>=x1 AND x<x2 AND y>=y1 AND y<y2
        LPOKE varptr(oo$)+4*anzsel,j
        INC anzsel
      ENDIF
    ELSE
      IF flag
        PRINT "skipped typ=";typ;"(";j;")"
        flag=0
      ENDIF
    ENDIF
    IF (i MOD 1000)=0
      flag=1
    ENDIF
  NEXT i
  ' print len(oo$)/4;" ";
  RETURN left$(oo$,anzsel*4)
ENDFUNCTION
PROCEDURE writepois(idx$)
  LOCAL i,j
  IF len(idx$)/4
    PRINT space$(level*2);"WRITEPOIS ";LEN(idx$)/4
    FOR i=0 TO LEN(idx$)/4-1
      j=cvl(MID$(idx$,i*4+1,4))
      @processline(j)
    NEXT i
  ENDIF
RETURN

FUNCTION makearea$(idx$)
  LOCAL a$,i,xmin,xmay,ymin,ymax,t$,x,y,j
  LOCAL ptr
  LOCAL flag
  LOCAL eee
  eee=LEN(idx$)/4-1
  flag=1
  PRINT space$(level*2);"MAKEAREA ";
  xmin=360*100000
  ymin=360*100000
  xmax=-360*100000
  ymax=-360*100000
  ptr=VARPTR(idx$)
  FOR i=0 TO eee
    ' j=cvl(mid$(idx$,i*4+1,4))
    j=LPEEK(ptr+i*4)
    typ=ztyp(j)
    IF typ<>1
      ' t$=zeilen$(j)
      x=zx(j)
      y=zy(j)
      ' x=round(val(@getval$(t$,"X"))*100000) ! there is a strange roundoff error without round...
      ' y=round(val(@getval$(t$,"Y"))*100000)
      ' print x,y
      xmin=MIN(xmin,x)
      ymin=MIN(ymin,y)
      xmax=MAX(xmax,x)
      ymax=MAX(ymax,y)
    ELSE
      IF flag
        PRINT "skipped typ=";typ;"(";j;")";CHR$(13);CHR$(9);
        FLUSH
        flag=0
      ENDIF
    ENDIF
    IF (i MOD 1000)=0
      flag=1
    ENDIF
  NEXT i
  IF xmin=xmax AND ymin=ymax
    PRINT "OOO lauter doppelte...",
    FOR i=0 TO LEN(idx$)/4-1
      j=cvl(MID$(idx$,i*4+1,4))
      PRINT j,
    NEXT i
    PRINT
  ENDIF
  ADD xmax,1
  ADD ymax,1
  SUB xmin,1
  SUB ymin,1
  a$=mkl$(xmin)+mkl$(ymin)+mkl$(xmax)+mkl$(ymax)
  PRINT xmin;":";xmax;" ";ymin;":";ymax;
  PRINT
  PRINT (xmin-bx1)/(bx2-bx1)*bw,bh-(ymin-by1)/(by2-by1)*bh,(xmax-bx1)/(bx2-bx1)*bw,bh-(ymax-by1)/(by2-by1)*bh
  FLUSH
  BOX (xmin-bx1)/(bx2-bx1)*bw,bh-(ymin-by1)/(by2-by1)*bh,(xmax-bx1)/(bx2-bx1)*bw,bh-(ymax-by1)/(by2-by1)*bh
  SHOWPAGE
  RETURN a$
ENDFUNCTION

PROCEDURE processline(idx)
  LOCAL typ,x,y
  record$=""
  typ=ztyp(idx)
  IF typ<>1
    x=zx(idx)
    y=zy(idx)
    name$=@getval$(zeilen$(idx),"NAME")
    IF left$(name$)=CHR$(34) AND RIGHT$(name$)=CHR$(34)
      name$=DECLOSE$(name$)
    ENDIF
    name$=name$+CHR$(0)
    typ=2
    record$=CHR$(typ)+mkl$(LEN(name$)+5+8)+mkl$(x)+mkl$(y)+name$
    INC poicount
  ELSE if typ=1
    PRINT "Error, this file cannot be processed!"
    QUIT
  ELSE ! data-records fuer die unbekannten typen...
    data$=@getval$(zeilen$(idx),"DATA")
    IF left$(data$)=CHR$(34) AND RIGHT$(data$)=CHR$(34)
      data$=DECLOSE$(data$)
    ENDIF
    data$=xtrim$(data$)
    WHILE len(data$)
      SPLIT data$," ",0,a$,data$
      record$=record$+CHR$(VAL("0x"+a$))
    WEND
    record$=CHR$(typ)+mkl$(LEN(record$)+5)+record$
  ENDIF
  PRINT #2,record$;
  INC recordcount
RETURN

FUNCTION getval$(t$,f$)
  LOCAL a$,val$
  val$=""
  SPLIT t$," ",1,a$,t$
  WHILE LEN(a$)
    a$=TRIM$(a$)
    SPLIT a$,"=",1,name$,val$
    EXIT IF UPPER$(name$)=UPPER$(f$)
    val$=""
    SPLIT t$," ",1,a$,t$
  WEND
  RETURN val$
ENDFUNCTION
