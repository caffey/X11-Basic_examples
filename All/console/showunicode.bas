' Displays all Unicode Charackters on the console terminal
' as far as they are supported by the console.
' written in X11-Basic (c) Markus hoffmann 2014
'

FOR page=0 TO 0x40
  PRINT HEX$(page*256)
  FOR i=0 TO 15
    PRINT HEX$(i*16);": ";
    FOR j=0 TO 15
      unicode=page*256+i*16+j
      ' print hex$(unicode);" :";
      IF unicode<0x80
        PRINT CHR$(unicode);
      ELSE IF unicode<0x800
        PRINT CHR$(0xc0+(unicode/64 AND 0x1f));
        PRINT CHR$(0x80+(unicode AND 0x3f));
      ELSE
        PRINT CHR$(0xe0+(unicode/64/64 AND 0xf));
        PRINT CHR$(0x80+(unicode/64 AND 0x3f));
        PRINT CHR$(0x80+(unicode AND 0x3f));
      ENDIF
      FLUSH
    NEXT j
    FLUSH
    PRINT
  NEXT i
  PAUSE 0.1
NEXT page
END
