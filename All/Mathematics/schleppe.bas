i=0
SIZEW 1,600,800
PBOX 0,0,640,800
istwert=0
sollwert=0

' dt=1 millisek
dt=1

tau=50*dt
fakt=1-exp(ln(0.1)/tau)
PRINT fakt

p=8
i=1
d=0

p=12.5
i=1.25
d=0

a=p+d/dt+i*dt
b=2*d/dt-p
c=d/dt

DO
  metasollwert=@f(i)
  ADD sollwert,(metasollwert-istwert)*a+h1*b+h2*c
  ' sollwert=metasollwert

  h2=h1
  h1=(metasollwert-istwert)

  ADD istwert,(sollwert-istwert)*fakt
  COLOR get_color(65535,0,0)
  CIRCLE 20+istwert,i,3
  COLOR get_color(0,65535,0)
  CIRCLE 20+sollwert,i,3
  COLOR get_color(0,0,65535)
  CIRCLE 20+metasollwert,i,3
  INC i
  altsollwert=sollwert
  metaaltsollwert=metasollwert
  VSYNC
  IF i>800
    i=0
  ENDIF
LOOP
QUIT

FUNCTION f(n)
  IF n>200
    RETURN 0
  ELSE if n<50
    RETURN 20
  ELSE if n>100
    RETURN 320
  ENDIF
  RETURN (n-50)*6+20
ENDFUNC
