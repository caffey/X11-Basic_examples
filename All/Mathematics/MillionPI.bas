' "Where is your phone number in Pi?"   by wanderer 2015
' -----------------------------------
' Programming Language: X11-Basic
'
' Demonstrates some of the "big integer" capacities of X11-Basic,
' applied also to fractional numbers.
' May be used and extended freely.
'
' This program calculates PI in as many digits of Pi as you wish.
' Instead of Pi you can also calculate and search e, the square root of 2,
' or (almost) any other square root.
'
' The program writes every number it calculates to a text file,
' so you can later open or edit it in a text editor.
' The names of these files are "bn_pi.txt", "bn_e.txt", and "bn_sqrt2.txt".
' These are initialized to 10000 digits the first time the program is run.
' Then they can be extended by means of the "Recalculate" option.
' The files for other square roots are accordingly called e.g. "bn_sqrt7" for
' the square root of 7, etc.
'
' On a Pentium IV 2.66 GHz running Windows XP, calculating 100,000 digits of Pi
' took 37 seconds for the bytecode file. (Most of this time was used for conver-
' ting the calculated number into a string.) Since the computing time increases
' approximately proportional to the square of the number of digits, calculating
' ten million digits would take approximately 100 hours.
'

IF ANDROID?
  baseDir$="/mnt/sdcard/bas/"
ELSE
  baseDir$=""
ENDIF

DIM fixedNum$(5)
filename$()=[" ","bn_pi.txt","bn_e.txt","bn_sqrt2.txt"," "]
numberName$()=[" ","Pi","e","Sqrt(2)"," "]
searchNumber$="234"
squareNumber$="3"

waitX1%=0
waitY1%=0
scrsave$=" "

OPENW 1
GET_GEOMETRY 1,scrx%,scry%,scrw%,scrh%
textW=MAX(MIN(MIN(scrw%,scrh%)/30,16),8)
linelength%=INT(scrw%/textW)
COLOR COLOR_RGB(0,0,0)
PBOX 0,0,scrw%-1,scrh%-1
COLOR COLOR_RGB(1,1,0)
GPRINT "Initializing Numbers"
GPRINT "Please Wait..."
DEFMOUSE 2
SHOWPAGE
FOR i%=1 TO 3
  IF EXIST(baseDir$+filename$(i%))    ! Loads Pi, e, and sqrt(2) from existing files.
    fixedNum$(i%)=@loadfile$(baseDir$+filename$(i%))
  ELSE     !'Initialize Pi, e, and sqrt(2) to 10000 digits.
    SELECT i%
    CASE 1
      fixedNum$(1)=@PiString$(10000)
    CASE 2
      fixedNum$(2)=@EString$(10000)
    CASE 3
      fixedNum$(3)=@SqrtString$(2,10000)
    ENDSELECT
    BSAVE baseDir$+filename$(i%),VARPTR(fixedNum$(i%)),LEN(fixedNum$(i%))
  ENDIF
NEXT i%
DEFMOUSE 0
CLEARW 1
SHOWPAGE
DIM waitText$(3)

'Start of main loop:
DO
  startAgain:
  txt$="Choose a number where to search:"
  btns$="Pi|e|sqrt(2)|sqrt(n)|EXIT"
  ALERT 0,txt$,1,btns$,selection%
  EXIT IF selection%=5
  IF selection%=4    ! arbitrary square root
    txt$="Search square root of: "+Chr$(27)+squareNumber$
    ALERT 0,txt$,0,"OK|Cancel",sele%,reTxt$
    IF sele%=2
      GOTO startAgain
    ENDIF
    squareNumber$=WORD$(reTxt$,1,Chr$(13))
    sqN%=Val(Trim$(squareNumber$))
    IF sqN%<0
      sqN%=0
    ENDIF
    filename$(4)="bn_sqrt"+Str$(sqN%)+".txt"
    numberName$(4)="Sqrt("+Str$(sqN%)+")"
    IF EXIST(baseDir$+filename$(4))
      fixedNum$(4)=@loadfile$(baseDir$+filename$(4))
    ELSE     !'Initialize sqrt(sqN%) to 10000 digits.
      GPRINT "Recalculating "+numberName$(selection%)
      GPRINT "to 10000 digits."
      GPRINT "Please wait."
      DEFMOUSE 2
      SHOWPAGE
      fixedNum$(4)=@SqrtString$(sqN%,10000)
      DEFMOUSE 0
      BSAVE baseDir$+filename$(4),VARPTR(fixedNum$(4)),LEN(fixedNum$(4))
    ENDIF
  ENDIF
  digits%=Len(fixedNum$(selection%))-InStr(fixedNum$(selection%),".")
  txt$=numberName$(selection%) + " is calculated with "
  txt$=txt$+Chr$(27)+Str$(digits%)+STRING$(5,CHR$(0))+"|"
  txt$=txt$+"digits. (Edit to recalculate.)|Input your phone number|or any other number|(without spaces):"+Chr$(27)+searchNumber$+STRING$(8,CHR$(0))
  btns$="Search|Recalculate|Cancel"
  ALERT 0, txt$, 0, btns$, sele2%, reTxt$
  'Print selection%; " - "; sele2%; " |"; reTxt$; "|"
  newDigits%=Val(Trim$(WORD$(reTxt$, 1, Chr$(13))))
  searchNumber$=WORD$(reTxt$, 2, Chr$(13))
  'Note: we should not TRIM searchNumber$ itself,
  ' because we use it as the new default text in the next ALERT,
  ' and ALERT does not allow to input a string longer than the default text.
  SELECT sele2%
  CASE 1              !'Search for a number
    posi%=INSTR(fixedNum$(selection%),TRIM$(searchNumber$))
    COLOR COLOR_RGB(1,1,0.4),COLOR_RGB(0.1,0.1,1)
    IF posi%
      GPRINT "Found "+TRIM$(searchNumber$)+" in "+numberName$(selection%)
      GPRINT "at position "+Str$(posi%-2)+":"
      ret=@PrintFoundNumber(fixedNum$(selection%))
      'Print "Found at "; posi%-2
    ELSE
      GPRINT TRIM$(searchNumber$)+" was not found in "+STR$(LEN(fixedNum$(selection%))-2)+" digits"
      GPRINT "of "+numberName$(selection%)+"."
      ' Print "Not found"
    ENDIF
    GPRINT
    COLOR COLOR_RGB(1,1,0),COLOR_RGB(0,0,0)
    GPRINT chr$(27);"[m";"Press any key"
    GPRINT "or click mouse to continue."
    SHOWPAGE
    KEYEVENT
    CLEARW 1
    SHOWPAGE
  CASE 2            !'Recalculate a number
    IF newDigits%<=digits%
      txt$="New number of digits is not greater|than the current one.|"
      txt$=txt$+"Recalculating makes no sense."
      ALERT 1, txt$, 1, "OK", s%
      GOTO startAgain
    ENDIF
    digits%=newDigits%
    GPRINT "Recalculating "+numberName$(selection%)
    GPRINT "to "+Str$(digits%)+" digits."
    GPRINT "Please wait."
    DEFMOUSE 2
    SHOWPAGE
    tt=TIMER
    SELECT selection%
    CASE 1
      fixedNum$(1)=@PiString$(digits%)
    CASE 2
      fixedNum$(2)=@EString$(digits%)
    CASE 3
      fixedNum$(3)=@SqrtString$(2,digits%)
    CASE 4
      fixedNum$(4)=@SqrtString$(sqN%,digits%)
    ENDSELECT
    GPRINT "Calculation ";selection%;" = ";TIMER-tt;" seconds."
    BSAVE baseDir$+filename$(selection%),VARPTR(fixedNum$(selection%)),LEN(fixedNum$(selection%))
    DEFMOUSE 0
  CASE 3
    ' Cancelled (nothing to do)
  ENDSELECT
LOOP
END

FUNCTION PrintFoundNumber(nString$)
  LOCAL startString%,startX%
  startString%=MAX(posi%-(lineLength%-Len(Trim$(searchNumber$)))/2, 1)
  startX%=ltextlen(Mid$(nString$, startString%, posi%-startString%))
  'COLOR color_rgb(1,0,0)
  'pbox startX%, textW*4, startX%+ltextlen(Trim$(searchNumber$)), textW*6
  p$=MID$(nString$,startString%,lineLength%)
  COLOR color_rgb(1,1,0.4),COLOR_RGB(1,0,0)
  GPRINT AT(4,4);REPLACE$(p$,Trim$(searchNumber$),CHR$(27)+"[34m"+Trim$(searchNumber$)+CHR$(27)+"[35m")
  GPRINT INT(posi%-(lineLength%-Len(Trim$(searchNumber$)))/2);"      ";posi%
  ' GPRINT AT(4,4);MID$(nString$,startString%,posi%)
  RETURN 0
ENDFUNCTION

FUNCTION PiString$(digits%)
  LOCAL expon%,myPi&,denom&

  expon%=(digits%+26)/9    ! Use more digits because of rounding errors.
  PRINT "Preparing denominator... expon%=";expon%
  denom&=@bn_power&(1000000000,expon%)
  PRINT "Calculating Pi with ";digits%;" digits...       "
  myPi&=@bnx_pi&(denom&)
  ' PRINT "myPI&=";myPi&
  PRINT "Generating string...    "
  'Return @bnx_radix$(myPi&, denom&, 10, digits%+2)
  'RETURN Left$(@bnx_radixPower$(myPi&,9*expon%,10),digits%+2)
  ' Much faster:
  p$=STR$(myPI&)
  p$=LEFT$(p$)+"."+RIGHT$(p$,LEN(p$)-1)
  RETURN LEFT$(p$,digits%+2)
ENDFUNCTION

FUNCTION EString$(digits%)
  LOCAL expon%,denom&,e&
  expon%=(digits%+26)/9    ! Use more digits because of rounding errors.
  GPRINT AT(1,1);"Preparing denominator..."
  denom&=@bn_power&(1000000000,expon%)
  GPRINT AT(1,1);"Calculating e...        "
  e&=@bnx_e&(denom&)
  GPRINT AT(1,1);"Generating string...    "
  'Return @bnx_radix$(e&, denom&, 10, digits%+2)
  ' Return Left$(@bnx_radixPower$(e&, 9*expon%, 10), digits%+2)
  ' Much faster:
  p$=STR$(e&)
  p$=LEFT$(p$)+"."+RIGHT$(p$,LEN(p$)-1)
  RETURN LEFT$(p$,digits%+2)
ENDFUNCTION

FUNCTION SqrtString$(n%, digits%)
  LOCAL expon%,denom&,root&
  expon%=(digits%+17)/9
  denom&=@bn_power&(1000000000,expon%)
  GPRINT AT(1,1);"Calculating square root..."
  root&=SQRT(n%*denom&*denom&)
  GPRINT AT(1,1);"Generating string...      "
  'Return @bnx_radix$(root&, denom&, 10, digits%+3)
  ' Return Left$(@bnx_radixPower$(root&, 9*expon%, 10), digits%+4)
  ' Much faster:
  p$=STR$(root&)
  p$=LEFT$(p$)+"."+RIGHT$(p$,LEN(p$)-1)
  RETURN LEFT$(p$,digits%+4)
ENDFUNCTION

FUNCTION bnx_pi&(denom&)
  'Calculates Pi (Gauss-Legendre Algorithm), returns numerator.
  LOCAL a&,olda&,b&,t&,x&
  PRINT "[";
  FLUSH
  a&=denom&
  b&=SQRT(denom&*denom& DIV 2)
  t&=denom& DIV 4
  x&=1
  REPEAT
    olda&=a&
    a&=(a&+b&) DIV 2
    b&=SQRT(b&*olda&)
    SUB olda&,a&
    t&=t&-((x&*olda&*olda&) DIV denom&)
    ADD x&,x&
    PRINT ".";
    FLUSH
  UNTIL olda&=0
  '	Add a&, b&
  '	num& = a&*a&/(4*t&)
  PRINT "]"
  RETURN (a&*a&) DIV t&      ! here should also a&=b&.
ENDFUNCTION

FUNCTION bnx_e&(denom&)
  'Return value / denom& = e.
  LOCAL divisor&,diff&,num&
  num&=denom&
  diff&=denom&
  divisor&=1
  REPEAT
    DIV diff&,divisor&
    INC divisor&
    ADD num&,diff&
  UNTIL diff&=0
  RETURN num&
ENDFUNCTION

FUNCTION bn_power&(a&,b%)
  ' (Returns 1 if b% negative.)
  LOCAL c&,i%
  c&=1
  FOR i%=1 To b%
    c&=c&*a&
  NEXT i%
  RETURN c&
ENDFUNCTION
FUNCTION loadfile$(f$)
  LOCAL t$
  IF NOT EXIST(f$)
    RETURN ""
  ENDIF
  OPEN "I",#1,f$
  t$=INPUT$(#1,LOF(#1))
  CLOSE #1
  RETURN t$
ENDFUNCTION
