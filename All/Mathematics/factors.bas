' Copyright Nick Warne 2014: nick@ukfsn.org
' Licence: Public Domain
' A small X11Basic program to produce factors of any number.
' Using xbc to build a binary, this is _extremely_ fast beyond belief:
'   xbc -s -virtualm factors.bas -o factors
' Examples:
' try 9014
' and...
' 73939133
' 7393913
' 739391
' 73939
' etc.

'inline function to test if input is a number
DEFFN isanum(n$)=val?(n$)=LEN(n$)
CLS
again:
c=1
f=0
PRINT "Input a number to factorize"
INPUT f$
IF @isanum(f$)=TRUE
  f=VAL(f$)
ELSE
  PRINT "Not a number!"
  GOTO again
ENDIF
'only need to test up to the sqr of number
FOR a=1 TO floor(sqr(f))
  IF floor(f/a)=(f/a)
    PRINT c*2;":";a;"x";(f/a)
    INC c
  ENDIF
NEXT a
PRINT ""
IF c=2
  PRINT f;" is a prime"
ELSE
  PRINT f;" has ";(c-1)*2;" factors"
ENDIF
PRINT ""
GOTO again
