#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define FALSE    0
#define TRUE     (!FALSE)

const char version[]="1.02";
 
char ifilename[100]="input.txt";
int stat[256];
char *buffer;
char old[20];
int intellig=4;
long laenge;
long alaenge=10000;
   
void usage(){
  printf("\n Bedienungsanleitung: \n");
  printf(" -------------------- \n\n");
  printf("Das Programm kann mit folgenden Parametern aufgerufen werden:  \n");
  printf("\n");
  printf("-input file       ---   Einlesefile      [%s]\n",ifilename);
  printf("-intelligenz [1-10] ---   IQ             [%d]\n",intellig);
  printf("-laenge           ---   Ausgabelaenge    [%d]\n",alaenge); 
  printf("\n");
}
void intro(){
  printf("***************************************************************\n");
  printf("*             BRABBEL                 V. %s             *\n",version);
  printf("*                   von Markus Hoffmann 1997                  *\n");
  printf("*                                                             *\n");
  printf("* Programmversion vom 11.01.2006                              *\n");
  printf("***************************************************************\n\n"); 
}

void init(int anzahl, char *argumente[]) {
  int count;

  /* Kommandozeile bearbeiten   */

  for(count=0;count<anzahl;count++) {
    if (strcmp(argumente[count],"-input")==FALSE) strcpy(ifilename,argumente[count+1]); 
    else if (strcmp(argumente[count],"-intelligenz")==FALSE)  intellig=atoi(argumente[count+1]);
    else if (strcmp(argumente[count],"-laenge")==FALSE)  alaenge=atoi(argumente[count+1]);   
  }
}

int irandom(int m) { return((random() & 0xffff)*m/0x10000); }

char getbest(){ 
  int best,i,sum=0,zu;

  for(i=0;i<256;i++) sum+=stat[i];
	
  if(sum==0) { 
    printf("\n");
    fprintf(stderr,"\n SUMERROR <%s>\n",old);
    strncpy(old,buffer,intellig);
    return(buffer[intellig]);
  }
  zu=irandom(sum);	
  for(i=0;i<256;i++) {
    zu-=stat[i];
    if(zu<0) return(i);
  }
  fprintf(stderr,"ERROR\n");
  return(-1);
}

void dostat() {       
  char *pos;
  pos=strstr(buffer,old);
  if(pos>buffer+laenge-intellig) printf("\n Text ist auf das Ende gelaufen.\n");
  while(pos && pos<buffer+laenge) {
    stat[(unsigned int)pos[intellig] & 0xff]++;
    pos=strstr(pos+1,old);
  }
}


long lof( FILE *n) {	
  long laenge,position;
  position=ftell(n);
  if(fseek(n,0,2)==0){
    laenge=ftell(n);
    if(fseek(n,position,0)==0) return(laenge);
  } 
  return(-1);
}



main(int anzahl, char *argumente[]) {
  FILE *dptr;
  char best;
  int i,j=0,count;

  if(anzahl<=2) {intro(); usage(); return(0);} /* zu weinig Parameter */
  init(anzahl, argumente);            /* Kommandozeile einlesen */
 
  srandom(200); 
  
  dptr=fopen(ifilename,"r");
  laenge=lof(dptr);
  printf(" %s %d [",ifilename,laenge);
  buffer=malloc(laenge+1);
  j=fread(buffer,1,laenge,dptr);  
  fclose(dptr);
  printf("] %d\n",j);
  buffer[laenge+1]=0;
  strncpy(old,buffer,intellig);
 
  for(count=0;count<alaenge;count++) {
    for(i=0;i<256;i++) stat[i]=0;
    dostat();
    best=getbest();
    for(i=1;i<intellig;i++) old[i-1]=old[i];
    old[intellig-1]=best;
    printf("%c",best); 
  }
  free(buffer);
}
