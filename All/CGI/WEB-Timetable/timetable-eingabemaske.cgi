#!/usr/bin/xbasic
' Web interface for the timetable from the Employees schedule data
' cgi-script with X11-Basic (c) Markus Hoffmann
PRINT "Content-type: text/html"+chr$(13)
PRINT ""+chr$(13)
FLUSH
PRINT "<html><head><TITLE>Timetable Input CGI</TITLE><head><body>"
startdate$=date$
enddate$=date$
reason$="holiday"
location$="paradise"
t$=env$("REQUEST_URI")
WORT_SEP t$,"?",1,a$,t$
WORT_SEP t$,"?",1,t$,u$
PRINT "<pre>"
PRINT t$
WORT_SEP t$,"&",1,a$,t$
WHILE len(a$)
  WORT_SEP a$,"=",1,a$,b$
  IF a$="v"
    selectedname$=b$
  ELSE if a$="startdate"
    startdate$=b$
  ELSE if a$="enddate"
    enddate$=b$
  ELSE if a$="button"
    button$=b$
  ELSE if a$="location"
    location$=b$
  ELSE if a$="reason"
    reason$=@purify$(b$)
    PRINT reason$
  ELSE if a$="text"
    text$=@purify$(b$)
  ELSE
    PRINT "Unknown parameter:"+a$
  ENDIF
  WORT_SEP t$,"&",1,a$,t$
WEND

version$="1.00"
host$="http://mskpc14"
@get_wikiwords

@init
anzpeople=200
DIM name$(anzpeople)
DIM vorname$(anzpeople)
DIM nachname$(anzpeople)
DIM email$(anzpeople)

DIM awaystart(anzpeople,20),awayend(anzpeople,20),awaylocation$(anzpeople,20),awayreason$(anzpeople,20)
DIM awaycount(anzpeople)
ARRAYFILL name$(),""

anzpeople=0

' Namen prozessieren

content$=@getpagecontent("Employees_Schedule")

' Den raw-content extrahieren:

SPLIT content$,"</textarea>",0,a$,content3$
content3$="</textarea>"+content3$

SPLIT a$,"<textarea",0,content1$,rawcontent$
content1$=content1$+"<textarea"
i=instr(rawcontent$,">")
content1$=content1$+left$(rawcontent$,i)
rawcontent$=right$(rawcontent$,len(rawcontent$)-i)
storecontent$=content$
content$=rawcontent$
' print content$

content$=replace$(content$,"||",chr$(10)+"|")

flag2=0
WHILE len(content$)
  SPLIT content$,chr$(10),0,t$,content$
  IF glob(t$,"{|*")
    flag2=1
  ENDIF
  IF flag2
    @processline(t$)
    IF glob(t$,"*|}*")
      flag2=0
      BREAK
    ENDIF
  ENDIF
WEND

'      if left$(t$,6)="<form "
'        split t$," ",0,a$,t$
'        while len(t$)
'	  split t$," ",0,a$,t$
'	 ' print i,a$
'	wend
'      endif
'      if left$(t$,7)="<input "
'         split t$," ",0,a$,t$
'        while len(t$)
'	  split t$," ",0,a$,t$
'	  split a$,"=",0,a$,b$
'	  b$=left$(b$,len(b$)-1)
'	  b$=right$(b$,len(b$)-1)
'          if a$="value"
'	    value$=b$
'	  else if a$="name"
'	    if b$="wpStarttime"
'	      wpstarttime$=value$
'	    else if b$="wpEdittime"
'	      wpedittime$=value$
'	    endif
'	    ' print b$,value$
'	  endif
'	  ' print i,a$,b$
'	wend
PRINT "</pre>"
SPLIT selectedname$,"_",0,selectednachname$,selectedvorname$

IF button$="Add" OR left$(button$,6)="Delete"
  FOR i=0 TO anzpeople-1
    IF selectedname$=nachname$(i)+"_"+vorname$(i)
      nameselected=i
    ENDIF
  NEXT i

  IF button$="Add"
    PRINT "<h3>Add</h3>"
    PRINT startdate$,enddate$,location$,reason$
    PRINT "For: "+selectedname$
    awaycount=awaycount(nameselected)
    awaystart(nameselected,awaycount)=julian(startdate$)
    awayend(nameselected,awaycount)=julian(enddate$)
    awaylocation$(nameselected,awaycount)=location$
    awayreason$(nameselected,awaycount)=reason$
    awaycount(nameselected)=awaycount(nameselected)+1
  ELSE if left$(button$,6)="Delete"
    delnr=val(right$(button$,len(button$)-6))
    PRINT "<h3>Delete  Nr."+str$(delnr)+"</h3>"
    PRINT "For: "+selectedname$
    awaycount=awaycount(nameselected)
    IF awaycount>delnr
      awaystart(nameselected,delnr)=awaystart(nameselected,awaycount)
      awayend(nameselected,delnr)=awayend(nameselected,awaycount)
      awaylocation$(nameselected,delnr)=awaylocation$(nameselected,awaycount)
      awayreason$(nameselected,delnr)=awayreason$(nameselected,awaycount)
      awaycount(nameselected)=awaycount(nameselected)-1
    ENDIF
  ENDIF
  PRINT "<p>"
  PRINT "<h1>Please confirm</h1>"

  newcontent$=content1$
  content$=rawcontent$
  content$=replace$(content$,"||",chr$(10)+"|")
  flag=0
  acount=0
  matching=0
  WHILE len(content$)
    SPLIT content$,chr$(10),0,t$,content$
    IF flag=0
      newcontent$=newcontent$+t$+chr$(10)
      IF left$(t$,2)="{|"
        flag=1
      ENDIF
    ELSE
      IF left$(t$,2)="|-"
        INC acount
        CLR bcount
        CLR matching
        newcontent$=newcontent$+t$+chr$(10)
      ELSE
        IF acount<2
          newcontent$=newcontent$+t$+chr$(10)
        ELSE
          IF bcount=0 AND glob(t$,"*"+selectednachname$+"*")
            INC matching
            newcontent$=newcontent$+t$+chr$(10)
          ELSE if bcount=1 AND glob(t$,"*"+selectedvorname$+"*")
            INC matching
            newcontent$=newcontent$+t$+chr$(10)
          ELSE if matching<2 OR bcount<5
            newcontent$=newcontent$+t$+chr$(10)
          ELSE
            IF bcount=5
              newcontent$=newcontent$+"|"
              awaycount=awaycount(nameselected)
              FOR i=0 TO awaycount-1
                newcontent$=newcontent$+juldate$(awaystart(nameselected,i))
                IF i<awaycount-1
                  newcontent$=newcontent$+"<br>"
                ENDIF
              NEXT i
              newcontent$=newcontent$+chr$(10)
            ELSE if bcount=6
              newcontent$=newcontent$+"|"
              awaycount=awaycount(nameselected)
              FOR i=0 TO awaycount-1
                newcontent$=newcontent$+juldate$(awayend(nameselected,i))
                IF i<awaycount-1
                  newcontent$=newcontent$+"<br>"
                ENDIF
              NEXT i
              newcontent$=newcontent$+chr$(10)
            ELSE if bcount=7
              newcontent$=newcontent$+"|"
              awaycount=awaycount(nameselected)
              FOR i=0 TO awaycount-1
                newcontent$=newcontent$+awaylocation$(nameselected,i)
                IF i<awaycount-1
                  newcontent$=newcontent$+"<br>"
                ENDIF
              NEXT i
              newcontent$=newcontent$+chr$(10)
            ELSE if bcount=8
              newcontent$=newcontent$+"|"
              awaycount=awaycount(nameselected)
              FOR i=0 TO awaycount-1
                newcontent$=newcontent$+@subword$(awayreason$(nameselected,i))
                IF i<awaycount-1
                  newcontent$=newcontent$+"<br>"
                ENDIF
              NEXT i
              newcontent$=newcontent$+chr$(10)
            ENDIF
          ENDIF
          INC bcount
        ENDIF
      ENDIF
    ENDIF
  WEND

  newcontent$=newcontent$+content3$
  PRINT newcontent$
  FLUSH
  QUIT
ENDIF
PRINT "<p>"
PRINT "<form name=querybox action="+host$+"/cgi-bin/timetable-eingabemaske.cgi method=get>"
PRINT "<ol>"
PRINT "<li> select Name from the list"
PRINT "<li> click the check button, to see which entries are already there"
PRINT "<li> press corresponding delete button if you want to delete an entry"
PRINT "<li> fill in data and press add button, if you want to add an entry"
PRINT "<li> <b>to confirm your changes you will be directed to the wiki-page. Here"
PRINT "you simply have to press the save button. thats it.</b>"
PRINT "If the wiki complains (identity changed or something), try the save button"
PRINT "a second time."
PRINT "</ol>"
nameselected=-1
PRINT "Name: <select name=v>"
FOR i=0 TO anzpeople-1
  PRINT "<option value="+nachname$(i)+"_"+vorname$(i);
  IF selectedname$=nachname$(i)+"_"+vorname$(i)
    PRINT " selected";
    nameselected=i
  ENDIF
  PRINT ">"+vorname$(i)+" "+nachname$(i)+"</option>"
NEXT i
PRINT "</select>"
PRINT "<input name=button value="+chr$(34)+"Check"+chr$(34)+" type="+chr$(34)+"submit"+chr$(34)+">"
PRINT "<p>"
'print "Nameselected=";nameselected
' Tabelle mit den bisherigen abwesenheiten:
IF nameselected<>-1
  IF awaycount(nameselected)>0
    ' print "Awaycount=";awaycount(nameselected)
    PRINT "<table border=1 cellspacing=0>"
    FOR i=0 TO awaycount(nameselected)-1
      PRINT "<td>";juldate$(awaystart(nameselected,i));"</td><td>";
      PRINT juldate$(awayend(nameselected,i));"</td><td>";awaylocation$(nameselected,i);
      PRINT "</td><td>";awayreason$(nameselected,i);"</td><td>"
      PRINT "<input name=button value="+chr$(34)+"Delete"+str$(i)+chr$(34)+" type="+chr$(34)+"submit"+chr$(34)+">";"</td><tr>"
    NEXT i
    PRINT "</table><p>"
  ENDIF
ENDIF

PRINT "Date of first absence: <INPUT  type=text name=startdate ";
PRINT "value="+chr$(34)+startdate$+chr$(34)+" size=10 maxlength=10><br>"
PRINT "Date of last absence: <INPUT  type=text name=enddate ";
PRINT "value="+chr$(34)+enddate$+chr$(34)+" size=10 maxlength=10><br>"
PRINT "Location: <INPUT  type=text name=location ";
PRINT "value="+chr$(34)+location$+chr$(34)+" size=16 maxlength=16><br>"
PRINT "Reason: <INPUT  type=text name=reason ";
PRINT "value="+chr$(34)+reason$+chr$(34)+" size=40 maxlength=40><br>"

PRINT "<input name=button value="+chr$(34)+"Add"+chr$(34)+" type="+chr$(34)+"submit"+chr$(34)+">"
PRINT "<input type=hidden name=format value=2>"
PRINT "<hr><h6>(c) Markus Hoffmann timetable-input.cgi V."+version$+" with <a href="+chr$(34)+"http://x11-basic.sourceforge.net/"+chr$(34)+">X11-Basic</a></h6></body></html>"
FLUSH
QUIT

PROCEDURE init
  DIM wochentage$(7)
  wochentage$(0)="Sun"
  wochentage$(1)="Mon"
  wochentage$(2)="Tue"
  wochentage$(3)="Wed"
  wochentage$(4)="Thu"
  wochentage$(5)="Fri"
  wochentage$(6)="Sat"
  wochentage$(7)="Sun"
  jahr=val(right$(date$,4))
RETURN

PROCEDURE processline(ln$)
  IF left$(ln$,2)="|}"
  ELSE if left$(ln$,2)="{|"
  ELSE if left$(ln$,9)="|colspan="
  ELSE if left$(ln$,9)="|rowspan="
  ELSE if left$(ln$,7)="|align="
  ELSE if left$(ln$,4)="|'''"
  ELSE
    IF left$(ln$)="|"
      IF left$(ln$,2)="|-"
        IF len(name$(anzpeople))
          IF awaycount>0
            ' for kk=0 to awaycount-1
            ' for ii=0 to ndays-1
            ' if timestamp(ii)>=awaystart(kk) and timestamp(ii)<=awayend(kk)
            ' if len(awaylocation$(kk))
            ' table$(ii,anzpeople)=table$(ii,anzpeople)+awaylocation$(kk)+"<br>"+awayreason$(kk)
            ' else
            ' table$(ii,anzpeople)=table$(ii,anzpeople)+"*"+"<br>"+awayreason$(kk)
            ' endif
            ' endif
            ' next ii
            ' next kk
          ENDIF
          INC anzpeople
        ENDIF
        awaycount=0
        c=0
      ELSE
        ln$=right$(ln$,len(ln$)-1)
        a$=ln$
        a$=replace$(a$,"]","")
        a$=replace$(a$,"[","")
        a$=replace$(a$,"&lt;","<")
        a$=replace$(a$,"&gt;",">")
        IF c=0
          SPLIT a$,"|",0,a$,b$
          IF len(b$)
            a$=b$
          ENDIF
          name$(anzpeople)=a$
          nachname$(anzpeople)=a$
        ELSE if c=1
          name$(anzpeople)=a$+" "+name$(anzpeople)
          vorname$(anzpeople)=a$
        ELSE if c=2
          SPLIT a$,"<br>",0,a$,b$
          SPLIT a$,"|",0,a$,b$
          IF len(b$)
            a$=b$
          ENDIF
          name$(anzpeople)="<a href="+a$+"> "+name$(anzpeople)+"</a>"
        ELSE if c=3
        ELSE if c=4
        ELSE if c=5
          awaycount=0
          WHILE len(a$)
            SPLIT a$,"<br>",0,b$,a$
            awaystart(anzpeople,awaycount)=julian(b$)
            ' print anzpeople,awaycount,b$,awaystart(anzpeople,awaycount)
            INC awaycount
          WEND
        ELSE if c=6
          awaycount=0
          WHILE len(a$)
            SPLIT a$,"<br>",0,b$,a$
            awayend(anzpeople,awaycount)=julian(b$)
            IF len(b$)>7
              INC awaycount
            ENDIF
          WEND
          awaycount(anzpeople)=awaycount
        ELSE if c=7
          FOR mmm=0 TO awaycount(anzpeople)-1
            WORT_SEP a$,"<br>",0,b$,a$
            awaylocation$(anzpeople,mmm)=b$
          NEXT mmm
        ELSE if c=8
          FOR mmm=0 TO awaycount(anzpeople)-1
            WORT_SEP a$,"<br>",0,b$,a$
            awayreason$(anzpeople,mmm)=b$
          NEXT mmm
        ELSE
          PRINT a$,c
        ENDIF
        INC c
      ENDIF
    ENDIF
  ENDIF
RETURN
PROCEDURE submit(p$)
  LOCAL page$,condata$
  LOCAL server$
  server$="mskpc14.desy.de"
  page$="/wiki/index.php?title="+p$+"&action=submit"
  OPEN "UC",#1,server$,80
  PRINT #1,"POST "+page$+" HTTP/1.1"+chr$(13)
  PRINT #1,"Host: "+server$+chr$(13)
  PRINT #1,"User-Agent: X11-Basic/1.12"+chr$(13)
  PRINT #1,"From: frog@my.my"
  PRINT #1,"Content-Type: multipart/form-data; boundary=DEADFACE"
  condata$=condata$+"DEADFACE"+chr$(13)+chr$(10)
  condata$=condata$+"Content-Disposition: form-data; name="+chr$(34)+"wpSection"+chr$(34)+chr$(13)+chr$(10)
  condata$=condata$+chr$(13)+chr$(10)
  condata$=condata$+chr$(13)+chr$(10)
  condata$=condata$+"DEADFACE"+chr$(13)+chr$(10)
  condata$=condata$+"Content-Disposition: form-data; name="+chr$(34)+"wpStarttime"+chr$(34)+chr$(13)+chr$(10)
  condata$=condata$+chr$(13)+chr$(10)
  condata$=condata$+wpstarttime$+chr$(13)+chr$(10)
  condata$=condata$+"DEADFACE"+chr$(13)+chr$(10)
  condata$=condata$+"Content-Disposition: form-data; name="+chr$(34)+"wpEdittime"+chr$(34)+chr$(13)+chr$(10)
  condata$=condata$+chr$(13)+chr$(10)
  condata$=condata$+wpedittime$+chr$(13)+chr$(10)
  condata$=condata$+"DEADFACE"+chr$(13)+chr$(10)
  condata$=condata$+"Content-Disposition: form-data; name="+chr$(34)+"wpScrolltop"+chr$(34)+chr$(13)+chr$(10)
  condata$=condata$+chr$(13)+chr$(10)
  condata$=condata$+chr$(13)+chr$(10)
  condata$=condata$+"DEADFACE"+chr$(13)+chr$(10)
  condata$=condata$+"Content-Disposition: form-data; name="+chr$(34)+"wpTextbox1"+chr$(34)+chr$(13)+chr$(10)
  condata$=condata$+chr$(13)+chr$(10)
  condata$=condata$+textbox1$+chr$(13)+chr$(10)
  condata$=condata$+chr$(13)+chr$(10)
  condata$=condata$+"DEADFACE"+chr$(13)+chr$(10)
  condata$=condata$+"Content-Disposition: form-data; name="+chr$(34)+"wpSummary"+chr$(34)+chr$(13)+chr$(10)
  condata$=condata$+chr$(13)+chr$(10)
  condata$=condata$+"Update from script"+chr$(13)+chr$(10)
  condata$=condata$+"DEADFACE"+chr$(13)+chr$(10)
  condata$=condata$+"Content-Disposition: form-data; name="+chr$(34)+"wpSave"+chr$(34)+chr$(13)+chr$(10)
  condata$=condata$+chr$(13)+chr$(10)
  condata$=condata$+"Seite speichern"+chr$(13)+chr$(10)
  condata$=condata$+"DEADFACE"+chr$(13)+chr$(10)
  condata$=condata$+"Content-Disposition: form-data; name="+chr$(34)+"wpEditToken"+chr$(34)+chr$(13)+chr$(10)
  condata$=condata$+chr$(13)+chr$(10)
  condata$=condata$+edittoken$+chr$(13)+chr$(10)
  condata$=condata$+"DEADFACE"+chr$(13)+chr$(10)
  condata$=condata$+"Content-Disposition: form-data; name="+chr$(34)+"wpAutoSummary"+chr$(34)+chr$(13)+chr$(10)
  condata$=condata$+chr$(13)+chr$(10)
  condata$=condata$+chr$(13)+chr$(10)
  PRINT #1,"Content-Length: "+str$(condata$)
  PRINT #1,chr$(13)
  FLUSH #1
  PRINT #1,condata$
  FLUSH #1
  LINEINPUT #1,response$
  SPLIT response$," ",0,protocol$,response$
  SPLIT response$," ",0,htmlerror$,response$
  PRINT "Response: ";response$
  CLOSE #1
RETURN
FUNCTION getpagecontent(p$)
  LOCAL page$
  LOCAL server$
  LOCAL a$,b$,t$
  ret$=""
  server$="mskpc14.desy.de"
  page$="/wiki/index.php?title="+p$+"&action=edit&"
  PRINT page$
  OPEN "UC",#1,server$,80
  PRINT #1,"GET "+page$+" HTTP/1.1"+chr$(13)
  PRINT #1,"Host: "+server$+chr$(13)
  PRINT #1,"User-Agent: X11-Basic/1.12"+chr$(13)
  PRINT #1,chr$(13)
  FLUSH #1
  LINEINPUT #1,response$
  SPLIT response$," ",0,protocol$,response$
  SPLIT response$," ",0,htmlerror$,response$
  PRINT "Response: ";response$
  IF val(htmlerror$)=200
    ' Parse Header
    LINEINPUT #1,t$
    t$=replace$(t$,chr$(13),"")
    WHILE len(t$)
      ' print t$,len(t$)
      SPLIT t$,":",0,a$,b$
      IF a$="Content-Length"
        length=val(b$)
      ELSE if a$="Transfer-Encoding"
        encoding$=trim$(b$)
      ENDIF
      LINEINPUT #1,t$
      t$=replace$(t$,chr$(13),"")
    WEND
    'print "Len=";length;" Bytes."
    IF length
      ret$=input$(#1,length)
    ELSE if encoding$="chunked"
      LINEINPUT #1,ct$
      length=val("0x"+ct$)
      'print length
      WHILE length
        ret$=ret$+input$(#1,length)
        LINEINPUT #1,ct$
        length=val("0x"+ct$)
        ' print length
      WEND
    ELSE
      PRINT encoding$,len(encoding$)
      FOR i=0 TO 10
        LINEINPUT #1,t$
        PRINT "<pre>"+t$+"</pre>"
      NEXT i
    ENDIF
  ELSE
    PRINT "Error: could not get data from the WIKI!"
  ENDIF
  CLOSE #1
  RETURN replace$(ret$,chr$(13),"")
ENDFUNCTION

FUNCTION purify$(g$)
  LOCAL i
  g$=replace$(g$,"+"," ")
  g$=replace$(g$,"%0A"," ")
  g$=replace$(g$,"%0D"," ")
  FOR i=0 TO 255
    g$=replace$(g$,"%"+upper$(hex$(i,2,2)),chr$(i))
  NEXT i
  RETURN g$
ENDFUNCTION
FUNCTION subword$(aa$)
  LOCAL i
  IF anzwords
    FOR i=0 TO anzwords-1
      aa$=replace$(aa$,word$(i),"[["+word$(i)+"]]")
    NEXT i
  ENDIF
  RETURN aa$
ENDFUNCTION
PROCEDURE get_wikiwords
  DIM word$(10000)
  anzwords=0
  LOCAL f$,t$
  f$="/dev/shm/wikiwords.dat"
  IF exist(f$)
    OPEN "I",#1,f$
    WHILE not eof(#1)
      LINEINPUT #1,t$
      word$(anzwords)=t$
      INC anzwords
    WEND
    CLOSE #1
  ENDIF
RETURN
