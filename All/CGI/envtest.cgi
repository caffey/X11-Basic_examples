#!/usr/bin/xbasic
' Test cgi-script with X11-Basic (c) Markus Hoffmann
PRINT "Content-type: text/html"+chr$(13)
PRINT ""+chr$(13)
FLUSH
PRINT "<html><head><TITLE>Test CGI</TITLE><head><body>"
PRINT "<h1>Commandline:</h1>"
i=0
WHILE len(param$(i))
  PRINT str$(i)+": "+param$(i)+"<br>"
  INC i
WEND
PRINT "<h1>Environment:</h1>"
PRINT "<pre>"
FLUSH
SYSTEM "env"
PRINT "</pre>"

PRINT "<h1>Stdin:</h1>"
PRINT "<pre>"

length=val(env$("CONTENT_LENGTH"))
IF length
  FOR i=0 TO length-1
    t$=t$+chr$(inp(-2))
  NEXT i
  PRINT t$
ENDIF
PRINT "</pre>"
PRINT "<FORM METHOD=POST ACTION=/cgi-bin/envtest.cgi>"
PRINT "Name: <INPUT NAME=name><BR>"
PRINT "Email: <INPUT NAME=email><BR>"
PRINT "<INPUT TYPE=submit VALUE="+chr$(34)+"Test POST Method"+chr$(34)+">"
PRINT "</FORM>"
PRINT "<hr><h6>(c) Markus Hoffmann cgi with X11-basic</h6></body></html>"
FLUSH
QUIT
