#!/usr/bin/xbasic
'
'  Web-Interface fuer das Sprach-Synthesizersystem (c) Markus Hoffmann 2006
'
'

host$=""
stimme$()=["","weiblich 1","m&auml;nnlich 2","weiblich 3","m&auml;nnlich 4","weiblich 5","m&auml;nnlich 6","weiblich 7","f&uuml;r bayrisch"]
PRINT "Content-type: text/html"
PRINT
t$=env$("REQUEST_URI")
SPLIT t$,"?",1,a$,t$
SPLIT t$,"?",1,t$,u$
IF len(t$)<2
  x1=-2
  y1=-2
  x2=2
  y2=2
ELSE
  SPLIT t$,"&",1,a$,t$
  WHILE len(a$)
    SPLIT a$,"=",1,a$,b$
    IF a$="v"
      voice$=b$
    ELSE if a$="d"
      dehn$=b$
    ELSE if a$="f"
      freq$=b$
    ELSE if a$="text"
      text$=@purify$(b$)
    ENDIF
    SPLIT t$,"&",1,a$,t$
  WEND
ENDIF
IF len(voice$)=0
  voice$="de3"
ENDIF
IF len(freq$)=0
  freq$="1"
ENDIF
IF len(dehn$)=0
  dehn$="1"
ENDIF
r$=env$("REMOTE_ADDR")
h$=env$("REMOTE_HOST")
OPEN "A",#1,"/tmp/WEBSPEECH.log"
PRINT #1,date$+" "+time$+" "+r$+" "+h$+" "+voice$+" "+text$
CLOSE #1
FLUSH
IF len(text$)
  SYSTEM "export PATH=/usr/bin:/usr/local/bin ; txt2snd -v "+voice$+" -f "+freq$+" -d "+dehn$+" -s "+chr$(34)+text$+chr$(34)+""
  FLUSH
ENDIF

PRINT "<HTML> <HEAD> <TITLE>Markus Hoffmann's Sprachinterface</TITLE></HEAD>"
PRINT "<BODY bgcolor="#ffffff" link=2200aa vlink=008800>"
PRINT "<h6>(c) Markus Hoffmann 2006"
VERSION
PRINT "</h6>"
PRINT "<center><H1> Sprachausgabeinterface "+env$("HTTP_HOST")+":</H1><HR>"
PRINT "Tippen Sie hier bitte einen deutschsprachigen Text ein. Dieser wird dann auf dem Sprachserver ausgegeben"
PRINT "Sie k&ouml;nnen weiterhin die Stimme ausw&auml;hlen, sowie das Sprachtempo und die Tonlage."
PRINT "<p>"
PRINT "<form name=querybox action="+host$+"/cgi-bin/sprachserver.cgi method=get>"
PRINT "Stimme: <select name=v>"
FOR i=1 TO 8
  PRINT "<option value=de"+str$(i);
  IF val(right$(voice$))=i
    PRINT " selected";
  ENDIF
  PRINT ">"+stimme$(i)+"</option>"
NEXT i
PRINT "</select><P>"
PRINT "Tonh&ouml;he: <INPUT  type=text name=f value="+chr$(34)+freq$+chr$(34)+" size=5 maxlength=5><br>"
PRINT "Sprechtempo: <INPUT  type=text name=d value="+chr$(34)+dehn$+chr$(34)+" size=5 maxlength=5><br>"
PRINT "Text=<TEXTAREA name=text  ROWS="5" COLS="70" maxlength=1024 >"
PRINT text$
PRINT "</TEXTAREA><p>"
PRINT "<input value="+chr$(34)+"Text ausgeben"+chr$(34)+" type="+chr$(34)+"submit"+chr$(34)+">"
PRINT "<input type=hidden name=format value=2>"
PRINT "</form><p></center><HR><br>"
PRINT "<I>Kommentare oder Anregungen zu dieser WWW-Seite bitte "
PRINT "<A HREF=mailto:hoffmann@physik.uni-bonn.de>hierhin</A>.</I><P>"
PRINT "<FONT FACE="+chr$(34)+"ARIAL,HELVETICA"+chr$(34)+" SIZE=1>"
PRINT "Erzeugt am "+time$+" "+date$
PRINT "</FONT></BODY></HTML>"
QUIT
FUNCTION purify$(g$)
  LOCAL i
  g$=replace$(g$,"+"," ")
  g$=replace$(g$,"%0A"," ")
  g$=replace$(g$,"%0D"," ")
  FOR i=0 TO 255
    g$=replace$(g$,"%"+upper$(hex$(i,2,2)),chr$(i))
  NEXT i
  RETURN g$
ENDFUNCTION
