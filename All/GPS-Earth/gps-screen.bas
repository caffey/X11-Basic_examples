' gets a screendump from a Garmin GPS-Receiver
' e.g. the Garmin Etrex         (c) Markus Hoffmann 2005
'
'
devicename$="/dev/ttyS1"
outputfilename$="screen.xpm"
CLR interactive,showit
@loadsettings(env$("HOME")+"/.garminrc")
WHILE LEN(PARAM$(i))
  IF LEFT$(PARAM$(i))="-"
    IF param$(i)="--help" OR PARAM$(i)="-h"
      @intro
      @using
    ELSE IF PARAM$(i)="--version"
      @intro
      QUIT
    ELSE IF PARAM$(i)="--interactive"
      interactive=TRUE
    ELSE IF PARAM$(i)="--show" OR PARAM$(i)="-s"
      showit=TRUE
    ELSE IF PARAM$(i)="-o"
      INC i
      IF LEN(PARAM$(i))
        outputfilename$=PARAM$(i)
      ENDIF
    ELSE
      collect$=collect$+PARAM$(i)+" "
    ENDIF
  ELSE
    ' devicename$=param$(i)
    ' IF NOT EXIST(devicename$)
    '   PRINT "gps-screen: "+devicename$+": file or path not found"
    '   quit
    ' ENDIF
  ENDIF
  INC i
WEND

PRINT "Open device ",devicename$
OPEN "UX:9600,N,8,1",#1,devicename$,0

@init_device

fertig=0
@getscreen
totzeit=timer
DO
  PAUSE 0.001
  IF TIMER-totzeit>5
    @sendmessage(10,mki$(5))
    totzeit=TIMER
    fertig=1
  ENDIF
  IF inp?(#1)
    @status(2,"READ")
    WHILE inp?(#1) AND LEN(INKEY$)=0
      @procmessage(@getmessage$())
    WEND
    @status(2,"OK")
    totzeit=TIMER
  ENDIF
  EXIT if fertig=1
LOOP
@ende
PROCEDURE getscreen
  CLR anzscanlines
  @sendmessage(10,MKI$(32))
RETURN

PROCEDURE init_device
  ' Garmin identifizieren
  @sendmessage(254," ")
  @procmessage(@getmessage$())
RETURN
PROCEDURE ende
  ' Quit the program, close all open files remove all installations
  CLOSE
  QUIT
RETURN
FUNCTION getmessage$()
  LOCAL s$,flag,flag2,chk,chk2
  t=TIMER
  CLR s$,flag,flag2
  DO
    IF inp?(#1)
      t$=CHR$(INP(#1))
      ' PRINT "Got:";ASC(t$)
      IF t$=CHR$(16)
        IF flag2=0
          flag2=1
        ELSE
          IF flag=0
            flag=1
          ELSE
            s$=s$+t$
            flag=0
          ENDIF
        ENDIF
      ELSE
        IF flag2=0
          PRINT "protokollfehler: muell ";ASC(t$)
        ELSE
          IF flag=1 AND t$=CHR$(3)
            GOTO t
          ELSE IF flag=1
            PRINT "Protokoll-ERROR 16"
            flag=0
          ENDIF
          s$=s$+t$
        ENDIF
      ENDIF
    ELSE
      PAUSE 0.01
    ENDIF
    IF TIMER-t>2
      @status(1,"TIMEOUT")
      s$="TIMEOUT 0 ?"
      BEEP
      fertig=1
      GOTO t
    ENDIF
  LOOP
  t:
  pid=ASC(MID$(s$,1,1))
  chk=ASC(MID$(s$,LEN(s$),1)) and 255
  chk2=0
  FOR i=1 TO LEN(s$)-1
    chk2=chk2-ASC(MID$(s$,i,1))
  NEXT i
  chk2=chk2 AND 255
  IF chk=chk2 AND pid<>6 AND pid<>21
    @sendACK(pid)
  ELSE IF chk<>chk2
    @status(1,"Uebertragungsfehler CHK")
  ENDIF
  RETURN s$
ENDFUNCTION
PROCEDURE procmessage(t$)
  LOCAL pid,plen
  pid=ASC(LEFT$(t$,1))
  plen=ASC(MID$(t$,2,1))
  IF pid=6
    ' ACK, alles OK, ignorieren
    @status(1,"OK")
  ELSE IF pid=69
    @procscanline(t$)
  ELSE IF pid=21
    @status(1,"NAK !")
  ELSE IF pid=-2 OR pid=10
    PRINT "Paket nicht erlaubt ! Pid="; pid
  ELSE IF pid=-1
    @procproductdata(t$)
  ELSE
    @status(2,"? Paket pid="+STR$(pid)+" !")
    PRINT "? Paket pid="+STR$(pid)+" !  LEN=";plen
    t$=MID$(t$,3,plen)
    FOR i=0 TO LEN(t$)-1
      PRINT hex$(ASC(MID$(t$,i+1,1)) AND 255,2,2,1)'
    NEXT i
    PRINT
  ENDIF
RETURN
PROCEDURE procproductdata(ut$)
  garminPID=CVI(MID$(ut$,3,2))
  garminVER=CVI(MID$(ut$,5,2))
  garminMES$=MID$(t$,7,LEN(ut$)-7-1)
  garminmes$=REPLACE$(garminmes$,CHR$(0)," ")
  PRINT "Connected: "
  PRINT "PID=",garminPID
  PRINT "VER=",garminVER
  PRINT "MES=",garminMES$
RETURN
PROCEDURE procscanline(t$)
  LOCAL plen,bmflag,offset
  plen=ASC(MID$(t$,2,1))
  t$=MID$(t$,3,plen)
  bmflag=LPEEK(VARPTR(t$)+0)
  '  print "BMFLAG=";bmflag
  '  print "PLEN=";plen
  IF bmflag=0
    screenplanes=LPEEK(VARPTR(t$)+12)
    sht=LPEEK(VARPTR(t$)+20)
    screenwidth=LPEEK(VARPTR(t$)+16)
    screenbytes=screenplanes*screenwidth/8*sht
    ' print hex$(cvl(mid$(t$,5,4)))'cvl(mid$(t$,9,4))'
    ' print cvl(mid$(t$,13,4)),cvl(mid$(t$,17,4)),cvl(mid$(t$,21,4))'
    ' print hex$(cvl(mid$(t$,25,4))),cvl(mid$(t$,29,4)),cvl(mid$(t$,33,4))'
    ' print cvl(mid$(t$,37,4))
    PRINT "Planes: ";screenplanes
    PRINT "Dimension: "+STR$(screenwidth)+"x"+STR$(sht)
    PRINT "Bytes: ";screenplanes*screenwidth/8*sht
    escreenbuf$=SPACE$(screenplanes*screenwidth/8*sht)
  ELSE
    offset=cvl(MID$(t$,5,4))
    @progress(sht,anzscanline)
    '      print "OFFSET=";offset
    BMOVE varptr(t$)+8,VARPTR(escreenbuf$)+offset,screenwidth/8*screenplanes
    INC anzscanline
    ' print anzscanline
    IF anzscanline=sht
      ' bsave "escreen",varptr(escreenbuf$),len(escreenbuf$)
      PRINT
      IF showit
        @show_screenshot
      ENDIF
      IF interactive
        @saveescreen
      ELSE
        @savexpm(outputfilename$)
      ENDIF
      fertig=1
    ENDIF
  ENDIF
RETURN
PROCEDURE show_screenshot
  LOCAL x,y,b,a
  sx=0
  sy=0
  sw=640
  sh=400
  CLEARW
  grau=GET_COLOR(65530/2,65530/2,65530/2)
  hellgrau=GET_COLOR(65530/1.5,65530/1.5,65530/1.5)
  hell=GET_COLOR(65530/1.1,65530/1.1,65530/1.1)
  weiss=GET_COLOR(65530,65530,65530)
  schwarz=GET_COLOR(0,0,0)
  rot=GET_COLOR(65530,0,0)

  IF sht>0 AND screenplanes>0 AND screenwidth>0
    DEFMOUSE 2
    FOR x=screenwidth-1 downto 0
      FOR y=0 TO sht-1
        a=ASC(MID$(escreenbuf$,1+x/8*screenplanes+y*screenwidth*screenplanes/8,1)) and 255
        b=x mod 4
        IF btst(a,b*2) AND btst(a,b*2+1)
          COLOR schwarz
        ELSE if btst(a,b*2)=0 AND btst(a,b*2+1)
          COLOR grau
        ELSE if btst(a,b*2) AND btst(a,b*2+1)=0
          COLOR hellgrau
        ELSE
          COLOR weiss
        ENDIF
        PLOT sx+sw-sht+y,sy+sh-x
      NEXT y
      SHOWPAGE
    NEXT x
    GET sx+sw-sht,sy+sh-screenwidth,sht,screenwidth,escreen$
    SHOWPAGE
    DEFMOUSE 0
  ENDIF
RETURN
PROCEDURE saveescreen
  FILESELECT "save screendump ...","./*.xpm","screen.xpm",f$
  IF len(f$)
    IF exist(f$)
      IF form_alert(2,"[1][Datei existiert schon !|Ersetzen ?][Ja|ABBRUCH]")=1
        DEFMOUSE 2
        SHOWPAGE
        @savexpm(f$)
        DEFMOUSE 0
      ENDIF
    ELSE
      DEFMOUSE 2
      SHOWPAGE
      @savexpm(f$)
      DEFMOUSE 0
    ENDIF
  ENDIF
  SHOWPAGE
RETURN

PROCEDURE savexwd(f$)
  BSAVE f$,VARPTR(escreen$),LEN(escreen$)
RETURN
PROCEDURE savexpm(f$)
  PRINT "Save screen as: "+f$
  OPEN "O",#4,f$
  PRINT #4,"/* XPM */"
  PRINT #4,"static char *magick[] = {"
  PRINT #4,"/* columns rows colors chars-per-pixel */"
  PRINT #4,CHR$(34)+STR$(sht)+" "+STR$(screenwidth)+" 4 1"+CHR$(34)+","
  PRINT #4,CHR$(34)+"  c #ffffff"+CHR$(34)+","
  PRINT #4,CHR$(34)+"- c #aaaaaa"+CHR$(34)+","
  PRINT #4,CHR$(34)+"* c #555555"+CHR$(34)+","
  PRINT #4,CHR$(34)+"# c #000000"+CHR$(34)+","
  PRINT #4,"/* pixels      erzeugt von GPS-Earth "+version$+" (garmin.bas) */"
  FOR x=screenwidth-1 downto 0
    PRINT #4,CHR$(34);
    FOR y=0 TO sht-1
      a=ASC(MID$(escreenbuf$,1+x/8*screenplanes+y*screenwidth*screenplanes/8,1)) and 255
      b=x mod 4
      IF btst(a,b*2) AND btst(a,b*2+1)
        PRINT #4,"#";
      ELSE if btst(a,b*2)=0 AND btst(a,b*2+1)
        PRINT #4,"*";
      ELSE if btst(a,b*2) AND btst(a,b*2+1)=0
        PRINT #4,"-";
      ELSE
        PRINT #4," ";
      ENDIF
    NEXT x
    PRINT #4,CHR$(34)+","
  NEXT y
  PRINT #4,"};"
  CLOSE #4
RETURN

PROCEDURE sendACK(pid)
  @sendmessage(6,CHR$(pid))
RETURN

PROCEDURE sendmessage(id,m$)
  LOCAL i,a,chk,s$

  chk=0-id-LEN(m$)
  s$=CHR$(16)+CHR$(id)+CHR$(LEN(m$))
  IF LEN(m$)=16
    s$=s$+CHR$(16)
  ENDIF
  IF LEN(m$)
    FOR i=1 TO LEN(m$)
      a=ASC(MID$(m$,i,1)) and 255
      s$=s$+CHR$(a)
      IF a=16
        s$=s$+CHR$(16)
      ENDIF
      chk=chk-a
    NEXT i
  ENDIF
  chk=chk AND 255
  s$=s$+CHR$(chk)
  IF chk=16
    s$=s$+CHR$(16)
  ENDIF
  s$=s$+CHR$(16)+CHR$(3)
  BPUT #1,VARPTR(s$),LEN(s$)
  FLUSH #1
RETURN

PROCEDURE sendnack
  @sendmessage(21,"")
RETURN
PROCEDURE status(n,ssst$)
  PRINT "STATUS=";n;"  ";ssst$
RETURN

PROCEDURE intro
  PRINT "gps-screen.bas V.1.11 (c) Markus Hoffmann 2004-2005"
  PRINT "grabs a screenshot from a GARMIN GPS device and saves it"
  PRINT "as portable pixmap (*xpm)"
  PRINT
  VERSION
RETURN
PROCEDURE using
  PRINT "Usage: gps-screen [options] [device]"
  PRINT "Options:"
  PRINT "  -h, --help               Display this information"
  PRINT "  --interactive            interactively save screen"
  PRINT "  -s                       show screen"
  PRINT "  --show                   show screen"
  PRINT "  -o <file>                Place the output into <file> (Default: screen.xpm)"
RETURN

' Get the devicename from GPS-Earth-conf file

PROCEDURE loadsettings(f$)
  LOCAL t$,a$,b$
  IF exist(f$)
    OPEN "I",#9,f$
    WHILE not eof(#9)
      LINEINPUT #9,t$
      t$=TRIM$(t$)
      IF left$(t$)<>"#"
        SPLIT t$,"=",1,a$,b$
        IF upper$(a$)="DEVICE"
          devicename$=b$
        ENDIF
      ENDIF
    WEND
    CLOSE #9
  ENDIF
RETURN
PROCEDURE progress(a,b)
  LOCAL t$
  PRINT chr$(13);"[";STRING$(b/a*32,"-");">";STRING$((1.03-b/a)*32,"-");"| ";STR$(INT(b/a*100),3,3);"% ]";
  FLUSH
RETURN

