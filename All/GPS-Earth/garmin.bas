' Programm for comunication with Garmin (etrex) GPS receiver
' (c) Markus Hoffmann 2002-2005

'           Version 1.03     1/2002
' garmin    Version 1.04     1/2003    Markus Hoffmann
'
'           Version 1.05     4/2004    Markus Hoffmann
'           Version 1.06     2/2005    Markus Hoffmann
'
'           (There was a Problem with the serial-device implementation
'           of X11Basic. Fixed in version 1.10-4)
'
' please use X11Basic Version 1.12 or newer
'
'
'           added gpx import    Dec. 2008 Markus Hoffmann
'
'
'
DIM almanac$(35), waypoint$(32000),track$(100000),polygon$(100000)
DIM sym$(256,3)
DIM tracklist$(20),trackl1(20),trackl2(20)
DIM screenmsg$(256)

version$="V.1.05"
CLR anzal,anztrackl,anzpvt,anzwp,anztrack,anzpoly
@init
IF exist("waypoint.wpt")
  @do_loadwp("waypoint.wpt")
ENDIF
IF exist("base.map")
  @do_loadpoly("base.map")
ENDIF
@redraw
'########## Hauptprogramm ##########
'##########     begin     ##########

' initials
@init_device
@showrange
jjj:
@sendmessage(10,CHR$(49)+CHR$(0))   ! PVT aktivieren
MENUDEF menue$(),m
DEFMOUSE 0
GET 0,0,1,1,omary$
' @sendmessage(10,mki$(3))
DO
  MENU
  PAUSE 0.001
  MENU
  MOUSE mx,my,mk
  IF mx>bx AND my>by AND mx<bx+bw AND my<by+bh
    IF mk=1
      PUT @kx(omarx)-5,@ky(omary)-5,omary$
      marx=@ox(mx)
      mary=@oy(my)
      COLOR schwarz
      PBOX 8*89,16*26,100*8,16*28
      COLOR grau
      BOX 8*89,16*26,100*8,16*28
      COLOR gruen
      SETFONT normfont$
      TEXT 8*90,16*27,@breite$(mary)
      TEXT 8*89,16*28,@laenge$(marx)
      GET @kx(marx)-5,@ky(mary)-5,10,10,omary$
      CIRCLE @kx(marx),@ky(mary),4
      LINE @kx(marx)-5,@ky(mary),@kx(marx)+5,@ky(mary)
      LINE @kx(marx),@ky(mary)-5,@kx(marx),@ky(mary)+5
      SHOWPAGE
      ox=mx
      oy=my
      omarx=marx
      omary=mary
      PAUSE 0.05
      ' Finde die drunterliegenden Map_elemente
      FOR i=0 TO anzpoly-1
        t$=polygon$(i)
        l1=cvf(MID$(t$,7,4))
        l2=cvf(MID$(t$,11,4))
        l3=cvf(MID$(t$,15,4))
        l4=cvf(MID$(t$,19,4))
        IF marx>l1 AND marx<l2 AND mary>l3 AND mary<l4
          anz=cvi(MID$(t$,23,2))
          name$=MID$(t$,25+anz*8,LEN(t$)-25-anz*8)
          name$=REPLACE$(name$,CHR$(0),"")
          IF @inpoly(t$,marx,mary)
            PRINT i,name$
          ENDIF
        ENDIF
      NEXT i
      MENU
      MOUSE mx,my,mk
      IF mk
        DEFLINE 0,2,2,2
        GRAPHMODE 3
        REPEAT
          MOTIONEVENT mx,my,x,y,mk
          w=180/pi*atan2(my-oy,mx-ox)
          d=@distance(@ox(ox),@oy(oy),@ox(mx),@oy(my))
          DEFTEXT 1,0.1,0.2,w
          COLOR gruen
          LINE ox,oy,mx,my
          LTEXT ox+0.5*(mx-ox),oy+0.5*(my-oy),STR$(d,4,4)+" km"
          SHOWPAGE
          LINE ox,oy,mx,my
          LTEXT ox+0.5*(mx-ox),oy+0.5*(my-oy),STR$(d,4,4)+" km"

        UNTIL mk=0
        GRAPHMODE 1
      ENDIF
    ELSE if mk=2
      DEFMOUSE 3
      GRAPHMODE 3
      smx=mx
      smy=my
      REPEAT
        MOTIONEVENT mx,my,x,y,mk
        mx=MAX(smx+4,mx)
        my=MAX(smy+4,my)
        BOX smx,smy,mx,my
        SHOWPAGE
        BOX smx,smy,mx,my
      UNTIL mk=0
      xxmin=@ox(smx)
      yymin=@oy(my)
      xxmax=@ox(mx)
      ymax=@oy(smy)
      xmin=xxmin
      ymin=yymin
      xmax=xxmax
      GRAPHMODE 1
      @sort_wp
      @redraw
    ELSE if mk=4
      xmin=xmin-(xmax-xmin)/2
      ymin=ymin-(ymax-ymin)/2
      xmax=xmax+(xmax-xmin)/3
      ymax=ymax+(ymax-ymin)/3
      xmin=MAX(-180,MIN(180,xmin))
      xmax=MAX(-180,MIN(180,xmax))
      ymin=MAX(-90,MIN(90,ymin))
      ymax=MAX(-90,MIN(90,ymax))
      @calc_cutoff
      @redraw
    ENDIF
  ENDIF
  z$=INKEY$
  IF LEN(z$)<>0
    IF UPPER$(LEFT$(z$))="Q"
      @ende
    ELSE IF UPPER$(LEFT$(z$))="C"
      @paper
    ELSE IF UPPER$(LEFT$(z$))="."
      @sonder
    ELSE IF UPPER$(LEFT$(z$))="S"
      @saveal
    ELSE IF UPPER$(LEFT$(z$))="W"
      CLR anzwp
      @sendmessage(10,CHR$(7)+CHR$(0))
    ELSE IF UPPER$(LEFT$(z$))="P"
      @sendmessage(10,CHR$(2)+CHR$(0))
    ELSE IF LEFT$(z$)=CHR$(27)
      @sendmessage(10,CHR$(0)+CHR$(0))
    ELSE IF LEFT$(z$)=CHR$(8) OR LEFT$(z$)=CHR$(127)
      @sendmessage(10,CHR$(8)+CHR$(0))
    ELSE IF UPPER$(LEFT$(z$))="A"
      anzal=0
      @sendmessage(10,CHR$(1)+CHR$(0))
    ELSE IF UPPER$(LEFT$(z$))="N"
      @newwp
    ELSE IF UPPER$(LEFT$(z$))="R"
      @sendmessage(10,CHR$(4)+CHR$(0))
    ELSE IF UPPER$(LEFT$(z$))="I"
      @sendmessage(254," ")
    ELSE IF UPPER$(LEFT$(z$))="K"
      PRINT @qthlocator$(mary,-marx)
    ELSE IF UPPER$(LEFT$(z$))="D"
      @sendmessage(10,CHR$(5)+CHR$(0))
    ELSE IF UPPER$(LEFT$(z$))="T"
      anztrack=0
      @sendmessage(10,CHR$(6)+CHR$(0))
    ELSE IF UPPER$(LEFT$(z$))="L"
      @loadwp
    ELSE
      @status(3,"unknown key: "+STR$(ASC(LEFT$(z$, 1))))
      PRINT "unknown key: "+STR$(ASC(LEFT$(z$, 1)))
    ENDIF
    @showrange
  ENDIF
  MENU
  IF TIMER-totzeit>5
    @sendmessage(10,mki$(5))     ! get time
    totzeit=TIMER
  ENDIF

  IF inp?(#1)
    @status(2,"READ")
    WHILE inp?(#1) AND LEN(INKEY$)=0
      @procmessage(@getmessage$())
    WEND
    @status(2,"OK")
    totzeit=TIMER
    @showrange
  ENDIF
LOOP
@ende

' Initialize Graphic settings and window size etc.

PROCEDURE init
  LOCAL i,j,t$,d,a
  DIM menue$(128)
  DIM dist(10000)
  '
  CLR trackopen
  sx=0
  sy=0
  sw=800
  sh=512
  SIZEW ,sw,sh
  DEFMOUSE 2
  rot=GET_COLOR(65530,0,0)
  rosa=GET_COLOR(65535,32000,32000)
  orange=GET_COLOR(65530,32000,0)
  gelb=GET_COLOR(65530,65535,0)
  landgelb=GET_COLOR(65535,65535,32000)
  grau=GET_COLOR(65530/2,65530/2,65530/2)
  grau2=GET_COLOR(65530/4,65530/4,65530/4)
  hellgrau=GET_COLOR(65530/1.5,65530/1.5,65530/1.5)
  hell=GET_COLOR(65530/1.1,65530/1.1,65530/1.1)
  weiss=GET_COLOR(65530,65530,65530)
  schwarz=GET_COLOR(0,0,0)
  lila=GET_COLOR(65530,0,65530)
  blau=GET_COLOR(10000,10000,65530)
  wasserblau=GET_COLOR(32000,32000,65535)
  hellblau=GET_COLOR(10000,65535,65530)
  gruen=GET_COLOR(0,65535,0)
  wattgruen=GET_COLOR(32000,65535,32000)
  lila=GET_COLOR(65530,0,65530)
  normfont$="-*-fixed-bold-r-normal-*-16-*-iso8859-*"
  mediumfont$="-*-fixed-bold-r-normal-*-13-*-iso8859-*"
  smallfont$="-*-fixed-medium-r-normal-*-10-*-iso8859-*"
  tinyfont$="-*-fixed-medium-r-normal-*-8-*-iso8859-*"
  helveticatinyfont$="-*-helvetica-medium-r-normal-*-8-*-iso8859-*"
  helveticasmallfont$="-*-helvetica-medium-r-normal-*-10-*-iso8859-*"
  helveticamediumfont$="-*-helvetica-bold-r-normal-*-12-*-iso8859-*"
  helveticanormfont$="-*-helvetica-bold-r-normal-*-14-*-iso8859-*"
  SETFONT normfont$
  bx=0
  by=18
  bw=640
  bh=400

  ' Defaults:

  devicename$="/dev/ttyS1"
  masstab=30/100
  xmin=6
  xmax=8
  ymin=50
  ymax=51
  d=@distance(xmin,ymin,xmax,ymin)/bw

  xmax=xmin+(xmax-xmin)/d*masstab
  d=@distance(xmin,ymin,xmax,ymin)/bw
  a=@distance(xmin,ymin,xmin,ymax)/bh
  ymax=ymin+(ymax-ymin)*d/a
  ARRAYFILL screenmsg$(),"UNKNOWN"
  RESTORE screenmessages
  READ a$
  WHILE a$<>"***"
    READ b$
    screenmsg$(VAL("0x"+a$))=b$
    READ a$
  WEND
  FOR j=0 TO 1
    FOR i=0 TO 255
      sym$(i,j)=inline$("D$&$$<$'D$B$$,$$D$'$$8$%T$($$$$$$$$$$$$$$$$$") ! Flag
    NEXT i
  NEXT j
  symname$()=["Anker","Glocke","Diamand","Diamand2",""]
  maenneken$=inline$("$$&$$@$'D$+$$b$+\$CD%b$+\$C$$`$'T$3$$`$'$$$$") ! Man
  sym$(0,0)=inline$("$$$$$,$'D$.$$T$%\$`$$4$%&&*<0c$C$$($$$$$$$$$") ! (G) Marina
  sym$(1,0)=inline$("$$$$$,$%\$CD%b$+\$CD%b$+`$c`3\$%T$2$$4$$$$$$") ! Glocke
  sym$(2,0)=inline$("$$$$$$$$$$'T'TD4%&$^;04G&%$4&&$(4$.$$4$$$$$$") ! Diamand1
  sym$(3,0)=inline$("$$'$%W$<&&$88B52,LDFF%.4,LDFF252))(,,'$<T$@$") ! Diamand2
  sym$(4,0)=inline$("$$$$$$$$$$&$$<$%D$+D%cDCK'J$$<$%D$*$$B$+$$$$") ! Airport
  sym$(5,0)=inline$("9%%8*)4<9%U8+)4<C%D4(%$42%$\('D42%$\('D4$$$$") ! Restaurant
  sym$(6,0)=inline$("$$$$$$$%T$AD'6$-<$+$%T$1,$ID'@$+$$($$$$$$$$$") ! (G) Dollar/Bank
  sym$(7,0)=inline$("$$$$$$$$$$'D,5$V.&T(,$DP('/D,4$$$$$$$$$$$$$$") ! (G) fishing
  sym$(8,0)=inline$("$$$$$2$%0$0T%W$/`$ST&c$/`%3T(c$3$$$$$$$$$$$$") ! (G) gas station
  sym$(9,0)=inline$("D$&$$<$'D$B$$,$$D$'$$8$%T$($$$$$$$$$$$$$$$$$") ! Flag
  sym$(10,0)=inline$("$$$$$$$*\$@4&$D4c'`,(*D:>%<,)DD:b%`$$$$$$$$$") ! (G) residence
  sym$(11,0)=inline$("$$$$$)$58%I4+C$A\%U$*($<\%GD*2$<$$$$$$$$$$$$") ! (G) Restaurant
  sym$(12,0)=inline$("T$+$$6$',$04%M$*X$[,'<D<E%D(0,4TTJ*&<$+$cc`$") ! Baustelle
  sym$(13,0)=inline$("$$$$$+$*F$H,&3$+`%a4-9$I8&94-C$C`$@$$$$$$$$$") ! (G) Bar
  sym$(14,0)=inline$("$$$$$-$7Z'CD'V$-\$cD'P$+D$/$%W$=,$D$$$$$$$$$") ! (G) Totenkopf/Danger
  sym$(15,0)=inline$("T$0T'$D4%&$&4'-06=,UG,*%D<(662-+%&$,('$0T$0$") ! Smiley
  sym$(16,0)=inline$("$$T$)$$N$)2DMH'0T'F$(,$/4$<D&$$$$$$$$$$$$$$$") ! Pin
  sym$(17,0)=inline$("($$43$%*<(1G,=E5Z&MD%_DVW'b*%H-^,EQ6>VTL$$$$") ! Busybee
  sym$(18,0)=inline$("$$$$$$$$$$$$$$$$$$'$$@$%T$($$$$$$$$$$$$$$$$$") ! Point (Waypoint)
  sym$(19,0)=inline$("$$$$$$$$$%$$*($8D%,$(<$,4$7\+T$$$$$$$$$$$$$$") ! (G) Shipwreck
  sym$(20,0)=inline$("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$") ! leer
  sym$(21,0)=inline$("$$$$$$$$$$'D'Z$,L$DD&6$.,$WD'T$$$$$$$$$$$$$$") ! (G) man overboard
  sym$(23,0)=inline$("$$&$$D$%D$2$$H$(4$8D&>$/`%`$$$$$$$$$$$$$$$$$") ! Camp
  sym$(28,0)=inline$("$$$$$$$$b%`,(,D5F%+L)bD;F%*,(4D4b%`$$$$$$$$$") ! Help
  sym$(36,0)=inline$("$$$$$$$$$$$$$$$$$$'$$@$%T$($$$$$$$$$$$$$$$$$") ! Point
  sym$(37,0)=inline$("$$$$$$$$$$`%'`0c][cc]ccccccKC`0c$4`$'\$$$$$$") ! Fisch
  sym$(38,0)=inline$("$$$$$($%4$+T%YD1:$)T$B$'4$A$'9D1`$A$$8$%$$$$") ! Dollar
  sym$(150,0)=inline$("$$$$$%$$P$3T$2$'\$b,3WDBV$D,$TD0b%`$$$$$$$$$") ! (G) Bootsrampe
  sym$(151,0)=inline$("$$$$$$$$D$,$$<$'D$3$%b$2<$UT+3Dc$$$$$$$$$$$$") ! (G) Zelt/Camping
  sym$(152,0)=inline$("$$$$$'$00$T$$3TBQ&ZX'/4B0'`T''$0$$$$$$$$$$$$") ! (G) Restroom
  sym$(153,0)=inline$("$$$$$$$'D$@$$$$$($04&V$4T%c$'\$+D$@$$$$$$$$$") ! (G) Shower
  sym$(154,0)=inline$("$$$$$,$'$$+$+b$C<$$,$CD%&$(,$=$$`$$$$$$$$$$$") ! (G) Drinking Water
  sym$(155,0)=inline$("$$$$$$$$b%`@2)T^T$1D%O$1R%Y\+SDC$$$$$$$$$$$$") ! (G) Telephone
  sym$(156,0)=inline$("$$$$$3DC&%&,(<D5^%CL)\D5F%(,(3DC$$$$$$$$$$$$") ! (G) medical facility
  sym$(157,0)=inline$("$$$$$3DC&%',(ZD:&%>,(\D5&%&,(4D4b%`$$$$$$$$$") ! (G) Information (?)
  sym$(158,0)=inline$("$$$$$3Dc&&',,\DHF&6,,\DDF&',,4DDb'`$$$$$$$$$") ! (G) Parking Area
  sym$(159,0)=inline$("$$$$$$$%D$-$%&$,4$4D&%$4,$F4([Da$$($$$$$$$$$") ! (G) Park
  sym$(160,0)=inline$("$$$$$$$$`%b$$L$&:'7T+X$(,$DD&$$$$$$$$$$$$$$$") ! (G) Picnic area
  sym$(161,0)=inline$("$$$$$$$$G$B$%3Tcc'U`2[T_c'W`3T$$$$$$$$$$$$$$") ! (G) scenic area
  sym$(162,0)=inline$("$$$$$'$$0$$$$3$',$%D$($$T$&$(3D3$$$$$$$$$$$$") ! (G) Skiing Area
  sym$(163,0)=inline$("$$$$$$$$D$'D*5$?T$3D%U$5N&M$%$$$$$$$$$$$$$$$") ! (G) Swimming Area
  sym$(164,0)=inline$("$$$$$$$$N$+8$<$%N$+8$<$%N%;8.T$$$$$$$$$$$$$$") ! (G) Dam
  sym$(166,0)=inline$("T$+$$6$',$04%M$*X$[,'<D<E%D(0,4TTJ*&<$+$cc`$") ! Baustelle
  sym$(169,0)=inline$("$$$$$$$$T$@D&5$5H%%T+%$6(%(D&@$+$$$$$$$$$$$$") ! (G) Fussball/Park
  sym$(170,0)=inline$("$$$$$$$$$$&$$X$(b%c`3`TW0$TT'$$$$$$$$$$$$$$$") ! (G) Car
  sym$(171,0)=inline$("$$$$$$$$4$'$$+$$`%'$+`$CT%G$*,$4$$$$$$$$$$$$") ! (G) hunting area
  sym$(172,0)=inline$("$$$$$$$$$%GT+Y$)8$94%C$+$$7T'W$0$$$$$$$$$$$$") ! (G) Shopping Center
  sym$(173,0)=inline$("$$$$$,$%<$<<*$4D)&$X.34S)&D(,3Tc$$$$$$$$$$$$") ! (G) hotel
  sym$(174,0)=inline$("$$$$$%T$C$c`*+T\B&W<-L$GD$3$%J$00%D$$$$$$$$$") ! (G) Mine
  sym$(175,0)=inline$("$$$$$0$&T$>$'<$/X$1T%]$+H$Z4'A$<X%D$$$$$$$$$") ! (G) Trail Head
  sym$(176,0)=inline$("$$$$$%$4X%@T*'$<`%aD'+$@8%7\3W$<$$$$$$$$$$$$") ! (G) Truck Stop
  sym$(178,0)=inline$("D$&$$<$'D$B$$,$$D$'$$8$%T$($$$$$$$$$$$$$$$$$") ! Flag
  sym$(179,0)=inline$("D$*$$8$'T$.D%.$(H$E4&(D46%%(,&4D,H$FT'1bc$($") ! Zelt
  sym$(180,0)=inline$("$$$$$$$$$$$$$2$C,%$4(3TccKcJ=UD<$$$$$$$$$$$$") ! Car
  sym$(181,0)=inline$("$$&$$<$%D$+$$`$'`$c$$c$3b%cT'cTcD$*$$<$%$$$$") ! Baum
  sym$(182,0)=inline$("$$$$$$$$$$$$$$$$ccb5FCcc\$@\+$=D$H$'T$*$$<$$") ! Bruecke
  sym$(183,0)=inline$("$$$$$$$$$$&$$<$%D$+D%cDCK'J$$<$%D$*$$B$+$$$$") ! Airport
  sym$(184,0)=inline$("$$$$$$$$\$(D$6$'\$;D'B$1\$[D'B$)\$($$$$$$$$$") ! Tanke
  sym$(185,0)=inline$("$$$$$$$$$$&$$($%D$'D$\$$H$7D$\$$$$$$$$$$$$$$") ! Anker
  sym$(186,0)=inline$("<$&8$$$%(D.,'\$3\'cTCc%cb3c\ac%[`+SD3`$C$$@$") ! Bombe
  sym$(187,0)=inline$("c[`%4$)$`8+U4W)*08<U5S)'`8(U4')$08$U4$)$c[`$") ! Parkplatz
  sym$(188,0)=inline$("$$'$$W$0'''ccTD4&%$,(,D5F%+\+T$$$$$$$$$$$$$$") ! Haus
  sym$(189,0)=inline$("$$$$$$$$0$&Tc_>$QL&ZD3cccccK]UD<*%D$$$$$$$$$") ! Train
  sym$(190,0)=inline$("\$@4&$D4%&$&43JV5A9)Q9J98=91HD-$%&$,(%$,\$@$") ! STOP
  sym$(191,0)=inline$("$$$$$3$3*%E<*P$'<$>T'?$1@$_T'T$$$$$$$$$$$$$$") ! Telefon

  ' Einer Reihe
  sym$(4,1)=inline$("$$$$$$$$$$4$'H$B\%_T+3$0@$UD$*$$$$$$$$$$$$$$") ! (G) Trackback Point
  sym$(5,1)=inline$("$$$$$$$%$$0$%T$'$$($$@$+0%H,,7$<T$@$$$$$$$$$") ! (G) golf course
  sym$(6,1)=inline$("$$$$$$$$$$$$$$$$D$+$$`$'D$($$$$$$$$$$$$$$$$$") ! Point2
  sym$(7,1)=inline$("$$$$$$$$$$$$$,$%T$3D%b$+T$2$$4$$$$$$$$$$$$$$") ! Point3
  sym$(8,1)=inline$("$$$$$$$$$$&$$B$+\$AT'K$2\$CD%\$%$$$$$$$$$$$$") ! Point4
  ' sym$(6,1)=inline$("$$$$$$$$$$$$$$$$D$2$$L$'$$$$$$$$$$$$$$$$$$$$") ! (G) City (small)
  ' sym$(7,1)=inline$("$$$$$$$$$$$$$,$'4$5$%($(D$0$$$$$$$$$$$$$$$$$") ! (G) City (medium)
  ' sym$(8,1)=inline$("$$$$$$$$$$&$$X$(,$DD&&$,4$6$$T$$$$$$$$$$$$$$") ! (G) City (large)
  sym$(15,1)=inline$("$$$$$$D$H$1D%3DCc'c00W$0P$Z$$<$%c'`$$$$$$$$$") ! (G) Car repair
  sym$(16,1)=inline$("$$$$$2$%($/\-U$6\%($3$$H$&4$-$$<$$$$$$$$$$$$") ! (G) Fast Food
  sym$(17,1)=inline$("$$$00$TTc'`P1.TYL$;D%`$'D$+D$6$%,$`$$$$$$$$$") ! (G) Fitness Center
  sym$(18,1)=inline$("$$$$$,$%T'3$B\%]V'C\'cD3V$c$'c$c$$$$$$$$$$$$") ! (G) Movie Theater
  sym$(22,1)=inline$("$$$$$3Uc%($(:$5<%('H5T5$])`(43Uc$$$$$$$$$$$$") ! (G) Post office
  sym$(23,1)=inline$("$$$$$$$$`+bL:^EJM)R@4L5&c+`<1D$$$$$$$$$$$$$$") ! (G) RV Park
  sym$(24,1)=inline$("$$$$$$$%D$-$%($(,$DD&%$4&&'\3\$'$$($$$$$$$$$") ! (G) school
  sym$(28,1)=inline$("$$$$$3$%*$0<%UD/b$R\&UD/R%3\(cD3$$$$$$$$$$$$") ! (G) Convienience STore
  sym$(29,1)=inline$("$$$$$3T%%$)8CT5%9)9X445Ac)8$44%c$$$$$$$$$$$$") ! (G) Theater
  sym$(34,1)=inline$("$$$$$$$$$%V$([$M((8L284%2$*$$b$3$$$$$$$$$$$$") ! (G) scales
  sym$(35,1)=inline$("$$$$$$$$D$1$%&$-H%14(=$7,$I$%,$'$$$$$$$$$$$$") ! (G) Toll Booth
  sym$(41,1)=inline$("$$$$$$$%$$*$%($&,%F<%($&$$*4$,$$$$$$$$$$$$$$") ! (G) Bruecke
  sym$(42,1)=inline$("$$$$$$$$`%`4()$9(%%4)5$48%84(4$$$$$$$$$$$$$$") ! (G) Building
  sym$(43,1)=inline$("$$$$$$$$$$&$$X$(,$JD&V$-,$HD&$$$$$$$$$$$$$$$") ! (G) Friedhof
  sym$(44,1)=inline$("$$$$$$$%D$0$$4$%D$1$%&$,(%(4(C$C$$$$$$$$$$$$") ! (G) Kirche
  sym$(45,1)=inline$("$$$$$3D%&'`,,3DD&'`,,3DDF'`,$$D$$$$$$$$$$$$$") ! (G) Civil
  sym$(46,1)=inline$("$$$$$$$$$$,$$D$%D%E$%'$&$$*$$,$$$$$$$$$$$$$$") ! (G) Crossing
  sym$(47,1)=inline$("$$$$$$$+D$E$(($94%$D&>$.,$H4%)$)L$,$$$$$$$$$") ! (G) ghost town
  sym$(48,1)=inline$("$$$$$$$$L$=4%T$*L$]4'T$2L%]4+T$$$$$$$$$$$$$$") ! (G) Levee
  sym$(49,1)=inline$("$$$$$$$$$$&$%cTCT$a4)?%N8%8$$$$$$$$$$$$$$$$$") ! (G) military
  sym$(50,1)=inline$("$$$$$$$%T$B$$L$'D$-$%@$*T$<D&>$.<$T$$$$$$$$$") ! (G) oil field
  sym$(51,1)=inline$("$$$$$$$$D$3D'b$3`%aT+7$<(%($$$$%$$$$$$$$$$$$") ! (G) Tunnel
  sym$(52,1)=inline$("$$$$$1D$=$*4$,D$,$$D'E$C(%`,$3Tc$$$$$$$$$$$$") ! (G) Beach
  sym$(53,1)=inline$("$$$$$2$$($\,(1DY,&N4)G$)<$2$%,$(D$$$$$$$$$$$") ! (G) Forest
  sym$(54,1)=inline$("$$$$$$$$$$&$$($)4$ND)I$-$$$$$$$$$$$$$$$$$$$$") ! (G) Summit

  ' Zweier Reihe (Flug Symbole)

  sym$(0,2)=inline$("$$$$$$$$$$($$4$%D$3D'c$C$$($$<$'$$$$$$$$$$$$") ! (G) Flugzeug/Airport
  sym$(4,2)=inline$("$$$$$$$$T$@D&-$6H%.4(]$6H%,D&0$+$$$$$$$$$$$$") ! (G) heliportsym$(4,2)=inline$("$$$$$$$$T$@D&-$6H%.4(]$6H%,D&0$+$$$$$$$$$$$$") ! (G) heliport
  sym$(5,2)=inline$("$$$$$$$$T$@D&-$5H%.4(=$6H%,D&0$+$$$$$$$$$$$$") ! (G) private Field
  sym$(6,2)=inline$("$$$$$$$$T$@D&%$4(%$4(%$4(%$D&0$+$$$$$$$$$$$$") ! (G) Soft field
  sym$(7,2)=inline$("$$$$$$$%$$($$4$%$$($$<$'D$3$%J$00%H$$$$$$$$$") ! (G) tall tower
  sym$(8,2)=inline$("$$$$$$$$$$$$$$$%D$3$%J$00%H$$$$$$$$$$$$$$$$$") ! (G) short tower
  sym$(9,2)=inline$("$$$$$%$$*$$@,'$T<%G$',$+D$3$%[$0+%D$$$$$$$$$") ! (G) glider area
  sym$(10,2)=inline$("$$$$$$$T$'T$3cTCc$C\$@$'T$A$$T$&$$$$$$$$$$$$") ! (G) ultralight area
  sym$(11,2)=inline$("$$$$$,$'<$T4($DDb'b4(N$.4$:$$\$&D$0$$$$$$$$$") ! (G) parachute area
  sym$(18,2)=inline$("$$$$$,$'D$.$$T$%\$`$$4$%&&*<0c$C$$($$$$$$$$$") ! (G) Seaplane base

  COLOR grau
  PBOX sx,sy,sw,sh
  COLOR blau
  TEXT 4*8,28*16,"ACTION:"
  TEXT 4*8,29*16,"STATUS:"
  TEXT 4*8,30*16,"RESULT:"

  RESTORE menudata
  FOR i=0 TO 127
    READ t$
    menue$(i)=t$
    EXIT IF menue$(i)="***"
  NEXT i
  menue$(i)=""
  menue$(i+1)=""

  @loadsettings(env$("HOME")+"/.garminrc")
  @legende
  @hscalerbar(sx+670,sy+170,100)
RETURN
PROCEDURE init_device
  IF not EXIST(devicename$)
    ~form_alert(1,"[1][Cannot find the serial device|"+devicename$+"][ QUIT |CONTINUE]")
    OPEN "O",#1,"/tmp/delme"
  ELSE
    OPEN "UX:9600,N,8,1",#1,devicename$
  ENDIF
  @garmincancel
  WHILE inp?(#1)
    ~inp(#1)
  WEND
  ' Garmin identifizieren
  @sendmessage(254," ")
  @procmessage(@getmessage$())
  @procmessage(@getmessage$())
RETURN

PROCEDURE laengentreu
  LOCAL a,d
  DEFMOUSE 2
  SHOWPAGE
  d=@distance(xmin,ymin,xmax,ymin)/bw
  a=@distance(xmin,ymin,xmin,ymax)/bh
  PRINT d,a
  ymax=ymin+(ymax-ymin)*d/a
  @sort_wp
  @redraw
  DEFMOUSE 0
RETURN
PROCEDURE loadsettings(f$)
  LOCAL t$,a$,b$
  IF exist(f$)
    OPEN "I",#9,f$
    WHILE not eof(#9)
      LINEINPUT #9,t$
      t$=TRIM$(t$)
      IF left$(t$)<>"#"
        SPLIT t$,"=",1,a$,b$
        IF upper$(a$)="DEVICE"
          devicename$=b$
        ELSE if UPPER$(a$)="XRANGE"
          b$=LEFT$(b$,LEN(b$)-1)
          b$=RIGHT$(b$,LEN(b$)-1)
          SPLIT b$,":",1,a$,b$
          xmin=VAL(a$)
          xmax=VAL(b$)
        ELSE if UPPER$(a$)="YRANGE"
          b$=LEFT$(b$,LEN(b$)-1)
          b$=RIGHT$(b$,LEN(b$)-1)
          SPLIT b$,":",1,a$,b$
          ymin=VAL(a$)
          ymax=VAL(b$)
        ENDIF
      ENDIF
    WEND
    CLOSE #9
  ENDIF
RETURN
PROCEDURE savesettings
  OPEN "O",#9,env$("HOME")+"/.garminrc"
  PRINT #9,"# Settings for garmin.bas (c) Markus Hoffmann"
  PRINT #9,"# "+date$+" "+time$
  PRINT #9,"DEVICE=";devicename$
  PRINT #9,"XRANGE=[";xmin;":";xmax;"]"
  PRINT #9,"YRANGE=[";ymin;":";ymax;"]"
  CLOSE #9
RETURN

PROCEDURE m(k)
  PRINT "MENU: #";k
  ON k gosub info
  ON k-10 gosub newall,n,loadtrack,loadwp,13,loadpoly,loadal,n,TWPinfo,n,exporttrack,exportwp,exportmap,exportgpx,importtrack,importwp,importmap,importgpx,n,savetrack,savewp,savemap,saveal,saveescreen,n,preferences,savesettings,20,ende
  ON k-41 gosub newwp,placewp,editwp,deletewp,deleteallwp,n,drawtrack,optimizetrack,tracktopoly,deletetrack,deletealltrack,n,editpoly,deletepoly,deletemap
  ON k-57 gosub 23,getal,getwp,gettrack,getroute,28,garminpos,garmintime,getscreen,33,sendal,sendwp,sendtrack,n,garmincancel
  ON k-75 gosub garminpvton,garminpvtoff,lichtan,lichtaus,asyncan,asyncaus,n,garminoff
  ON k-85 gosub laengentreu,savegrafik,savefig,41,hoehenprofil,sonder,gotowp,gotopoly
RETURN

PROCEDURE info
  ~form_alert(1,"[0][**** GARMIN.BAS *****(X11-Basic)****|Frontend for a GARMIN GPS receiver.|Connected is a:||"+garminmes$+"|Product-ID="+str$(garminpid)+", Version="+str$(garminver)+"||(c) Markus Hoffmann 2002-2005][ OK ]")
RETURN

PROCEDURE newall
  CLR anzwp,anztrack,anzal,anztrackl
  CLOSE
  RUN
RETURN

' Commands:
' 10 0    Cancel
' 10 1    Get Almanac
' 10 2    Get Position
' 10 3    Get Proximity Wpt  (*)not supported
' 10 4    Get Route
' 10 5    Get Time
' 10 6    Get Tracks
' 10 7    Get Waypoints
' 10 8    Swich off
' 11 11   et Tone=MSG+KEY then power off. (*)not supported
' 10 32   Get Screendump
' 10 49   PVT on
' 10 50   PVT off

PROCEDURE getroute
  @sendmessage(10,CHR$(4)+CHR$(0))
RETURN
PROCEDURE getal
  CLR anzal
  @sendmessage(10,CHR$(1)+CHR$(0))
RETURN
PROCEDURE getwp
  CLR anzwp
  @sendmessage(10,CHR$(7)+CHR$(0))
RETURN
PROCEDURE gettrack
  CLR anztrack
  @sendmessage(10,CHR$(6)+CHR$(0))
RETURN
PROCEDURE getscreen
  CLR anzscanlines
  @sendmessage(10,MKI$(32))
RETURN
PROCEDURE garmincancel
  @sendmessage(10,mki$(0))
RETURN
PROCEDURE garminoff
  @sendmessage(10,CHR$(8)+CHR$(0))
RETURN
PROCEDURE garminpos
  @sendmessage(10,CHR$(2)+CHR$(0))
RETURN
PROCEDURE garmintime
  @sendmessage(10,CHR$(5)+CHR$(0))
RETURN
PROCEDURE garminpvton
  ' PVT aktivieren
  @sendmessage(10,CHR$(49)+CHR$(0))
RETURN
PROCEDURE garminpvtoff
  ' PVT deaktivieren
  @sendmessage(10,CHR$(50)+CHR$(0))
RETURN

PROCEDURE ende
  ' Quit the program, close all open files remove all installations
  CLOSE
  QUIT
RETURN
PROCEDURE TWPinfo
  LOCAL t$
  DEFMOUSE 2
  SHOWPAGE
  @calc_trackinfo
  t$=STR$(anzwp)+" waypoints and "+STR$(anztrack)+" trackpoints."
  IF track_len>2000
    t$=t$+"||Length:   "+STR$(INT(track_len/100)/10)+" km"
  ELSE
    t$=t$+"||Length:   "+STR$(INT(track_len))+" m"
  ENDIF
  t$=t$+"|Zeitspanne: "+@jdate$(track_adate)+" - "+@jdate$(track_bdate)
  t$=t$+"|Weg-Zeit: "+STR$(INT(track_zeit/3600),2,2,1)+":"+STR$(INT((track_zeit MOD 3600)/60),2,2,1)+":"+STR$(INT(track_zeit MOD 60),2,2,1)
  t$=t$+"|Max.   Geschw.: "+STR$(INT(max_v*10)/10)+" km/h"
  IF track_zeit
    t$=t$+"|mittl. Geschw.: "+STR$(INT(track_len/track_zeit*3.6*10)/10)+" km/h"
  ENDIF
  t$=t$+"|H�hen: "+STR$(INT(track_altmin))+" m - "+STR$(INT(track_altmax))+" m"
  DEFMOUSE 0
  ~form_alert(1,"[0]["+t$+"][ OK ]")
RETURN
FUNCTION jdate$(n)
  LOCAL t$,tagzeit
  tagzeit=n mod (24*3600)
  t$=@days2date$(INT(n/(24*3600)))
  t$=t$+" "+STR$(INT(tagzeit/3600),2,2,1)+":"+STR$(INT((tagzeit MOD 3600)/60),2,2,1)+":"+STR$(INT(tagzeit MOD 60),2,2,1)
  RETURN t$
ENDFUNC
FUNCTION days2date$(days)
  !    2447892      Julian date for 00:00 12/31/89
  RETURN juldate$(2447892+days)
ENDFUNCTION
FUNCTION getmessage$()
  t=TIMER
  s$=""
  flag=0
  flag2=0

  DO
    IF inp?(#1)
      t$=CHR$(INP(#1))
      ' PRINT "Got:";ASC(t$)
      IF t$=CHR$(16)
        IF flag2=0
          flag2=1
        ELSE
          IF flag=0
            flag=1
          ELSE
            s$=s$+t$
            flag=0
          ENDIF
        ENDIF
      ELSE
        IF flag2=0
          PRINT t$;
          FLUSH
          @status(1,"ERROR/SKIP")
        ELSE
          IF flag=1 AND t$=CHR$(3)
            GOTO t
          ELSE IF flag=1
            @status(1,"ERROR 16")
            flag=0
          ENDIF
          s$=s$+t$
        ENDIF
      ENDIF
    ELSE
      PAUSE 0.01
    ENDIF
    IF TIMER-t>2
      @status(1,"TIMEOUT")
      s$=""
      GOTO t
    ENDIF
  LOOP
  t:
  pid=ASC(MID$(s$,1,1))
  chk=ASC(MID$(s$,LEN(s$),1)) and 255
  chk2=0
  FOR i=1 TO LEN(s$)-1
    chk2=chk2-ASC(MID$(s$,i,1))
  NEXT i
  chk2=chk2 AND 255
  IF chk=chk2 AND pid<>6 AND pid<>21
    @sendACK(pid)
  ELSE IF chk<>chk2
    @status(1,"CHK-SUM ERROR")
  ENDIF
  RETURN s$
ENDFUNCTION
FUNCTION kx(dux)
  RETURN bx+(dux-xmin)/(xmax-xmin)*bw
ENDFUNC
FUNCTION ky(duy)
  RETURN by+bh-(duy-ymin)/(ymax-ymin)*bh
ENDFUNC

FUNCTION ox(dux)
  RETURN (dux-bx)*(xmax-xmin)/bw+xmin
ENDFUNCTION

FUNCTION oy(duy)
  RETURN -(duy-by-bh)*(ymax-ymin)/bh+ymin
ENDFUNCTION

PROCEDURE loadwp
  LOCAL dlen,f$

  FILESELECT "load waypoints ...","./*.wpt","waypoint.wpt",f$
  IF len(f$)
    IF exist(f$)
      IF anzwp
        IF form_alert(1,"[2][Daten an bestehende Daten anf�gen|oder ersetzen ?][anf�gen|ersetzen]")=2
          CLR anzwp
        ENDIF
      ENDIF
      DEFMOUSE 2
      @do_loadwp(f$)
      @status(3,STR$(anzwp)+"WP geladen")
      @redraw
      @showrange
      DEFMOUSE 0
    ELSE
      ~form_alert(1,"[3][File "+f$+"|not found !][ OH ]")
    ENDIF
  ENDIF
RETURN
PROCEDURE placewp
  LOCAL k,x,y,alt,ox,oy,depth,symbl,name$,comment$,subclass$,cc$,state$
  DEFMOUSE 3
  SHOWPAGE
  COLOR schwarz
  PAUSE 0.3
  WHILE k=0
    MOUSEEVENT ox,oy,k
  WEND
  CIRCLE ox,oy,2
  SHOWPAGE
  depth=1e25
  alt=1e25
  symbl=16    ! Pin
  comment$="WP placed by GPS-Earth"
  name$="new "+STR$(anzwp)
  subclass$=mki$(0)+mkl$(0)+mkl$(-1)+mkl$(-1)+mkl$(-1)
  cc$="  "
  state$="  "
  x=@ox(ox)/180*2^31
  y=@oy(oy)/180*2^31
  PRINT x,y
  waypoint$(anzwp)=CHR$(0)+CHR$(255)+CHR$(0)+CHR$(0x60)+mki$(symbl)+subclass$+mkl$(y)+mkl$(x)+mkf$(alt)+mkf$(depth)+mkf$(dist)+state$+cc$+name$+CHR$(0)+comment$+mkl$(0)+CHR$(0)
  INC anzwp
  @sort_wp
  @redraw
  @showrange
  DEFMOUSE 0
RETURN
PROCEDURE exportgpx
  LOCAL f$,i,t$,x,y,alt,n$,b$,depth,name$,comment$,facility$,city$,addr$,crossr$
  LOCAL ers
  ers=1
  IF anzwp OR anztrack
    FILESELECT "export GPX geodata ...","./*.lst","a.gpx",f$
    IF len(f$)
      IF exist(f$)
        ers=form_alert(3,"[1][File already exists! |replace ?][replace|CANCEL]")=1
      ENDIF
      IF ers
        DEFMOUSE 2
        SHOWPAGE
        OPEN "O",#2,f$
        PRINT #2,"<?xml version='1.0' encoding='ISO-8859-1' standalone='yes'?>"
        PRINT #2,"<gpx version='1.1' creator='GPS-Earth' xmlns='http://www.topografix.com/GPX/1/1' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:schemaLocation='http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd'>"
        IF anzwp
          FOR i=0 TO anzwp-1
            t$="  "+waypoint$(i)
            name$=MID$(t$,51,LEN(t$)-51)
            name$=REPLACE$(name$,CHR$(0),"|")+"|"
            SPLIT name$,"|",0,name$,comment$
            SPLIT comment$,"|",0,comment$,facility$
            SPLIT facility$,"|",0,facility$,city$
            SPLIT city$,"|",0,city$,addr$
            SPLIT addr$,"|",0,addr$,crossr$
            IF crossr$="|"
              crossr$=""
            ENDIF
            y=180/2^31*CVL(MID$(t$,27,4))
            x=180/2^31*CVL(MID$(t$,31,4))
            alt=cvf(MID$(t$,35,4))
            depth=cvf(MID$(t$,39,4))
            dist=cvf(MID$(t$,43,4))
            color=ASC(MID$(t$,4,1))
            attr=ASC(MID$(t$,6,1))
            class=ASC(MID$(t$,3,1))
            state$=MID$(t$,47,2)
            cc$=MID$(t$,49,2)
            displ=ASC(MID$(t$,5,1))
            subclass$=HEX$(cvi(MID$(t$,9,2)),4,4,1)+HEX$(cvl(MID$(t$,11,4)),8,8,1)+HEX$(cvl(MID$(t$,15,4)),8,8,1)+HEX$(cvl(MID$(t$,19,4)),8,8,1)+HEX$(cvl(MID$(t$,23,4)),8,8,1)
            PRINT #2,"<wpt lat='";y;"' lon='";x;"'>";
            PRINT #2,"<name>";name$;"</name>";
            '        print #2," Symbl=0x";hex$(CVI(mid$(t$,7,2)),4,4,1);
            IF alt<9e24
              PRINT #2,"<ele>";alt;"</ele>";
            ENDIF
            IF depth<9e24
              PRINT #2," depth=";depth;
            ENDIF
            IF dist<9e24
              PRINT #2," dist=";dist;
            ENDIF
            IF class
              PRINT #2," Class=";class;
            ENDIF
            IF upper$(subclass$)<>"000000000000FFFFFFFFFFFFFFFFFFFFFFFF"
              PRINT #2," Subclass=";subclass$;
            ENDIF
            IF color<>-1
              PRINT #2," Color=";color;
            ENDIF
            IF displ
              PRINT #2," displ=";displ;
            ENDIF
            IF attr<>6*16
              PRINT #2," attr=";attr;
            ENDIF
            IF state$<>"  "
              PRINT #2," State=";CHR$(34);state$;CHR$(34);
            ENDIF
            IF cc$<>"  "
              PRINT #2," Cc=";CHR$(34);cc$;CHR$(34);
            ENDIF
            IF len(comment$)
              PRINT #2," Comment=";CHR$(34);comment$;CHR$(34);
            ENDIF
            IF len(facility$)
              PRINT #2," Facility=";CHR$(34);facility$;CHR$(34);
            ENDIF
            IF len(city$)
              PRINT #2," City=";CHR$(34);city$;CHR$(34);
            ENDIF
            IF len(addr$)
              PRINT #2," Addr=";CHR$(34);addr$;CHR$(34);
            ENDIF
            IF len(crossr$)
              PRINT #2," Crossr=";CHR$(34);crossr$;CHR$(34);
            ENDIF
            PRINT #2,"</wpt>"
          NEXT i
        ENDIF
        IF anztrack
          IF anztrackl
            FOR j=0 TO anztrackl-1
              PRINT #2,"<trk><name>";tracklist$(j);"</name><number>";j;"</number>"
              PRINT #2,"<trkseg>"
              FOR i=trackl1(j) TO trackl2(j)
                t$=track$(i)
                y=180/2^31*CVL(MID$(t$,1,4))
                x=180/2^31*CVL(MID$(t$,5,4))
                date=CVL(MID$(t$,9,4))
                alt=CVf(MID$(t$,13,4))
                depth=CVf(MID$(t$,17,4))
                new=ASC(MID$(t$,21,1))
                dist=@distance(ox,oy,x,y)*1000
                IF new
                  PRINT #2,"</trkseg><trkseg>"
                  CLR vv,vh,vx,vy,vz
                ENDIF
                PRINT #2,"  <trkpt lat='";y;"' lon='";x;"'>";
                IF alt<9e24
                  PRINT #2,"<ele>";alt;"</ele>";
                ENDIF
                IF date<>-1
                  ' print #2," date=";date;
                ENDIF
                PRINT #2,"</trkpt>"
              NEXT i
              PRINT #2,"</trkseg>"
              PRINT #2,"</trk>"
            NEXT j
          ELSE
            PRINT #2,"<trk>"
            PRINT #2,"<trkseg>"
            FOR i=0 TO anztrack-1
              t$=track$(i)
              y=180/2^31*CVL(MID$(t$,1,4))
              x=180/2^31*CVL(MID$(t$,5,4))
              date=CVL(MID$(t$,9,4))
              alt=CVf(MID$(t$,13,4))
              depth=CVf(MID$(t$,17,4))
              new=ASC(MID$(t$,21,1))
              dist=@distance(ox,oy,x,y)*1000
              IF new
                PRINT #2,"</trkseg><trkseg>"
                CLR vv,vh,vx,vy,vz
              ENDIF
              odate=date
              ox=x
              oy=y
              oalt=alt
              PRINT #2,"  <trkpt lat='";y;"' lon='";x;"'>";
              IF alt<9e24
                PRINT #2,"<ele>";alt;"</ele>";
              ENDIF
              IF date<>-1
                '  print #2," date=";date;
              ENDIF

              PRINT #2,"</trkpt>"
            NEXT i
            PRINT #2,"</trkseg>"
            PRINT #2,"</trk>"
          ENDIF
        ENDIF
        PRINT #2,"</gpx>"
        CLOSE #2
        DEFMOUSE 0
        SHOWPAGE
      ENDIF
    ENDIF
  ELSE
    ~form_alert(1,"[3][Keine Geodaten vorhanden!][ OH ]")
  ENDIF
RETURN
PROCEDURE exportwp
  LOCAL ers,f$,i,t$,x,y,alt,n$,b$,depth,name$,comment$,facility$,city$,addr$,crossr$
  ers=2
  IF anzwp
    FILESELECT "export waypoints ...","./*.lst","waypoint.lst",f$
    IF len(f$)
      IF exist(f$)
        ers=form_alert(3,"[1][File already exists! |append or replace ?][append|replace|CANCEL]")=2
      ENDIF
      IF ers<3
        DEFMOUSE 2
        SHOWPAGE
        IF ers=1
          OPEN "A",#2,f$
        ELSE
          OPEN "O",#2,f$
        ENDIF
        PRINT #2,"# Waypoints from garmin.bas (c) Markus Hoffmann"
        PRINT #2,"# ";anzwp;" waypoints. "+date$+" "+time$
        PRINT #2,"# Format=0 "
        FOR i=0 TO anzwp-1
          t$="  "+waypoint$(i)
          name$=MID$(t$,51,LEN(t$)-51)
          name$=REPLACE$(name$,CHR$(0),"|")+"|"
          SPLIT name$,"|",0,name$,comment$
          SPLIT comment$,"|",0,comment$,facility$
          SPLIT facility$,"|",0,facility$,city$
          SPLIT city$,"|",0,city$,addr$
          SPLIT addr$,"|",0,addr$,crossr$
          IF crossr$="|"
            crossr$=""
          ENDIF
          y=180/2^31*CVL(MID$(t$,27,4))
          x=180/2^31*CVL(MID$(t$,31,4))
          alt=cvf(MID$(t$,35,4))
          depth=cvf(MID$(t$,39,4))
          dist=cvf(MID$(t$,43,4))
          color=ASC(MID$(t$,4,1))
          attr=ASC(MID$(t$,6,1))
          class=ASC(MID$(t$,3,1))
          state$=MID$(t$,47,2)
          cc$=MID$(t$,49,2)
          displ=ASC(MID$(t$,5,1))
          subclass$=HEX$(cvi(MID$(t$,9,2)),4,4,1)+HEX$(cvl(MID$(t$,11,4)),8,8,1)+HEX$(cvl(MID$(t$,15,4)),8,8,1)+HEX$(cvl(MID$(t$,19,4)),8,8,1)+HEX$(cvl(MID$(t$,23,4)),8,8,1)
          PRINT #2," Name=";CHR$(34);name$;CHR$(34);SPACE$(MAX(6,LEN(n$))-len(n$));
          PRINT #2," Symbl=0x";HEX$(CVI(MID$(t$,7,2)),4,4,1);
          PRINT #2," X=";x;" Y=";y;
          IF alt<9e24
            PRINT #2," alt=";alt;
          ENDIF
          IF depth<9e24
            PRINT #2," depth=";depth;
          ENDIF
          IF dist<9e24
            PRINT #2," dist=";dist;
          ENDIF
          IF class
            PRINT #2," Class=";class;
          ENDIF
          IF upper$(subclass$)<>"000000000000FFFFFFFFFFFFFFFFFFFFFFFF"
            PRINT #2," Subclass=";subclass$;
          ENDIF
          IF color<>-1
            PRINT #2," Color=";color;
          ENDIF
          IF displ
            PRINT #2," displ=";displ;
          ENDIF
          IF attr<>6*16
            PRINT #2," attr=";attr;
          ENDIF
          IF state$<>"  "
            PRINT #2," State=";CHR$(34);state$;CHR$(34);
          ENDIF
          IF cc$<>"  "
            PRINT #2," Cc=";CHR$(34);cc$;CHR$(34);
          ENDIF
          IF len(comment$)
            PRINT #2," Comment=";CHR$(34);comment$;CHR$(34);
          ENDIF
          IF len(facility$)
            PRINT #2," Facility=";CHR$(34);facility$;CHR$(34);
          ENDIF
          IF len(city$)
            PRINT #2," City=";CHR$(34);city$;CHR$(34);
          ENDIF
          IF len(addr$)
            PRINT #2," Addr=";CHR$(34);addr$;CHR$(34);
          ENDIF
          IF len(crossr$)
            PRINT #2," Crossr=";CHR$(34);crossr$;CHR$(34);
          ENDIF
          PRINT #2
        NEXT i
        CLOSE #2
        DEFMOUSE 0
        SHOWPAGE
      ENDIF
    ENDIF
  ELSE
    ~form_alert(1,"[3][Keine Wegpunkte vorhanden!][ OH ]")
  ENDIF
RETURN

FUNCTION inpoly(t$,x,y)
  LOCAL l1,l2,l3,l4,anz,i,j,xpi,xpj,ypi,ypj,c
  c=0
  '        l1=cvf(mid$(t$,7,4))
  '        l2=cvf(mid$(t$,11,4))
  '        l3=cvf(mid$(t$,15,4))
  '        l4=cvf(mid$(t$,19,4))
  anz=cvi(MID$(t$,23,2))
  i=0
  j=anz-1
  WHILE i<anz
    xpi=cvf(MID$(t$,25+8*i,4))
    xpj=cvf(MID$(t$,25+8*j,4))
    ypi=cvf(MID$(t$,25+8*i+4,4))
    ypj=cvf(MID$(t$,25+8*j+4,4))

    IF ((ypi<=y AND y<ypj) OR (ypj<=y AND y<ypi))
      IF (x<(xpj-xpi)*(y-ypi)/(ypj-ypi)+xpi)
        c=(not c)
      ENDIF
    ENDIF
    j=i
    INC i
  WEND
  RETURN c
ENDFUNCTION

PROCEDURE exportmap
  LOCAL ers,f$,i,j,t$,typ,flags,alt,l1,l2,l3,l4,anz,name$
  ers=2
  IF anzwp
    FILESELECT "export map ...","./*.lst","map.lst",f$
    IF len(f$)
      IF exist(f$)
        ers=form_alert(3,"[1][File already exists! |append or replace ?][append|replace|CANCEL]")=2
      ENDIF
      IF ers<3
        DEFMOUSE 2
        SHOWPAGE
        IF ers=1
          OPEN "A",#2,f$
        ELSE
          OPEN "O",#2,f$
        ENDIF
        PRINT #2,"# Map elements from GPS-Earth (c) Markus Hoffmann"
        PRINT #2,"# ";anzpoly;" map elements. "+date$+" "+time$
        PRINT #2,"# Region: ";
        l1=360
        l2=-360
        l3=90
        l4=-90
        FOR i=0 TO anzpoly-1
          t$=polygon$(i)
          l1=MIN(l1,cvf(MID$(t$,7,4)))
          l2=MAX(l2,cvf(MID$(t$,11,4)))
          l3=MIN(l3,cvf(MID$(t$,15,4)))
          l4=MAX(l4,cvf(MID$(t$,19,4)))
        NEXT i
        PRINT #2,"[";l1;":";l2;"]";"[";l3;":";l4;"]"
        FOR i=0 TO anzpoly-1
          t$=polygon$(i)
          typ=ASC(MID$(t$,1,1)) and 255
          flags=ASC(MID$(t$,2,1)) and 255
          alt=cvf(MID$(t$,3,4))
          l1=cvf(MID$(t$,7,4))
          l2=cvf(MID$(t$,11,4))
          l3=cvf(MID$(t$,15,4))
          l4=cvf(MID$(t$,19,4))
          anz=cvi(MID$(t$,23,2))
          name$=MID$(t$,25+anz*8,LEN(t$)-25-anz*8)
          name$=REPLACE$(name$,CHR$(0),"")
          PRINT #2,"Name=";CHR$(34);name$;CHR$(34);SPACE$(MAX(20,LEN(name$))-len(name$));
          PRINT #2," Typ=0x";HEX$(typ,2,2,1);" Flags=0x";HEX$(flags,2,2,1);
          PRINT #2," X1=";l1;" X2=";l2;" Y1=";l3;" Y2=";l4;
          IF alt<9e24
            PRINT #2," alt=";alt;
          ENDIF
          PRINT #2," Data["+STR$(anz)+"]=";
          FOR j=0 TO anz-1
            PRINT #2,cvf(MID$(t$,25+8*j,4));",";cvf(MID$(t$,25+8*j+4,4));";";
          NEXT j
          PRINT #2
        NEXT i
        CLOSE #2
        DEFMOUSE 0
        SHOWPAGE
      ENDIF
    ENDIF
  ELSE
    ~form_alert(1,"[3][No map elements defined !][ OH ]")
  ENDIF
RETURN
PROCEDURE importgpx
  LOCAL f$,t$,a$,b$,x,y,alt,depth,data$,gpxok,xmlok
  LOCAL level,verbose,data$,totaltrkpt,anztrk,anzwpt
  CLR gpxok,xmlok
  verbose=-1
  level=-1
  FILESELECT "import GPX file ...","./*.gpx","a.gpx",f$
  IF len(f$)
    IF exist(f$)
      IF anzwp OR anztrack
        IF form_alert(1,"[2][Daten an bestehende Daten anf�gen|oder ersetzen ?][anf�gen|ersetzen]")=2
          CLR anzwp,anztrack
        ENDIF
      ENDIF
      DEFMOUSE 2
      SHOWPAGE
      OPEN "I",#2,f$
      data$=input$(#2,lof(#2))
      CLOSE #2

      PRINT @processtag$(data$)

      IF gpxok AND xmlok
        PRINT anzwpt;" Waypoints."
        PRINT anztrk;" Tracks."
        PRINT totaltrkpt;" Trackpoints."
      ELSE
        PRINT "The File-Format has errors! Maybe not a GPX file."
      ENDIF
      @status(3,STR$(anzwp)+" WP loaded")
      @sort_wp
      @redraw
      @showrange
      DEFMOUSE 0
    ENDIF
  ENDIF
RETURN

FUNCTION processtag$(t$)
  LOCAL a$,b$,block$,tagoptions$,ergebnis$,value$,tagtyp$
  LOCAL ndate$,ntime$,timestamp,x,y,lat,lon,date,name$,alt,depth
  LOCAL new,cc$,state$,subclass$
  INC level
  WHILE len(t$)
    WHILE left$(t$)=" " OR LEFT$(t$)=CHR$(13) OR LEFT$(t$)=CHR$(10)
      t$=RIGHT$(t$,LEN(t$)-1)
    WEND
    SPLIT t$,"<",0,ergebnis$,t$
    IF len(t$)
      SPLIT t$,">",0,a$,t$
      SPLIT a$," ",0,tagtyp$,tagoptions$
      IF verbose>0
        PRINT space$(level*2);tagtyp$;
        IF len(tagoptions$)
          PRINT "(";tagoptions$;")";
        ENDIF
      ENDIF
      IF right$(a$)<>"/"
        IF verbose>0
          PRINT "=";
        ENDIF
        endtag$="</"+tagtyp$+">"
        SPLIT t$,endtag$,0,block$,t$
        IF len(block$)
          IF instr(block$,"<")
            IF verbose>0
              PRINT "{"
            ENDIF
          ENDIF
          value$=@processtag$(block$)
          IF verbose>0
            IF instr(value$," ")
              PRINT chr$(34);value$;CHR$(34);
            ELSE
              PRINT value$;
            ENDIF
            IF instr(block$,"<")
              PRINT space$(level*2);"}";
            ENDIF
          ENDIF
        ENDIF
      ENDIF
      IF verbose>0
        PRINT
      ENDIF
      IF tagtyp$="ele"
        ele=VAL(value$)
        IF verbose>=0
          PRINT ele
        ENDIF
      ELSE if tagtyp$="number"
        number=VAL(value$)
        IF verbose>=0
          PRINT number
        ENDIF
      ELSE if tagtyp$="time"
        year=VAL(MID$(value$,1,4))
        month=VAL(MID$(value$,6,2))
        day=VAL(MID$(value$,9,2))
        hour=VAL(MID$(value$,12,2))
        minute=VAL(MID$(value$,15,2))
        second=VAL(MID$(value$,18,2))
        ndate$=STR$(day,2,2)+"."+STR$(month,2,2,1)+"."+STR$(year,4,4)
        ntime$=STR$(hour,2,2)+":"+STR$(minute,2,2,1)+":"+STR$(second,2,2,1)
        '      print ndate$,ntime$
        timestamp=@getunixtime(ndate$,ntime$)
        '      print timestamp
      ELSE if tagtyp$="name"
        name$=value$
        IF verbose>=0
          PRINT name$
        ENDIF
      ELSE if tagtyp$="?xml"
        xmlok=true
        IF verbose>=0
          PRINT "XML OK."
        ENDIF
      ELSE if tagtyp$="gpx"
        gpxok=true
        IF verbose>=0
          PRINT "GPX OK."
        ENDIF
      ELSE if tagtyp$="trkpt"
        lat=VAL(@getval$(tagoptions$,"lat"))
        lon=VAL(@getval$(tagoptions$,"lon"))
        y=INT(lat/180*2^31)
        x=INT(lon/180*2^31)
        alt=ele
        date=timestamp
        depth=1e25
        IF anztrkpt=0
          new=1
        ELSE
          new=0
        ENDIF
        PRINT "Trackpt #";anztrkpt;" lat=";lat;" lon=";lon;" ele=";ele;" time=";timestamp
        track$(anztrack)=mkl$(y)+mkl$(x)+mkl$(date)+mkf$(alt)+mkf$(depth)+CHR$(new)
        INC anztrack
        ele=-1
        timestamp=-1
        INC anztrkpt
      ELSE if tagtyp$="wpt"
        lat=VAL(@getval$(tagoptions$,"lat"))
        lon=VAL(@getval$(tagoptions$,"lon"))
        depth=1e25
        alt=1e25
        attr=6*16
        color=-1
        class=0
        displ=0
        symbl=0
        comment$=""
        subclass$=mki$(0)+mkl$(0)+mkl$(-1)+mkl$(-1)+mkl$(-1)
        cc$="  "
        state$="  "

        alt=ele
        y=INT(lat/180*2^31)
        x=INT(lon/180*2^31)
        PRINT "WPT #";anzwpt;" name=";CHR$(34);name$;CHR$(34);" lat=";lat;" lon=";lon;" ele=";ele;" time=";timestamp
        waypoint$(anzwp)=CHR$(class)+CHR$(color)+CHR$(displ)+CHR$(attr)+mki$(symbl)+subclass$+mkl$(y)+mkl$(x)+mkf$(alt)+mkf$(depth)+mkf$(dist)+state$+cc$+name$+CHR$(0)+comment$+CHR$(0)+facility$+CHR$(0)+city$+CHR$(0)+addr$+CHR$(0)+crossr$+CHR$(0)
        INC anzwp
        ele=-1
        timestamp=-1
        name$="noname"
        INC anzwpt
      ELSE if tagtyp$="trk"
        PRINT "Track(";anztrkpt;") #";anztrk;" number=";number;" name=";name$
        number=-1
        ADD totaltrkpt,anztrkpt
        CLR anztrkpt
        INC anztrk
        name$=""
      ENDIF
    ENDIF
  WEND
  DEC level
  '  if instr(ergebnis$," ")
  '    ergebnis$=chr$(34)+ergebnis$+chr$(34)
  '  endif
  RETURN ergebnis$
ENDFUNCTION
FUNCTION getval$(t$,f$)
  LOCAL a$,val$,name$
  val$=""
  SPLIT t$," ",1,a$,t$
  WHILE len(a$)
    a$=TRIM$(a$)
    SPLIT a$,"=",1,name$,val$
    EXIT if UPPER$(name$)=UPPER$(f$)
    val$=""
    SPLIT t$,",",1,a$,t$
  WEND
  IF left$(val$)=CHR$(34) AND RIGHT$(val$)=CHR$(34)
    val$=RIGHT$(val$,LEN(val$)-1)
    val$=LEFT$(val$,LEN(val$)-1)
  ENDIF
  IF left$(val$)="'" AND RIGHT$(val$)="'"
    val$=RIGHT$(val$,LEN(val$)-1)
    val$=LEFT$(val$,LEN(val$)-1)
  ENDIF

  RETURN val$
ENDFUNCTION
FUNCTION getunixtime(dat$,tim$)
  LOCAL h,m,s
  h=VAL(LEFT$(tim$,2))
  m=VAL(MID$(tim$,4,2))
  s=VAL(RIGHT$(tim$,2))
  RETURN (JULIAN(dat$)-julian("01.01.1970"))*24*60*60+(h-1)*3600+m*60+s
ENDFUNCTION

PROCEDURE importwp
  LOCAL f$,t$,a$,b$,x,y,alt,depth
  FILESELECT "import Waypoints ...","./*.lst","waypoint.lst",f$
  IF len(f$)
    IF exist(f$)
      IF anzwp
        IF form_alert(1,"[2][Daten an bestehende Daten anf�gen|oder ersetzen ?][anf�gen|ersetzen]")=2
          CLR anzwp
        ENDIF
      ENDIF
      DEFMOUSE 2
      SHOWPAGE
      OPEN "I",#2,f$
      WHILE not eof(#2)
        LINEINPUT #2,t$
        t$=TRIM$(t$)
        IF left$(t$)="#"
          ' nixtun
        ELSE
          depth=1e25
          alt=1e25
          attr=6*16
          color=-1
          class=0
          displ=0
          symbl=0
          comment$=""
          name$="noname"
          subclass$=mki$(0)+mkl$(0)+mkl$(-1)+mkl$(-1)+mkl$(-1)
          cc$="  "
          state$="  "
          WHILE len(t$)
            SPLIT t$," ",1,a$,t$
            SPLIT a$,"=",1,a$,b$
            a$=UPPER$(a$)
            IF a$="X"
              x=INT(VAL(b$)/180*2^31)
            ELSE if a$="Y"
              y=INT(VAL(b$)/180*2^31)
            ELSE if a$="ALT"
              alt=VAL(b$)
            ELSE if a$="ATTR"
              attr=VAL(b$)
            ELSE if a$="DEPTH"
              depth=VAL(b$)
            ELSE if a$="NAME"
              name$=RIGHT$(LEFT$(b$,LEN(b$)-1),LEN(b$)-2)
            ELSE if a$="COMMENT"
              comment$=RIGHT$(LEFT$(b$,LEN(b$)-1),LEN(b$)-2)
            ELSE if a$="FACILITY"
              facility$=RIGHT$(LEFT$(b$,LEN(b$)-1),LEN(b$)-2)
            ELSE if a$="CITY"
              city$=RIGHT$(LEFT$(b$,LEN(b$)-1),LEN(b$)-2)
            ELSE if a$="ADDR"
              addr$=RIGHT$(LEFT$(b$,LEN(b$)-1),LEN(b$)-2)
            ELSE if a$="CROSSR"
              crossr$=RIGHT$(LEFT$(b$,LEN(b$)-1),LEN(b$)-2)
            ELSE if a$="STATE"
              state$=RIGHT$(LEFT$(b$,LEN(b$)-1),LEN(b$)-2)
            ELSE if a$="CC"
              cc$=RIGHT$(LEFT$(b$,LEN(b$)-1),LEN(b$)-2)
            ELSE if a$="SUBCLASS"
              subclass$=""
              FOR i=0 TO 16 STEP 2
                subclass$=subclass$+mki$(VAL("0x"+MID$(b$,i*2+1,4)))
              NEXT i
            ELSE if a$="CLASS"
              class=VAL(b$)
            ELSE if a$="COLOR"
              color=VAL(b$)
            ELSE if a$="DISPL"
              displ=VAL(b$)
              ' print displ
            ELSE if a$="DIST"
              dist=VAL(b$)
            ELSE if a$="SYMBL"
              symbl=VAL(b$)
            ELSE
              PRINT a$,b$
            ENDIF
          WEND
          IF len(cc$)<>2
            cc$="  "
          ENDIF
          IF len(state$)<>2
            state$="  "
          ENDIF
          IF len(subclass$)<>18
            PRINT "Formatfehler !"
          ELSE
            waypoint$(anzwp)=CHR$(class)+CHR$(color)+CHR$(displ)+CHR$(attr)+mki$(symbl)+subclass$+mkl$(y)+mkl$(x)+mkf$(alt)+mkf$(depth)+mkf$(dist)+state$+cc$+name$+CHR$(0)+comment$+CHR$(0)+facility$+CHR$(0)+city$+CHR$(0)+addr$+CHR$(0)+crossr$+CHR$(0)
            INC anzwp
          ENDIF
        ENDIF
      WEND
      CLOSE #2
      @status(3,STR$(anzwp)+" WP loaded")
      @sort_wp
      @redraw
      @showrange
      DEFMOUSE 0
    ENDIF
  ENDIF
RETURN
PROCEDURE importmap
  LOCAL i,g$,f$,t$,p$,a$,b$,x,y,alt,anz,name$,l1,l2,l3,l4,typ,flags
  DIM px(100000),py(100000)
  FILESELECT "import Map elements ...","./*.lst","map.lst",f$
  IF len(f$)
    IF exist(f$)
      IF anzpoly
        IF form_alert(1,"[2][Daten an bestehende Daten anf�gen|oder ersetzen ?][anf�gen|ersetzen]")=2
          CLR anzpoly
        ENDIF
      ENDIF
      DEFMOUSE 2
      SHOWPAGE
      OPEN "I",#2,f$
      g$=input$(#2,lof(#2))
      WHILE len(g$)
        SPLIT g$,CHR$(10),0,t$,g$
        PRINT anzpoly
        '        t$=@longlineinput$(2)
        t$=TRIM$(t$)
        IF left$(t$)="#"
          ' nixtun
        ELSE
          l1=360
          l2=-360
          l3=360
          l4=-360
          anz=0
          typ=0
          flags=0
          name$="noname"
          alt=1e25
          WHILE len(t$)
            SPLIT t$," ",1,a$,t$
            SPLIT a$,"=",1,a$,b$
            a$=UPPER$(a$)
            IF a$="ALT"
              alt=VAL(b$)
            ELSE if a$="TYP"
              typ=VAL(b$)
            ELSE if a$="FLAGS"
              flags=VAL(b$)
            ELSE if a$="NAME"
              name$=RIGHT$(LEFT$(b$,LEN(b$)-1),LEN(b$)-2)
            ELSE if LEFT$(a$,4)="DATA"
              WHILE len(b$)
                SPLIT b$,";",0,a$,b$
                SPLIT a$,",",0,p$,a$
                x=VAL(p$)
                y=VAL(a$)
                px(anz)=x
                py(anz)=y
                l1=MIN(x,l1)
                l2=MAX(x,l2)
                l3=MIN(y,l3)
                l4=MAX(y,l4)

                INC anz
              WEND
            ELSE
              PRINT a$,b$
            ENDIF
          WEND
          p$=CHR$(typ)+CHR$(flags)+mkf$(alt)+mkf$(l1)+mkf$(l2)+mkf$(l3)+mkf$(l4)+mki$(anz)
          FOR i=0 TO anz-1
            p$=p$+mkf$(px(i))+mkf$(py(i))
          NEXT i
          p$=p$+name$+CHR$(0)
          polygon$(anzpoly)=p$
          INC anzpoly
        ENDIF
      WEND
      CLOSE #2
      @status(3,STR$(anzpoly)+" MAP elements loaded")
      @redraw
      @showrange
      DEFMOUSE 0
    ENDIF
  ENDIF
RETURN
FUNCTION longlineinput$(channel)
  LOCAL a,t$
  a=INP(channel)
  WHILE a<>10
    t$=t$+CHR$(A)
    a=INP(channel)
  WEND
  RETURN t$
ENDFUNCTION

PROCEDURE do_loadwp(f$)
  LOCAL dlen
  OPEN "I",#2,f$
  WHILE inp?(#2)
    dlen=INP(#2) and 255
    waypoint$(anzwp)=INPUT$(#2,dlen)
    INC anzwp
  WEND
  CLOSE #2
  @sort_wp
RETURN

PROCEDURE loadpoly
  LOCAL dlen,f$
  FILESELECT "load map ...","./*.map","base.map",f$
  IF len(f$)
    IF exist(f$)
      IF anzpoly
        IF form_alert(1,"[2][Daten an bestehende Daten anf�gen|oder ersetzen ?][anf�gen|ersetzen]")=2
          CLR anzpoly
        ENDIF
      ENDIF
      DEFMOUSE 2
      @do_loadpoly(f$)
      @status(3,STR$(anzpoly)+" MAP elements loaded.")
      @redraw
      @showrange
      DEFMOUSE 0
    ELSE
      ~form_alert(1,"[3][File "+f$+"|not found !][ OH ]")
    ENDIF
  ENDIF
RETURN

PROCEDURE do_loadpoly(f$)
  LOCAL dlen
  OPEN "I",#2,f$
  WHILE inp?(#2)
    dlen=cvi(input$(#2,2)) and 0xffff
    polygon$(anzpoly)=INPUT$(#2,dlen)
    INC anzpoly
  WEND
  CLOSE #2
RETURN

PROCEDURE sort_wp
  LOCAL xx,yy,x,y,t$,c1,c2
  DIM dist(anzwp)
  DIM index(anzwp)
  xx=(xmax+xmin)/2
  yy=(ymax+ymin)/2
  c2=1e11
  c1=@distance(xx,yy,xmax,ymax)
  PRINT "calculate distances ..."
  FOR i=0 TO anzwp-1
    t$="  "+waypoint$(i)
    y=180/2^31*CVL(MID$(t$,27,4))
    x=180/2^31*CVL(MID$(t$,31,4))
    dist(i)=@distance(xx,yy,x,y)
    index(i)=i
  NEXT i
  SORT dist(),anzwp,index()
  PRINT "SWAPPING:"
  w$()=waypoint$()
  FOR i=0 TO anzwp-1
    j=index(i)
    waypoint$(i)=w$(j)
    ' print dist(i),c1
    IF dist(i)>c1
      c2=MIN(c2,i)
    ENDIF
  NEXT i
  PRINT "cutoff:",c2
  waypoint_cutoff=c2
RETURN
PROCEDURE calc_cutoff
  LOCAL xx,yy,c1,c2
  xx=(xmax+xmin)/2
  yy=(ymax+ymin)/2
  c2=1e11
  c1=@distance(xx,yy,xmax,ymax)
  FOR i=0 TO anzwp-1
    IF dist(i)>c1
      c2=i
      EXIT if true
    ENDIF
  NEXT i
  PRINT "cutoff:",c2
  waypoint_cutoff=c2
RETURN

PROCEDURE loadtrack
  LOCAL dlen,f$

  FILESELECT "load track ...","./*.track","my.track",f$
  IF len(f$)
    IF exist(f$)
      IF anztrack
        IF form_alert(1,"[2][Daten an bestehende Daten anf�gen|oder ersetzen ?][anf�gen|ersetzen]")=2
          CLR anztrack,anztrackl
        ENDIF
      ENDIF
      DEFMOUSE 2
      SHOWPAGE
      OPEN "I",#2,f$
      trackl1(anztrackl)=anztrack
      WHILE inp?(#2) AND anztrack<20000
        dlen=INP(#2) and 255
        track$(anztrack)=INPUT$(#2,dlen)
        INC anztrack
      WEND
      CLOSE #2
      trackname$=f$
      SPLIT trackname$,".track",1,trackname$,f$
      IF rinstr(trackname$,"/")
        trackname$=RIGHT$(trackname$,LEN(trackname$)-rinstr(trackname$,"/"))
      ENDIF
      tracklist$(anztrackl)=trackname$
      trackl2(anztrackl)=anztrack-1
      INC anztrackl
      @status(3,STR$(anztrack)+" TR geladen")
      @redraw
      @showrange
    ELSE
      ~form_alert(1,"[3][File "+f$+"|not found !][ OH ]")
    ENDIF
  ENDIF
RETURN
PROCEDURE exporttrack
  LOCAL ers,x,y,t$
  ers=2
  IF anztrack
    FILESELECT "export track ...","./*.lst","track.lst",f$
    IF len(f$)
      IF exist(f$)
        ers=form_alert(3,"[1][Datei existiert bereits! |anfuegen oder ersetzen ?][anf�gen|ersetzen|CANCEL]")=2
      ENDIF
      IF ers<3
        DEFMOUSE 2
        SHOWPAGE
        IF ers=1
          OPEN "A",#2,f$
        ELSE
          OPEN "O",#2,f$
        ENDIF
        PRINT #2,"# Track von garmin.bas (c) Markus Hoffmann"
        PRINT #2,"# Gesamt: ";anztrack;" Punkte. "+date$+" "+time$
        IF anztrackl
          PRINT #2,"# Tracks:"
          FOR i=0 TO anztrackl-1
            PRINT #2,"# "+tracklist$(i)+": ";trackl1(i);" bis ";trackl2(i)
          NEXT i
        ENDIF
        PRINT #2,"# Format=0 "
        FOR i=0 TO anztrack-1
          t$=track$(i)
          y=180/2^31*CVL(MID$(t$,1,4))
          x=180/2^31*CVL(MID$(t$,5,4))
          date=CVL(MID$(t$,9,4))
          alt=CVf(MID$(t$,13,4))
          depth=CVf(MID$(t$,17,4))
          new=ASC(MID$(t$,21,1))
          dist=@distance(ox,oy,x,y)*1000
          IF new
            PRINT #2," **";
            CLR vv,vh,vx,vy,vz
          ELSE
            dt=date-odate
            dx=@distance(ox,y,x,y)*1000
            dy=@distance(x,oy,x,y)*1000
            IF dt
              vz=INT((alt-oalt)/dt*100)/100
              vx=INT(dx/dt*100)/100
              vy=INT(dy/dt*100)/100
              vh=dist/dt*3.6
              vv=(alt-oalt)/dt*3.6
            ELSE
              CLR vx,vy,vz,vh,vv
            ENDIF
            PRINT #2,"  +";
          ENDIF
          odate=date
          ox=x
          oy=y
          oalt=alt
          PRINT #2," X=";x;" Y=";y;
          IF alt<9e24
            PRINT #2," alt=";alt;
          ENDIF
          IF date<>-1
            PRINT #2," date=";date;
          ENDIF
          IF depth<9e24
            PRINT #2," depth=";depth;
          ENDIF
          IF i>0
            PRINT #2," dist=";INT(1000*dist)/1000;" m";
            IF new=0 AND dt
              PRINT #2," V*=(";vx;",";vy;",";vz;") m/s v=";INT(10*sqrt(vh*vh+vv*vv))/10;" km/h";
            ENDIF
          ENDIF
          PRINT #2
        NEXT i
        CLOSE #2
        DEFMOUSE 0
        SHOWPAGE
      ENDIF
    ENDIF
  ELSE
    ~form_alert(1,"[3][Keine Trackpunkte vorhanden!][ OH ]")
  ENDIF
RETURN
PROCEDURE importtrack
  LOCAL f$,t$,oanztrack,a$,b$,x,y,alt,depth
  FILESELECT "import track ...","./*.lst","my.lst",f$
  IF len(f$)
    IF exist(f$)
      IF anztrack
        IF form_alert(1,"[2][Daten an bestehende Daten anf�gen|oder ersetzen ?][anf�gen|ersetzen]")=2
          CLR anztrack
        ENDIF
      ENDIF
      DEFMOUSE 2
      SHOWPAGE
      trackl1(anztrackl)=anztrack
      oanztrack=anztrack
      OPEN "I",#2,f$
      WHILE not eof(#2)
        LINEINPUT #2,t$
        t$=TRIM$(t$)
        IF left$(t$)="#" OR LEN(t$)=0
          ' nixtun
        ELSE
          SPLIT t$," ",1,a$,t$
          IF a$="**"
            new=1
          ELSE if a$="+"
            new=0
          ELSE
            PRINT "Formatfehler !"
            new=1
          ENDIF
          date=-1
          alt=1e25
          depth=1e25
          WHILE len(t$)
            SPLIT t$," ",1,a$,t$
            SPLIT a$,"=",1,a$,b$
            a$=UPPER$(a$)
            IF a$="X"
              x=INT(VAL(b$)/180*2^31)
            ELSE if a$="Y"
              y=INT(VAL(b$)/180*2^31)
            ELSE if a$="ALT"
              alt=VAL(b$)
            ELSE if a$="DATE"
              date=VAL(b$)
            ELSE if a$="DEPTH"
              depth=VAL(b$)
            ELSE
              PRINT a$,b$
            ENDIF
          WEND
          track$(anztrack)=mkl$(y)+mkl$(x)+mkl$(date)+mkf$(alt)+mkf$(depth)+CHR$(new)
          INC anztrack
        ENDIF
      WEND
      CLOSE #2
      trackname$=f$
      SPLIT trackname$,".lst",1,trackname$,f$
      IF rinstr(trackname$,"/")
        trackname$=RIGHT$(trackname$,LEN(trackname$)-rinstr(trackname$,"/"))
      ENDIF
      tracklist$(anztrackl)=trackname$
      trackl2(anztrackl)=anztrack-1
      INC anztrackl

      @status(3,STR$(anztrack)+" TR loaded")
      @redraw
      @showrange
      DEFMOUSE 0
    ENDIF
  ENDIF
RETURN

PROCEDURE drawtrack
  LOCAL oanztrack,x,y,alt,ox,oy,depth,new
  mindist=10
  new=1
  IF anztrack
    IF form_alert(1,"[2][Daten an bestehende Daten anf�gen|oder ersetzen ?][anf�gen|ersetzen]")=2
      CLR anztrack
    ENDIF
  ENDIF
  trackl1(anztrackl)=anztrack
  oanztrack=anztrack
  DEFMOUSE 3
  SHOWPAGE
  COLOR schwarz
  MOUSEEVENT ox,oy,k
  GRAPHMODE 3
  IF k<>4*256
    REPEAT
      MOTIONEVENT x,y,a,b,k
      LINE ox,oy,x,y
      SHOWPAGE
      LINE ox,oy,x,y
      IF k=2*256
        ox=x
        oy=y
        new=1
      ELSE if k=1*256
        IF @distance(@ox(ox),@oy(oy),@ox(x),@oy(y))*1000>mindist
          LINE ox,oy,x,y
          track$(anztrack)=mkl$(@oy(y)/180*2^31)+mkl$(@ox(x)/180*2^31)+mkl$(date)+mkf$(alt)+mkf$(depth)+CHR$(new)
          INC anztrack
          CIRCLE x,y,2
          new=0
          ox=x
          oy=y
        ENDIF
      ENDIF
    UNTIL k=4*256
    GRAPHMODE 1
  ENDIF
  tracklist$(anztrackl)="Drawn Track"
  trackl2(anztrackl)=anztrack-1
  INC anztrackl
  @redraw
  @showrange
  DEFMOUSE 0
RETURN

PROCEDURE calc_trackinfo
  LOCAL i,t$,x,y,date,alt,new,dist,odate,ox,oy,new,dt,oalt
  CLR max_v,track_len,track_zeit,track_bdate,track_altmax
  track_altmin=1e24
  track_adate=1e24
  FOR i=0 TO anztrack-1
    t$=track$(i)
    y=180/2^31*CVL(MID$(t$,1,4))
    x=180/2^31*CVL(MID$(t$,5,4))
    date=CVL(MID$(t$,9,4))
    track_adate=MIN(date,track_adate)
    track_bdate=MAX(date,track_bdate)
    alt=CVf(MID$(t$,13,4))
    track_altmin=MIN(alt,track_altmin)
    track_altmax=MAX(alt,track_altmax)
    new=ASC(MID$(t$,21,1))
    dist=@distance(ox,oy,x,y)*1000
    IF new
      CLR vv,vh,vx,vy,vz
    ELSE
      dt=date-odate
      dist=@distance(ox,oy,x,y)*1000
      dx=@distance(ox,y,x,y)*1000
      dy=@distance(x,oy,x,y)*1000
      IF dt
        vz=INT((alt-oalt)/dt*100)/100
        vx=INT(dx/dt*100)/100
        vy=INT(dy/dt*100)/100
        vh=dist/dt*3.6
        vv=(alt-oalt)/dt*3.6
        max_v=MAX(max_v,sqrt(vh*vh+vv*vv))
      ELSE
        CLR vx,vy,vz,vh,vv
      ENDIF
      ADD track_len,dist
      ADD track_zeit,dt
    ENDIF
    odate=date
    ox=x
    oy=y
    oalt=alt
  NEXT i
RETURN

PROCEDURE sendtrack
  LOCAL dlen,f$,t$,i,bg$,tn$
  tn$="X11-BASIC"
  IF anztrack
    i=form_alert(1,"[2][Welchen Namen verwenden ?]["+tn$+"|"+trackname$+"|ACTIVE LOG]")
    IF i=1
      tn$=tn$
    ELSE if i=2
      tn$=trackname$
    ELSE if i=3
      tn$="ACTIVE LOG"
    ENDIF
    IF form_alert(1,"[2][Den aktuellen Track, |"+STR$(anztrack)+" Punkte|jetzt als "+tn$+" zum GPS senden? ][Ja|Nein]")=1
      DEFMOUSE 2
      GET sx+(sw-100)/2,sy+(sh-50)/2,sx+(sw+100)/2,sy+(sh+50)/2,bg$
      COLOR weiss
      PBOX sx+(sw-100)/2,sy+(sh-50)/2,sx+(sw+100)/2,sy+(sh+50)/2
      COLOR schwarz
      BOX sx+(sw-100)/2,sy+(sh-50)/2,sx+(sw+100)/2,sy+(sh+50)/2
      @sendmessage(27,mki$(anztrack))
      @do_hscaler(sx+670,sy+170,100,0)
      @sendmessage(99,CHR$(1)+CHR$(255)+tn$+chr$(0))
      FOR i=0 TO anztrack-1
        COLOR weiss
        PBOX sx+(sw-100)/2,sy+(sh-50)/2,sx+(sw+100)/2,sy+(sh+50)/2
        COLOR schwarz,grau
        BOX sx+(sw-100)/2,sy+(sh-50)/2,sx+(sw+100)/2,sy+(sh+50)/2
        TEXT sx+(sw-100)/2+10,sy+sh/2-10,"sende ..."
        TEXT sx+(sw-100)/2+10,sy+sh/2+10,"# "+STR$(i,5,5)
        SHOWPAGE
        @sendmessage(34,track$(i))
        @do_hscaler(sx+670,sy+170,100,i/anztrack)
      NEXT i
      @do_hscaler(sx+670,sy+170,100,1)
      @sendmessage(12,mki$(6))
      PUT sx+(sw-100)/2,sy+(sh-50)/2,bg$
      DEFMOUSE 0
    ENDIF
  ELSE
    ~form_alert(1,"[3][Es ist kein Track geladen !][ OH ]")
  ENDIF
RETURN
PROCEDURE sendwp
  LOCAL dlen,f$,t$,i,bg$
  IF anzwp
    IF form_alert(1,"[2][Aktuelle Wegpunkte, |"+STR$(anzwp)+" Punkte|jetzt zum GPS senden? ][Ja|Nein]")=1
      DEFMOUSE 2
      GET sx+(sw-100)/2,sy+(sh-50)/2,sx+(sw+100)/2,sy+(sh+50)/2,bg$
      COLOR weiss
      PBOX sx+(sw-100)/2,sy+(sh-50)/2,sx+(sw+100)/2,sy+(sh+50)/2
      COLOR schwarz
      BOX sx+(sw-100)/2,sy+(sh-50)/2,sx+(sw+100)/2,sy+(sh+50)/2
      @sendmessage(27,mki$(anzwp))
      @do_hscaler(sx+670,sy+170,100,0)

      FOR i=0 TO anzwp-1
        COLOR weiss
        PBOX sx+(sw-100)/2,sy+(sh-50)/2,sx+(sw+100)/2,sy+(sh+50)/2
        COLOR schwarz,grau
        BOX sx+(sw-100)/2,sy+(sh-50)/2,sx+(sw+100)/2,sy+(sh+50)/2
        TEXT sx+(sw-100)/2+10,sy+sh/2-10,"sende ..."
        TEXT sx+(sw-100)/2+10,sy+sh/2+10,"# "+STR$(i,5,5)
        SHOWPAGE
        @sendmessage(35,waypoint$(i))
        @do_hscaler(sx+670,sy+170,100,i/anzwp)
      NEXT i
      @do_hscaler(sx+670,sy+170,100,1)
      @sendmessage(12,mki$(7))
      PUT sx+(sw-100)/2,sy+(sh-50)/2,bg$
      DEFMOUSE 0
    ENDIF
  ELSE
    ~form_alert(1,"[3][Es sind keine WP geladen !][ OH ]")
  ENDIF
RETURN
PROCEDURE loadal
  LOCAL dlen,f$
  FILESELECT "load almanach ...","./*.dat","almanach.dat",f$
  IF len(f$)
    IF exist(f$)
      CLR anzal
      DEFMOUSE 2
      OPEN "I",#2,f$
      WHILE inp?(#2)
        dlen=INP(#2) and 255
        almanac$(anzal)=INPUT$(#2,dlen)
        INC anzal
      WEND
      CLOSE #2
      @status(3,STR$(anzal)+" AL geladen")
      @showrange
      DEFMOUSE 0
    ELSE
      ~form_alert(1,"[3][Datei "+f$+"|existiert nicht !][ OH ]")
    ENDIF
  ENDIF
RETURN
PROCEDURE sendal
  LOCAL dlen,f$,t$,i,bg$
  IF anzal
    IF form_alert(1,"[2][Aktueller Almanac, |"+STR$(anzal)+" Satelitendaten|jetzt zum GPS senden? ][Ja|Nein]")=1
      DEFMOUSE 2
      GET sx+(sw-100)/2,sy+(sh-50)/2,sx+(sw+100)/2,sy+(sh+50)/2,bg$
      COLOR weiss
      PBOX sx+(sw-100)/2,sy+(sh-50)/2,sx+(sw+100)/2,sy+(sh+50)/2
      COLOR schwarz
      BOX sx+(sw-100)/2,sy+(sh-50)/2,sx+(sw+100)/2,sy+(sh+50)/2
      @sendmessage(27,mki$(anzal))
      @do_hscaler(sx+670,sy+170,100,0)

      FOR i=0 TO anzal-1
        COLOR weiss
        PBOX sx+(sw-100)/2,sy+(sh-50)/2,sx+(sw+100)/2,sy+(sh+50)/2
        COLOR schwarz,grau
        BOX sx+(sw-100)/2,sy+(sh-50)/2,sx+(sw+100)/2,sy+(sh+50)/2
        TEXT sx+(sw-100)/2+10,sy+sh/2-10,"sende ..."
        TEXT sx+(sw-100)/2+10,sy+sh/2+10,"# "+STR$(i,5,5)
        SHOWPAGE
        @sendmessage(31,almanac$(i))
        @do_hscaler(sx+670,sy+170,100,i/anzal)
      NEXT i
      @do_hscaler(sx+670,sy+170,100,1)
      @sendmessage(12,mki$(1))
      PUT sx+(sw-100)/2,sy+(sh-50)/2,bg$
      DEFMOUSE 0
    ENDIF
  ELSE
    ~form_alert(1,"[3][Es sind keine AL geladen !][ OH ]")
  ENDIF
RETURN
PROCEDURE sonder
  LOCAL a,f$,i
  ALERT 0,"Message-Nr eingeben:|MSGNR="+CHR$(27)+"14  ",2,"send|CANCEL",a,f$
  PRINT f$,a
  IF a=1
    i=VAL(f$)
    PRINT i
    @sendmessage(10,MKI$(i))
  ENDIF
RETURN
@sendmessage(11,chr$(8)) ! Kontrast setzen
PROCEDURE lichtan
  @sendmessage(15,chr$(0))
RETURN
PROCEDURE lichtaus
  @sendmessage(15,chr$(1))
RETURN
PROCEDURE asyncan
  @sendmessage(28,mki$(-1))
RETURN
PROCEDURE asyncaus
  @sendmessage(28,mki$(0))
RETURN

FUNCTION qthlocator$(breite,laenge)
  PRINT "laenge=";laenge,"Breite=";breite
  t$=CHR$(INT(laenge/20)+ASC("J"))
  t$=t$+CHR$(ASC("J")+INT(breite/10))
  laenge=laenge-int(laenge/20)*20
  laenge=laenge/2
  t$=t$+STR$(INT(laenge))
  breite=breite-int(breite/10)*10
  t$=t$+STR$(INT(breite))
  breite=breite-int(breite)
  laenge=laenge-int(laenge)
  t$=t$+CHR$(ASC("A")+24*laenge)
  t$=t$+CHR$(ASC("A")+24*breite)
  RETURN t$
ENDFUNC

FUNCTION breite$(x)
  LOCAL posx$
  IF x>0
    posx$="N"
  ELSE
    posx$="S"
  ENDIF
  x=ABS(x)
  posx$=posx$+RIGHT$("00"+STR$(INT(x)),2)+":"
  x=x-INT(x)
  x=x*60
  posx$=posx$+RIGHT$("00"+STR$(INT(x)),2)+":"
  x=x-INT(x)
  x=x*60
  posx$=posx$+RIGHT$("00"+STR$(INT(x)),2)+"."
  x=x-INT(x)
  x=x*10
  posx$=posx$+STR$(INT(x))
  RETURN posx$
ENDFUNC
FUNCTION laenge$(x)
  LOCAL posx$
  IF x>0
    posx$="E"
  ELSE
    posx$="W"
  ENDIF
  x=ABS(x)
  posx$=posx$+RIGHT$("000"+STR$(INT(x)),3)+":"
  x=x-INT(x)
  x=x*60
  posx$=posx$+RIGHT$("00"+STR$(INT(x)),2)+":"
  x=x-INT(x)
  x=x*60
  posx$=posx$+RIGHT$("00"+STR$(INT(x)),2)+"."
  x=x-INT(x)
  x=x*10
  posx$=posx$+STR$(INT(x))
  RETURN posx$
ENDFUNC
FUNCTION conv_laenge(posx$)
  LOCAL x
  x=VAL(MID$(posx$,2,2))+VAL(MID$(posx$,5,2))/60+VAL(MID$(posx$,8,6))/60/60
  IF UPPER$(LEFT$(posx$,1))="S"
    x=-x
  ENDIF
  RETURN x
ENDFUNC
FUNCTION conv_breite(posy$)
  LOCAL y
  y=VAL(MID$(posy$,2,3))+VAL(MID$(posy$,6,2))/60+VAL(MID$(posy$,9,6))/60/60
  IF UPPER$(LEFT$(posy$,1))="W"
    y=-y
  ENDIF
  RETURN y
ENDFUNC

PROCEDURE hoehenprofil
  LOCAL i,x,y,date,alt,depth,new,dist,laenge,v
  DEFMOUSE 2
  SHOWPAGE
  laenge=0
  gnutmp$="/tmp/"+STR$(timer)+".gnu"
  dattmp$="/tmp/"+STR$(timer)+".dat"
  OPEN "O",#11,dattmp$
  FOR i=0 TO anztrack-1
    t$=track$(i)
    y=180/2^31*CVL(MID$(t$,1,4))
    x=180/2^31*CVL(MID$(t$,5,4))
    date=CVL(MID$(t$,9,4))
    alt=CVf(MID$(t$,13,4))
    depth=CVf(MID$(t$,17,4))
    new=ASC(MID$(t$,21,1))
    IF new
      CLR vv,vh,vx,vy,vz,dist
    ELSE
      dist=@distance(ox,oy,x,y)*1000
      dt=date-odate
      dx=@distance(ox,y,x,y)*1000
      dy=@distance(x,oy,x,y)*1000
      IF dt
        vz=INT((alt-oalt)/dt*100)/100
        vx=INT(dx/dt*100)/100
        vy=INT(dy/dt*100)/100
        vh=dist/dt*3.6
        vv=(alt-oalt)/dt*3.6
      ELSE
        CLR vx,vy,vz,vh,vv
      ENDIF
    ENDIF
    odate=date
    ox=x
    oy=y
    oalt=alt

    v=sqrt(vh*vh+vv*vv)
    PRINT #11,STR$(i)+" "+STR$(laenge)+" "+STR$(date)+" "+STR$(alt)+" "+STR$(v)
    ADD laenge,dist
  NEXT i
  CLOSE #11
  OPEN "O",#11,gnutmp$
  PRINT #11,"set multi"
  PRINT #11,"set grid"
  PRINT #11,"set xlabel "+CHR$(34)+"Distanz [km]"+CHR$(34)
  PRINT #11,"set ylabel "+CHR$(34)+"Hoehe [m]"+CHR$(34)
  PRINT #11,"set y2label "+CHR$(34)+"Geschwindigkeit [km/h]"+CHR$(34)
  PRINT #11,"set origin 0,0"
  PRINT #11,"set size 1,0.5"
  PRINT #11,"plot [:][-10:200] "+CHR$(34)+dattmp$+CHR$(34)+" u ($2/1000):4 t ";
  PRINT #11,CHR$(34)+"Hoehe"+CHR$(34)+" w steps , ";
  PRINT #11,CHR$(34)+dattmp$+CHR$(34)+" u ($2/1000):($5) t ";
  PRINT #11,CHR$(34)+"Geschwindigkeit"+CHR$(34)+" w steps"
  PRINT #11,"set origin 0,0.5"
  PRINT #11,"set xlabel "+CHR$(34)+"Zeit [h]"+CHR$(34)
  PRINT #11,"plot [:][-10:200] "+CHR$(34)+dattmp$+CHR$(34)+" u ($3/3600):4 t ";
  PRINT #11,CHR$(34)+"Hoehe"+CHR$(34)+" w steps , ";
  PRINT #11,CHR$(34)+dattmp$+CHR$(34)+" u ($3/3600):5 t ";
  PRINT #11,CHR$(34)+"Geschwindigkeit"+CHR$(34)+" w steps"
  PRINT #11,"set nomulti"
  PRINT #11,"pause -1"
  CLOSE #11
  DEFMOUSE 0
  SHOWPAGE
  SYSTEM "gnuplot "+gnutmp$
  SYSTEM "rm -f "+gnutmp$+" "+dattmp$
RETURN

FUNCTION waypointselect(info$)
  LOCAL i,n$,a$
  DIM sel$(anzwp-1)
  FOR i=0 TO anzwp-1
    n$=MID$(waypoint$(i),49,LEN(waypoint$(i))-49)
    SPLIT n$,CHR$(0),0,n$,a$
    sel$(i)=STR$(i)+": 0x"+HEX$(cvi(MID$(waypoint$(i),5,2)),4,4,1)+" "+n$+CHR$(0)
  NEXT i
  RETURN listselect(info$,sel$())
ENDFUNCTION
FUNCTION polyselect(info$)
  LOCAL i,n$,a$,anz
  DIM sel$(anzpoly-1)
  FOR i=0 TO anzpoly-1
    anz=cvi(MID$(polygon$(i),23,2))
    n$=MID$(polygon$(i),25+anz*8,LEN(polygon$(i))-49)
    SPLIT n$,CHR$(0),0,n$,a$
    sel$(i)=STR$(i)+": 0x"+HEX$(ASC(MID$(polygon$(i),1,1)) AND 255,2,2,1)+" "+STR$(anz)+" "+n$+CHR$(0)
  NEXT i
  RETURN listselect(info$,sel$())
ENDFUNCTION

PROCEDURE gotowp
  LOCAL wpid,x,y,nxmin,nymin
  IF anzwp>0
    wpid=@waypointselect("Goto Waypoint ...")
    IF wpid>=0
      x=180/2^31*CVL(MID$(waypoint$(wpid),25,4))
      y=180/2^31*CVL(MID$(waypoint$(wpid),29,4))
      nxmin=y-(xmax-xmin)/2
      nymin=x-(ymax-ymin)/2
      xmax=nxmin+(xmax-xmin)
      ymax=nymin+(ymax-ymin)
      xmin=nxmin
      ymin=nymin
      @sort_wp
      @redraw
      @showrange
    ENDIF
  ELSE
    ~form_alert(1,"[3][No waypoint defined so far!][OK]")
  ENDIF
RETURN
PROCEDURE gotopoly
  LOCAL wpid,x,y,nxmin,nymin
  IF anzpoly>0
    wpid=@polyselect("Goto Map element ...")
    IF wpid>=0
      IF cvf(MID$(polygon$(wpid),11,4))-cvf(MID$(polygon$(wpid),7,4))>0 AND cvf(MID$(polygon$(wpid),19,4))-cvf(MID$(polygon$(wpid),15,4))>0
        xmin=cvf(MID$(polygon$(wpid),7,4))
        xmax=cvf(MID$(polygon$(wpid),11,4))
        ymin=cvf(MID$(polygon$(wpid),15,4))
        ymax=cvf(MID$(polygon$(wpid),19,4))
      ELSE
        x=(cvf(MID$(polygon$(wpid),7,4))+cvf(MID$(polygon$(wpid),11,4)))/2
        y=(cvf(MID$(polygon$(wpid),15,4))+cvf(MID$(polygon$(wpid),19,4)))/2
        nxmin=x-(xmax-xmin)/2
        nymin=y-(ymax-ymin)/2
        xmax=nxmin+(xmax-xmin)
        ymax=nymin+(ymax-ymin)
        xmin=nxmin
        ymin=nymin
      ENDIF
      @sort_wp
      @redraw
      @showrange
    ENDIF
  ELSE
    ~form_alert(1,"[3][No map element defined so far!][OK]")
  ENDIF
RETURN

PROCEDURE deletewp
  LOCAL i,n$,a$
  IF anzwp>0
    i=@waypointselect("Delete Waypoint:")
    IF i>=0
      PRINT "delete "+sel$(i)
      waypoint$(i)=waypoint$(anzwp-1)
      DEC anzwp
      @sort_wp
      @redraw
      @showrange
    ENDIF
  ELSE
    ~form_alert(1,"[3][No waypoint defined so far!][OK]")
  ENDIF
RETURN
PROCEDURE editwp
  LOCAL i,n$,a$
  IF anzwp>0
    i=@waypointselect("Select Waypoint:")
    IF i>=0
      @doeditwp(i)
    ENDIF
  ELSE
    ~form_alert(1,"[3][No waypoint defined so far!][OK]")
  ENDIF
RETURN
PROCEDURE editpoly
  LOCAL i,n$,a$
  IF anzpoly>0
    i=@polyselect("Select Map element:")
    IF i>=0
      @doeditpoly(i)
    ENDIF
  ELSE
    ~form_alert(1,"[3][No map element defined so far!][OK]")
  ENDIF
RETURN
PROCEDURE deletepoly
  LOCAL i,j
  IF anzpoly>0
    i=@polyselect("Delete Map element:")
    IF i>=0
      PRINT "delete "+sel$(i)
      polygon$(i)=polygon$(anzpoly-1)
      FOR j=i TO anzpoly-2
        polygon$(j)=polygon$(j+1)
      NEXT j
      DEC anzpoly
      @redraw
      @showrange
    ENDIF
  ELSE
    ~form_alert(1,"[3][No map element defined so far!][OK]")
  ENDIF
RETURN
PROCEDURE select_track_piece(info$)
  LOCAL i,date,os,t$
  ' Hier die Anzahl der zusammenhaengenden Stuecke bestimmen.
  anzt=0
  FOR i=0 TO anztrack-1
    new=ASC(MID$(track$(i),21,1))
    IF i=0
      new=1
    ENDIF
    IF new
      INC anzt
    ENDIF
  NEXT i
  DIM sel$(anzt-1),anf(anzt-1),tlen(anzt-1)
  PRINT anzt;" Zusammenhaengende Stuecke."
  anzt=0
  FOR i=0 TO anztrack-1
    t$=track$(i)
    new=ASC(MID$(t$,21,1))
    IF i=0
      anf(0)=0
      INC anzt
      os=0
      date=CVL(MID$(t$,9,4))
      new=0
    ENDIF
    IF new
      tlen(anzt-1)=i-os
      sel$(anzt-1)=STR$(anzt-1)+": "+STR$(os)+"-"+STR$(i-1)+" "+@jdate$(date)+CHR$(0)
      date=CVL(MID$(t$,9,4))
      anf(anzt)=i
      INC anzt
      os=i
    ENDIF
  NEXT i
  tlen(anzt-1)=i-os
  sel$(anzt-1)=STR$(anzt-1)+": "+STR$(os)+"-"+STR$(i-1)+" "+@jdate$(date)+CHR$(0)
  INC anzt
  track_select=listselect(info$,sel$())
RETURN

PROCEDURE deletetrack
  LOCAL i,j,anzt
  IF anztrack>0
    @select_track_piece("Delete Track:")
    IF track_select>=0
      PRINT "delete "+sel$(track_select)
      PRINT anf(track_select),tlen(track_select)
      FOR j=anf(track_select) TO anztrack-1-tlen(track_select)
        track$(j)=track$(j+tlen(track_select))
      NEXT j
      SUB anztrack,tlen(track_select)
      @redraw
      @showrange
    ENDIF
  ELSE
    ~form_alert(1,"[3][No track defined so far!][OK]")
  ENDIF
RETURN
PROCEDURE optimizetrack
  LOCAL i,j,anzt,alt,y1,x1,x2,x3,y3,y2,t$,oa,oe,c1,c2,c3,diff
  IF anztrack>0
    @select_track_piece("Select Track:")
    IF track_select>=0
      IF tlen(track_select)>2
        DIM rflags(anztrack)
        ARRAYFILL rflags(),0
        oa=anf(track_select)
        oe=oa+tlen(track_select)-1
        c1=oa
        c2=oa+1
        c3=oa+2
        DO
          t$=track$(c1)
          y1=180/2^31*CVL(MID$(t$,1,4))
          x1=180/2^31*CVL(MID$(t$,5,4))
          t$=track$(c2)
          y2=180/2^31*CVL(MID$(t$,1,4))
          x2=180/2^31*CVL(MID$(t$,5,4))
          t$=track$(c3)
          y3=180/2^31*CVL(MID$(t$,1,4))
          x3=180/2^31*CVL(MID$(t$,5,4))
          diff=@distance(x2,y2,x3,y3)+@distance(x1,y1,x2,y2)-@distance(x1,y1,x3,y3)
          IF diff<0.0005
            PRINT "removed #";c2
            rflags(c2)=1
          ELSE
            INC c1
          ENDIF
          INC c2
          INC c3
          EXIT if c3=oe
        LOOP
        CLR i,j
        DO
          WHILE rflags(j)=1 AND j<anztrack
            PRINT "skip ";j
            INC j
          WEND
          track$(i)=track$(j)
          INC i
          INC j
          EXIT if j=anztrack
        LOOP
        anztrack=i
        @redraw
        @showrange
      ENDIF
    ENDIF
  ELSE
    ~form_alert(1,"[3][No track defined so far!][OK]")
  ENDIF
RETURN
PROCEDURE tracktopoly
  LOCAL i,j,anzt,alt,y,x,t$,oa,oe,p$,mx1,mx2,my1,my2
  IF anztrack>0
    @select_track_piece("Select Track:")
    IF track_select>=0
      oa=anf(track_select)
      oe=oa+tlen(track_select)-1
      alt=CVf(MID$(track$(oa),13,4))
      typ=form_alert(2,"[0][Bitte Track-Typ auswaehlen:][Fussweg|Strasse|Hauptstrasse|Autobahn|Bezirk]")-1
      IF typ=4
        typ=15
      ENDIF
      p$=CHR$(typ)+CHR$(0)+mkf$(alt)+mkf$(0)+mkf$(0)+mkf$(0)+mkf$(0)+mki$(tlen(track_select))
      PRINT oa,oe
      PRINT anztrack,tlen(track_select)
      mx1=1000
      mx2=-1000
      my1=1000
      my2=-1000
      FOR j=oa TO oe
        t$=track$(j)
        y=180/2^31*CVL(MID$(t$,1,4))
        x=180/2^31*CVL(MID$(t$,5,4))
        mx1=MIN(x,mx1)
        mx2=MAX(x,mx2)
        my1=MIN(y,my1)
        my2=MAX(y,my2)
        PRINT x,y
        p$=p$+mkf$(x)+mkf$(y)
      NEXT j
      PRINT "Boundingbox: ";mx1,mx2,my1,my2
      t$=mkf$(mx1)+mkf$(mx2)+mkf$(my1)+mkf$(my2)
      BMOVE varptr(t$),VARPTR(p$)+6,4*4
      p$=p$+sel$(track_select)+CHR$(0)
      polygon$(anzpoly)=p$
      INC anzpoly
      @redraw
      @showrange
    ENDIF
  ELSE
    ~form_alert(1,"[3][No track defined so far!][OK]")
  ENDIF
RETURN
PROCEDURE deleteallwp
  IF anzwp>0
    IF form_alert(2,"[3][Really delete all waypoints ?][YES|NO]")=1
      anzwp=0
      @redraw
      @showrange
    ENDIF
  ELSE
    ~form_alert(1,"[3][No waypoint defined so far!][OK]")
  ENDIF
RETURN
PROCEDURE deletealltrack
  IF anztrack>0
    IF form_alert(2,"[3][Really delete all tracks ?][YES|NO]")=1
      anztrack=0
      @redraw
      @showrange
    ENDIF
  ELSE
    ~form_alert(1,"[3][No tracks defined so far!][OK]")
  ENDIF
RETURN
PROCEDURE deletemap
  IF anzpoly>0
    IF form_alert(2,"[3][Really delete all map elements ?][YES|NO]")=1
      anzpoly=0
      @redraw
      @showrange
    ENDIF
  ELSE
    ~form_alert(1,"[3][No map elements defined so far!][OK]")
  ENDIF
RETURN
PROCEDURE doeditpoly(pid)
  LOCAL t$,typ,flags,alt,l1,l2,l3,l4,anz,name$,p$
  t$=polygon$(pid)
  typ=ASC(MID$(t$,1,1))
  flags=ASC(MID$(t$,2,1))
  alt=cvf(MID$(t$,3,4))
  l1=cvf(MID$(t$,7,4))
  l2=cvf(MID$(t$,11,4))
  l3=cvf(MID$(t$,15,4))
  l4=cvf(MID$(t$,19,4))
  anz=cvi(MID$(t$,23,2))
  name$=MID$(t$,25+anz*8,LEN(t$)-25-anz*8)
  name$=REPLACE$(name$,CHR$(0),".")

  t$="Edit map element #"+STR$(pid)+":|========================||"
  t$=t$+"Name:    "+CHR$(27)+name$+STRING$(MAX(32,LEN(name$))-len(name$),CHR$(10))+"|"
  t$=t$+"Breiten:  "+@breite$(l3)+" - "+@breite$(l4)+"|"
  t$=t$+"L�ngen:   "+@laenge$(l1)+" - "+@laenge$(l2)+"|"
  t$=t$+"Altitude (m):"+CHR$(27)+STR$(alt,5,5)+"|"
  t$=t$+"Typ:  "+CHR$(27)+"0x"+HEX$(typ,2,2)+"|"
  t$=t$+"flags:   "+CHR$(27)+"0x"+HEX$(flags,2,2)+"|"
  t$=t$+"Content:   "+STR$(anz)+" Points.|"
  PRINT replace$(REPLACE$(t$,CHR$(27),""),CHR$(10),"")
  ALERT 0,t$,1,"OK|CANCEL",a,f$
  IF a=1
    MEMDUMP varptr(f$),LEN(f$)
    f$=REPLACE$(f$,CHR$(10),"")
    SPLIT f$,CHR$(13),0,name$,f$
    SPLIT f$,CHR$(13),0,a$,f$
    alt=VAL(a$)
    SPLIT f$,CHR$(13),0,a$,f$
    typ=VAL(a$)
    SPLIT f$,CHR$(13),0,a$,f$
    flags=VAL(a$)
    p$=CHR$(typ)+CHR$(flags)+mkf$(alt)+MID$(polygon$(pid),7,4*4+2+8*anz)+name$+CHR$(0)
    polygon$(pid)=p$
    @redraw
  ENDIF
RETURN
PROCEDURE doeditwp(wpid)
  LOCAL color,class,x,y,alt,t$,name$,comment$
  t$="  "+waypoint$(wpid)
  class=ASC(MID$(t$,3,1))
  color=ASC(MID$(t$,4,1)) and 255
  displ=ASC(MID$(t$,5,1))
  attr=ASC(MID$(t$,6,1))
  styp=cvi(MID$(t$,7,2))
  y=180/2^31*CVL(MID$(t$,27,4))
  x=180/2^31*CVL(MID$(t$,31,4))
  alt=cvf(MID$(t$,35,4))
  depth=cvf(MID$(t$,39,4))
  dist=cvf(MID$(t$,43,4))
  name$=MID$(t$,51,LEN(t$)-51)
  name$=REPLACE$(name$,CHR$(0),"|")+"|"
  SPLIT name$,"|",0,name$,comment$
  SPLIT comment$,"|",0,comment$,facility$
  SPLIT facility$,"|",0,facility$,city$
  SPLIT city$,"|",0,city$,addr$
  SPLIT addr$,"|",0,addr$,crossr$
  IF crossr$="|"
    crossr$=""
  ENDIF
  state$=MID$(t$,47,2)
  cc$=MID$(t$,49,2)
  oldname$=name$
  again_edit:
  subclass$=HEX$(cvi(MID$(t$,9,2)),4,4,1)+HEX$(cvl(MID$(t$,11,4)),8,8,1)+HEX$(cvl(MID$(t$,15,4)),8,8,1)+HEX$(cvl(MID$(t$,19,4)),8,8,1)+HEX$(cvl(MID$(t$,23,4)),8,8,1)

  t$="Edit waypoint #"+STR$(wpid)+":|========================||"
  t$=t$+"Name:    "+CHR$(27)+name$+STRING$(MAX(10,LEN(name$))-len(name$),CHR$(10))+"|"
  t$=t$+"Breite:  "+CHR$(27)+@breite$(y)+"|"
  t$=t$+"L�nge:   "+CHR$(27)+@laenge$(x)+"|"
  t$=t$+"H�he (m):"+CHR$(27)+STR$(alt,5,5)+"|"
  t$=t$+"Tiefe:   "+CHR$(27)+STR$(depth,5,5)+"|"
  t$=t$+"Dist:    "+CHR$(27)+STR$(dist,5,5)+"|"
  t$=t$+"Symbol:  "+CHR$(27)+"0x"+HEX$(styp,4,4)+"|"
  t$=t$+"Class:   "+CHR$(27)+"0x"+HEX$(class,2,2)+"|"
  t$=t$+"Subclass:"+CHR$(27)+subclass$+"|"
  t$=t$+"Color:   "+CHR$(27)+"0x"+HEX$(color,2,2)+"|"
  t$=t$+"Displ:   "+CHR$(27)+STR$(displ,1,1)+"|"
  t$=t$+"Attr:    "+CHR$(27)+"0x"+HEX$(attr,2,2)+"|"
  t$=t$+"State:   "+CHR$(27)+state$+"|"
  t$=t$+"CC:      "+CHR$(27)+cc$+"|"

  t$=t$+"Comment:    "+CHR$(27)+comment$+STRING$(MAX(36,LEN(comment$))-len(comment$),CHR$(10))+"|"
  t$=t$+"Facility:   "+CHR$(27)+facility$+STRING$(32-len(facility$),CHR$(10))+"|"
  t$=t$+"City:       "+CHR$(27)+city$+STRING$(32-len(city$),CHR$(10))+"|"
  t$=t$+"Addr:       "+CHR$(27)+addr$+STRING$(32-len(addr$),CHR$(10))+"|"
  t$=t$+"Cross_road: "+CHR$(27)+crossr$+STRING$(32-len(crossr$),CHR$(10))+"|"
  '  print replace$(replace$(t$,chr$(27),""),chr$(10),"")
  ALERT 0,t$,1,"OK|CANCEL|DELETE|SEND TO GPS",a,f$
  IF a=3
    IF form_alert(2,"[3][Really delete Waypoint #"+STR$(wpid)+" ?|"+oldname$+"][OK|CANCEL]")=1
      waypoint$(wpid)=waypoint$(anzwp-1)
      DEC anzwp
      @sort_wp
      @redraw
      @showrange
    ENDIF
  ELSE if a=1 OR a=4
    MEMDUMP varptr(f$),100
    f$=REPLACE$(f$,CHR$(10),"")
    SPLIT f$,CHR$(13),0,name$,f$
    SPLIT f$,CHR$(13),0,posy$,f$
    SPLIT f$,CHR$(13),0,posx$,f$
    SPLIT f$,CHR$(13),0,a$,f$
    alt=VAL(a$)
    SPLIT f$,CHR$(13),0,a$,f$
    depth=VAL(a$)
    SPLIT f$,CHR$(13),0,a$,f$
    dist=VAL(a$)
    SPLIT f$,CHR$(13),0,a$,f$
    symbl=VAL(a$)
    SPLIT f$,CHR$(13),0,a$,f$
    class=VAL(a$)
    SPLIT f$,CHR$(13),0,subclass$,f$
    SPLIT f$,CHR$(13),0,a$,f$
    color=VAL(a$)
    SPLIT f$,CHR$(13),0,a$,f$
    displ=VAL(a$)
    SPLIT f$,CHR$(13),0,a$,f$
    attr=VAL(a$)
    SPLIT f$,CHR$(13),0,state$,f$
    SPLIT f$,CHR$(13),0,cc$,f$
    IF len(cc$)<>2
      cc$="  "
    ENDIF
    IF len(state$)<>2
      state$="  "
    ENDIF

    SPLIT f$,CHR$(13),0,comment$,f$
    SPLIT f$,CHR$(13),0,facility$,f$
    SPLIT f$,CHR$(13),0,city$,f$
    SPLIT f$,CHR$(13),0,addr$,f$
    SPLIT f$,CHR$(13),0,crossr$,f$
    y=VAL(MID$(posy$,2,2))+VAL(MID$(posy$,5,2))/60+VAL(MID$(posy$,8,4))/60/60
    x=VAL(MID$(posx$,2,3))+VAL(MID$(posx$,6,2))/60+VAL(MID$(posx$,9,4))/60/60
    IF UPPER$(LEFT$(posx$,1))="S"
      x=-x
    ENDIF
    IF UPPER$(LEFT$(posy$,1))="W"
      y=-y
    ENDIF
    PRINT "New: x=";x,"y=";y
    IF name$<>oldname$
      IF form_alert(1,"[2][Do you want to replace the old |waypoint "+oldname$+"?|Or do you want to keep it ?][REPLACE|KEEP IT]")=2
        wpid=anzwp
        INC anzwp
      ENDIF
      ' Testen, ob Name schon vergeben  ....
      oldwpid=wpid
      FOR i=0 TO anzwp-1
        IF name$+CHR$(0)=MID$(waypoint$(i),49,LEN(name$)+1)
          wpid=i
        ENDIF
      NEXT i
      IF wpid<>oldwpit
        IF form_alert(1,"[1][Name is already in Use !| Replace the Waypoint ?][YES|NO]")=2
          wpid=oldwpid
          GOTO again_edit
        ENDIF
        IF oldwpid<anzwp-1
          waypoint$(oldwpid)=waypoint$(anzwp-1)
          DEC anzwp
        ENDIF
      ENDIF
    ENDIF
    subclass$=mki$(0)+mkl$(0)+mkl$(-1)+mkl$(-1)+mkl$(-1)

    w$=CHR$(class)+CHR$(color)+CHR$(displ)+CHR$(attr)+mki$(symbl)+subclass$+mkl$(y/180*2^31)+mkl$(x/180*2^31)+mkf$(alt)+mkf$(depth)+mkf$(dist)+state$+cc$+name$+CHR$(0)+comment$+CHR$(0)+facility$+CHR$(0)+city$+CHR$(0)+addr$+CHR$(0)+crossr$+CHR$(0)
    IF len(w$)<250
      IF a=4
        IF form_alert(1,"[2][Wegpunkt "+name$+" |jetzt senden ?][Ja|Nein]")=1
          @sendmessage(27,mki$(1))
          @sendmessage(35,w$)
          @sendmessage(12,mki$(7))
        ENDIF
        GOTO again_edit
      ENDIF
      waypoint$(wpid)=w$
      @sort_wp
      @redraw
      @showrange
    ELSE
      ~form_alert(1,"[3][The Comments are too long!|Please use shorter comments !][OH]")
      GOTO again_edit
    ENDIF
  ENDIF
RETURN

PROCEDURE newwp
  LOCAL x,y,w$,name$,alt,depth,dist,subclass$,comment$,facility$
  ' Create a default Waypoint
  name$="new "+STR$(anzwp)
  x=10
  y=50
  alt=1e25
  depth=1e25
  dist=1e25
  subclass$=mki$(0)+mkl$(0)+mkl$(-1)+mkl$(-1)+mkl$(-1)
  comment$="Waypoint created by GPS-Earth"
  facility$="X11-Basic"
  w$=CHR$(0)+CHR$(255)+CHR$(0)+CHR$(0x60)+mki$(18)+subclass$+mkl$(y/180*2^31)+mkl$(x/180*2^31)+mkf$(alt)+mkf$(depth)+mkf$(dist)+"  "+"  "+name$+CHR$(0)+comment$+CHR$(0)+facility$+CHR$(0)+CHR$(0)+CHR$(0)+CHR$(0)
  waypoint$(anzwp)=w$
  INC anzwp
  @doeditwp(anzwp-1)
RETURN

PROCEDURE procalmanacdata(t$)
  LOCAL plen
  plen=ASC(MID$(t$,2,1))
  almanac$(anzal)=MID$(t$,3,plen)
  INC anzal
RETURN

PROCEDURE procsatstat(t$)
  LOCAL i
  LOCAL plen
  plen=ASC(MID$(t$,2,1))
  SETFONT normfont$
  COLOR grau
  PBOX 650,290,730,410
  FOR i=0 TO 11
    sv=ASC(MID$(t$,i*8+8,1))
    elev=ASC(MID$(t$,i*8+7,1))
    frac=cvi(MID$(t$,i*8+3,2))/2048
    db=cvi(MID$(t$,i*8+5,2))
    tracked=ASC(MID$(t$,i*8+9,1))
    flag=ASC(MID$(t$,i*8+10,1)) and 255
    IF tracked
      COLOR weiss
    ELSE
      COLOR rot
    ENDIF
    IF sv=-1
      TEXT 650,300+i*10,"--"
    ELSE
      TEXT 650,300+i*10,STR$(sv+1,2,2,1)
      BOX 670,294+i*10,670+db/10000*50,303+i*10
      COLOR schwarz
      PBOX 670,295+i*10,670+db/10000*50,302+i*10
      COLOR weiss
      TEXT 680,300+i*10,HEX$(flag,2,2,1)+" "+HEX$(tracked,2,2,1)
    ENDIF
  NEXT i
  SHOWPAGE
RETURN

' Process a GARMIN protocol message
' many of these codes are inofficial, so this is partially very experimental

PROCEDURE procmessage(t$)
  LOCAL pid,plen
  IF len(t$)
    pid=ASC(LEFT$(t$,1))
    plen=ASC(MID$(t$,2,1))
    IF pid=0
      PRINT "PID 0: empfangen. TEST mode $"+HEX$(cvl(MID$(t$,3,4)))
    ELSE IF pid=1
      PRINT "PID 1: Signal amplitude: $"+HEX$(cvl(MID$(t$,3,4)))
    ELSE IF pid=6
      ' ACK, alles OK, ignorieren
      @status(1,"OK")
    ELSE IF pid=9
      PRINT "PID 9: Unknown "+STR$(cvi(MID$(t$,3,2)))
    ELSE IF pid=10
      PRINT "PID 10: ";
      request=cvi(MID$(t$,3,2))
      IF request=0
        PRINT "TRANSFER CANCEL !"
      ELSE if request=1
        PRINT "ALMANAC query !"
      ELSE if request=2
        PRINT "position query !"
      ELSE if request=3
        PRINT "proximity waypoint query !"
      ELSE if request=4
        PRINT "route query !"
      ELSE if request=5
        PRINT "query time !"
      ELSE if request=6
        PRINT "query track download !"
      ELSE if request=7
        PRINT "query waypoint download !"
      ELSE if request=8
        PRINT "power off query !"
      ELSE if request=11
        PRINT "beep & power off query !"
      ELSE
        PRINT "Unknown request: "+STR$(cvi(MID$(t$,3,2)))
      ENDIF
    ELSE IF pid=12
      @procxfercmplt(t$)
      @do_hscaler(sx+670,sy+170,100,1)
    ELSE IF pid=13
      PRINT "PID 13: EVENT "+STR$(cvi(MID$(t$,3,2)))+" CLK="+STR$(cvl(MID$(t$,7,4)))
      a=cvi(MID$(t$,3,2))
      IF a=5
        PRINT "Sateliite #"+STR$(cvi(MID$(t$,5,2)))+" acquired"
      ELSE if a=7
        PRINT "Navigation Quality "+STR$(cvi(MID$(t$,5,2)))+"D"
      ELSE if a=12
        PRINT "Keyboadr event"+STR$(cvi(MID$(t$,5,2)))
      ELSE if a=16
        PRINT "Screen message"+STR$(cvi(MID$(t$,5,2)))
        PRINT screenmsg$(cvi(MID$(t$,5,2)))
        ALERT 1,"GPS-Message: |"+screenmsg$(cvi(MID$(t$,5,2))),1,"OK",dummy
      ELSE if a=18
        PRINT "Power on sequence"+STR$(cvi(MID$(t$,5,2)))
      ELSE if a=49
        PRINT "MAP redraw event "+STR$(cvi(MID$(t$,5,2)))
      ELSE
        PRINT "unknown"
      ENDIF
    ELSE IF pid=14
      @proctimedata(t$)
    ELSE IF pid=17 OR pid=24
      @procpositiondata(t$)
    ELSE IF pid=19
      PRINT "Paket 18 proximity data: PRN=";ASC(MID$(t$,3+3,1));", TOW=";cvd(MID$(t$,3+18,8));", Psydorange: ";cvd(MID$(t$,3+26,8))
    ELSE IF pid=21
      @status(1,"NAK !")
    ELSE IF pid=22
      PRINT "MESG22: Sat #";ASC(MID$(t$,3+20,1))+1
    ELSE IF pid=26
      @procsatstat(t$)
    ELSE IF pid=27
      @procrecords(t$)
      @do_hscaler(sx+670,sy+170,100,0)
    ELSE IF pid=28
      PRINT "PID 28: Request for EnableAsyncEvents"
    ELSE IF pid=29
      @procroutename(t$)
    ELSE IF pid=30
      @procroutewpt(t$)
    ELSE IF pid=31
      @procalmanacdata(t$)
      @do_hscaler(sx+670,sy+170,100,anzal/records)
    ELSE IF pid=32
      PRINT "Software Version: "+MID$(t$,3,7)
    ELSE IF pid=34
      @proctrackdata(t$)
      @do_hscaler(sx+670,sy+170,100,anztrack/records)
    ELSE IF pid=35
      @procwpdata(t$)
      @do_hscaler(sx+670,sy+170,100,anzwp/records)
    ELSE IF pid=39
      @proctemperature(t$)
    ELSE IF pid=40
      @procvoltage(t$)
    ELSE IF pid=42
      PRINT "PID 42: Satellite select"
    ELSE IF pid=51
      @procpvtdata(t$)
    ELSE IF pid=54
      PRINT "Message 54: Sat#";ASC(MID$(t$,3+8,1))+1;": C(50Hz)=";cvl(MID$(t$,3,4));" UK=<";
      FOR i=0 TO 3
        PRINT hex$(ASC(MID$(t$,3+4+i,4)) AND 255,2,2,1);" ";
      NEXT i
      PRINT ">"
    ELSE IF pid=56
      PRINT "MSG56: SAT #";ASC(MID$(t$,36+3,1))+1;" "
    ELSE IF pid=57
      PRINT "SAT #";ASC(MID$(t$,34+3,1))+1;" ACQUIRED PSEUDORANGE"
    ELSE IF pid=58
      a=cvl(MID$(t$,3,4))
      PRINT "BUTTON ";HEX$(a,4,4,1);": ";
      IF a AND 1
        PRINT "UP ";
      ENDIF
      IF a AND 2
        PRINT "DOWN ";
      ENDIF
      IF a AND 4
        PRINT "OK ";
      ENDIF
      IF a AND 8
        PRINT "PAGE ";
      ENDIF
      IF a AND 16
        PRINT "ON/OFF ";
      ENDIF
      PRINT
    ELSE IF pid=98
      @procroutelink(t$)
    ELSE IF pid=99
      @proctrackname(t$)
    ELSE IF pid=69
      @procscanline(t$)
    ELSE if pid AND 255=248
      ' Extended product information
      PRINT "Extended product information received:"
      a$=t$
      WHILE len(a$)
        SPLIT a$,CHR$(0),0,b$,a$
        PRINT ">";b$
      WEND
    ELSE IF pid=-3
      @procprotocollarray(t$)
    ELSE IF pid=-2
      PRINT "PID=254, Identification request!"
      ' Identifiaction. Wir senden unsere eigene Kennung zurueck:
      @sendmessage(255,mki$(0x17)+mki$(0xdd)+"GPS-Earth V.1.00 (c) Markus Hoffmann"+chr$(0))
      @sendmessage(248,"This is the GARMIN frontend for Linux !"+chr$(0))
      cap$="A"+mki$(10)+"A"+mki$(100)+"A"+mki$(200)+"A"+mki$(201)+"A"+mki$(300)
      cap$=cap$+"A"+mki$(301)+"A"+mki$(500)+"A"+mki$(600)+"A"+mki$(700)+"A"+mki$(800)
      cap$=cap$+"D"+mki$(108)+"D"+mki$(600)+"D"+mki$(700)+"D"+mki$(800)   ! Diese Liste muss noch vervollstaendigt werden ....
      @sendmessage(253,cap$)
    ELSE IF pid=-1
      @procproductdata(t$)
    ELSE
      @status(2,"? Paket pid="+STR$(pid)+" !")
      PRINT "? Paket pid="+STR$(pid)+" !  LEN=";plen
      t$=MID$(t$,3,plen)
      @procunknown(t$)

    ENDIF
  ENDIF
RETURN
PROCEDURE procunknown(t$)
  LOCAL i,gruba,grubb,plen
  plen=LEN(t$)
  FOR i=0 TO LEN(t$)-1
    PRINT hex$(ASC(MID$(t$,i+1,1)) AND 255,2,2,1)'
  NEXT i
  PRINT " "+CHR$(34);
  FOR i=0 TO LEN(t$)-1
    a=ASC(MID$(t$,i+1,1)) and 255

    IF a<32 OR a>asc("z")
      a=ASC(".")
    ENDIF
    PRINT chr$(a);
  NEXT i
  PRINT chr$(34)
  FOR i=0 TO plen/4-1
    gruba=cvl(MID$(t$,i*4+1,4))
    grubb=cvf(MID$(t$,i*4+1,4))
    PRINT i;": ";HEX$(gruba,8,8,1);" ";grubb
  NEXT i
RETURN
PROCEDURE procroutelink(pt$)
  LOCAL a,i
  PRINT "ROUTE: LINK=";
  a=cvi(MID$(pt$,3,2))
  IF a=0
    PRINT "LINE ";
  ELSE if a=1
    PRINT "LINK ";
  ELSE if a=2
    PRINT "NET ";
  ELSE if a=3
    PRINT "DIRECT ";
  ELSE if a=255
    PRINT "SNAP ";
  ELSE
    PRINT "UNKNOWN ";
  ENDIF
  PRINT "SUBCLASS=";
  FOR i=0 TO 17
    a=ASC(MID$(pt$,5+i,1))
    PRINT hex$(a,2,2,1);
  NEXT i
  PRINT " ";
  PRINT "IDENT="+CHR$(34);MID$(pt$,5+18,ASC(MID$(pt$,2,1))-5-18)+CHR$(34)
RETURN
PROCEDURE procpositiondata(pt$)
  oposx=posx
  oposy=posy
  posy=CVD(MID$(pt$,3,8))*180/pi
  posx=CVD(MID$(pt$,11,8))*180/pi
  PRINT "Position: ",posx,posy,@breite$(posy),@laenge$(posx)
  COLOR schwarz
  PBOX 8*89,16*28,8*100,16*30
  COLOR grau
  BOX 8*89,16*28,8*100,16*30
  COLOR gelb
  SETFONT normfont$
  TEXT 90*8,16*29-2,@breite$(posy)
  TEXT 89*8,16*30-2,@laenge$(posx)
  COLOR rot
  IF posx>xmin AND posx<xmax AND posy>ymin AND posy<ymax
    @menneken(@kx(posx),@ky(posy))
  ENDIF
  ' CIRCLE @kx(posx),@ky(posy),4
  ' LINE @kx(posx)-5,@ky(posy),@kx(posx)+5,@ky(posy)
  ' LINE @kx(posx),@ky(posy)-5,@kx(posx),@ky(posy)+5
  SHOWPAGE

RETURN

PROCEDURE procproductdata(ut$)
  garminPID=CVI(MID$(ut$,3,2))
  garminVER=CVI(MID$(ut$,5,2))
  garminMES$=MID$(t$,7,LEN(ut$)-7-1)
  garminmes$=REPLACE$(garminmes$,CHR$(0),"|")
  SETFONT helveticasmallfont$
  TEXT sx+sw-19*8,sy+160,garminMES$
RETURN

PROCEDURE procprotocollarray(t$)
  LOCAL plen,anz
  plen=ASC(MID$(t$,2,1))
  anz=plen/3
  PRINT "Es werden folgende ";anz;" Protokolle unterstuetzt:"
  FOR i=0 TO anz-1
    PRINT MID$(t$,3+i*3,1);CVI(MID$(t$,3+i*3+1,2));" ";
  NEXT i
RETURN

PROCEDURE procrecords(t$)
  records=CVI(MID$(t$,3,2))
  @status(3,"load "+STR$(records)+"...")
RETURN

PROCEDURE procroutename(t$)
  COLOR schwarz
  PBOX 8*20,16*31,8*35,16*32
  COLOR gelb
  TEXT 8*20,16*32-2,"Route: "+MID$(t$,3,ASC(MID$(t$,2,1))-1)+"  "
  PRINT "Route: "+MID$(t$,3,ASC(MID$(t$,2,1))-1)+"  "
  SHOWPAGE
RETURN
PROCEDURE procpvtdata(t$)
  LOCAL alt,epe,eph,epv,i,fix,tow,posx,posy,s$,speed

  alt=cvf(MID$(t$,3,4))
  epe=cvf(MID$(t$,7,4))/2
  eph=cvf(MID$(t$,11,4))/2
  epv=cvf(MID$(t$,15,4))/2
  fix==ASC(MID$(t$,19,1))*256+ASC(MID$(t$,20,1))
  tow=cvd(MID$(t$,21,8))
  posy=cvd(MID$(t$,29,8))*180/pi
  posx=cvd(MID$(t$,37,8))*180/pi
  veast=cvf(MID$(t$,45,4))
  vnorth=cvf(MID$(t$,49,4))
  vup=cvf(MID$(t$,53,4))
  mslhght=cvf(MID$(t$,57,4))
  leap=cvi(MID$(t$,61,2))
  wndays=cvl(MID$(t$,63,4))
  PRINT "fix=";fix,
  PRINT "tow=";tow,
  PRINT "mslhgt=";mslhgt,
  PRINT "leap=";leap,
  PRINT "wndays=";wndays

  ' im Log speichern
  IF anzpvt=0
    OPEN "A",#6,"PVT-"+STR$(wndays)+".dat"
    OPEN "A",#7,"position"+STR$(wndays)+".dat"
  ENDIF
  INC anzpvt
  s$=CHR$(ASC(MID$(t$,2,1)))+MID$(t$,3,plen)
  BPUT #6,VARPTR(s$),LEN(s$)

  COLOR hellgrau
  CIRCLE @kx(oposx),@ky(oposy),4
  LINE @kx(oposx)-5,@ky(oposy),@kx(oposx)+5,@ky(oposy)
  LINE @kx(oposx),@ky(oposy)-5,@kx(oposx),@ky(oposy)+5
  COLOR schwarz
  PBOX 8*69,16*26,8*80,16*28
  PBOX 8*59,16*26,8*69,16*30
  PBOX 8*49,16*26,8*59,16*30
  PBOX 8*39,16*26,8*49,16*30

  COLOR grau
  BOX 8*69,16*26,8*80,16*28
  BOX 8*59,16*28,8*69,16*30
  BOX 8*59,16*26,8*69,16*28
  BOX 8*49,16*26,8*59,16*30
  BOX 8*39,16*26,8*49,16*30
  CIRCLE 8*54,16*28,32
  SETFONT normfont$
  TEXT 8*54-32-8,16*28+5,"W"
  TEXT 8*54+32,16*28+5,"E"

  COLOR gelb
  TEXT 60*8,16*29-2,"H�he:"
  TEXT 60*8,16*30-2,STR$(INT(alt),5,5)+" m"
  TEXT 60*8,16*27-2,"speed:"
  speed=sqrt(veast^2+vnorth^2+vup^2)
  TEXT 60*8,16*28-2,STR$(INT(speed),4,4)+" km/h"
  TEXT 40*8,16*27-2,"Genauigk:"
  TEXT 40*8,16*28-2,"x,y: "+STR$(INT(eph),3,3)+" m"
  TEXT 40*8,16*29-2,"h:   "+STR$(INT(epv),3,3)+" m"
  TEXT 40*8,16*30-2,"ges: "+STR$(INT(epe),3,3)+" m"
  TEXT 70*8,16*27-2,@breite$(posy)
  TEXT 69*8,16*28-2,@laenge$(posx)
  IF speed>0
    DEFLINE ,3
    LINE 8*54,16*28,8*54+32*veast/speed,16*28+32*vnorth/speed
  ENDIF
  DEFLINE ,1

  posx=-posx
  COLOR rot
  CIRCLE @kx(posx),@ky(posy),4
  LINE @kx(posx)-5,@ky(posy),@kx(posx)+5,@ky(posy)
  LINE @kx(posx),@ky(posy)-5,@kx(posx),@ky(posy)+5
  SHOWPAGE
  PRINT #7,tow'-posx'posy'eph'alt+mslhgt'epv
  FLUSH #7
RETURN

PROCEDURE proctimedata(t$)
  LOCAL month,day,year,hour,minute,second
  month=ASC(MID$(t$,3,1))
  day=ASC(MID$(t$,4,1))
  year=CVI(MID$(t$,5,2))
  hour=CVI(MID$(t$,7,2))
  minute=ASC(MID$(t$,9,1))
  second=ASC(MID$(t$,10,1))
  ndate$=STR$(day,2,2)+"."+STR$(month,2,2,1)+"."+STR$(year,4,4)
  ntime$=STR$(hour,2,2)+":"+STR$(minute,2,2,1)+":"+STR$(second,2,2,1)
  COLOR schwarz
  PBOX 8*89,16*30,8*100,16*32
  COLOR grau
  BOX 8*89,16*30,8*100,16*32
  COLOR gelb
  SETFONT normfont$
  TEXT 89*8,16*31-2,ndate$
  TEXT 90*8,16*32-2,ntime$
  SHOWPAGE
RETURN
PROCEDURE procvoltage(t4)
  LOCAL u
  u=cvi(MID$(t$,3,2))/100
  COLOR grau
  PBOX 0,420,60,436
  COLOR gelb
  SETFONT normfont$
  TEXT 2,433,"U="+STR$(u)+" V"
  ' print "Voltage: ";cvi(mid$(t$,3,2))/100;" V"
  SHOWPAGE
RETURN
PROCEDURE proctemperature(t4)
  LOCAL t
  t=cvi(MID$(t$,3,2))
  COLOR grau
  PBOX 60,420,110,436
  COLOR gelb
  SETFONT normfont$
  TEXT 62,433,"T="+STR$(t)+" �C"
  SHOWPAGE
RETURN
PROCEDURE procscanline(t$)
  LOCAL plen,bmflag,offset
  plen=ASC(MID$(t$,2,1))
  t$=MID$(t$,3,plen)
  bmflag=LPEEK(VARPTR(t$)+0)
  ' print "BMFLAG=";bmflag
  ' print "PLEN=";plen
  IF bmflag=0
    screenplanes=LPEEK(VARPTR(t$)+12)
    screenwidth=LPEEK(VARPTR(t$)+16)
    sht=LPEEK(VARPTR(t$)+20)
    screenbytes=screenplanes*screenwidth/8*sht
    PRINT hex$(cvl(MID$(t$,5,4)))'cvl(MID$(t$,9,4))'
    '  print cvl(mid$(t$,13,4)),cvl(mid$(t$,17,4)),cvl(mid$(t$,21,4))'
    PRINT hex$(cvl(MID$(t$,25,4))),cvl(MID$(t$,29,4)),cvl(MID$(t$,33,4))'
    PRINT cvl(MID$(t$,37,4))
    PRINT "Planes: ";screenplanes
    PRINT "Dimension: "+STR$(screenwidth)+"x"+STR$(sht)
    PRINT "Bytes: ";screenplanes*screenwidth/8*sht
    escreenbuf$=SPACE$(screenplanes*screenwidth/8*sht)
  ELSE
    offset=cvl(MID$(t$,5,4))
    @progress(sht,anzscanline)
    '   print "OFFSET=";offset
    BMOVE varptr(t$)+8,VARPTR(escreenbuf$)+offset,screenwidth/8*screenplanes
    INC anzscanline
    '  print anzscanline
    IF anzscanline=sht
      '    bsave "escreen",varptr(escreenbuf$),len(escreenbuf$)
      PRINT
      @show_screenshot
    ENDIF
  ENDIF
RETURN

PROCEDURE proctrackdata(t$)
  LOCAL plen,x,y,s$,date,alt,depth,new
  plen=ASC(MID$(t$,2,1))
  track$(anztrack)=MID$(t$, 3, plen)
  IF anztrackl
    trackl2(anztrackl-1)=anztrack
  ENDIF
  INC anztrack
  y=180/2^31*CVL(MID$(t$,3,4))
  x=180/2^31*CVL(MID$(t$,7,4))
  date=CVL(MID$(t$,11,4))
  alt=CVf(MID$(t$,15,4))
  depth=CVf(MID$(t$,19,4))
  new=ASC(MID$(t$,23,1))
  IF x>xmin AND x<xmax AND y>ymin AND y<ymax
    IF left$(STR$(alt))="5"
      COLOR hellblau
    ELSE if LEFT$(STR$(alt))="4"
      COLOR weiss
    ELSE if LEFT$(STR$(alt))="3"
      COLOR rot
    ELSE if LEFT$(STR$(alt))="2"
      COLOR orange
    ELSE if LEFT$(STR$(alt))="1"
      COLOR gelb
    ELSE if alt>50
      COLOR gruen
    ELSE
      COLOR lila
    ENDIF
    IF alt<50
      COLOR blau
    ENDIF
    IF new
      PLOT @kx(x),@ky(y)
    ELSE
      LINE @kx(otx),@ky(oty),@kx(x),@ky(y)
    ENDIF
    IF new
      PCIRCLE @kx(x),@ky(y),2
    ELSE
      CIRCLE @kx(x),@ky(y),1
    ENDIF
  ENDIF
  otx=x
  oty=y
RETURN

PROCEDURE proctrackname(t$)
  LOCAL dlen,disp,col
  dlen=ASC(MID$(t$,2,1))
  disp=ASC(MID$(t$,3,1))
  col=ASC(MID$(t$,4,1))
  name$=MID$(t$,5,dlen-3)
  tracklist$(anztrackl)=name$
  trackl1(anztrackl)=anztrack
  trackl2(anztrackl)=anztrack
  INC anztrackl
  COLOR schwarz
  PBOX 8*2,16*31,8*25,16*32
  COLOR gelb,schwarz
  TEXT 8*2,16*32-1,"Track: "+MID$(t$,5,dlen-3)+"   "
RETURN

PROCEDURE legende
  COLOR schwarz,schwarz
  PBOX sx+sw-19*8,by,sx+sw,by+120
  COLOR blau
  PBOX sx+sw-32,by,sx+sw,by+16
  COLOR gruen
  PBOX sx+sw-32,by+16,sx+sw,by+32
  COLOR gelb
  PBOX sx+sw-32,by+32,sx+sw,by+48
  COLOR orange
  PBOX sx+sw-32,by+48,sx+sw,by+64
  COLOR rot
  PBOX sx+sw-32,by+64,sx+sw,by+80
  COLOR weiss
  PBOX sx+sw-32,by+80,sx+sw,by+96
  COLOR hellblau
  PBOX sx+sw-32,by+96,sx+sw,by+112

  COLOR weiss
  BOX sx+sw-32,by,sx+sw,by+16
  BOX sx+sw-32,by+16,sx+sw,by+32
  BOX sx+sw-32,by+32,sx+sw,by+48
  BOX sx+sw-32,by+48,sx+sw,by+64
  BOX sx+sw-32,by+80,sx+sw,by+96
  BOX sx+sw-32,by+96,sx+sw,by+112
  SETFONT helveticasmallfont$
  TEXT sx+sw-66,by+10,"< 50 m"
  TEXT sx+sw-78,by+16+10,"50-100 m"
  TEXT sx+sw-85,by+32+10,"100-200 m"
  TEXT sx+sw-85,by+48+10,"200-300 m"
  TEXT sx+sw-85,by+64+10,"300-400 m"
  TEXT sx+sw-85,by+80+10,"400-500 m"
  TEXT sx+sw-70,by+96+10,"> 500 m"
RETURN

PROCEDURE menneken(x,y)
  PRINT x,y
  IF len(omenneken$)
    PUT oldmennekenx,oldmennekeny,omenneken$
  ENDIF
  CLR omenneken$
  GET x-8,y-8,x+8,y+8,omenneken$
  oldmennekenx=x-8
  oldmennekeny=y-8
  GRAPHMODE 4
  PUT_BITMAP maenneken$,x-8,y-8,16,16
  GRAPHMODE 1
RETURN

PROCEDURE norden(winkel)
  DIM nordx(5),nordy(5)
  nordx(0)=0
  nordy(0)=0
  nordx(1)=8
  nordy(1)=26
  nordx(2)=0
  nordy(2)=20
  nordx(3)=-8
  nordy(3)=26
  nordx(4)=0
  nordy(4)=0
  COLOR weiss
  POLYFILL 5,nordx(),nordy(),bx+16,by
  COLOR schwarz,weiss
  POLYLINE 5,nordx(),nordy(),bx+16,by
  SETFONT smallfont$
  TEXT bx+14,by+20,"N"
RETURN

PROCEDURE savegrafik
  FILESELECT "save screen ...","./*.xwd","output.xwd",f$
  IF len(f$)
    IF exist(f$)
      IF form_alert(2,"[1][File already exists !|Replace ?][Yes|CANCEL]")=1
        DEFMOUSE 2
        SHOWPAGE
        ' bsave f$,varptr(escreen$),len(escreen$)
        @savexwd(f$)
        DEFMOUSE 0
      ENDIF
    ELSE
      DEFMOUSE 2
      SHOWPAGE
      @savexwd(f$)
      DEFMOUSE 0
    ENDIF
  ENDIF
  SHOWPAGE
RETURN
PROCEDURE redraw
  LOCAL i,x,y,t$
  DEFMOUSE 2
  SHOWPAGE
  DEFLINE 0,1
  COLOR hell
  PBOX bx,by,bx+bw,by+bh
  COLOR schwarz
  BOX bx,by,bx+bw,by+bh
  CLIP bx,by,bw,bh
  i=@distance(@ox(bx+bw/4*3),@oy(by+100),@ox(bx+bw/4*3+100),@oy(by+100))
  masstab=i/100

  ' Draw the Map
  IF anzpoly>0
    FOR i=0 TO anzpoly-1
      @draw_polyline(polygon$(i))
    NEXT i
  ENDIF
  COLOR schwarz,hell
  DEFLINE 0,1
  FOR i=0 TO 4
    PBOX bx+bw-20,by+i*20,bx+bw-20+3,by+i*20+10
    PBOX bx+bw-120+i*20,by+20,bx+bw-120+i*20+10,by+20+3
  NEXT i
  LINE bx+bw-20+3,by,bx+bw-20+3,by+100
  LINE bx+bw-20,by,bx+bw-20,by+100
  LINE bx+bw-20-3,by+100,bx+bw-20+6,by+100
  LINE bx+bw-120,by+20,bx+bw-20,by+20
  LINE bx+bw-120,by+20+3,bx+bw-20,by+20+3
  SETFONT normfont$
  IF masstab<=0.01
    TEXT bx+bw-120+10,by+16,STR$(masstab*100*1000,4,4)+" m"
  ELSE
    TEXT bx+bw-120+10,by+16,STR$(masstab*100,4,4)+" km"
  ENDIF
  i=@distance(@ox(bx+bw/4*3),@oy(by),@ox(bx+bw/4*3),@oy(by+100))
  IF i<=1
    TEXT bx+bw-50,by+120,STR$(i*1000,4,4)+" m"
  ELSE
    TEXT bx+bw-50,by+120,STR$(i,4,4)+" km"
  ENDIF
  COLOR hellgrau
  ' Gitternetz
  DEFLINE ,1
  IF abs(INT(xmin)-int(xmax))>0 AND abs(INT(xmin)-int(xmax))<50
    FOR i=INT(xmin) TO INT(xmax)
      LINE @kx(i),by,@kx(i),by+bw
      TEXT @kx(i)-24,by+bh-16,STR$(abs(i),3,3)
    NEXT i
  ENDIF
  IF int(ymin)<>int(ymax) AND abs(INT(ymin)-int(ymax))<50
    FOR i=INT(ymin) TO INT(ymax)
      LINE bx,@ky(i),bx+bw,@ky(i)
      TEXT bx+bw-16,@ky(i)-3,STR$(abs(i),2,2)
    NEXT i
  ENDIF

  ' Karten-Raender
  COLOR weiss
  PBOX bx+bw,by,bx+bw+6,by+bh
  PBOX bx,by+bh,bx+bw,by+bh+6
  COLOR schwarz
  LINE bx+bw,by,bx+bw,by+bh
  LINE bx+bw+3,by,bx+bw+3,by+bh
  LINE bx+bw+6,by,bx+bw+6,by+bh
  stepw1=1/60
  stepw2=1/600
  IF ymax-ymin>2
    stepw1=1
    stepw2=1/60
  ELSE if ymax-ymin>1
    stepw1=1/60
    stepw2=1/120
  ENDIF
  start=INT(ymin/stepw1)*stepw1
  stop=INT(ymax/stepw1)*stepw1+stepw1
  FOR y=start TO stop STEP 2*stepw1
    PBOX bx+bw+3,@ky(y),bx+bw+6,@ky(y+stepw1)
    FOR x=y TO y+stepw1*2 STEP 2*stepw2
      PBOX bx+bw,@ky(x),bx+bw+3,@ky(x+stepw2)
    NEXT x
  NEXT y

  DEFLINE ,1
  @norden(0)
  IF anzwp
    IF masstab>0.2
      SETFONT helveticasmallfont$
    ELSE if masstab<0.005
      SETFONT normfont$
    ELSE
      SETFONT mediumfont$
    ENDIF
    COLOR schwarz,weiss
    GRAPHMODE 4
    FOR i=0 TO MIN(anzwp-1,waypoint_cutoff)
      t$="  "+waypoint$(i)
      y=180/2^31*CVL(MID$(t$,27,4))
      x=180/2^31*CVL(MID$(t$,31,4))
      IF x>xmin AND x<xmax AND y>ymin AND y<ymax
        symbol=cvi(MID$(t$,7,2))
        displ=ASC(MID$(t$,5,1))
        IF (waypoint_cutoff<800 OR waypoint_cutoff=1e12) AND displ<>1
          name$=MID$(t$,51,LEN(t$)-51)
          SPLIT name$,CHR$(0),0,name$,comment$
          comment$=REPLACE$(MID$(t$,52+LEN(name$),LEN(t$)-52-len(name$)),CHR$(0),"|")
          '  print "$"+hex$(symbol,4,4,1),name$,comment$
          SPLIT comment$,"|",0,comment$,a$

          IF len(comment$) AND displ=2
            name$=comment$
          ENDIF
          TEXT @kx(x)-3*len(name$),@ky(y)-8,name$
        ENDIF
        PUT_BITMAP sym$(symbol AND 255,(symbol AND 0x7000)/0x2000),@kx(x)-8,@ky(y)-8,16,16
      ENDIF
    NEXT i

  ENDIF
  IF posx>xmin AND posx<xmax AND posy>ymin AND posy<ymax
    oldmennekenx=@kx(posx)-8
    oldmennekeny=@ky(posy)-8
    GET oldmennekenx,oldmennekeny,@kx(posx)+8,@ky(posy)+8,omenneken$
    COLOR rot,hell
    PUT_BITMAP maenneken$,@kx(posx)-8,@ky(posy)-8,16,16
  ENDIF
  GRAPHMODE 1
  DEFLINE ,INT(1/masstab/1000*4+1)
  IF anztrack
    ott=trackopen
    trackopen=0
    FOR i=0 TO anztrack-1
      @proctrackdata(" "+chr$(len(track$(i)))+track$(i))
      DEC anztrack
    NEXT i
    trackopen=ott
  ENDIF
  DEFLINE ,1
  CLIP sx,sy,sx+sw,sy+sh
  DEFMOUSE 0
  SHOWPAGE
RETURN

PROCEDURE procwpdata(t$)
  LOCAL plen,x,y,name$,a$
  plen=ASC(MID$(t$,2,1))
  waypoint$(anzwp)=MID$(t$,3,plen)
  INC anzwp
  '  memdump varptr(t$)+2,plen
  y=180/2^31*CVL(MID$(t$,27,4))
  x=180/2^31*CVL(MID$(t$,31,4))
  IF x>xmin AND x<xmax AND y>ymin AND y<ymax
    SETFONT smallfont$
    COLOR grau
    name$=MID$(t$,51,plen-2-48)
    SPLIT name$,CHR$(0),1,name$,a$
    TEXT @kx(x)+5,@ky(y)+10,name$
    COLOR gelb
    CIRCLE @kx(x),@ky(y),3
  ENDIF
RETURN
PROCEDURE procroutewpt(t$)
  LOCAL plen,x,y
  plen=ASC(MID$(t$,2,1))
  y=180/2^31*CVL(MID$(t$,27,4))
  x=180/2^31*CVL(MID$(t$,31,4))
  PRINT "ROUTE WPT: NAME=";MID$(t$,51,6);" x=";x;" y=";y
RETURN

PROCEDURE procxfercmplt(t$)
  @status(3,STR$(records)+" loaded (typ="+STR$(ASC(MID$(t$, 3, 1)))+")")
  IF trackopen
    CLOSE #5
    trackopen=0
  ENDIF
RETURN

PROCEDURE saveal
  LOCAL ers,f$,i
  IF anzal
    FILESELECT "save almanac ...","./*.alm","almanac.alm",f$
    IF len(f$)
      IF exist(f$)
        ers=form_alert(3,"[1][This File already exist!|append oder replace ?][APPEND|REPLACE|CANCEL]")=2
      ENDIF
      IF ers<3
        DEFMOUSE 2
        SHOWPAGE
        IF ers=1
          OPEN "A",#2,f$
        ELSE
          OPEN "O",#2,f$
        ENDIF
        FOR i=0 TO anzal-1
          PRINT #2,CHR$(LEN(almanac$(i)));almanac$(i);
        NEXT i
        CLOSE #2
        DEFMOUSE 0
        SHOWPAGE
      ENDIF
    ENDIF
  ELSE
    ~form_alert(1,"[3][There is no almanc !][ OH ]")
  ENDIF
RETURN

PROCEDURE savetrack
  LOCAL t$,sel$,ers,f$,i,name$,a1,a2
  IF anztrack
    ers=1
    IF anztrackl
      f$="Which Track do you want to save ?||"
      t$="all"
      FOR i=0 TO anztrackl-1
        f$=f$+tracklist$(i)+": "+STR$(trackl2(i)-trackl1(i))+" points.|"
        t$=t$+"|"+tracklist$(i)
      NEXT i
      ALERT 2,f$,1,t$,ers
    ENDIF
    IF ers=1
      t$="save all tracks ..."
      name$="track.track"
      a1=0
      a2=anztrack-1
    ELSE
      i=ers-2
      t$="save track "+tracklist$(i)+" ..."
      name$=tracklist$(i)+".track"
      a1=trackl1(i)
      a2=trackl2(i)
    ENDIF
    FILESELECT t$,"./*.track",name$,f$
    IF len(f$)
      IF exist(f$)
        ers=form_alert(3,"[1][This File already exist!|anfuegen oder ersetzen ?][anf�gen|ersetzen|CANCEL]")=2
      ENDIF
      IF ers<3
        DEFMOUSE 2
        SHOWPAGE
        IF ers=1
          OPEN "A",#2,f$
        ELSE
          OPEN "O",#2,f$
        ENDIF
        FOR i=a1 TO a2
          PRINT #2,CHR$(LEN(track$(i)));track$(i);
        NEXT i
        CLOSE #2
        DEFMOUSE 0
        SHOWPAGE
      ENDIF
    ENDIF
  ELSE
    ~form_alert(1,"[3][There is no Track !][ OH ]")
  ENDIF
RETURN

PROCEDURE savewp
  LOCAL ers,f$,i
  IF anzwp
    FILESELECT "save waypoints ...","./*.wpt","waypoint.wpt",f$
    IF len(f$)
      IF exist(f$)
        ers=form_alert(3,"[1][File does already exist! |anfuegen oder ersetzen ?][anf�gen|ersetzen|CANCEL]")=2
      ENDIF
      IF ers<3
        DEFMOUSE 2
        SHOWPAGE
        IF ers=1
          OPEN "A",#2,f$
        ELSE
          OPEN "O",#2,f$
        ENDIF
        FOR i=0 TO anzwp-1
          PRINT #2,CHR$(LEN(waypoint$(i)))+waypoint$(i);
        NEXT i
        CLOSE #2
        DEFMOUSE 0
        SHOWPAGE
      ENDIF
    ENDIF
  ELSE
    ~form_alert(1,"[3][Keine Wegpunkte vorhanden!][ OH ]")
  ENDIF
RETURN
PROCEDURE savemap
  LOCAL ers,f$,i
  IF anzpoly
    FILESELECT "save map polygons ...","./*.map","base.map",f$
    IF len(f$)
      IF exist(f$)
        ers=form_alert(3,"[1][File does already exist! |anfuegen oder ersetzen ?][anf�gen|ersetzen|CANCEL]")=2
      ENDIF
      IF ers<3
        DEFMOUSE 2
        SHOWPAGE
        IF ers=1
          OPEN "A",#2,f$
        ELSE
          OPEN "O",#2,f$
        ENDIF
        FOR i=0 TO anzpoly-1
          PRINT #2,mki$(LEN(polygon$(i)))+polygon$(i);
        NEXT i
        CLOSE #2
        DEFMOUSE 0
        SHOWPAGE
      ENDIF
    ENDIF
  ELSE
    ~form_alert(1,"[3][Keine Polygone vorhanden!][ OH ]")
  ENDIF
RETURN

PROCEDURE sendACK(pid)
  @sendmessage(6,CHR$(pid))
RETURN

PROCEDURE sendmessage(id,m$)
  LOCAL i,a,chk,s$

  chk=0-id-LEN(m$)
  s$=CHR$(16)+CHR$(id)+CHR$(LEN(m$))
  IF LEN(m$)=16
    s$=s$+CHR$(16)
  ENDIF
  IF LEN(m$)
    FOR i=1 TO LEN(m$)
      a=ASC(MID$(m$,i,1)) and 255
      s$=s$+CHR$(a)
      IF a=16
        s$=s$+CHR$(16)
      ENDIF
      chk=chk-a
    NEXT i
  ENDIF
  chk=chk AND 255
  s$=s$+CHR$(chk)
  IF chk=16
    s$=s$+CHR$(16)
  ENDIF
  s$=s$+CHR$(16)+CHR$(3)
  BPUT #1,VARPTR(s$),LEN(s$)
  FLUSH #1
RETURN

PROCEDURE sendnack
  @sendmessage(21,"")
RETURN

PROCEDURE pcv
  LOCAL i
  DIM px(n),py(n)
  FOR i=0 TO n-1
    px(i)=MAX(-50,MIN(@kx(cvf(MID$(p$,25+8*i,4))),sx+sw))
    py(i)=MAX(-50,MIN(@ky(cvf(MID$(p$,25+8*i+4,4))),sy+sh))
  NEXT i
  name$=MID$(p$,25+n*8,LEN(p$)-25)

  IF flags AND 1
    px(n)=px(0)
    py(n)=py(0)
    INC n
  ENDIF
RETURN

PROCEDURE draw_polyline(p$)
  LOCAL n,typ,i,flags,alt,mx1,mx2,my1,my2,name$
  mx1=cvf(MID$(p$,7,4))
  mx2=cvf(MID$(p$,11,4))
  my1=cvf(MID$(p$,15,4))
  my2=cvf(MID$(p$,19,4))
  IF mx1<xmax AND mx2>xmin AND my1<ymax AND my2>ymin
    IF int(@kx(mx1))<>int(@kx(mx2)) OR INT(@ky(my1))<>int(@ky(my2))
      n=cvi(MID$(p$,23,2)) and 0xffff
      typ=ASC(LEFT$(p$))
      flags=ASC(MID$(p$,2,1))
      '  alt=cvf(mid$(p$,3,4))

      ' flags:
      '    Bit 0:    offen (0) oder geschlossen
      '    Bit 1:    one-way (1) oder two-way
      '    Bit 2:    Comment anzeigen (1) oder nicht (0)

      IF typ=0                 ! Fussweg
        IF masstab<0.02
          @pcv
          COLOR grau2
          DEFLINE 0,0.01/masstab,2
          POLYLINE n,px(),py()
        ENDIF
      ELSE if typ=1            ! kleine Strasse
        IF masstab<0.25
          @pcv
          COLOR grau2
          DEFLINE 0,INT(1/masstab/150*4+1)+2,2
          POLYLINE n,px(),py()
          COLOR weiss
          DEFLINE ,INT(1/masstab/150*4+1),2
          POLYLINE n,px(),py()
        ELSE if masstab<0.5
          @pcv
          COLOR grau2
          DEFLINE 0,1,2
          POLYLINE n,px(),py()
        ENDIF
      ELSE if typ=2        ! Hauptstrasse
        IF masstab<0.3
          @pcv
          COLOR grau2
          DEFLINE 0,INT(1.5/masstab/150*4+1)+2,2
          POLYLINE n,px(),py()
          COLOR hell
          DEFLINE ,INT(1.5/masstab/150*4+1),2
          POLYLINE n,px(),py()
        ELSE if masstab<1
          @pcv
          COLOR grau2
          DEFLINE 0,1,2
          POLYLINE n,px(),py()
        ENDIF
      ELSE if typ=3        ! Autobahn
        IF masstab<3
          @pcv
          COLOR gelb
          DEFLINE 0,8,2
          POLYLINE n,px(),py()
          COLOR rot
          DEFLINE ,6,2
          POLYLINE n,px(),py()
          COLOR grau2
          DEFLINE ,1,2
          POLYLINE n,px(),py()
        ENDIF

      ELSE if typ=15            ! Bezirksgrenze
        @pcv
        COLOR grau
        DEFLINE 1,1,2
        POLYLINE n,px(),py()
      ELSE if typ=16            ! Landesgrenze
        @pcv
        COLOR weiss
        DEFLINE 0,3,2
        POLYLINE n,px(),py()
        COLOR rosa
        DEFLINE ,1,2
        POLYLINE n,px(),py()
      ELSE if typ=32        ! Fluss
        IF masstab<0.3
          @pcv
          COLOR wasserblau
          DEFLINE 0,INT(10/masstab/150*4+1),2
          POLYLINE n,px(),py()
        ELSE if masstab<2
          @pcv
          COLOR wasserblau
          DEFLINE 0,1,2
          POLYLINE n,px(),py()
        ENDIF
      ELSE if typ=100         ! Landflaeche
        COLOR landgelb
        @pcv

        POLYFILL n,px(),py()
        COLOR rot
        DEFMARK ,0
        POLYMARK n,px(),py()
      ELSE if typ=101         ! Wasserflaeche
        @pcv

        COLOR wasserblau
        POLYFILL n,px(),py()
        COLOR rot
        DEFMARK ,0
        POLYMARK n,px(),py()
      ELSE if typ=102         ! Watt
        @pcv
        COLOR wattgruen
        POLYFILL n,px(),py()
        COLOR rot
        DEFMARK ,0
        POLYMARK n,px(),py()
      ELSE if typ=103         ! Bebaute Flaeche
        @pcv
        COLOR rosa
        POLYFILL n,px(),py()
        COLOR rot
        DEFMARK ,0
        POLYMARK n,px(),py()
      ELSE                    ! Unbekannt
        @pcv
        COLOR blau
        DEFLINE 0,1,2
        DEFMARK ,3,5
        POLYMARK n,px(),py()
      ENDIF
      IF btst(flags,2)
        IF @kx(mx2)-@kx(mx1)>8*len(name$)
          PRINT name$
          GRAPHMODE 4
          COLOR hellgrau
          SETFONT helveticanormfont$
          TEXT @kx((mx2+mx1)/2)-8*len(name$)/2,@ky((my2+my1)/2)+4,name$
          GRAPHMODE 1
        ENDIF
      ENDIF
    ENDIF
  ENDIF
RETURN

PROCEDURE showrange
  COLOR schwarz,schwarz
  PBOX sx+sw-19*8,by+200,sx+sw,by+280
  IF anzal>31
    COLOR gelb
  ELSE
    COLOR grau
  ENDIF
  TEXT sx+sw-18*8,15*16,"ALMANAC"
  IF anzwp>0
    COLOR gelb
  ELSE
    COLOR grau
  ENDIF
  TEXT sx+sw-18*8,16*16,"#WP: "+STR$(waypoint_cutoff)+"/"+STR$(anzwp)
  IF anztrack>0
    COLOR gelb
  ELSE
    COLOR grau
  ENDIF
  TEXT sx+sw-18*8,17*16,"#TR: "+STR$(anztrack,5,5)
  IF anzpoly>0
    COLOR gelb
  ELSE
    COLOR grau
  ENDIF
  TEXT sx+sw-18*8,18*16,"#MAP: "+STR$(anzpoly,6,6)
  SHOWPAGE
RETURN

PROCEDURE status(n,ssst$)
  COLOR grau
  PBOX 12*8,16*(30+1-n)+2,(12+30)*8,16*(30-n)+2
  COLOR weiss
  TEXT 12*8,16*(30+1-n),LEFT$(ssst$+"                          ",28)
  SHOWPAGE
RETURN

PROCEDURE preferences
  LOCAL ret,ok,cancel,br1,br2,lon1,lon2,devtext
  LOCAL tree0$,obj0$,obj1$
  string0$="Preferences ..."+CHR$(0)+SPACE$(0)
  string1$=""+CHR$(0)
  string2$=""+CHR$(0)
  tedinfo0$=mkl$(VARPTR(string0$))+mkl$(VARPTR(string1$))+mkl$(VARPTR(string2$))+mki$(3)+mki$(0)+mki$(2)+mki$(4209)+mki$(0)+mki$(65535)+mki$(0)+mki$(0)
  obj1$=mki$(2)+mki$(-1)+mki$(-1)+mki$(22)+mki$(0)+mki$(16)+mkl$(VARPTR(tedinfo0$))+mki$(16)+mki$(16)+mki$(560)+mki$(16)
  string3$="Interface Device:"+CHR$(0)
  obj2$=mki$(3)+mki$(-1)+mki$(-1)+mki$(28)+mki$(0)+mki$(0)+mkl$(VARPTR(string3$))+mki$(16)+mki$(48)+mki$(136)+mki$(16)
  string4$="Breite:"+CHR$(0)
  obj3$=mki$(4)+mki$(-1)+mki$(-1)+mki$(28)+mki$(0)+mki$(0)+mkl$(VARPTR(string4$))+mki$(16)+mki$(64)+mki$(56)+mki$(16)
  string5$="L�nge:"+CHR$(0)
  obj4$=mki$(5)+mki$(-1)+mki$(-1)+mki$(28)+mki$(0)+mki$(0)+mkl$(VARPTR(string5$))+mki$(16)+mki$(80)+mki$(56)+mki$(16)
  string6$="bis"+CHR$(0)
  obj5$=mki$(6)+mki$(-1)+mki$(-1)+mki$(28)+mki$(0)+mki$(0)+mkl$(VARPTR(string6$))+mki$(280)+mki$(64)+mki$(56)+mki$(16)
  string7$="bis"+CHR$(0)
  obj6$=mki$(7)+mki$(-1)+mki$(-1)+mki$(28)+mki$(0)+mki$(0)+mkl$(VARPTR(string7$))+mki$(280)+mki$(80)+mki$(56)+mki$(16)
  devtext=7
  string8$=devicename$+CHR$(0)+SPACE$(39)
  string9$="_______________________________________"+CHR$(0)
  string10$="XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"+CHR$(0)
  tedinfo1$=mkl$(VARPTR(string8$))+mkl$(VARPTR(string9$))+mkl$(VARPTR(string10$))+mki$(3)+mki$(0)+mki$(0)+mki$(4513)+mki$(0)+mki$(1)+mki$(39)+mki$(39)
  obj7$=mki$(8)+mki$(-1)+mki$(-1)+mki$(29)+mki$(8)+mki$(0)+mkl$(VARPTR(tedinfo1$))+mki$(160)+mki$(48)+mki$(240)+mki$(16)
  br1=8
  string11$=@breite$(ymin)+CHR$(0)+SPACE$(12)
  string12$="____________"+CHR$(0)
  string13$="XXXXXXXXXXXX"+CHR$(0)
  tedinfo2$=mkl$(VARPTR(string11$))+mkl$(VARPTR(string12$))+mkl$(VARPTR(string13$))+mki$(3)+mki$(0)+mki$(0)+mki$(4513)+mki$(0)+mki$(1)+mki$(12)+mki$(12)
  obj8$=mki$(9)+mki$(-1)+mki$(-1)+mki$(29)+mki$(8)+mki$(0)+mkl$(VARPTR(tedinfo2$))+mki$(160)+mki$(64)+mki$(96)+mki$(16)
  br2=9
  string14$=@breite$(ymax)+CHR$(0)+SPACE$(12)
  string15$="____________"+CHR$(0)
  string16$="XXXXXXXXXXXX"+CHR$(0)
  tedinfo3$=mkl$(VARPTR(string14$))+mkl$(VARPTR(string15$))+mkl$(VARPTR(string16$))+mki$(3)+mki$(0)+mki$(0)+mki$(4513)+mki$(0)+mki$(1)+mki$(12)+mki$(12)
  obj9$=mki$(10)+mki$(-1)+mki$(-1)+mki$(29)+mki$(8)+mki$(0)+mkl$(VARPTR(tedinfo3$))+mki$(320)+mki$(64)+mki$(96)+mki$(16)
  lon1=10
  string17$=@laenge$(xmin)+CHR$(0)+SPACE$(13)
  string18$="_____________"+CHR$(0)
  string19$="XXXXXXXXXXXXX"+CHR$(0)
  tedinfo4$=mkl$(VARPTR(string17$))+mkl$(VARPTR(string18$))+mkl$(VARPTR(string19$))+mki$(3)+mki$(0)+mki$(0)+mki$(4513)+mki$(0)+mki$(1)+mki$(13)+mki$(13)
  obj10$=mki$(11)+mki$(-1)+mki$(-1)+mki$(29)+mki$(8)+mki$(0)+mkl$(VARPTR(tedinfo4$))+mki$(160)+mki$(80)+mki$(104)+mki$(16)
  lon2=11
  string20$=@laenge$(xmax)+CHR$(0)+SPACE$(13)
  string21$="_____________"+CHR$(0)
  string22$="XXXXXXXXXXXXX"+CHR$(0)
  tedinfo5$=mkl$(VARPTR(string20$))+mkl$(VARPTR(string21$))+mkl$(VARPTR(string22$))+mki$(3)+mki$(0)+mki$(0)+mki$(4513)+mki$(0)+mki$(1)+mki$(13)+mki$(13)
  obj11$=mki$(12)+mki$(-1)+mki$(-1)+mki$(29)+mki$(8)+mki$(0)+mkl$(VARPTR(tedinfo5$))+mki$(320)+mki$(80)+mki$(104)+mki$(16)
  ok=12
  string23$="OK"+CHR$(0)
  obj12$=mki$(13)+mki$(-1)+mki$(-1)+mki$(26)+mki$(7)+mki$(0)+mkl$(VARPTR(string23$))+mki$(520)+mki$(64)+mki$(56)+mki$(64)
  cancel=13
  string24$="CANCEL"+CHR$(0)
  obj13$=mki$(0)+mki$(-1)+mki$(-1)+mki$(26)+mki$(37)+mki$(0)+mkl$(VARPTR(string24$))+mki$(520)+mki$(144)+mki$(56)+mki$(64)
  obj0$=mki$(-1)+mki$(1)+mki$(13)+mki$(20)+mki$(0)+mki$(16)+mkl$(135424)+mki$(bx+(bw-592)/2)+mki$(by+(bh-224)/2)+mki$(592)+mki$(224)
  tree0$=obj0$+obj1$+obj2$+obj3$+obj4$+obj5$+obj6$+obj7$+obj8$+obj9$+obj10$
  tree0$=tree0$+obj11$+obj12$+obj13$
  ~form_dial(0,0,0,0,0,bx,by,bw,bh)
  ~form_dial(1,0,0,0,0,bx,by,bw,bh)
  '  ~objc_draw(varptr(tree0$),0,-1,0,0)
  ret=form_do(VARPTR(tree0$))
  IF ret=ok
    SPLIT string8$,CHR$(0),1,devicename$,b$
    CLOSE #1
    OPEN "UX:9600,N,8,1",#1,devicename$
    @init_device
  ENDIF
  ~form_dial(2,0,0,0,0,bx,by,bw,bh)
  ~form_dial(3,0,0,0,0,bx,by,bw,bh)
  SHOWPAGE
RETURN
PROCEDURE paper

  ' display graphics window with grid an axis
  ' Globale Vars: bx,by,bw,bh, grid,
  '               xmin,xmax,ymin,ymax,xstep,ystep

  LOCAL textfaktor,x$,y$,x2$,y2$,void,x,y

  COLOR schwarz
  PBOX bx,by,bx+bw,by+bh
  textfaktor=bw/10000
  void=0
  COLOR grau
  BOX bx,by,bx+bw,by+bh

  FOR x=0 TO MAX(ABS(xmin),ABS(xmax)) STEP xstep
    COLOR hellgrau

    IF x>xmin AND x<xmax
      LINE @kx(x),@ky(ymin),@kx(x),@ky(ymax)
    ENDIF
    IF -x>xmin AND -x<xmax
      LINE @kx(-x),@ky(ymin),@kx(-x),@ky(ymax)
    ENDIF
    COLOR schwarz
    IF x>xmin AND x<xmax
      LINE @kx(x),@ky(0)+2,@kx(x),@ky(0)-2
    ENDIF
    IF -x>xmin AND -x<xmax
      LINE @kx(-x),@ky(0)+2,@kx(-x),@ky(0)-2
    ENDIF
    x$=TRIM$(STR$(x))
    x2$=TRIM$(STR$(-x))
    IF @kx(x)-0.06*LEN(x$)*130/2>void+2
      COLOR schwarz
      DEFTEXT 0,textfaktor,textfaktor,0
      LTEXT @kx(x)-textfaktor*LEN(x$)*130/2,@ky(0)+textfaktor*50,x$
      void=ltextlen(x$)
      LTEXT @kx(-x)-textfaktor*LEN(x2$)*130/2,@ky(0)+textfaktor*50,x2$
    ENDIF
  NEXT x

  FOR y=0 TO MAX(ABS(ymin),ABS(ymax)) STEP ystep
    COLOR hellgrau
    IF y>ymin AND y<ymax
      LINE @kx(xmin),@ky(y),@kx(xmax),@ky(y)
    ENDIF
    IF -y>ymin AND -y<ymax
      LINE @kx(xmin),@ky(-y),@kx(xmax),@ky(-y)
    ENDIF
    COLOR schwarz
    IF y>ymin AND y<ymax
      LINE @kx(0)-2,@ky(y),@kx(0)+2,@ky(y)
    ENDIF
    IF -y>ymin AND -y<ymax
      LINE @kx(0)-2,@ky(-y),@kx(0)+2,@ky(-y)
    ENDIF
    y$=TRIM$(STR$(y))
    y2$=TRIM$(STR$(-y))
    DEFLINE 0,1
    DEFTEXT 0,textfaktor,textfaktor,0
    IF y>ymin AND y<ymax
      LTEXT @kx(0)-textfaktor*130*LEN(y$)-2,@ky(y)-textfaktor*50,y$
    ENDIF
    IF -y>ymin AND -y<ymax
      LTEXT @kx(0)-textfaktor*130*LEN(y2$)-2,@ky(-y)-textfaktor*50,y2$
    ENDIF
  NEXT y

  ' Koordinatenachsen
  COLOR weiss
  IF SGN(xmin)<>SGN(xmax)
    LINE @kx(0),@ky(ymin),@kx(0),@ky(ymax)
  ENDIF
  IF SGN(ymin)<>SGN(ymax)
    LINE @kx(xmin),@ky(0),@kx(xmax),@ky(0)
  ENDIF
  FOR i=-3 TO 3
    ' Pfeile
    IF SGN(xmin)<>SGN(xmax)
      LINE @kx(0)+i,@ky(ymax)+10,@kx(0),@ky(ymax)
    ENDIF
    IF SGN(ymin)<>SGN(ymax)
      LINE @kx(xmax)-10,@ky(0)+i,@kx(xmax),@ky(0)
    ENDIF
  NEXT i
  ' Beschriftung
  COLOR weiss
  DEFTEXT 1,textfaktor*2,textfaktor*3,0
  LTEXT bx+bw-textfaktor*2*130*LEN(ex$),@ky(0)-textfaktor*2*160,ex$
  LTEXT @kx(0)+textfaktor*2*130,by+130*textfaktor*1.5,ey$
  DEFTEXT ,textfaktor,textfaktor,-90
  COLOR gelb
  LTEXT bx+bw-textfaktor*130,by+bh-130*textfaktor,"(C) MARKUS HOFFMANN 9'1995"
  SHOWPAGE
  DEFTEXT ,,,0
RETURN

PROCEDURE stepsize
  ' calculate stepsize from range-data

  LOCAL i

  xstep=(xmax-xmin)/10
  ystep=(ymax-ymin)/8
  FOR i=-13 TO 13
    IF xstep>=8*10^i AND xstep<2*10^(i+1)
      xstep=10^(i+1)
    ELSE IF xstep>=2*10^(i+1) AND xstep<5*10^(i+1)
      xstep=2*10^(i+1)
    ELSE IF xstep>=5*10^(i+1) AND xstep<8*10^(i+1)
      xstep=5*10^(i+1)
    ENDIF
    IF ystep>=8*10^i AND ystep<2*10^(i+1)
      ystep=10^(i+1)
    ELSE IF ystep>=2*10^(i+1) AND ystep<5*10^(i+1)
      ystep=2*10^(i+1)
    ELSE IF ystep>=5*10^(i+1) AND ystep<8*10^(i+1)
      ystep=5*10^(i+1)
    ENDIF
  NEXT i
RETURN

PROCEDURE hscalerbar(scaler_x,scaler_y,scaler_w)
  LOCAL i,k

  COLOR schwarz
  PBOX scaler_x,scaler_y,scaler_x+scaler_w,scaler_y+20
  COLOR weiss,grau
  BOX scaler_x,scaler_y,scaler_x+scaler_w,scaler_y+20

  FOR i=0 TO 100 STEP 5
    IF (i MOD 50)=0
      k=7
      TEXT scaler_x+i/100*scaler_w-len(STR$(i))*2.5,scaler_y+37,STR$(i)
    ELSE IF (i MOD 10)=0
      k=5
    ELSE
      k=3
    ENDIF
    LINE scaler_x+i/100*scaler_w,scaler_y+20,scaler_x+i/100*scaler_w,scaler_y+20+k
  NEXT i
RETURN

PROCEDURE savefig
  FILESELECT "export fig ...","./*.fig","test.fig",f$
  IF len(f$)
    IF exist(f$)
      IF form_alert(2,"[1][Datei existiert schon !|Ersetzen ?][Ja|ABBRUCH]")=1
        DEFMOUSE 2
        SHOWPAGE
        @makefig(f$)
        DEFMOUSE 0
      ENDIF
    ELSE
      DEFMOUSE 2
      @makefig(f$)
      DEFMOUSE 0
    ENDIF
  ENDIF
RETURN
PROCEDURE saveescreen
  FILESELECT "save screendump ...","./*.xpm","screen.xpm",f$
  IF len(f$)
    IF exist(f$)
      IF form_alert(2,"[1][Datei existiert schon !|Ersetzen ?][Ja|ABBRUCH]")=1
        DEFMOUSE 2
        SHOWPAGE
        @savexpm(f$)
        DEFMOUSE 0
      ENDIF
    ELSE
      DEFMOUSE 2
      SHOWPAGE
      @savexpm(f$)
      DEFMOUSE 0
    ENDIF
  ENDIF
  SHOWPAGE
RETURN
PROCEDURE savexwd(f$)
  LOCAL t$
  GET bx,by,bw,bh,t$
  BSAVE f$,VARPTR(t$),LEN(t$)
RETURN
PROCEDURE savexpm(f$)
  PRINT "Save screen as: "+f$
  OPEN "O",#4,f$
  PRINT #4,"/* XPM */"
  PRINT #4,"static char *magick[] = {"
  PRINT #4,"/* columns rows colors chars-per-pixel */"
  PRINT #4,CHR$(34)+STR$(sht)+" "+STR$(screenwidth)+" 4 1"+CHR$(34)+","
  PRINT #4,CHR$(34)+"0 c #ffffff"+CHR$(34)+","
  PRINT #4,CHR$(34)+"1 c #aaaaaa"+CHR$(34)+","
  PRINT #4,CHR$(34)+"2 c #555555"+CHR$(34)+","
  PRINT #4,CHR$(34)+"3 c #000000"+CHR$(34)+","
  PRINT #4,"/* pixels      erzeugt von GPS-Earth "+version$+" (garmin.bas) */"
  FOR x=screenwidth-1 downto 0
    PRINT #4,CHR$(34);
    FOR y=0 TO sht-1
      a=ASC(MID$(escreenbuf$,1+x/8*screenplanes+y*screenwidth*screenplanes/8,1)) and 255
      b=x mod 4
      IF btst(a,b*2) AND btst(a,b*2+1)
        PRINT #4,"3";
      ELSE if btst(a,b*2)=0 AND btst(a,b*2+1)
        PRINT #4,"2";
      ELSE if btst(a,b*2) AND btst(a,b*2+1)=0
        PRINT #4,"1";
      ELSE
        PRINT #4,"0";
      ENDIF
    NEXT x
    PRINT #4,CHR$(34)+","
  NEXT y
  PRINT #4,"};"
  CLOSE #4
RETURN

PROCEDURE show_screenshot
  LOCAL x,y,b,a,bg$
  IF sht>0 AND screenplanes>0 AND screenwidth>0
    GET sx+sw-sht,sy+sh-screenwidth,sht,screenwidth,bg$
    DEFMOUSE 2
    FOR x=screenwidth-1 downto 0
      FOR y=0 TO sht-1
        a=ASC(MID$(escreenbuf$,1+x/8*screenplanes+y*screenwidth*screenplanes/8,1)) and 255
        b=x mod 4
        IF btst(a,b*2) AND btst(a,b*2+1)
          COLOR schwarz
        ELSE if btst(a,b*2)=0 AND btst(a,b*2+1)
          COLOR grau
        ELSE if btst(a,b*2) AND btst(a,b*2+1)=0
          COLOR hellgrau
        ELSE
          COLOR weiss
        ENDIF
        PLOT sx+sw-sht+y,sy+sh-x
      NEXT y
      SHOWPAGE
    NEXT x
    GET sx+sw-sht,sy+sh-screenwidth,sht,screenwidth,escreen$
    ~form_alert(1,"[0][continue ?][OK]")
    PUT sx+sw-sht,sy+sh-screenwidth,bg$
    DEFMOUSE 0
  ENDIF
RETURN

PROCEDURE progress(a,b)
  LOCAL t$
  PRINT chr$(13);"[";STRING$(b/a*32,"-");">";STRING$((1.03-b/a)*32,"-");"| ";STR$(INT(b/a*100),3,3);"% ]";
  FLUSH
RETURN

PROCEDURE makefig(figfilename$)
  LOCAL plen,x,y,s$,date,alt,depth,new,t$,a$,x1$,x2$,y1$,y2$,a,b,c,d

  OPEN "O",#4,figfilename$
  PRINT #4,"#FIG 3.2"
  PRINT #4,"Landscape"
  PRINT #4,"Center"
  PRINT #4,"Metric"
  PRINT #4,"A4"
  PRINT #4,"100.00"
  PRINT #4,"Single"
  PRINT #4,"-2"
  PRINT #4,"1200 2"
  IF exist("Karten/Karten.dat")
    OPEN "I",#7,"Karten/Karten.dat"
    WHILE not eof(#7)
      LINEINPUT #7,t$
      SPLIT t$," ",1,y1$,t$
      SPLIT t$," ",1,x1$,t$
      SPLIT t$," ",1,y2$,t$
      SPLIT t$," ",1,x2$,t$
      a=@kx(@conv_breite(x1$))
      b=@ky(@conv_laenge(y1$))
      c=@kx(@conv_breite(x2$))
      d=@ky(@conv_laenge(y2$))
      PRINT a,b,c,d
      IF c-a>10 AND d-b>10 AND c-a<1000 AND d-b<1000 AND a>-1000 AND b>-1000 AND a<3000 AND b<3000
        IF left$(t$)=CHR$(34)
          t$=RIGHT$(t$,LEN(t$)-1)
        ENDIF
        IF right$(t$)=CHR$(34)
          t$=LEFT$(t$,LEN(t$)-1)
        ENDIF
        PRINT "Karte: ",t$
        PRINT #4,"2 5 0 1 0 -1 150 0 -1 0.000 0 0 -1 0 0 5"
        PRINT #4,"	0 Karten/"+t$
        PRINT #4,"	 "+STR$(INT(45*@kx(@conv_breite(x1$))))+" "+STR$(INT(45*@ky(@conv_laenge(y1$))));
        PRINT #4," "+STR$(INT(45*@kx(@conv_breite(x1$))))+" "+STR$(INT(45*@ky(@conv_laenge(y2$))));
        PRINT #4," "+STR$(INT(45*@kx(@conv_breite(x2$))))+" "+STR$(INT(45*@ky(@conv_laenge(y2$))));
        PRINT #4," "+STR$(INT(45*@kx(@conv_breite(x2$))))+" "+STR$(INT(45*@ky(@conv_laenge(y1$))));
        PRINT #4," "+STR$(INT(45*@kx(@conv_breite(x1$))))+" "+STR$(INT(45*@ky(@conv_laenge(y1$))))
      ENDIF
    WEND
    CLOSE #7
  ENDIF

  @ps_dicke(1)
  @ps_font(14)
  @ps_color(0)
  @ps_tmode(0)
  @ps_box(bx,by,bx+bw,by+bh)
  FOR i=0 TO 4
    @ps_pbox(bx+bw-20,by+i*20,bx+bw-20+3,by+i*20+10)
    @ps_pbox(bx+bw-120+i*20,by+20,bx+bw-120+i*20+10,by+20+3)
  NEXT i
  @ps_line(bx+bw-20+3,by,bx+bw-20+3,by+100)
  @ps_line(bx+bw-20,by,bx+bw-20,by+100)
  @ps_line(bx+bw-20-3,by+100,bx+bw-20+6,by+100)

  @ps_line(bx+bw-120,by+20,bx+bw-20,by+20)
  @ps_line(bx+bw-120,by+20+3,bx+bw-20,by+20+3)
  i=@distance(@ox(bx+bw/4*3),@oy(by),@ox(bx+bw/4*3),@oy(by+100))
  masstab=i/100
  IF i<=1
    @ps_text(bx+bw-120+10,by+16,str$(i*1000,4,4)+" m")
  ELSE
    @ps_text(bx+bw-120+10,by+16,str$(i,4,4)+" km")
  ENDIF
  i=@distance(@ox(bx+bw/4*3),@oy(by+100),@ox(bx+bw/4*3+100),@oy(by+100))
  IF i<=1
    @ps_text(bx+bw-50,by+120,str$(i*1000,4,4)+" m")
  ELSE
    @ps_text(bx+bw-50,by+120,str$(i,4,4)+" km")
  ENDIF
  IF abs(INT(xmin)-int(xmax))>0 AND abs(INT(xmin)-int(xmax))<50
    FOR i=INT(xmin) TO INT(xmax)
      @ps_dotline(@kx(i),by,@kx(i),by+bw)
      @ps_text(@kx(i)-24,by+bh-16,str$(abs(i),3,3))
    NEXT i
  ENDIF
  IF int(ymin)<>int(ymax) AND abs(INT(ymin)-int(ymax))<50
    FOR i=INT(ymin) TO INT(ymax)
      @ps_dotline(bx,@ky(i),bx+bw,@ky(i))
      @ps_text(bx+bw-16,@ky(i)-3,str$(abs(i),2,2))
    NEXT i
  ENDIF

  @ps_color(0)
  @ps_groesse(18)
  @ps_font(1)
  FOR i=0 TO anzwp-1
    t$=waypoint$(i)
    y=180/2^31*CVL(MID$(t$,25,4))
    x=180/2^31*CVL(MID$(t$,29,4))
    @ps_color(2)
    @ps_circle(@kx(x),@ky(y),2)
    @ps_color(1)
    a$=MID$(t$,49,LEN(t$)-48)
    SPLIT a$,CHR$(0),0,a$,b$
    @ps_text(@kx(x),@ky(y),a$)
  NEXT i
  FOR i=0 TO anztrack-1
    t$=track$(i)
    y=180/2^31*CVL(MID$(t$,1,4))
    x=180/2^31*CVL(MID$(t$,5,4))
    date=CVL(MID$(t$,9,4))
    alt=CVf(MID$(t$,13,4))
    depth=CVf(MID$(t$,17,4))
    new=ASC(MID$(t$,21,1))
    IF new
      @ps_color(3)
      @ps_circle(@kx(x),@ky(y),1)
    ELSE
      @ps_color(6)
      @ps_CIRCLE(@kx(x),@ky(y),0.6)
    ENDIF
    IF alt>100
      @ps_color(10)
    ELSE
      @ps_color(12)
    ENDIF
    IF new
      @ps_line(@kx(x),@ky(y),@kx(x),@ky(y))
    ELSE
      @ps_line(@kx(otx),@ky(oty),@kx(x),@ky(y))
    ENDIF
    otx=x
    oty=y
  NEXT i

  @ps_angle(90)
  @ps_font(10)
  @ps_tmode(0)
  @ps_groesse(8)
  @ps_text(bx+bw/2,by+bh-5,"Garmin.bas, X11-Basic "+version$+" (c) Markus Hoffmann "+date$)
  CLOSE #4
  ' system "fig2dev -L ps -z A4 -c "+figfilename$+" "+psfilename$
  ' system "rm "+figfilename$
  ' system "chmod 666 "+psfilename$
RETURN

PROCEDURE do_hscaler(scaler_x,scaler_y,scaler_w,wert)
  COLOR schwarz
  PBOX scaler_x+1,scaler_y+1,scaler_x+scaler_w,scaler_y+20
  COLOR gelb
  PBOX scaler_x+1,scaler_y+1,scaler_x+1+(scaler_w-2)*wert,scaler_y+20
RETURN
PROCEDURE ps_line(x1,y1,x2,y2)
  @mmm
  PRINT #4,"2 1 0 "+STR$(dicken)+" "+STR$(colorn)+" 7 0 0 -1 0.000 0 0 -1 0 0 2"
  PRINT #4," "+STR$(x1)+" "+STR$(y1)+" "+STR$(x2)+" "+STR$(y2)
RETURN
PROCEDURE ps_dotline(x1,y1,x2,y2)
  @mmm
  PRINT #4,"2 1 2 "+STR$(dicken)+" "+STR$(colorn)+" 7 0 0 -1 3.000 0 0 -1 0 0 2"
  PRINT #4," "+STR$(x1)+" "+STR$(y1)+" "+STR$(x2)+" "+STR$(y2)
RETURN
PROCEDURE ps_box(x1,y1,x2,y2)
  @mmm
  PRINT #4,"2 1 0 "+STR$(dicken)+" "+STR$(colorn)+" 7 0 0 -1 0.000 0 0 -1 0 0 5"
  PRINT #4," "+STR$(x1)+" "+STR$(y1)+" "+STR$(x2)+" "+STR$(y1);
  PRINT #4," "+STR$(x2)+" "+STR$(y2)+" "+STR$(x1)+" "+STR$(y2);
  PRINT #4," "+STR$(x1)+" "+STR$(y1)
RETURN
PROCEDURE ps_pbox(x1,y1,x2,y2)
  @mmm
  PRINT #4,"2 1 0 "+STR$(dicken)+" "+STR$(colorn)+" 7 0 0 20 0.000 0 0 -1 0 0 5"
  PRINT #4," "+STR$(x1)+" "+STR$(y1)+" "+STR$(x2)+" "+STR$(y1);
  PRINT #4," "+STR$(x2)+" "+STR$(y2)+" "+STR$(x1)+" "+STR$(y2);
  PRINT #4," "+STR$(x1)+" "+STR$(y1)
RETURN
PROCEDURE ps_circle(x1,y1,r)
  LOCAL x2,y2
  x2=x1+2*r
  y2=y1+2*r
  @mmm
  PRINT #4,"1 3 0 "+STR$(dicken)+" "+STR$(colorn)+" 7 50 0 20 0.000 1 0 ";
  PRINT #4," "+STR$(x1)+" "+STR$(y1)+" "+STR$(INT(r*45))+" "+STR$(INT(r*45));
  PRINT #4," "+STR$(x1)+" "+STR$(y1)+" "+STR$(x2)+" "+STR$(y2)
RETURN
PROCEDURE ps_text(x1,y1,t$)
  @mmm
  PRINT #4,"4 "+STR$(tmoden)+" 0 40 0 "+STR$(fontn)+" "+STR$(groessen)+" "+STR$(anglen,6,6)+" 4 165 4830 "+STR$(INT(x1))+" "+STR$(INT(y1))+" "+t$+"\001"
RETURN
PROCEDURE mmm
  MUL x1,45
  MUL y1,45
  MUL x2,45
  MUL y2,45
  y1=INT(y1)
  y2=INT(y2)
  x1=INT(x1)
  x2=INT(x2)
RETURN

PROCEDURE ps_color(c)
  colorn=c
RETURN
PROCEDURE ps_pattern(c)
  patternn=c
RETURN
PROCEDURE ps_dicke(c)
  dicken=c
RETURN
PROCEDURE ps_angle(c)
  anglen=c/180*pi
RETURN
PROCEDURE ps_groesse(c)
  groessen=c
RETURN
PROCEDURE ps_tmode(c)
  tmoden=c
RETURN
PROCEDURE ps_font(c)
  fontn=c
RETURN

FUNCTION distance(lona,lata,lonb,latb)
  LOCAL DEG2RAD,l0,l1,b0,b1
  DEG2RAD=(3.14159265358979323846/180.0)
  l0=lona*DEG2RAD
  l1=lonb*DEG2RAD
  b0=lata*DEG2RAD
  b1=latb*DEG2RAD
  RETURN 6371*2*asin(sqrt(cos(b1)*cos(b0)*sin(0.5*(l1-l0))*sin(0.5*(l1-l0))+sin(0.5*(b1-b0))*sin(0.5*(b1 - b0))))
ENDFUNC

menudata:
DATA "GPS-Earth","  Info","----------------","- 1","- 2","- 3","- 4","- 5","- 6",""
DATA "File"
DATA "  new "
DATA "---------------------------"
DATA "  load tracks ..."
DATA "  load waypoints ..."
DATA "  load routes ..."
DATA "  load map ..."
DATA "  load almanac ..."
DATA "---------------------------"
DATA "  Track & WP Info "
DATA "---------------------------"
DATA "  export track ..."
DATA "  export waypoints ..."
DATA "  export map ..."
DATA "  export gpx ..."
DATA "  import track ..."
DATA "  import waypoints ..."
DATA "  import map ..."
DATA "  import gpx ..."
DATA "---------------------------"
DATA "  save tracks ..."
DATA "  save waypoints ..."
DATA "  save map ..."
DATA "  save almanac ..."
DATA "  save screendump ..."
DATA "---------------------------"
DATA "  Preferences ..."
DATA "  save settings"
DATA "---------------------------"
DATA "  Quit                  Q",""
DATA "Edit"
' data "  select waypoint ... "
DATA "  new waypoint ... "
DATA "  place waypoint ... "
DATA "  edit waypoint ... "
DATA "  delete waypoint ... "
DATA "  delete all waypoints "
DATA "-----------------------------------"
DATA "  draw track ... "
DATA "  optimize track ... "
DATA "  convert track to map element ... "
DATA "  delete track ... "
DATA "  delete all tracks "
DATA "-----------------------------------"
DATA "  edit map element ..."
DATA "  delete map element ..."
DATA "  delete all map elements",""
DATA "Transfer"
DATA "  get almanac             A"
DATA "  get waypoints           W"
DATA "  get tracks              T"
DATA "  get routes              R"
DATA "------------------------------"
DATA "  get position            P"
DATA "  get time & date         D"
DATA "  get screendump"
DATA "------------------------------"
DATA "  send Almanac"
DATA "  send Waypoints"
DATA "  send Track"
DATA "  send Route"
DATA "------------------------------"
DATA "  interrupt transfer   ESC",""
DATA "Command"
DATA "  PVT on"
DATA "  PVT off"
DATA "  Licht on"
DATA "  Licht off"
DATA "  Async on"
DATA "  Async off"
DATA "------------------------"
DATA "  GPS off          BSP",""
DATA "Display"
DATA "  L�ngentreu"
DATA "  save graphic ..."
DATA "  make *.fig ..."
DATA "------------------------"
DATA "  Diagramm d <--> v"
DATA "  experimental ..."
DATA "  goto waypoint ..."
DATA "  goto map element ...",""
DATA "Setup"
DATA "  Time ..."
DATA "  Display ..."
DATA "  Units ..."
DATA "  Interface ..."
DATA "  System ..."
DATA ""
DATA "***"

screenmessages:
DATA 00,Can't Change Active WPT
DATA 02,Start Altitude Change
DATA 03,Final Altitude Alert
DATA 05,Approaching <waypoint>
DATA 07,Arrival at <waypoint>
DATA 08,Cannot Navigate Locked Route
DATA 0a,Stored Data was Lost
DATA 0b,Database Memory Failed
DATA 0c,No Position
DATA 14,Transfer Complete
DATA 16,Route is Full
DATA 17,Route is not Empty
DATA 18,Route Waypoint Can't be Deletd
DATA 19,Route Waypoint was Deleted
DATA 1a,Received an Invalid WPT
DATA 1b,Timer Has Expired
DATA 1c,Transfer has been Completed
DATA 1d,Vertical Nav Cancelled
DATA 1e,WPT Memory is Full
DATA 1f,Already Exists
DATA 21,Accuracy has been Degraded
DATA 22,Anchor Drag Alarm
DATA 23,Battery Power is Low
DATA 24,CDI Alarm
DATA 25,Leg not Smoothed
DATA 26,Memory Battery is Low
DATA 27,Need 2D Altitude
DATA 28,No DGPS Position
DATA 29,Oscillator Needs Adjustment
DATA 2a,Poor GPS Coverage
DATA 2c,Receiver has Failed
DATA 2d,Power Down and Re-init
DATA 2e,Read Only Mem has Failed
DATA 2f,RTCM Input has Failed
DATA 30,No RTCM Input
DATA 31,Searching the Sky
DATA 32,Steep Turn Ahead
DATA 33,Inside SUA
DATA 34,SUA Near & Ahead
DATA 35,SUA Ahead < 10 min
DATA 36,Near SUA < 2 nm
DATA "***"
