DECLARE SUB procroutename (t$)
DECLARE SUB proctrackname (t$)
DECLARE FUNCTION mystr$ (a!)
DECLARE SUB proctrackdata (t$)
DECLARE SUB savetrack ()
DECLARE SUB saveal ()
DECLARE SUB procalmanacdata (t$)
DECLARE SUB loadwp ()
DECLARE SUB newwp ()
DECLARE SUB savewp ()
DECLARE SUB procrecords (t$)
DECLARE SUB procxfercmplt (t$)
DECLARE SUB procwpdata (t$)
DECLARE SUB clearwp ()
DECLARE SUB showwpanz ()
DECLARE SUB procprotocollarray (t$)
DECLARE SUB procmessage (t$)
DECLARE SUB procproductdata (t$)
DECLARE SUB procpositiondata (t$)
DECLARE SUB proctimedata (t$)
DECLARE FUNCTION toint! (m$)
DECLARE SUB sendACK (pid)
DECLARE FUNCTION getmessage$ ()
DECLARE SUB sendmessage (id!, m$)
DECLARE SUB dosmoot ()
DECLARE FUNCTION smoot! (x!)
DECLARE SUB loaddatafile ()
DECLARE SUB openoutputfile ()
DECLARE FUNCTION exist! (f$)
DECLARE SUB range1 ()
DECLARE SUB range2 ()
DECLARE SUB qBOX (x1!, y1!, x2!, y2!, c!)
DECLARE SUB dtp ()
DECLARE FUNCTION edit$ (x!, y!, t$)
DECLARE SUB window1 ()
DECLARE SUB window2 ()
DECLARE FUNCTION get.c3$ ()
DECLARE FUNCTION get.c4$ ()
DECLARE FUNCTION text! (x!, y!, s!, t$)
DECLARE SUB status (n!, t$)
DECLARE FUNCTION unit$ (s$)
DECLARE SUB onedat ()
DECLARE SUB ende ()
DECLARE SUB saved (s$)
DECLARE SUB loadd (s$)
DECLARE SUB stepsize ()
DECLARE SUB showrange ()
DECLARE SUB paper ()
DECLARE SUB BOX (x1!, y1!, x2!, y2!)
DECLARE SUB pbox (x1!, y1!, x2!, y2!)
DECLARE FUNCTION max! (a!, b!)
DECLARE FUNCTION ox! (ax!)
DECLARE FUNCTION kx! (x!)
DECLARE FUNCTION ky! (y!)
DECLARE FUNCTION get.c2$ ()
DECLARE FUNCTION get.c1$ ()
DECLARE SUB pause (t!)
DECLARE FUNCTION value! (s$)

'
' Energya    Version 1.03     9/1995    Markus Hoffmann
'  (c)   LBNL   Lawrence Berkeley National Laboratory   1995
'
' Letzte Bearbeitung: 21.9.1995
' latest change: Luke TS-8 , improvements
'
  COMMON SHARED xmin, xmax, ymin, ymax, xstep, ystep, ex$, ey$, ex, ey
  COMMON SHARED xxmin, xxmax, yymin, yymax, counter
  COMMON SHARED xxmin2, xxmax2, yymin2, yymax2
  COMMON SHARED bx, by, bw, bh, version$
  COMMON SHARED oldx, oldy, olddy
  COMMON SHARED oposx, oposy, posx, posy, vx, vy, vz, errx, erry, erre
  COMMON SHARED font$()
  COMMON SHARED waypoint$(), track$(), almanac$()
  COMMON SHARED flag
  COMMON SHARED existflag, tname$
  COMMON SHARED anztrack, anzal, anzwp, records
  DIM almanac$(32), waypoint$(600), track$(2000)

version$ = "V.1.04"

SCREEN 12         ' 640 * 480 , 16 colors
COLOR 15: PRINT CHR$(11); "      GARMIN   " + version$ + "  (c) 1995 - LBNL   by Markus Hoffmann  "; CHR$(2); " "; CHR$(1);

' Scaling fuer das Koordinatenprogramm  (defaults)
xxmin = -6.95: xxmax = -7.15
yymin = 50.7: yymax = 50.75
xxmin2 = -40: xxmax2 = 10
yymin2 = -4: yymax2 = 1

' Physikalische Einheit fuer die Achsen (defaults)
ex$ = "E"
ey$ = "N"
      
tname$ = "trackdat.trk"


' # Fontdaten einlesen fuer den Linienfont    #
' # Linienfont (c) Markus Hoffmann            #

DIM font$(255)
RESTORE fontdata
READ a
WHILE a <> 0
  READ b
  WHILE b <> 255
    font$(a) = font$(a) + CHR$(b)
    READ b
  WEND
  READ a
WEND

'########## Hauptprogramm ##########
'##########     begin     ##########

' initials
OPEN "com1:9600,N,8,1,DS,CS,RS,CD" FOR RANDOM AS #1
OPEN tname$ FOR OUTPUT AS #5
LOCATE 27, 40: COLOR 14: PRINT "Track: "; tname$

'IF exist("energya.dat") THEN
'  CALL loadd("energya.dat")
'END IF

CALL window1
' call paper
'  call window2: paper: window1


' CALL openoutputfile
 CALL qBOX(5, 315, 300, 403, 2)

' Garmin identifizieren
CALL sendmessage(254, " ")
t$ = getmessage$
CALL procmessage(t$)
t$ = getmessage$
CALL procmessage(t$)

' Position request
CALL sendmessage(10, CHR$(2) + CHR$(0))
t$ = getmessage$
CALL procmessage(t$)
t$ = getmessage$
CALL procmessage(t$)
' time request
CALL sendmessage(10, CHR$(5) + CHR$(0))
t$ = getmessage$
CALL procmessage(t$)
t$ = getmessage$
CALL procmessage(t$)


 '  MENU
 '
  LOCATE 30, 2
  COLOR 14: PRINT "F"; : COLOR 12: PRINT "ile, ";
  COLOR 14: PRINT "A"; : COLOR 12: PRINT "lmanac, ";
  COLOR 14: PRINT "D"; : COLOR 12: PRINT "ate, ";
  COLOR 14: PRINT "P"; : COLOR 12: PRINT "osition, ";
  COLOR 14: PRINT "R"; : COLOR 12: PRINT "oute, ";
  COLOR 14: PRINT "T"; : COLOR 12: PRINT "rack, ";
 
  COLOR 14: PRINT "C"; : COLOR 12: PRINT "lear, ";
  COLOR 14: PRINT "S"; : COLOR 12: PRINT "ave set, ";
  COLOR 14: PRINT "BSP"; : COLOR 12: PRINT " off, ";
  COLOR 14: PRINT "Q"; : COLOR 12: PRINT "uit.";
  CALL BOX(0, 16, 639, 479)
  showrange
jjj:
' PVT aktivieren
CALL sendmessage(10, CHR$(49) + CHR$(0))
' t$ = getmessage$
t$ = getmessage$
CALL procmessage(t$)
DO
  COLOR 15
  z$ = INKEY$
  IF LEN(z$) <> 0 THEN
    IF UCASE$(LEFT$(z$, 1)) = "Q" THEN CALL ende
    IF UCASE$(LEFT$(z$, 1)) = "C" THEN CALL paper
    IF UCASE$(LEFT$(z$, 1)) = "X" THEN CALL range1
 IF UCASE$(LEFT$(z$, 1)) = "F" THEN
      LOCATE 15, 29:
       ffi$ = edit$(25, 21, file$)
'        IF NOT exist(ffi$) THEN
'  doch:
'          CLOSE #5
'          file$ = ffi$
'          CALL openoutputfile

'          anzdata = 0: flag = 0: stable = 0
'        ELSE
'          LOCATE 27, 2
'          PRINT "**** This file already exist ! ****": LOCATE 28, 2
'          INPUT " Overwrite this file ? ", jn$
'          LOCATE 27, 2
'          PRINT "                                   ": LOCATE 28, 2
'          PRINT "                                   "
'
'          IF LEFT$(UCASE$(jn$), 1) = "Y" THEN
'            GOTO doch
'          END IF
'        END IF
    END IF
    IF UCASE$(LEFT$(z$, 1)) = " " THEN
      flag = 0: stable = 0
      CALL sendmessage(10, CHR$(49) + CHR$(0))
    END IF
    IF UCASE$(LEFT$(z$, 1)) = "M" THEN
      stable = NOT stable
      flag = -1
    END IF
    IF UCASE$(LEFT$(z$, 1)) = "Z" THEN flag = NOT flag
    IF UCASE$(LEFT$(z$, 1)) = "S" THEN
      CALL savewp: saveal: savetrack
    ELSEIF UCASE$(LEFT$(z$, 1)) = "W" THEN
      anzwp = 0: CALL sendmessage(10, CHR$(7) + CHR$(0))
    ELSEIF UCASE$(LEFT$(z$, 1)) = "P" THEN
      CALL sendmessage(10, CHR$(2) + CHR$(0))
    ELSEIF LEFT$(z$, 1) = CHR$(27) THEN
      CALL sendmessage(10, CHR$(0) + CHR$(0))
    ELSEIF LEFT$(z$, 1) = CHR$(8) THEN
      CALL sendmessage(10, CHR$(8) + CHR$(0))
    ELSEIF UCASE$(LEFT$(z$, 1)) = "A" THEN
      anzal = 0: CALL sendmessage(10, CHR$(1) + CHR$(0))
    ELSEIF UCASE$(LEFT$(z$, 1)) = "N" THEN
      CALL newwp
    ELSEIF UCASE$(LEFT$(z$, 1)) = "R" THEN
      CALL sendmessage(10, CHR$(4) + CHR$(0))
    ELSEIF UCASE$(LEFT$(z$, 1)) = "D" THEN
      CALL sendmessage(10, CHR$(5) + CHR$(0))
    ELSEIF UCASE$(LEFT$(z$, 1)) = "T" THEN
      anztrack = 0: CALL sendmessage(10, CHR$(6) + CHR$(0))
    ELSEIF UCASE$(LEFT$(z$, 1)) = "L" THEN
      CALL loadwp
    ELSE
      CALL status(3, "Unbek. Taste: " + STR$(ASC(LEFT$(z$, 1))))
    END IF
    showrange
  END IF
  IF TIMER - totzeit > 5 THEN
      CALL sendmessage(10, CHR$(5) + CHR$(0))
      totzeit = TIMER
  END IF

  IF NOT EOF(1) THEN
    CALL status(2, "READ")
  WHILE NOT EOF(1) AND LEN(INKEY$) = 0
    t$ = getmessage$
    CALL procmessage(t$)
  WEND
  CALL status(2, "OK")
  totzeit = TIMER
  showrange
  END IF
LOOP
CALL ende

errortrap:
  SELECT CASE ERR
    CASE 53
      existflag = 0
      RESUME NEXT
  END SELECT
  PRINT "Error #"; ERR
RESUME NEXT



fontdata:
DATA 1,50,90,50,10,85,40,151,10,15,40,255,2,50,10,50,90,85,60,151,90,15,60,255
DATA 3,255,8,0,50,50,100,100,0,255,32,255,33,50,0,50,70,151,90,50,100,255
DATA 34,40,0,40,30,161,0,60,30,255
DATA 35,0,40,100,40,101,60,100,60,141,0,40,100,161,0,60,100,255
DATA 36,100,25,75,0,25,0,0,25,25,50,75,50,100,75,75,100,25,100,0,75,141,0,40,100,161,0,60,100,255
DATA 37,0,100,100,0,111,10,20,10,20,20,10,20,10,10,191,90,80,90,80,80,90,80,90,90,255
DATA 38,100,50,50,100,0,75,60,30,40,0,30,30,100,100,255
DATA 39,60,0,40,30,255,40,60,0,40,30,40,70,60,100,255
DATA 41,40,0,60,30,60,70,40,100,255
DATA 42,10,50,90,50,151,10,50,90,121,20,80,80,181,20,20,80,255
DATA 43,10,50,90,50,151,10,50,90,255,44,60,70,40,100,255,45,10,50,90,50,255
DATA 46,50,100,49,100,255,47,10,100,90,0,255
' 0
DATA 48,100,30,70,0,30,0,0,30,0,70,30,100,70,100,100,70,100,30,141,40,60,60,255
' 1
DATA 49,40,10,50,0,50,100,255
' 2
DATA 50,0,30,30,0,70,0,100,30,100,50,0,100,100,100,255
' 3
DATA 51,10,20,30,0,70,0,100,30,100,40,50,50,100,60,100,70,70,100,30,100,10,80,255
' 4
DATA 52,70,100,70,0,10,60,100,60,255
' 5
DATA 53,100,0,0,0,0,40,70,40,100,60,100,70,70,100,30,100,0,70,255
' 6
DATA 54,70,0,30,0,0,30,0,70,30,100,70,100,100,70,100,60,60,60,0,50,255
' 7
DATA 55,0,0,100,0,100,30,30,100,255
' 8
DATA 56,0,25,25,0,75,0,100,25,75,50,25,50,0,25,126,50,0,75,25,100,75,100,100,75,75,50,255
' 9
DATA 57,0,25,25,0,75,0,100,25,100,75,75,100,35,100,10,75,101,25,25,50,100,50,255
' :
DATA 58,50,30,50,31,151,70,50,71,255
' ;
DATA 59,50,30,50,31,151,70,40,100,255
' <
DATA 60,90,20,0,50,90,80,255
' =
DATA 61,10,30,90,30,111,70,90,70,255
' >
DATA 62,10,20,100,50,10,80,255
DATA 63,0,25,25,0,75,0,100,25,50,50,50,70,151,90,50,100,255
DATA 64,100,20,70,0,30,0,0,30,0,70,30,100,70,100,90,90,201,40,100,70,75,75,50,70,50,60,75,55,255
' A
DATA  65,0,100,0,30,30,0,70,0,100,30,100,100,100,50,0,50,255
' B
DATA  66,0,100,0,0 ,70,0,90,10,90,40,50,50,0,50,50,50,90,60,90,90,70,100,0,100,255
' C
DATA  67,100,20,70,0,30,0,0,30,0,70,30,100,70,100,100,80,255
' D
DATA  68,0,0,0,100,70,100,100,70,100,30,70,0,0,0,255
' E
DATA  69,100,0,0,0,0,100,100,100,101,50,100,50,255
' F
DATA  70,100,0,0,0,0,100,101,50,100,50,255
' G
DATA  71,100,20,70,0,30,0,0,30,0,70,30,100,100,100,100,60,50,60,255
' H
DATA  72,0,0,0,100,101,50,100,50,201,100,100,0,255
' I
DATA 73,50,0,50,100,141,0,60,0,141,100,60,100,255
' J
DATA 74,50,0,50,80,30,110,141,0,60,0,255
' K
DATA 75,0,0,0,100,101,50,100,0,101,50,100,100,255
' L
DATA 76,0,0,0,100,100,100,255
' M
DATA 77,0,100,0,0,50,50,100,0,100,100,255
' N
DATA 78, 0, 100, 0, 0, 100, 100, 100, 0, 255
' O
DATA 79,100,30,70,0,30,0,0,30,0,70,30,100,70,100,100,70,100,30,255
' P
DATA 80,0,100,0,0,75,0,100,25,75,50,0,50,255
' Q
DATA 81,100,30,70,0,30,0,0,30,0,70,30,100,70,100,100,70,100,30,171,70,100,100,255
' R
DATA 82,0,100,0,0,75,0,100,25,75,50,0,50,100,100,255
' S
DATA 83,100,25,75,0,25,0,0,25,25,50,75,50,100,75,75,100,25,100,0,75,255
' T
DATA 84,0,0,100,0,151,0,50,100,255
' U
DATA 85,0,0,0,75,25,100,75,100,100,75,100,0,255
' V
DATA 86,0,0,50,100,100,0,255
' W
DATA 87,0,0,25,100,50,0,75,100,100,0,255
' X
DATA 88,0,0,100,100,201,0,0,100,255
' Y
DATA 89,0,0,50,50,100,0,151,50,50,100,255
' Z
DATA 90,0,0,100,0,0,100,100,100,255
' [
DATA 91,50,0,10,0,10,100,50,100,255,92,10,10,90,90,255
' ]
DATA 93,50,0,90,0,90,100,50,100,255,94,10,30,50,0,90,30,255
' _
DATA 95,0,100,100,100,255,96,50,0,60,30,255
' a
DATA 97,0,60,30,40,70,40,100,60,100,100,201,70,70,100,30,100,0,80,30,60,70,60,100,80,255
' b
DATA 98,0,60,30,40,70,40,100,70,100,70,70,100,30,100,0,80,101,0,0,100,255
' c
DATA 99,100,60,70,40,30,40,0,70,0,70,30,100,70,100,100,80,255
' d
DATA 100,100,60,70,40,30,40,0,70,0,70,30,100,70,100,100,80,201,0,100,100,255
' e
DATA 101,0,70,100,70,70,40,30,40,0,70,0,70,30,100,70,100,100,85,255
' f
DATA 102,90,20,70,0,50,20,50,120,40,130,121,50,80,50,255
' g
DATA 103,100,60,70,40,30,40,0,70,0,70,30,100,70,100,100,80,201,40,100,120,60,130,20,120,60,110,100,120,255
' h
DATA 104,0,0,0,100,101,60,25,40,75,40,100,60,100,100,255
' i
DATA 105,50,40,50,100,151,10,50,20,255
' j
DATA 106,50,40,50,100,40,130,151,10,50,20,255
' k
DATA 107,0,0,0,100,101,60,100,100,101,60,90,30,255
' l
DATA 108,50,0,50,100,55,100,255
' m
DATA 109,0,40,0,100,101,60,25,40,30,40,50,60,50,100,151,60,70,40,75,40,100,60,100,100,255
' n
DATA 110,0,40,0,100,101,60,25,40,75,40,100,60,100,100,255
' o
DATA 111,70,40,30,40,0,70,0,70,30,100,70,100,100,70,70,40,255
' p
DATA 112,0,60,30,40,70,40,100,70,100,70,70,100,30,100,0,80,101,40,0,130,255
' q
DATA 113,100,60,70,40,30,40,0,70,0,70,30,100,70,100,100,80,201,40,100,130,255
' r
DATA 114,0,100,0,40,101,60,30,40,70,40,255
' s
DATA 115,100,50,75,40,25,40,0,55,25,70,75,70,100,85,75,100,25,100,0,85,255
' t
DATA 116,50,0,50,90,70,100,111,40,90,40,255
' u
DATA 117,0,40,0,70,30,100,70,100,100,70,100,40,201,40,100,100,255
' v
DATA 118,0,40,50,100,100,40,255
' w
DATA 119,0,40,25,100,50,40,75,100,100,40,255
' x
DATA 120,0,100,100,40,101,40,100,100,255
' y
DATA 121,0,40,50,100,201,40,25,130,255
' z
DATA 122,0,40,100,40,0,100,100,100,255
DATA 123,0,50,25,50,25,0,50,0,126,50,25,100,50,100,255
DATA 124,50,0,50,100,255,125,100,50,75,50,75,0,50,0,176,50,75,100,50,100,255
DATA 126,0,50,25,25,50,50,75,75,100,50,255,127,0,100,100,100,50,40,0,100,255,0

                 ' absolutes Ende (des Hauptprogramms)

SUB BOX (x1, y1, x2, y2)
' Draw a simple frame with coordinates x,y upper left and x,y down right

  LINE (x1, y1)-(x2, y1)
  LINE (x2, y1)-(x2, y2)
  LINE (x2, y2)-(x1, y2)
  LINE (x1, y2)-(x1, y1)
END SUB

SUB boxtext (x, y, f, t$)
COLOR 14: CALL BOX(x - 2, y - 2, x + 130 * f * LEN(t$) + 2, y + 130 * f + 2)
 void = text(x, y, f, t$)

END SUB

FUNCTION edit$ (crx, cry, t$)
old$ = t$
COLOR 15
LOCATE crx, cry: PRINT t$; CHR$(2); " ";
DO
  a$ = INKEY$
  IF LEN(a$) THEN
    IF a$ = CHR$(13) THEN GOTO ex
   
    IF a$ = CHR$(8) THEN
      IF LEN(t$) THEN
        t$ = LEFT$(t$, LEN(t$) - 1)
      ELSE
        BEEP
      END IF
    END IF
    IF a$ = CHR$(27) THEN t$ = ""
    IF a$ = CHR$(9) THEN t$ = old$
    IF ASC(a$) > 31 THEN t$ = t$ + a$
    LOCATE crx, cry: PRINT t$; CHR$(2); " ";
  END IF
LOOP
ex:
LOCATE crx, cry: PRINT t$; "  ";
edit$ = t$
t$ = old$
END FUNCTION

SUB ende
' Quit the program, close all open files remove all installations

  CLOSE #1
  CLOSE #5
  END
END SUB

FUNCTION exist (f$)
ON ERROR GOTO errortrap
 existflag = -1
 OPEN "I", #99, f$
 CLOSE #99
 ON ERROR GOTO 0
 exist = existflag
END FUNCTION

FUNCTION getmessage$
t = TIMER
s$ = ""
flag = 0
flag2 = 0
DO
  IF NOT EOF(1) THEN
    t$ = INPUT$(1, #1)
  '  PRINT "Got:"; ASC(t$)
    IF t$ = CHR$(16) THEN
      IF flag2 = 0 THEN
        flag2 = 1
      ELSE
        IF flag = 0 THEN
          flag = 1
        ELSE
          s$ = s$ + t$
          flag = 0
        END IF
      END IF
    ELSE
      IF flag2 = 0 THEN
        PRINT "protokollfehler: muell "; ASC(t$)
      ELSE

      IF flag = 1 AND t$ = CHR$(3) THEN
        GOTO t
      ELSEIF flag = 1 THEN
        PRINT "Protokoll-ERROR 16"
        flag = 0
      END IF
      s$ = s$ + t$
    END IF
    END IF
  END IF
 IF TIMER - t > 4 THEN
      CALL status(1, "TIMEOUT"): s$ = "TIMEOUT 0 ?"
      PLAY "MBT400O1L4A"
      GOTO t
  END IF
LOOP
t:
pid = ASC(MID$(s$, 1, 1))
chk = ASC(MID$(s$, LEN(s$), 1))
' PRINT "PID="; pid
' PRINT "CHK="; chk
chk2 = 0
FOR i = 1 TO LEN(s$) - 1
chk2 = chk2 - ASC(MID$(s$, i, 1))
NEXT i
chk2 = chk2 AND 255
' PRINT "CHK2="; ASC(CHR$(chk2))
IF chk = chk2 AND pid <> 6 AND pid <> 21 THEN
  CALL sendACK(pid)
ELSEIF chk <> chk2 THEN
  CALL status(1, "Uebertragungsfehler CHK")
END IF
getmessage$ = s$
END FUNCTION

FUNCTION kx (x)
kx = (x - xmin) * bw / (xmax - xmin) + bx
END FUNCTION

FUNCTION ky (y)
ky = -bh / (ymax - ymin) * (y - ymax) + by
END FUNCTION

SUB loadwp
OPEN "waypoint.dat" FOR INPUT AS #2
anzwp = 0
WHILE NOT EOF(2)
dlen = ASC(INPUT$(1, #2))
PRINT anzwp, dlen
waypoint$(anzwp) = INPUT$(dlen, #2)
anzwp = anzwp + 1
WEND
CLOSE #2
CALL status(3, STR$(anzwp) + "WP geladen")
END SUB

FUNCTION max (a, b)
IF a > b THEN max = a
IF b >= a THEN max = b
END FUNCTION

FUNCTION mystr$ (a)
t$ = STR$(a)
WHILE LEFT$(t$, 1) = " "
  t$ = RIGHT$(t$, LEN(t$) - 1)
WEND
mystr$ = t$
END FUNCTION

SUB newwp
  name$ = "TEST"
  alt = 0
  c = INT(bx / 8) + 1: r = INT(by / 16) + 2
  x = INT(bx / 8) * 8 + 4
  y = INT(by / 16) * 16 + 8
  CALL qBOX(x, y, x + 200, y + 120, 3)
  LINE (x + 3, y + 30)-(x + 197, y + 30)
  COLOR 14: void = text(x + 10, y + 10, .13, "Edit Waypoint:")
  COLOR 2
  LOCATE r + 2, c + 2: PRINT "Name  ="; name$
  LOCATE r + 3, c + 2: PRINT "Breite=";
  LOCATE r + 4, c + 2: PRINT "L�nge =";
  LOCATE r + 5, c + 2: PRINT "H�he  =";
  LOCATE r + 6, c + 2: PRINT "Typ   =";
  name$ = UCASE$(edit$(r + 2, c + 9, name$))
  wpid = -1
  FOR i = 0 TO anzwp - 1
    IF name$ + CHR$(0) = MID$(waypoint$(i), 49, LEN(name$) + 1) THEN
      wpid = i
    END IF
  NEXT i
  IF wpid = -1 THEN
    PRINT " ist neu !"
    x = 50
    y = 7
    styp = 150
    alt = 0
  ELSE
    styp = ASC(MID$(waypoint$(wpid), 5, 1))
    alt = CVS(MID$(waypoint$(wpid), 33, 4))
    x = 180 / 2 ^ 31 * CVL(MID$(waypoint$(wpid), 25, 4))
    y = 180 / 2 ^ 31 * CVL(MID$(waypoint$(wpid), 29, 4))
  END IF
  IF x > 0 THEN
    posx$ = "N"
  ELSE
    posx$ = "S"
  END IF
  x = ABS(x)
  IF y > 0 THEN
    posy$ = "E"
  ELSE
    posy$ = "W"
  END IF
  y = ABS(y)
  posx$ = posx$ + RIGHT$("00" + mystr$(INT(x)), 2) + ":"
  posy$ = posy$ + RIGHT$("000" + mystr$(INT(y)), 3) + ":"
  x = x - INT(x)
  y = y - INT(y)
  x = x * 60
  y = y * 60
  posx$ = posx$ + RIGHT$("00" + mystr$(INT(x)), 2) + ":"
  posy$ = posy$ + RIGHT$("00" + mystr$(INT(y)), 2) + ":"
  x = x - INT(x)
  y = y - INT(y)
  x = x * 60
  y = y * 60
  posx$ = posx$ + RIGHT$("00" + mystr$(INT(x)), 2) + "."
  posy$ = posy$ + RIGHT$("00" + mystr$(INT(y)), 2) + "."
  x = x - INT(x)
  y = y - INT(y)
  x = x * 10
  y = y * 10
  posx$ = posx$ + mystr$(INT(x))
  posy$ = posy$ + mystr$(INT(y))
nochmal:
  posx$ = UCASE$(edit$(r + 3, c + 9, posx$))
  posy$ = UCASE$(edit$(r + 4, c + 9, posy$))
  IF LEN(posx$) <> 11 OR LEN(posy$) <> 12 THEN GOTO nochmal
  x = VAL(MID$(posx$, 2, 2)) + VAL(MID$(posx$, 5, 2)) / 60 + VAL(MID$(posx$, 8, 4)) / 60 / 60
  y = VAL(MID$(posy$, 2, 3)) + VAL(MID$(posy$, 6, 2)) / 60 + VAL(MID$(posy$, 9, 4)) / 60 / 60
  IF UCASE$(LEFT$(posx$, 1)) = "S" THEN x = -x
  IF UCASE$(LEFT$(posy$, 1)) = "W" THEN y = -y

  alt = VAL(edit$(r + 5, c + 9, STR$(alt)))
  styp = VAL(edit$(r + 6, c + 9, STR$(styp)))


x = x * 2 ^ 31 / 180
y = y * 2 ^ 31 / 180

dtyp = 0
depth = 1E+25
dist = 0
comment$ = ""
facility$ = ""
city$ = ""
addr$ = ""
cr$ = ""

t$ = CHR$(0) + CHR$(255) + CHR$(dtyp) + CHR$(6 * 16) + CHR$(styp) + CHR$(0)
t$ = t$ + CHR$(0) + CHR$(0) + MKL$(0) + MKL$(-1) + MKL$(-1) + MKL$(-1)
t$ = t$ + MKL$(x) + MKL$(y) + MKS$(alt) + MKS$(depth)
t$ = t$ + MKS$(dist) + "D MH" + name$ + CHR$(0)
t$ = t$ + comment$ + CHR$(0)
t$ = t$ + facility$ + CHR$(0)
t$ = t$ + city$ + CHR$(0)
t$ = t$ + addr$ + CHR$(0)
t$ = t$ + cr$ + CHR$(0)
INPUT "Wirklich aendern ?", a$
IF UCASE$(LEFT$(a$, 1)) = "J" THEN
CALL sendmessage(27, CHR$(1) + CHR$(0))
CALL sendmessage(35, t$)
CALL sendmessage(12, CHR$(7) + CHR$(0))
END IF
END SUB

    FUNCTION ox (ax)
ox = (ax - bx) * (xmax - xmin) / bw + xmin
END FUNCTION

SUB paper
  ' display graphics window with grid an axis

  COLOR 15: CALL pbox(bx, by, bx + bw, by + bh)
  COLOR 5:  CALL BOX(bx, by, bx + bw, by + bh)
  COLOR 7
  textfaktor = bw / 10000
  void = 0
  FOR x = 0 TO max(ABS(xmin), ABS(xmax)) STEP xstep
    COLOR 7
    IF x > xmin AND x < xmax THEN LINE (kx(x), ky(ymin))-(kx(x), ky(ymax))
    IF -x > xmin AND -x < xmax THEN LINE (kx(-x), ky(ymin))-(kx(-x), ky(ymax))
    COLOR 0
    IF x > xmin AND x < xmax THEN LINE (kx(x), ky(0) + 2)-(kx(x), ky(0) - 2)
    IF -x > xmin AND -x < xmax THEN LINE (kx(-x), ky(0) + 2)-(kx(-x), ky(0) - 2)
    x$ = LTRIM$(STR$(x))
    x2$ = LTRIM$(STR$(-x))
    IF kx(x) - .06 * LEN(x$) * 130 / 2 > void + 2 THEN
      void = text(kx(x) - textfaktor * LEN(x$) * 130 / 2, ky(0) + textfaktor * 50, textfaktor, x$)
      void2 = text(kx(-x) - textfaktor * LEN(x2$) * 130 / 2, ky(0) + textfaktor * 50, textfaktor, x2$)
    END IF
  NEXT x

  FOR y = 0 TO max(ABS(ymin), ABS(ymax)) STEP ystep
    COLOR 7
    IF y > ymin AND y < ymax THEN LINE (kx(xmin), ky(y))-(kx(xmax), ky(y))
    IF -y > ymin AND -y < ymax THEN LINE (kx(xmin), ky(-y))-(kx(xmax), ky(-y))
    COLOR 0
    IF y > ymin AND y < ymax THEN LINE (kx(0) - 2, ky(y))-(kx(0) + 2, ky(y))
    IF -y > ymin AND -y < ymax THEN LINE (kx(0) - 2, ky(-y))-(kx(0) + 2, ky(-y))
    y$ = LTRIM$(STR$(y)): y2$ = LTRIM$(STR$(-y))
    IF y > ymin AND y < ymax THEN void = text(kx(0) - textfaktor * 130 * LEN(y$) - 2, ky(y) - textfaktor * 50, textfaktor, y$)
    IF -y > ymin AND -y < ymax THEN void = text(kx(0) - textfaktor * 130 * LEN(y2$) - 2, ky(-y) - textfaktor * 50, textfaktor, y2$)

  NEXT y

' Koordinatenachsen

  COLOR 4
  IF SGN(xmin) <> SGN(xmax) THEN LINE (kx(0), ky(ymin))-(kx(0), ky(ymax))
  IF SGN(ymin) <> SGN(ymax) THEN LINE (kx(xmin), ky(0))-(kx(xmax), ky(0))
  FOR i = -3 TO 3                ' Pfeile
    IF SGN(xmin) <> SGN(xmax) THEN LINE (kx(0) + i, ky(ymax) + 10)-(kx(0), ky(ymax))
    IF SGN(ymin) <> SGN(ymax) THEN LINE (kx(xmax) - 10, ky(0) + i)-(kx(xmax), ky(0))
  NEXT i

  'Beschriftung

  COLOR 0
  void = text(bx + bw - textfaktor * 2 * 130 * LEN(ex$), ky(0) - textfaktor * 2 * 160, textfaktor * 2, ex$)
  void = text(kx(0) + textfaktor * 2 * 130, by + 130 * textfaktor * 2, textfaktor * 2, ey$)

  void = text(bx + bw - textfaktor * 130 * 26, by + bh - 130 * textfaktor, textfaktor, "(C) MARKUS HOFFMANN 9'1995")
END SUB

SUB pause (t)
f = TIMER
WHILE TIMER - f < t
WEND

END SUB

SUB pbox (x1, y1, x2, y2)
  FOR x = x1 TO x2
    LINE (x, y1)-(x, y2)
  NEXT x
END SUB

SUB procalmanacdata (t$)
plen = ASC(MID$(t$, 2, 1))
almanac$(anzal) = MID$(t$, 3, plen)
anzal = anzal + 1

END SUB

SUB procmessage (t$)
pid = ASC(LEFT$(t$, 1))
plen = ASC(MID$(t$, 2, 1))
IF pid = 6 THEN
  ' ACK, alles OK, ignorieren
  CALL status(1, "OK")
ELSEIF pid = 12 THEN
  CALL procxfercmplt(t$)
ELSEIF pid = 20 OR pid = 14 THEN
  CALL proctimedata(t$)
ELSEIF pid = 21 THEN
  CALL status(1, "NAK !")
ELSEIF pid = 24 OR pid = 17 THEN
  CALL procpositiondata(t$)
ELSEIF pid = 27 THEN
  CALL procrecords(t$)
ELSEIF pid = 29 THEN
  CALL procroutename(t$)
ELSEIF pid = 31 THEN
  CALL procalmanacdata(t$)
ELSEIF pid = 34 THEN
  CALL proctrackdata(t$)
ELSEIF pid = 35 THEN
  CALL procwpdata(t$)
ELSEIF pid = 99 THEN
  CALL proctrackname(t$)
ELSEIF pid = 253 THEN
  CALL procprotocollarray(t$)
ELSEIF pid = 254 OR pid = 10 THEN
  PRINT "Paket nicht erlaubt ! Pid="; pid
ELSEIF pid = 255 THEN
  CALL procproductdata(t$)
ELSE
  CALL status(2, "? Paket pid=" + STR$(pid) + " !")
END IF
END SUB

SUB procpositiondata (t$)
COLOR 0
CIRCLE (kx(oposx), ky(oposy)), 4
LINE (kx(oposx) - 5, ky(oposy))-(kx(oposx) + 5, ky(oposy))
LINE (kx(oposx), ky(oposy) - 5)-(kx(oposx), ky(oposy) + 5)

posy = CVD(MID$(t$, 3, 8)) * 180 / 3.141592
posx = CVD(MID$(t$, 11, 8)) * 180 / 3.141592
COLOR 14: LOCATE 24, 42
PRINT "Position: x="; posx; " y="; posy
posx = -posx
CIRCLE (kx(posx), ky(posy)), 4
LINE (kx(posx) - 5, ky(posy))-(kx(posx) + 5, ky(posy))
LINE (kx(posx), ky(posy) - 5)-(kx(posx), ky(posy) + 5)

END SUB

SUB procproductdata (t$)
garminPID = toint(MID$(t$, 3, 2))
garminVER = toint(MID$(t$, 5, 2))
garminMES$ = MID$(t$, 7, LEN(t$) - 7 - 1)
CALL status(5, garminMES$)
END SUB

SUB procprotocollarray (t$)
plen = ASC(MID$(t$, 2, 1))
anz = plen / 3
PRINT "Es werden folgende "; anz; " Protokolle unterstuetzt:"
FOR i = 0 TO anz - 1
  PRINT MID$(t$, 3 + i * 3, 1); toint(MID$(t$, 3 + i * 3 + 1, 2)); " ";
NEXT i
END SUB

SUB procrecords (t$)
  records = toint(MID$(t$, 3, 2))
  CALL status(3, "load " + STR$(records) + "...")
END SUB

SUB procroutename (t$)
COLOR 14: LOCATE 28, 40: PRINT "Route: ";
PRINT MID$(t$, 3, ASC(MID$(t$, 2, 1)) - 1); "  "
END SUB

SUB proctimedata (t$)
month = ASC(MID$(t$, 3, 1))
day = ASC(MID$(t$, 4, 1))
year = toint(MID$(t$, 5, 2))
hour = toint(MID$(t$, 7, 2))
minute = ASC(MID$(t$, 9, 1))
second = ASC(MID$(t$, 10, 1))
ndate$ = STR$(day) + "." + STR$(month) + "." + STR$(year)
ntime$ = mystr$(hour) + ":" + mystr$(minute) + ":" + mystr$(second)
LOCATE 25, 42: PRINT "ZEIT: "; ndate$; "  "; ntime$


END SUB

SUB proctrackdata (t$)
plen = ASC(MID$(t$, 2, 1))
' track$(anztrack) = MID$(t$, 3, plen)
anztrack = anztrack + 1
y = 180 / 2 ^ 31 * CVL(MID$(t$, 3, 4))
x = -180 / 2 ^ 31 * CVL(MID$(t$, 7, 4))
PRINT #5, CHR$(plen); MID$(t$, 3, plen);
CIRCLE (kx(x), ky(y)), 1
END SUB

SUB proctrackname (t$)
dlen = ASC(MID$(t$, 2, 1))
disp = ASC(MID$(t$, 3, 1))
col = ASC(MID$(t$, 4, 1))
tname$ = LEFT$(MID$(t$, 5, dlen - 3) + "________", 8) + ".trk"
CLOSE #5
OPEN tname$ FOR OUTPUT AS #5
LOCATE 27, 40: COLOR 14: PRINT "Track: "; MID$(t$, 5, dlen - 3); "   "
END SUB

SUB procwpdata (t$)
plen = ASC(MID$(t$, 2, 1))
waypoint$(anzwp) = MID$(t$, 3, plen)
anzwp = anzwp + 1
y = 180 / 2 ^ 31 * CVL(MID$(t$, 27, 4))
x = -180 / 2 ^ 31 * CVL(MID$(t$, 31, 4))

CIRCLE (kx(x), ky(y)), 3
void = text(kx(x), ky(y) + 3, .05, MID$(t$, 51, 6))
END SUB

SUB procxfercmplt (t$)
CALL status(3, STR$(records) + " geladen (typ=" + STR$(ASC(MID$(t$, 3, 1))) + ")")
END SUB

SUB qBOX (x1, y1, x2, y2, c)
  COLOR 0: CALL pbox(x1, y1, x2, y2)
  COLOR c: CALL BOX(x1, y1, x2, y2): CALL BOX(x1 + 1, y1 + 1, x2 - 1, y2 - 1)
  CALL BOX(x1 + 3, y1 + 3, x2 - 3, y2 - 3)
END SUB

SUB range1
 CALL window1
 c = INT(bx / 8) + 1: r = INT(by / 16) + 2
 x = INT(bx / 8) * 8 + 4
 y = INT(by / 16) * 16 + 8
  CALL qBOX(x, y, x + 140, y + 120, 3)
      LINE (x + 3, y + 30)-(x + 137, y + 30)
      COLOR 14: void = text(x + 10, y + 10, .13, "Range:")
      COLOR 2
      LOCATE r + 2, c + 2: PRINT "xmin ="; xmin; ex$
      LOCATE r + 3, c + 2: PRINT "xmax ="; xmax; ex$
      LOCATE r + 4, c + 2: PRINT "x'min="; ymin; ey$
      LOCATE r + 5, c + 2: PRINT "x'max="; ymax; ey$
       xxmin = VAL(edit$(r + 2, c + 8, STR$(xxmin)))
       xxmax = VAL(edit$(r + 3, c + 8, STR$(xxmax)))
       yymin = VAL(edit$(r + 4, c + 8, STR$(yymin)))
       yymax = VAL(edit$(r + 5, c + 8, STR$(yymax)))
      CALL window1: paper
END SUB

SUB range2
 CALL window2
 c = INT(bx / 8) + 2: r = INT(by / 16) + 3
 x = INT(bx / 8) * 8 + 12
 y = INT(by / 16) * 16 + 24
  CALL qBOX(x, y, x + 140, y + 120, 3)
      LINE (x + 3, y + 30)-(x + 137, y + 30)
      COLOR 14: void = text(x + 10, y + 10, .13, "Range:")
      COLOR 2
      LOCATE r + 2, c + 2: PRINT "xmin ="; xmin; ex$
      LOCATE r + 3, c + 2: PRINT "xmax ="; xmax; ex$
      LOCATE r + 4, c + 2: PRINT "x'min="; ymin; ey$
      LOCATE r + 5, c + 2: PRINT "x'max="; ymax; ey$
       xxmin2 = VAL(edit$(r + 2, c + 8, STR$(xxmin2)))
       xxmax2 = VAL(edit$(r + 3, c + 8, STR$(xxmax2)))
       yymin2 = VAL(edit$(r + 4, c + 8, STR$(yymin2)))
       yymax2 = VAL(edit$(r + 5, c + 8, STR$(yymax2)))

      
      CALL window2: paper: window1

END SUB

SUB saveal
IF anzal > 0 THEN
  OPEN "almanac.dat" FOR OUTPUT AS #2
    FOR i = 0 TO anzal - 1
      PRINT #2, CHR$(LEN(almanac$(i))); almanac$(i);
    NEXT i
    CLOSE #2
  END IF
END SUB

SUB saved (s$)
  OPEN s$ FOR OUTPUT AS #10
  PRINT #10, " Data-File for ENERGYA.BAS (c) LBNL , Version: " + version$ + " : Date: " + DATE$
  PRINT #10, ex$
  PRINT #10, ey$
  PRINT #10, xxmin, xxmax, yymin, yymax
  PRINT #10, xxmin2, xxmax2, yymin2, yymax2

  CLOSE 10
END SUB

SUB savetrack

END SUB

SUB savewp
  IF anzwp > 0 THEN
    OPEN "waypoint.dat" FOR OUTPUT AS #2
    FOR i = 0 TO anzwp - 1
      PRINT #2, CHR$(LEN(waypoint$(i))); waypoint$(i);
    NEXT i
    CLOSE #2
  END IF
END SUB

SUB sendACK (pid)
CALL sendmessage(6, CHR$(pid))
END SUB

SUB sendmessage (id, m$)
  chk = 0 - id - LEN(m$)
  PRINT #1, CHR$(16); CHR$(id); CHR$(LEN(m$));
  IF LEN(m$) = 16 THEN
    PRINT #1, CHR$(16);
  END IF
  IF LEN(m$) THEN

  FOR i = 1 TO LEN(m$)
    a = ASC(MID$(m$, i, 1))
    PRINT #1, CHR$(a);
    IF a = 16 THEN
      PRINT #1, CHR$(16);
    END IF
    chk = chk - a
  NEXT i
  END IF
  chk = chk AND 255
  PRINT #1, CHR$(chk);
  IF chk = 16 THEN
    PRINT #1, CHR$(16);
  END IF
 ' PRINT "Sent: CHK="; chk
  PRINT #1, CHR$(16); CHR$(3);
END SUB

SUB sendnack
CALL sendmessage(21, "")
END SUB

SUB showrange

  LOCATE 27, 2: COLOR 12 + 2 * ABS(anzal > 31): PRINT "ALMANAC";
  LOCATE 28, 2: COLOR 12 + 2 * ABS(anzwp > 0): PRINT "#WP: "; anzwp;
  LOCATE 29, 2: COLOR 12 + 2 * ABS(anztrack > 0): PRINT "#TR: "; anztrack;
  COLOR 12
  LOCATE 23, 4: PRINT "ACTION:"
  LOCATE 24, 4: PRINT "STATUS:"
  LOCATE 25, 4: PRINT "RESULT:"
END SUB

SUB status (n, t$)
LOCATE 25 + 1 - n, 12: PRINT LEFT$(t$ + "                          ", 25);
END SUB

SUB stepsize
' calculate stepsize from range-data

xstep = (xmax - xmin) / 10
ystep = (ymax - ymin) / 10
FOR i = -13 TO 13
  IF xstep > 5 * 10 ^ i AND xstep < 5 * 10 ^ (i + 1) THEN xstep = 10 ^ (i + 1)
  IF ystep > 5 * 10 ^ i AND ystep < 5 * 10 ^ (i + 1) THEN ystep = 10 ^ (i + 1)
NEXT i

END SUB

FUNCTION text (x, y, s, t$)
IF LEN(t$) THEN
  FOR i = 1 TO LEN(t$)
    a = ASC(MID$(t$, i, 1))
    IF LEN(font$(a)) THEN
      FOR j = 1 TO LEN(font$(a)) STEP 2
        xx = ASC(MID$(font$(a), j, 1))
        yy = ASC(MID$(font$(a), j + 1, 1))
        IF xx > 100 THEN
          xx = xx - 101
        ELSE
          IF j <> 1 THEN
            LINE (cox * s + x, coy * s + y)-(xx * s + x, yy * s + y)
          END IF
        END IF
        cox = xx: coy = yy
        
      NEXT j
    END IF
     x = x + 130 * s
  NEXT i
END IF
text = x
END FUNCTION

FUNCTION toint (m$)
toint = ASC(MID$(m$, 1, 1)) + 256! * ASC(MID$(m$, 2, 1))
END FUNCTION

SUB window1
' Bildschirmfenster fuer die Grafik
bx = 5: by = 20: bw = 630: bh = 270
xmin = xxmin: ymin = yymin: xmax = xxmax: ymax = yymax
stepsize
END SUB

SUB window2
' 2.Bildschirmfenster fuer die Grafik
bx = 310: by = 300: bw = 320: bh = 170
xmin = xxmin2: xmax = xxmax2
ymin = yymin2: ymax = yymax2
stepsize
END SUB

