#include <stdio.h>

int main() {
    int left_edge, right_edge, top_edge, bottom_edge, max_iter,
        x_step, y_step, y0, x0, x, y, i,x_x, y_y, temp, the_char;

    left_edge   = -420;       // left edge = -2.1
    right_edge  = 300;        // right edge = 1.5
    top_edge    = 300;        // top edge = 1.5
    bottom_edge = -300;       // bottom edge = -1.5
    x_step      = 7;          // x step size
    y_step      = 15;         // y step size

    max_iter    = 200;        // max iteration depth

    for (y0 = top_edge; y0 > bottom_edge; y0 -= y_step) {
        for (x0 = left_edge; x0 < right_edge; x0 += x_step) {
            y = 0;
            x = 0;
            the_char = ' ';                     // char to be displayed
            for (i = 0; i < max_iter; i++) {
                x_x = (x * x) / 200;
                y_y = (y * y) / 200;
                if (x_x + y_y > 800 ) {
                    the_char = '0' + i;         // print digit 0...9
                    if (i > 9) {
                        the_char = '@';         //  print '@'
                    }
                    break;
                }
                temp = x_x - y_y + x0;

                if ((x < 0 && y > 0) || (x > 0 && y < 0)) {
                    y = (-1 * ((-1 * x * y) / 100)) + y0;
                } else {
                    y = x * y / 100 + y0;
                }
                x = temp;
            }
            printf("%c", the_char);
        }
        printf("\n");
    }
    return 0;
}
