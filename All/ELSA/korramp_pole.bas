'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'%
'% Name:
'%	rampe.bas
'%
'% Purpose:
'%	Erzeugung von Rampen fuer den gesamten ELSA-Zyklus mit Variabilitaeten
'%   fuer den Betrieb mit Polarisierten Elektronen
'%
'% Author:
'% 	Markus Hoffmann im feb 1999
'%
'% Modification History:
'%		25.02.99 Markus Hoffmann: erste Version
'%      08.03.99 einigermassen getestet
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ECHO OFF

anz_korrektoren=32
deltat=0.002

DIM vangle(anz_korrektoren)
DIM vflag(anz_korrektoren)
DIM hkicks(anz_korrektoren)
DIM vkicks(anz_korrektoren)
DIM harme(7),harmse(7),harmee(7),sincomp(7),coscomp(7)
DIM startbump(7),maxbump(7),endbump(7)

' Azimuthwinkel der Korrektoren
azdata:
DATA 0,0,1,0,2,0,4,0,7,8,0,10,0,11,0,0,12,12,13,0,14,15,0,17,19,20,0,22,0,23,0,0
RESTORE azdata
FOR i=0 TO anz_korrektoren-1
  READ a
  vangle(i)=pi/12*a
NEXT i

' Korrektoren fuer harmonische Korrektur benutzen oder nicht
vfdata:
DATA 1,0,1,0,1,0,1,0,1,1,0,1,0,1,0,0,1,0,1,0,1,1,0,1,1,1,0,1,0,1,0,0
RESTORE vfdata
FOR i=0 TO anz_korrektoren-1
  READ a
  vflag(i)=a
NEXT i

SETFONT "*Courier*24*"
schwarz=GET_COLOR(0,0,0)
weiss=GET_COLOR(65535,65535,65535)
gelb=GET_COLOR(65535,65535,0)
rot=GET_COLOR(65535,0,0)
lila=GET_COLOR(65535,0,65535)

' Resonanzdaten einlesen fuer imperfections
RESTORE resonanzdata
FOR i=0 TO 6
  READ n,e,st,en,s,c
  harme(i)=e/1000
  harmse(i)=st/1000
  harmee(i)=en/1000
  sincomp(i)=s
  coscomp(i)=c
NEXT i

COLOR schwarz
PBOX 0,0,640,400
COLOR gelb
TEXT 20,50,"Korrektoren-Rampen berechnen"

neuauslesen:
t$="Das Timing wird jetzt ausgelesen. Ist es gesetzt ?|"
t$=t$+"|Die aktuellen Einstellungen sind:|"

' Energien auslesen
'

injenergy=csget("ELS_ENERGY_MODEL.INJENERGY_AC")
extenergy=csget("ELS_ENERGY_MODEL.EXTENERGY_AC")
knickenergy=csget("ELS_ENERGY_MODEL.KNICKENERGY_AC")

' Timing auslesen
'
injshots=csget("ELS_TIMING_MODEL.INJTRAIN_DC")
injphases=csget("ELS_TIMING_MODEL.INJPHASES_DC")
injdamp=csget("ELS_TIMING_MODEL.INJDAMP_AC")
rampup=csget("ELS_TIMING_MODEL.RAMPUP_AC")
extprepare=csget("ELS_TIMING_MODEL.EXTPREPARE_AC")
exttime=csget("ELS_TIMING_MODEL.EXTTIME_AC")
rampdown=csget("ELS_TIMING_MODEL.RAMPDO_AC")
opttime=csget("ELS_TIMING_MODEL.OPTTIME_AC")
knicktime=csget("ELS_TIMING_MODEL.RAMPKNICK_AC")

' absolute Zeitpunkte
'
dampstart=injphases*injshots*0.020
rampstart=csget("ELS_TIMING_CYCLE.RAMPSTART_AC")
rampstop=csget("ELS_TIMING_CYCLE.RAMPSTOP_AC")
extstart=csget("ELS_TIMING_CYCLE.EXTSTART_AC")
extstop=csget("ELS_TIMING_CYCLE.EXTSTOP_AC")
zyklusstop=csget("ELS_TIMING_MODEL.CLENGTH_AC")
injprepstart=extstop+rampdown

' Rampform auslesen
'

rampform=csget("ELS_ENERGY_MODEL.RMODEL_DC")

xerror=1
t$=t$+"|Rampe von "+STR$(injenergy,4,4)+" GeV nach "+STR$(extenergy,4,4)+" GeV|"
IF injenergy=extenergy
  t$=t$+"*** Rampe ??? ***|"
  xerror=2
ENDIF
IF rampform=0
  t$=t$+"Rampe linear ! Gradient: "

  ' Gradienten berechnen
  '
  gradientup=(extenergy-injenergy)/rampup
  gradientdown=(extenergy-injenergy)/rampdown
  t$=t$+STR$(gradientup,5,5)+" GeV/s und "
  t$=t$+STR$(gradientdown,5,5)+" GeV/s |"

ELSE
  t$=t$+"***Rampform ungueltig  ! ****|"
  xerror=2
ENDIF
t$=t$+"|Zyklusstart   :=  0|"
t$=t$+"Daempfungsstart ="+STR$(dampstart,4,4)+"|"
t$=t$+"Rampstart       ="+STR$(rampstart,4,4)+"|"
t$=t$+"Rampstop        ="+STR$(rampstop,4,4)+"|"
t$=t$+"Extraktionsstart="+STR$(extstart,4,4)+"|"
t$=t$+"Extraktionsstop ="+STR$(extstop,4,4)+"|"
t$=t$+"Injprepstat     ="+STR$(injprepstart,4,4)+"|"
t$=t$+"Zykluslaenge    ="+STR$(zyklusstop,4,4)+"|"

balert=form_alert(xerror,"[2]["+t$+"][ OK |Neu auslesen|Simulieren|ABBRUCH]")
IF balert=2
  GOTO neuauslesen
ELSE if balert=4
  QUIT
ENDIF

DEFMOUSE 2
TEXT 200,200,"bitte warten..."

' zum testen
IF balert=3
  @status("--< Simulierte Werte, Test >--")
  PRINT "--< Simulierte Werte, Test >--"
  rampform=0
  rampstart=0.2
  injenergy=1.2
  extenergy=2.1
  rampup=0.5
  rampdown=0.4
  rampstop=rampstart+rampup
  extstart=rampstop+0.1
  extstop=extstart+0.8
  zyklusstop=extstop+rampdown+0.1
  injprepstart=extstop+rampdown

  gradientup=(extenergy-injenergy)/rampup
  gradientdown=(extenergy-injenergy)/rampdown
ENDIF
VSYNC
clen=round(zyklusstop/deltat)

'
'
' Erzeuge Energievektor

PRINT "Energievektor:"

@status("erzeuge Energievektor ...")
TEXT 200,300,"Zeitschritt="+STR$(deltat)+" s"
DIM tvec(clen-1),evec(clen-1),tvec2(clen-1)
DIM hivec(31,clen-1),vivec(31,clen-1)
DIM hivec2(31,clen-1),vivec2(31,clen-1)

@hscalerbar(430,360,100)
VSYNC
FOR i=0 TO clen-1
  tvec(i)=i*deltat
  etime=i*deltat
  IF etime<rampstart
    evec(i)=injenergy
  ELSE if (etime>=rampstart) AND (etime<rampstop)
    IF rampform=0
      evec(i)=injenergy+gradientup*(etime-rampstart)
    ENDIF
  ELSE if (etime>=rampstop) AND (etime<extstop)
    evec(i)=extenergy
  ELSE if (etime>=extstop) AND (etime<injprepstart)
    evec(i)=extenergy-gradientdown*(etime-extstop)
  ELSE
    evec(i)=injenergy
  ENDIF
  IF (i MOD 10)=0
    @do_hscaler(430,360,100,i/(clen-1))
    VSYNC
  ENDIF
NEXT i
DEFMOUSE 0
@parameter_eingabe
@plot_erampe(10,300,620,100)
@read_orbit(orbitdf$)

' Skalierungsfaktoren fuer Korrektoren auslesen
'
hscaling()=csvget("ELS_MAGNETE_HCORRS.SCALING_AM",32)
vscaling()=csvget("ELS_MAGNETE_VCORRS.SCALING_AM",32)

' Stromrampen erzeugen
'
PRINT "Stromrampen fuer die Korrektoren werden berechnet..."
@status("Stromrampen erzeugen")

' Energieskalierte Korrektur:

FOR j=0 TO 32-1
  hivec(j,:)=smul(evec(),hkicks(j)*hscaling(j))
  vivec(j,:)=smul(evec(),vkicks(j)*vscaling(j))
NEXT j

@rampen_plotten(10,50,620,250)

' Hinzufuegen der harm. Korrektur
'

PRINT "Hinzufuegen der harm. Korrektur"
@status("Hinzufuegen der harm. Korrektur")

FOR i=0 TO 6
  startbump(i)=rampstart+(harmse(i)-injenergy)/gradientup
  maxbump(i)=rampstart+(harme(i)-injenergy)/gradientup
  endbump(i)=rampstart+(harmee(i)-injenergy)/gradientup
NEXT i

DIM scalevec(clen-1)

FOR j=0 TO 6
  IF (harme(j)>injenergy) AND (harme(j)<extenergy)
    PRINT "Bump #"+STR$(j+1)+" [";
    FLUSH
    ARRAYFILL scalevec(),0
    FOR i=0 TO clen-1
      etime=i*deltat
      ' if etime<startbump(j)
      ' scalevec(i)=0
      IF (etime>=startbump(j)) AND (etime<maxbump(j))
        scalevec(i)=(etime-startbump(j))/(maxbump(j)-startbump(j))
      ELSE if (etime>=maxbump(j)) AND (etime<=endbump(j))
        scalevec(i)=(etime-endbump(j))/(maxbump(j)-endbump(j))
        ' else if etime>=maxbump
        ' scalevec(i)=0
      ENDIF
    NEXT i
    PRINT "+";
    FLUSH
    FOR i=0 TO 32-1
      IF (vscaling(i)<>0) AND (vflag(i)<>0)
        IF sincomp(j)<>0 OR coscomp(j)<>0
          vivec(i,:)=vivec(i,:)+smul(scalevec(),vscaling(i)*sincomp(j)*sin(harme(j)/0.44056*vangle(i))+vscaling(i)*coscomp(j)*cos(harme(j)/0.44056*vangle(i)))
        ENDIF
        PRINT ".";
      ELSE
        PRINT "-";
      ENDIF
      FLUSH
    NEXT i
    PRINT "]"
  ENDIF
NEXT j

@rampen_plotten(10,50,620,280)
@rampen_komprimieren

IF form_alert(2,"[2][Sollen die berechneten | Rampen wirklich | gesetzt werden ?][Ja| Nein ]")=1
  @vektoren_setzen
ENDIF

QUIT

PROCEDURE rampen_komprimieren
  LOCAL i,j,t,ctr
  PRINT "Rampen werden komprimiert..."
  @status("Rampen werden komprimiert...")

  ' Der Rampvektor eines Bump-Korrektors benoetigt die feinste Aufloesung...
  '

  ' Die Rampe wird mit den Rampstart-Trigger gestartet. Um der ISR-Rountine
  ' auf der VAC1 Zeit zur Vorbereitung der neuen Rampe zu geben, wird die Rampe
  ' bereits mit der Injektionspraeperationszeit beendet.
  '

  start=round(rampstart/deltat)+1
  stop=round(injprepstart/deltat)+1

  ' j = corrs(1);
  j=2

  hivec2(:,1)=hivec(:,1)
  vivec2(:,1)=vivec(:,1)
  t=0
  ctr=1

  FOR i=start TO stop
    IF (abs(hivec(j,i)-hivec(j,i-1))>0.0001) OR t>65535
      tvec2(t)=ctr
      hivec2(:,t)=hivec(:,i-1)
      vivec2(:,t)=vivec(:,i-1)
      ctr=1
      INC t
    ELSE
      ' gleicher Wert -> komprimieren
      INC ctr
    ENDIF
  NEXT i

  ' letzten Wert setzen
  ' keine zyklische Rampe (wird durch Rampstart-Trigger getriggert)
  '
  tvec2(t)=ctr
  tvec2(t+1)=0

  hivec2(:,t)=hivec(:,i-1)
  vivec2(:,t)=vivec(:,i-1)
  hivec2(:,t+1)=hivec(:,i-1)
  vivec2(:,t+1)=vivec(:,i-1)
  komprim_anz=t+2
  PRINT "Rampe: "+STR$(stop-start)

  PRINT "Komprimiert von "+STR$(clen-1)+" auf "+STR$(komprim_anz)

  @complot(2,10,320,200)

RETURN

PROCEDURE complot(bx,by,bw,bh)

  LOCAL i,j
  COLOR schwarz
  PBOX bx-2,by-2,bx+bw+2,by+bh+2
  DEFLINE ,1
  COLOR weiss
  BOX bx,by,bx+bw,by+bh

  FOR i=0 TO 31
    zwvec()=vivec2(i,:)
    COLOR gelb+i
    SCOPE zwvec(),0,-bh/3,by+bh/2
    VSYNC
  NEXT i
RETURN

PROCEDURE vektoren_setzen

  ' Rampen abbrechen
  CSPUT "ELS_MAGNETE_CORRCONTROL.CMD_DC",10
  PAUSE 2

  ' Lade Datensatz mit Nullstroemen
  CSPUT "ELS_MAGNETE_CORRCONTROL.FILENAME_SC","zerokick"
  CSPUT "ELS_MAGNETE_CORRCONTROL.CMD_DC",3
  PAUSE 2

  ' Setze alle Korrketorstroeme skalar auf 0
  CSPUT "ELS_MAGNETE_CORRCONTROL.CMD_DC",1
  PAUSE 5

  ' Setze Timingvektor auf willkuerliche Vorinitialisierung (um Nichtverschicken
  ' des richtigen Vektors durch Kontrollsystem auszuschliessen)
  DIM test(1)
  test(0)=20
  test(1)=0
  CSVPUT "ELS_MAGNETE_CORRCONTROL.TIME_DC",test()
  PAUSE 2

  ' Setze Stromwerte skalar nochmals einzeln auf 0

  FOR i=0 TO 31
    IF hscaling(i)<>0
      hname$="ELS_MAGNETE_HC"+STR$(i+1,2,2,1)+".STROM_AC"
      CSPUT hname$,0
    ENDIF
    IF vscaling(i)<>0
      vname$="ELS_MAGNETE_VC"+STR$(i+1,2,2,1)+".STROM_AC"
      CSPUT vname$,0
    ENDIF
  NEXT i

  ' Vektoren setzen
  '
  LOCAL i
  @status("Vektoren setzen...")
  PRINT "Laenge der Vektoren: "+STR$(komprim_anz)

  ' Abarbeitungsfrequenz auf 1/deltat Hz setzen
  CSPUT "ELS_MAGNETE_CORRCONTROL.IRQFREQ_DC",1/deltat

  CSVPUT "ELS_MAGNETE_CORRCONTROL.TIME_DC",tvec2(),komprim_anz

  FOR i=0 TO 31
    IF hscaling(i)<>0
      hname$="ELS_MAGNETE_HC"+STR$(i+1,2,2,1)+".STROM_AC"
      CSVPUT hname$,hivec2(i,:),komprim_anz
    ENDIF
    IF vscaling(i)<>0
      vname$="ELS_MAGNETE_VC"+STR$(i+1,2,2,1)+".STROM_AC"
      CSVPUT vname$,vivec2(i,:),komprim_anz
    ENDIF
  NEXT i

  ' Berechnen der Rampe

  CSPUT "ELS_MAGNETE_CORRCONTROL.CMD_DC",8
  PAUSE 2

  ' Gebe das Rampen frei
  CSPUT "ELS_MAGNETE_CORRCONTROL.CMD_DC",9
RETURN

PROCEDURE rampen_plotten(bx,by,bw,bh)
  ' Rampen plotten
  LOCAL i,j
  COLOR schwarz
  PBOX bx-2,by-2,bx+bw+2,by+bh+2
  DEFLINE ,1
  COLOR weiss
  BOX bx,by,bx+bw,by+bh
  DIM zwvec(clen-1)

  FOR i=0 TO 32-1
    zwvec()=vivec(i,:)
    COLOR gelb+i
    SCOPE zwvec(),tvec(),0,-bh/3,by+bh/2,bw/zyklusstop,bx
    VSYNC
  NEXT i
RETURN

PROCEDURE parameter_eingabe
  LOCAL bx,by,bw,bh,anzparm,akt,i

  anzparm=1+3*7
  DIM s$(anzparm),t$(anzparm)

  s$(0)="Korrektordatenfile:"
  t$(0)="pole_1200_990115"
  akt=0
  FOR i=0 TO 6
    IF (harme(i)>injenergy) AND (harme(i)<extenergy)
      s$(1+3*akt)="Resonanz "+STR$(i+1)+" [Gev]:"
      t$(1+3*akt)=STR$(harme(i))
      s$(1+3*akt+1)="SIN: [mrad]"
      t$(1+3*akt+1)=STR$(sincomp(i))
      s$(1+3*akt+2)="COS: [mrad]"
      t$(1+3*akt+2)=STR$(coscomp(i))
      INC akt
    ENDIF
  NEXT i
  anzparm=1+3*akt
  bx=100
  by=40
  bw=300
  bh=40+30+anzparm*20
  DEFTEXT 1,0.07,0.15,0
  DEFLINE ,2,2
  COLOR weiss
  PBOX bx-2,by-2,520+2,by+bh+2
  COLOR schwarz
  BOX bx,by,520,by+bh
  COLOR rot
  LTEXT bx+(bw-ltextlen("Harmonische Korrektur:"))/2,by+10,"Harmonische Korrektur:"
  COLOR schwarz
  FOR i=0 TO anzparm-1
    LTEXT bx+20,by+40+i*20,s$(i)
  NEXT i
  nochmal:
  akt=0
  ledit_curlen=25
  WHILE akt<anzparm
    t$(akt)=@ledit$(t$(akt),bx+bw-100,by+40+akt*20)
    IF ledit_status=1
      akt=MAX(0,akt-1)
    ELSE if ledit_status=2
      akt=MIN(akt+1,anzparm-1)
    ELSE if ledit_status=0
      akt=MIN(akt+1,anzparm)
    ENDIF
  WEND
  orbitdf$=t$(0)
  IF not EXIST("/sgt/elsa/data/korrektoren/"+orbitdf$+".kicks")
    ALERT 3,"File nicht gefunden !|"+orbitdf$,1,"Nochmal|ABBRUCH",balert
    IF balert=2
      QUIT
    ELSE
      GOTO nochmal
    ENDIF
  ENDIF
  akt=0
  FOR i=0 TO 6
    IF (harme(i)>injenergy) AND (harme(i)<extenergy)
      harme(i)=VAL(t$(1+akt*3))
      ' harmse(i)=harme(i)-0.1
      ' harmee(i)=harme(i)+0.1
      sincomp(i)=VAL(t$(1+akt*3+1))
      coscomp(i)=VAL(t$(1+akt*3+2))
      INC akt
    ENDIF
  NEXT i

  ERASE s$(),t$()
RETURN

PROCEDURE read_orbit(fname$)
  '
  ' Globale Korrektur einlesen
  '
  LOCAL t$,i,a$,b$
  PRINT "Orbit wird eingelesen..."
  IF fname$=""
    INPUT "Name des Korrekturdatensatzes = ",fname$
  ENDIF
  filename$="/sgt/elsa/data/korrektoren/"+fname$+".kicks"
  IF exist(filename$)
    OPEN "I",#1,filename$
    LINEINPUT #1,t$
    PRINT filename$+" [";
    WHILE not eof(#1)
      LINEINPUT #1,t$
      WORT_SEP t$," ",TRUE,a$,b$
      IF a$="HZ"
        WORT_SEP b$," ",TRUE,a$,b$
        i=VAL(a$)-1
        WORT_SEP b$," ",TRUE,a$,b$
        hkicks(i)=VAL(a$)
        vkicks(i)=VAL(b$)
        ' print i,val(a$),val(b$)

        PRINT ".";
      ENDIF
    WEND
    CLOSE #1
    PRINT "]"
  ELSE
    ALERT 3,"File nicht gefunden !|"+filename$,1," OH ",balert
  ENDIF
RETURN

DIM a(5000),t(5000)

t()=csvget("ELS_MAGNETE_DIPOL.TIMER_AC")
a()=csvget("ELS_MAGNETE_DIPOL.STROM_AC")
COLOR 0
DIM z(5000)
PBOX 0,0,640,400
COLOR 1
summe=0
i=0
REPEAT
  ADD summe,t(i)
  z(i)=summe
  PLOT i/10,200-summe/100
  PLOT i/10,300-a(i)/10
  INC i
UNTIL (summe+rampstart*1000)>=injprepstart*1000 OR i>=5000
anzbins=i

COLOR get_color(65535,0,0)
SCOPE a(),z(),0,-0.2,300,640/summe,0
VSYNC
PRINT summe

' Hilfsfunktion

FUNCTION ledit$(t$,x,y)
  LOCAL t2$,cc,curpos,a,b,c$,curlen
  t2$=t$

  cc=LEN(t$)
  curpos=x+ltextlen(LEFT$(t$,cc))
  curlen=MAX(20,ledit_curlen)

  DO
    COLOR weiss
    LTEXT x,y,t2$
    t2$=t$
    LINE curpos,y,curpos,y+curlen
    curpos=x+ltextlen(LEFT$(t$,cc))
    COLOR schwarz
    LTEXT x,y,t2$
    COLOR rot
    LINE curpos,y,curpos,y+curlen
    VSYNC
    KEYEVENT a,b,c$
    COLOR weiss
    IF b AND -256
      ' print ".";
      b=b and 255
      IF b=8 ! Bsp
        IF len(t2$)
          t$=LEFT$(t2$,cc-1)+RIGHT$(t2$,LEN(t2$)-cc)
          DEC cc
        ELSE
          BELL
        ENDIF
      ELSE if b=13 ! Ret
        ledit_status=0
        LINE curpos,y,curpos,y+curlen
        RETURN t2$
      ELSE if b=82
        ledit_status=1
        LINE curpos,y,curpos,y+curlen
        RETURN t2$
      ELSE if b=84
        ledit_status=2
        LINE curpos,y,curpos,y+curlen
        RETURN t2$
      ELSE if b=255 ! Del
        IF cc<len(t2$)
          t$=LEFT$(t2$,cc)+RIGHT$(t2$,LEN(t2$)-cc-1)
        ENDIF
      ELSE if b=81
        cc=MAX(0,cc-1)
      ELSE if b=83
        cc=MIN(LEN(t2$),cc+1)
      ENDIF
    ELSE
      t$=LEFT$(t2$,cc)+CHR$(b)+RIGHT$(t2$,LEN(t2$)-cc)
      INC cc
    ENDIF
  LOOP
ENDFUNCTION

PROCEDURE plot_erampe(bx,by,bw,bh)
  '
  '
  ' Rampe mal plotten
  COLOR schwarz
  PBOX bx,by,bx+bw,by+bh

  COLOR weiss
  BOX bx,by,bx+bw,by+bh
  SCOPE evec(),tvec(),0,-bh/3.5,by+bh,bw/zyklusstop,bx
  VSYNC
RETURN

PROCEDURE hscalerbar(scaler_x,scaler_y,scaler_w)
  LOCAL i,k

  COLOR 0
  PBOX scaler_x,scaler_y,scaler_x+scaler_w,scaler_y+20
  COLOR 1
  BOX scaler_x,scaler_y,scaler_x+scaler_w,scaler_y+20
  SETFONT "*Courier*10*"
  FOR i=0 TO 100 STEP 5
    IF (i MOD 50)=0
      k=7
      TEXT scaler_x+i/100*scaler_w-len(STR$(i))*2.5,scaler_y+37,STR$(i)
    ELSE if (i MOD 10)=0
      k=5
    ELSE
      k=3
    ENDIF
    LINE scaler_x+i/100*scaler_w,scaler_y+20,scaler_x+i/100*scaler_w,scaler_y+20+k
  NEXT i
RETURN
PROCEDURE do_hscaler(scaler_x,scaler_y,scaler_w,wert)
  COLOR 0
  PBOX scaler_x+1,scaler_y+1,scaler_x+scaler_w,scaler_y+20
  COLOR gelb
  PBOX scaler_x+1,scaler_y+1,scaler_x+1+(scaler_w-2)*wert,scaler_y+20
RETURN

PROCEDURE status(t$)
  COLOR schwarz
  PBOX 320,0,640,30
  COLOR rot
  SETFONT "*Helvetica*12*"
  TEXT 330,20,t$
  VSYNC
RETURN

resonanzdata:
' Imperfection Resonanzen
'    #, Energie [Mev], start, end bump, SIN, COS
DATA 1, 440.6,         440,  500,0,0
DATA 2, 881,           700, 1000,0,0
DATA 3,1322,          1200, 1450,0,0
DATA 4,1762,          1600, 1900,0,0
DATA 5,2203,          2050, 2300,0,0
DATA 6,2644,          2450, 2800,0,0
DATA 7,3084,          2950, 3200,0,0

