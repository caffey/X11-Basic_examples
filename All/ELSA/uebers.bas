uel=2^8
DIM a(uel)
OPEN "I",#1,"mondata_delta_0.5A.dat"
FOR i=0 TO 480
  LINEINPUT #1,t$
NEXT i
FOR i=0 TO uel
  LINEINPUT #1,t$
  FOR j=0 TO 5
    WORT_SEP t$," ",1,a$,t$
  NEXT j
  a(i)=(VAL(a$)-1000)/4096*2
NEXT i
CLOSE
SIZEW ,MIN(uel,1224),400

COLOR get_color(10000,10000,10000)
PBOX 0,0,MIN(uel,1224),400

COLOR get_color(65535,32000,0)
SCOPE a(),0,-2000,300
VSYNC
FFT a()
COLOR 1
FOR i=0 TO uel STEP 2
  LINE i,300,i,300-100000/uel*sqrt(a(i)^2+a(i+1)^2)
NEXT i
COLOR get_color(65535,65535,0)
FOR i=0 TO uel STEP 2
  DRAW to i,300-10000/uel*atan2(a(i+1),a(i))
NEXT i
VSYNC
