' Programm zum Faedeln einer Beamline
' Nov. 1998 Markus Hoffmann
'

' Parameter der Maximiert werden soll:
ECHO off
optimumparameter$="POL_LASER_AUSBEUTE.INTENSITY_AM"

DIM parameter$(100),pmax(100),pmin(100),wert(100)
DIM gradient(100),owert(100)
anzparameter=0

REPEAT
  READ parameter$(anzparameter),pmin(anzparameter),pmax(anzparameter),wert(anzparameter)
  gradient(anzparameter)=0
  owert(anzparameter)=wert(anzparameter)
  INC anzparameter
  PRINT parameter$(anzparameter-1)
UNTIL parameter$(anzparameter-1)="xxx"
DEC anzparameter

GOSUB setzewert

startwert=csget(optimumparameter$)
zwischenwert=startwert
count=0
DO
  GOSUB messe_gradient
  GOSUB gehe_gradient
  PAUSE 10
  altzwischenwert=zwischenwert
  zwischenwert=csget(optimumparameter$)
  PRINT "Faedelung um "+STR$(zwischenwert/startwert,4,4)+"% verbessert."
  rate=zwischenwert-altzwischenwert
  PRINT "Iteration #"+STR$(count)+"  brachte "+STR$(rate)
  IF rate<0
    PRINT "### Verschlechterung !!!"
  ELSE if rate=0
    PRINT "### Keine Optimierung !!!"
  ENDIF
  EXIT if rate<0.001
  INC count
LOOP

END

PROCEDURE setzewert
  LOCAL i
  PRINT "Werte werden gesetzt.."
  FOR i=0 TO anzparameter-1
    IF wert(i)<=pmax(i) AND wert(i)>=pmin(i)
      CSSET parameter$(i),wert(i)
      PAUSE 0.1
    ENDIF
  NEXT i
RETURN

PROCEDURE messe_gradient
  LOCAL i,s,altwert,t
  PRINT "Gradient wird berechnet..."
  s=zwischenwert
  PRINT "zu uebertreffen: ";s
  FOR i=0 TO anzparameter-1
    PRINT "pr�fe Parameter: "+parameter$(i)
    altwert=wert(i)
    wert(i)=wert(i)+pmax(i)/100
    CSPUT parameter$(i),wert(i)
    PAUSE 10
    t=csget(optimumparameter$)
    wert(i)=altwert
    CSPUT parameter$(i),wert(i)
    gradient(i)=(t-s)*100/pmax(i)
    PRINT "gradient[";i;"]="+STR$(gradient(i))
    PRINT
  NEXT i
RETURN

PROCEDURE gehe_gradient
  LOCAL i
  FOR i=0 TO anzparameter-1
    IF abs(gradient(i))>0.001
      wert(i)=wert(i)+gradient(i)*pmax(i)/100/2
    ENDIF
  NEXT i
  @setzewert
RETURN

' Parameter, an denen gedreht werden soll

DATA "POL_SOURCE1_COPS1.STROM1_AC",0,2,0.194
DATA "POL_SOURCE1_COPS1.STROM2_AC",0,2,0.162
DATA "POL_SOURCE1_COPS1.STROM3_AC",0,2,0
DATA "POL_SOURCE1_COPS1.STROM4_AC",0,2,0
DATA "POL_SOURCE1_COPS1.STROM5_AC",0,2,0.134
DATA "POL_SOURCE1_COPS1.STROM6_AC",0,2,0.118
DATA "POL_SOURCE1_COPS1.STROM7_AC",0,2,0.125
DATA "POL_SOURCE1_COPS1.STROM8_AC",0,2,0.031
'
DATA "POL_SOURCE1_COPS2.STROM1_AC",0,2,0.193
DATA "POL_SOURCE1_COPS2.STROM2_AC",0,2,0.016
DATA "POL_SOURCE1_COPS2.STROM3_AC",0,2,0.081
DATA "POL_SOURCE1_COPS2.STROM4_AC",0,2,0.054
DATA "POL_SOURCE1_COPS2.STROM5_AC",0,2,0.147
DATA "POL_SOURCE1_COPS2.STROM6_AC",0,2,0.060
DATA "POL_SOURCE1_COPS2.STROM7_AC",0,2,0.044
DATA "POL_SOURCE1_COPS2.STROM8_AC",0,2,0.022
'
DATA "POL_SOURCE1_COPS3.STROM1_AC",0,2,0.110
DATA "POL_SOURCE1_COPS3.STROM2_AC",0,2,0.020
DATA "POL_SOURCE1_COPS3.STROM3_AC",0,2,0.042
DATA "POL_SOURCE1_COPS3.STROM4_AC",0,2,0.009
DATA "POL_SOURCE1_COPS3.STROM5_AC",0,2,0.048
DATA "POL_SOURCE1_COPS3.STROM6_AC",0,2,0.040
DATA "POL_SOURCE1_COPS3.STROM7_AC",0,2,0.111
DATA "POL_SOURCE1_COPS3.STROM8_AC",0,2,0.018
'
DATA "POL_SOURCE1_COPS4.STROM1_AC",0,2,0
DATA "POL_SOURCE1_COPS4.STROM2_AC",0,2,0
DATA "POL_SOURCE1_COPS4.STROM3_AC",0,2,0.240
DATA "POL_SOURCE1_COPS4.STROM4_AC",0,2,0.170
DATA "POL_SOURCE1_COPS4.STROM5_AC",0,2,0.104
DATA "POL_SOURCE1_COPS4.STROM6_AC",0,2,0.197
DATA "POL_SOURCE1_COPS4.STROM7_AC",0,2,0.046
DATA "POL_SOURCE1_COPS4.STROM8_AC",0,2,1.245
'
DATA "POL_SOURCE1_NG1_1.STROM_AC",0,6,5.311
DATA "POL_SOURCE1_NG1_2.STROM_AC",0,6,4.460
DATA "POL_SOURCE1_NG1_3.STROM_AC",0,6,5.591
DATA "POL_SOURCE1_NG1_4.STROM_AC",0,6,1.2
DATA "POL_SOURCE1_NG1_5.STROM_AC",0,6,1.130
DATA "POL_SOURCE1_NG1_6.STROM_AC",0,6,1.423
DATA "POL_SOURCE1_NG1_7.STROM_AC",0,6,0
DATA "POL_SOURCE1_NG2_1.STROM_AC",0,6,1.150
DATA "POL_SOURCE1_NG2_2.STROM_AC",0,6,5.3
DATA "POL_SOURCE1_NG2_3.STROM_AC",0,6,2.513
DATA "POL_SOURCE1_NG2_4.STROM_AC",0,6,1.093
' data "POL_SOURCE1_NG2_5.STROM_AC",0,20000,11936
' data "POL_SOURCE1_NG2_6.STROM_AC",0,20000,11453
'
DATA "xxx",0,0,0
