' Macht aus ASCII-Dateien ein Menu im *.mdf-Format
' Markus Hoffmann 1999    V.1.00
'
ECHO off
i=0
chrw=9
chrh=16
yoffs=120
xoffs=10
maxw=0
DIM a$(200)
FILESELECT "ASCII-Datei auswaehlen:","./*.txt","",in$
IF exist(in$)
  WORT_SEP in$,".txt",1,out$,a$
  out$=out$+".mdf"
  PRINT "******"+out$
  OPEN "I",#1,in$
  WHILE not eof(#1)
    t$=lineinput$(#1)
    PRINT t$
    IF len(t$)>maxw
      maxw=LEN(t$)
    ENDIF
    a$(i)=t$
    INC i
  WEND
  CLOSE #1
  OPEN "O",#2,out$
  PRINT #2,"!"
  PRINT #2,"! ascii2mdf 1.1 - "+date$+" - Do not edit here !"
  PRINT #2,"!"
  PRINT #2,"MenuValidation: VALID=1"
  PRINT #2,"MenuVersion: VERSION=0 SV=1 SR=1 DATE=925124825 ORIGFNAME=ascii2mdf.mdf"
  PRINT #2,"MenuProperty: XP=0 YP=0 WIDTH="+STR$(chrw*maxw+2*xoffs)+" HEIGHT="+STR$(chrh*i+yoffs)+" WBGC=[26214,26214,26214] FGC=[65535,65535,65535] BGC=[39321,32896,32896] LFONT=*-Helvetica-Medium-r-*-12-*-*-*-*-*-*-*"

  FOR u=0 TO i-1
    PRINT #2,"MenuString: XP="+STR$(chrw+xoffs)+" YP="+STR$(chrh*u+yoffs)+" WIDTH=0 HEIGHT=13 FGC=[65535,65535,65535] BGC=[52428,52428,52428] FONT=*-Courier-Bold-r-*-14-*-*-*-*-*-*-* TEXT="+CHR$(34)+a$(u)+CHR$(34)
  NEXT u
  CLOSE #2

ENDIF
QUIT
