' Macht aus XFIG-Dateien ein Menu-mdf-Dateien
' Markus Hoffmann 1999
'
ECHO off
i=0
chrw=9
chrh=16
yoffs=120
xoffs=10
maxw=0
DIM a$(2000)
FILESELECT "XFIG-Datei auswaehlen:","/sgt9/home/mhoffman/figures/*.fig","",in$
IF exist(in$)
  OPEN "I",#1,in$
  WHILE not eof(#1)
    t$=lineinput$(#1)
    IF len(t$)>maxw
      maxw=LEN(t$)
    ENDIF
    PRINT t$
    a$(i)=t$
    INC i
  WEND
  CLOSE #1
  OPEN "O",#2,"output.mdf"
  PRINT #2,"!"
  PRINT #2,"! fig2mdf 1.1 - "+date$+" - Do not edit here !"
  PRINT #2,"!"
  PRINT #2,"MenuValidation: VALID=1"
  PRINT #2,"MenuVersion: VERSION=0 SV=1 SR=1 DATE=925124825 ORIGFNAME=ascii2mdf.mdf"
  PRINT #2,"MenuProperty: XP=0 YP=0 WIDTH="+STR$(chrw*80+2*xoffs)+" HEIGHT="+STR$(chrh*50+yoffs)+" WBGC=[26214,26214,26214] FGC=[65535,65535,65535] BGC=[39321,32896,32896] LFONT=*-Helvetica-Medium-r-*-12-*-*-*-*-*-*-*"
  lw=1
  FOR u=0 TO i-1
    t$=a$(u)
    WORT_SEP t$," ",1,w1$,w2$
    IF w1$="2"
      PRINT t$
      k$=TRIM$(a$(u+1))
      PRINT k$
      WORT_SEP k$," ",1,x1$,k$
      WORT_SEP k$," ",1,y1$,k$
      WORT_SEP k$," ",1,x2$,k$
      WORT_SEP k$," ",1,y2$,k$
      x1=INT(VAL(x1$)/50)
      y1=INT(VAL(y1$)/50)
      x2=INT(VAL(x2$)/50)
      y2=INT(VAL(y2$)/50)

      PRINT "Linie: "+x1$+" "+x2$
      PRINT #2,"MenuLine: XP="+STR$(x1)+" YP="+STR$(y1)+" X2="+STR$(x2)+" Y2="+STR$(y2)+" LINEWIDTH="+STR$(lw)+" FGC=[65535,65535,65535]"
      WHILE len(k$)
        x1=x2
        y1=y2
        WORT_SEP k$," ",1,x2$,k$
        WORT_SEP k$," ",1,y2$,k$

        x2=INT(VAL(x2$)/100)
        y2=INT(VAL(y2$)/100)
        PRINT #2,"MenuLine: XP="+STR$(x1)+" YP="+STR$(y1)+" X2="+STR$(x2)+" Y2="+STR$(y2)+" LINEWIDTH="+STR$(lw)+" FGC=[65535,65535,65535]"

      WEND
    ENDIF
  NEXT u
  CLOSE #2
ENDIF
QUIT

FUNCTION convert$(z$)
  PRINT z$
  RETURN ""
ENDFUNC
