t$="libsimlib.so"
IF not EXIST(t$)
  PRINT t$+" not found!"
  QUIT
ENDIF
PRINT t$
LINK #1,t$
CALL sym_adr(#1,"SimLibStartUp")
FLUSH

kd=-0.593
kf=0.632
CALL sym_adr(#1,"SimPutQdd"),D:kd
CALL sym_adr(#1,"SimPutQdf"),D:kf
CALL sym_adr(#1,"SimPutParticleMomentum"),D:1.6e9

' Orbit-Datei einlesen und fit mit virtuellen Korrektoren
' Als Fehlerquellen. Dies simuliert den Orbit und man erhaelt auch
' Zwischenwerte

o$="/home/hoffmann/physik/depol/orbits/orbit_unkorrigiert.dat"

DIM bpmspos(100),bpmread(100),bpmerror(100)
CLR nummon
DIM korrspos(100),result(100)
CLR numkorr
ARRAYFILL result(),4711

OPEN "I",#2,o$
WHILE not eof(#2)
  LINEINPUT #2, t$
  t$=TRIM$(t$)
  WORT_SEP t$," ",1,a$,b$
  WORT_SEP b$," ",1,b$,c$
  bpmspos(nummon)=VAL(a$)
  bpmread(nummon)=VAL(b$)
  bpmerror(nummon)=0.000001

  INC nummon
WEND
CLOSE #2

numkorr=20

rc=CALL(sym_adr(#1,"SimLibLinear"))
IF rc
  FOR i=0 TO 20
    korrspos(i)=i*5
  NEXT i

  rc2=CALL(sym_adr(#1,"SimLibMICADO"),nummon,numkorr,numkorr,L:varptr(bpmspos(0)),L:varptr(korrspos(0)),L:varptr(bpmread(0)),L:varptr(bpmerror(0)),0,L:varptr(result(0)),D:1/1e4)

  FOR i=0 TO 20
    PRINT korrspos(i),result(i)
  NEXT i

ENDIF
IF rc
  CLR e0,qx,qz,e1
  CALL sym_adr(#1,"SimGetXTune"),L:varptr(qx)
  CALL sym_adr(#1,"SimGetZTune"),L:varptr(qz)
  CALL sym_adr(#1,"SimGetXNaturalEmittance"),L:varptr(e0)
  CALL sym_adr(#1,"SimGetZNaturalEmittance"),L:varptr(e1)
  PRINT "Qx=";qx,"Qz=";qz,"Emmix=";e0,"Emmiz=";e1

  a$=SPACE$(2*(11*8+1+4))
  data$=SPACE$(79)
  PRINT "s [m]","Alphax","Betax"
  FOR g=0 TO 50 STEP 0.1
    ' print orbit
    CALL sym_adr(#1,"SimLibTwissParam"),L:2,D:g,L:varptr(a$)
    '    for i=0 to 100
    '      print hex$(peek(varptr(a$)+i) and 255,2,2);
    '    next i
    '    print
    alpha=cvd(a$)
    beta=cvd(MID$(a$,8+1,8))
    gamma=cvd(MID$(a$,2*8+1,8))
    my=cvd(MID$(a$,3*8+1,8))
    mysign=cvl(MID$(a$,4*8+1,4))
    disp=cvd(MID$(a$,5*8+1+4,8))
    ddisp=cvd(MID$(a$,6*8+1+4,8))
    orbit=cvd(MID$(a$,7*8+1+4,8))
    dorbit=cvd(MID$(a$,8*8+1+4,8))
    intdisp=cvd(MID$(a$,9*8+1+4,8))
    dintdisp=cvd(MID$(a$,10*8+1+4,8))

    POKE varptr(data$)+3*beta,ASC("*")
    POKE varptr(data$)+orbit*3000+40,ASC("%")
    ' print g,(g mod 1),(g mod 1)-1

    IF (g MOD 1)<0.0001 OR abs((g MOD 1)-1)<0.01
      PRINT g,data$
      data$=SPACE$(79)
    ENDIF
  NEXT g
ENDIF
UNLINK #1
QUIT
