' Programm zur Aussentemperaturauslese
' (c) 2000 Markus Hoffmann
'
'

stackcount=0
DIM stackt$(3)
DIM tempd(3)
DIM tempt(3)

oldtemp=0
tendenz=0
tempmin=100
tempmax=-40
OPEN "A",#1,"temp.log"
PRINT #1,"#   Neustart des loggers: "+date$+" "+time$
CSSETCALLBACK "SUP_WASSER_AUSSLUFT.TEMP_AM",intr
SIZEW 1,100,200
ALERT 0,"||||||||",1," QUIT ",b
CSCLEARCALLBACKS
QUIT

PROCEDURE intr
  temp=csget("SUP_WASSER_AUSSLUFT.TEMP_AM")
  PRINT date$+" "+time$+" ",timer,temp
  IF abs(temp-oldtemp)>0.02
    tendenz=abs((temp-oldtemp)>0.02)-abs((temp-oldtemp)<-0.02)
    oldtemp=temp
    PRINT "*"
    @thermometer(temp)
    stackt$(stackcount)=date$+" "+time$+" "+STR$(timer)+" "+STR$(temp)
    tempd(stackcount)=temp
    tempt(stackcount)=timer

    IF stackcount<2
      INC stackcount
    ELSE
      u=abs((tempd(1)-tempd(0))/(tempt(1)-tempt(0))*(timer-tempt(0))-temp+tempd(0))
      PRINT u
      IF u>0.05 AND u<5 OR (timer-tempt(0)>300)
        PRINT "<w>"
        PRINT #1,stackt$(0)
        FLUSH #1
        tempd(0)=tempd(1)
        tempd(1)=tempd(2)
        tempt(0)=tempt(1)
        tempt(1)=tempt(2)
        stackt$(0)=stackt$(1)
        stackt$(1)=stackt$(2)
      ENDIF
    ENDIF
  ENDIF
RETURN

PROCEDURE thermometer(tt)
  tempmin=MIN(tempmin,tt)
  tempmax=MAX(tempmax,tt)
  COLOR get_color(0,0,0)
  PBOX 0,0,100,160
  IF tendenz>0
    COLOR get_color(65535,0,0)
    LINE 0,50,50,0
    LINE 50,0,100,50
    LINE 100,50,75,50
    LINE 75,50,75,100
    LINE 75,100,25,100
    LINE 25,100,25,50
    LINE 25,50,0,50
  ELSE if tendenz<0
    COLOR get_color(0,0,65535)
    LINE 0,100,50,150
    LINE 50,150,100,100
    LINE 100,100,75,100
    LINE 75,100,75,50
    LINE 75,50,25,50
    LINE 25,50,25,100
    LINE 25,100,0,100
  ELSE
    COLOR get_color(32000,32000,0)
    PCIRCLE 20,100,5
  ENDIF
  COLOR get_color(65535,65535,65535)
  BOX 50,30,60,120
  LINE 47,100,50,100
  TEXT 38,104,"0"
  TEXT 40,40,"C"
  TEXT 70,150,"C"
  CIRCLE 35,30,2
  CIRCLE 65,140,2
  CIRCLE 55,126,10
  TEXT 10,150,STR$(tt,4,4)

  COLOR get_color(65535,0,0)
  PBOX 51,120,60,100-tt*2
  PCIRCLE 55,126,9
  COLOR get_color(65535,65535,65535)

  LINE 50,100-tempmin*2,60,100-tempmin*2
  LINE 50,100-tempmax*2,60,100-tempmax*2
  VSYNC
RETURN
