'
' Laed die Einstellungen fuer die Quelle 1.
' Letzte Bearbeitung: Markus Hoffmann Sept. 1999
'
ECHO off
dir$="/sgt/elsa/data/polq1/"
file$=dir$+"data.dat"

OPEN "I",#1,file$
WHILE not eof(#1)
  INPUT #1,t$
  IF left$(t$)<>"#"
    INPUT #1,wert
    CSSET t$,wert
  ENDIF
WEND
CLOSE #1

QUIT

