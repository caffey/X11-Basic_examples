
' Markus Hoffmann 23.04.2000

' Programm fuer die Online-Energiemessung mit den Touschek-Zaehlern
' V.1.00 noch Testbetrieb

ECHO off

kleistung=100
kamplitude=sqrt((kleistung+5.672)/791.4)-0.05504
periode=1200
scanbereich=0.01

umlauf=(csget("ELS_HF_MASTER.FREQ_DC")-36e5)/274/100
qz=csget("ELS_MAGNETE_OPTIK.QZ_AC")
energie=csget("ELS_MAGNETE_DIPOL.ENERGIE_AC")*1000
PRINT "Energie="+STR$(energie)+" MeV"
PRINT "Qz="+STR$(qz)+""
gammaa=energie/440.6485620
PRINT "Gamma a="+STR$(gammaa)+""

sema=0

frequenz=abs(gammaa-int(gammaa+1))*umlauf
gfreq=abs(qz-int(qz+1))*umlauf
PRINT "Arbeitspunktresonanz="+STR$(gfreq)+" kHz"

' fabs(gamma_a-(double)((int)(gamma_a+1)))*umlauf;
energiemin=energie*(1-scanbereich)
energiemax=energie*(1+scanbereich)
energiemin=2501
energiemax=2507
frequenzmin=@etofreq(energiemin)
frequenzmax=@etofreq(energiemax)

PRINT "Energie-Suchbereich: "+STR$(energiemin)+" MeV bis "+STR$(energiemax)+" MeV."
PRINT "Frequenz bei Sollenergie ="+STR$(frequenz)+" kHz"
PRINT "Rampe zwischen "+STR$(frequenzmin)+" kHz und "+STR$(frequenzmax)+" kHz."
PRINT "Periode: "+STR$(periode)+" Min"
PRINT "Kickerleistung: P="+STR$(kleistung)+" W"
CSSET "ELS_MAGNETE_SHAKER.AMPLITUDE_AC",0
CSSET "ELS_MAGNETE_SHAKER.FREQUENZ_AC",frequenz
CSSET "ELS_MAGNETE_SHAKER.AMPLITUDE_AC",kamplitude
CSSET "ELS_MAGNETE_SHAKER.FREQUENZ_AC",frequenzmin
PRINT ccserr
IF ccserr
  PRINT "Kann Shaker nicht erreichen..."
  STOP
ENDIF
' Farbdefinitionen
'weiss=GET_COLOR(65535,65535,65535)
'schwarz=GET_COLOR(0,0,0)
'rot=get_color(65535,0,0)
'orange=get_color(65535,65535/2,0)
'blau=get_color(0,0,65535/4)
'gelb=get_color(65535,65535,0)
'grau=get_color(65535/2,65535/2,65535/2)
'hellgrau=get_color(65535/3*2,65535/3*2,65535/3*2)

experiment$=csget$("SUP_GLOBAL_MESSAGE.EXPERIMENT_SC")

OPEN "A",#1,"brandau.log"
PRINT #1,"# Ausgabe von counter.bas vom "+date$
PRINT #1,"# Experiment: "+experiment$
PRINT #1,"# Energie: "+STR$(csget("ELS_MAGNETE_DIPOL.ENERGIE_AM"))+" GeV"
PRINT #1,"# Datum Uhrzeit Zeit[s] Strom[mA] Lebensdauer[min] Druck[mbar] Kickfrequenz Brandau Dipolfeld[T] "
' tbase=timer
tbase=0
CSSETCALLBACK "SUP_SISCOUNTER.FREQ5_AD",interrupt
PRINT "(";
DO
  PAUSE 1
  PAUSE 0.1
  ' Jetzt neue Frequenz setzen
  en=(energiemax-energiemin)/periode*((timer-tbase) MOD periode)+energiemin
  freq=@etofreq(en)
  PRINT "Frequenz: "+STR$(freq,5,5)+" kHz. Sollenergie: "+STR$(en,6,6)+" MeV."
  sema=1
  CSSET "ELS_MAGNETE_SHAKER.FREQUENZ_AC",freq
  sema=0
  frequenz=freq
LOOP
QUIT

FUNCTION etofreq(energie2)
  LOCAL gammaa
  gammaa=energie2/440.6485620
  RETURN abs(gammaa-int(gammaa+1))*umlauf
ENDFUNC

PROCEDURE interrupt
  IF sema=0
    strom=csget("ELS_DIAG_TOROID.STROM_AM")
    leben=csget("ELS_DIAG_TOROID.LEBEN_AM")/60
    nmr=csget("ELS_DIAG_NMR.FELD_AM")
    druck=csget("ELS_VAKUUM_SYS.IGPMEAN_AD")
    PRINT #1,date$+" "+time$+" "+STR$(timer-tbase);
    PRINT #1," "+STR$(strom)+" "+STR$(leben)+" "+STR$(druck)+" "+STR$(frequenz);
    PRINT #1," "+STR$(csget("SUP_SISCOUNTER.FREQ5_AD"))+" ";
    PRINT #1," "+STR$(csget("SUP_SISCOUNTER.FREQ6_AD"))+" ";
    PRINT #1," "+STR$(csget("SUP_SISCOUNTER.FREQ6_AD"))+" ";
    PRINT #1," "+STR$(csget("SUP_SISCOUNTER.FREQ11_AD"))+" ";
    PRINT #1," "+STR$(csget("SUP_SISCOUNTER.FREQ12_AD"))+" ";
    PRINT #1," "+STR$(csget("SUP_SISCOUNTER.FREQ14_AD"))+" ";
    PRINT #1," "+STR$(csget("SUP_SISCOUNTER.FREQ15_AD"))+" ";
    PRINT #1," "+STR$(csget("SUP_SISCOUNTER.FREQ16_AD"))+" "+STR$(nmr)
    FLUSH #1
    PRINT ".";
  ELSE
    PRINT "<->";
  ENDIF
  FLUSH
RETURN
