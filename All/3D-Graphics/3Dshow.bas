' WSHOW.bas is a program which calculates and draws 3 dimentional Objects
' with linear algebra    (c) Markus Hoffmann 1993
'
' Original version GFA-Basic on ATARI ST
' Ported to X11-Basic 
'
' latest modified: 2011-08-10 for X11-Basic V.1.18
' latest modified: 2013-03-10 for X11-Basic V.1.20
' latest modified: 2015-04-06 for X11-Basic V.1.23
'
DIM pxU(4),pyU(4)
IF ANDROID?      ! is probably an Android device
  imgpath$="/storage/tmp/"
ELSE IF WIN32?
  imgpath$="C:/"
ELSE
  imgpath$="/tmp/"
ENDIF
meldung$="WSHOW (c) Markus Hoffmann 1989 - 2001"
maxfl=80000
sortstart=0
CLR anzfl
CLR bildcount
' DIM welt(3+13*maxfl),welt(4+13*maxfl),welt(5+13*maxfl)
' DIM welt(6+13*maxfl),welt(7+13*maxfl),welt(8+13*maxfl)
' DIM welt(9+13*maxfl),welt(10+13*maxfl),welt(11+13*maxfl)
DIM welt(maxfl*13)
DIM dist(maxfl)
DIM index%(maxfl)
'
'
weiss=COLOR_RGB(1,1,1)
schwarz=COLOR_RGB(0,0,0)
grau=COLOR_RGB(0.5,0.5,0.5)
gelb=COLOR_RGB(1,1,0.5)
blau=COLOR_RGB(0.5,0.5,1)
rot=COLOR_RGB(0.5,0,0)

bildcount=1
bx%=0
by%=0
bw%=600
bh%=400
GET_GEOMETRY 1,bx%,by%,bw%,bh%
CLIP bx%,by%,bw%,bh%
' Koordinaten:
'
bw%=bh%
x1=-1
x2=1
y1=-1
y2=1
z1=-1
z2=1
'

sx=0.02
sy=0.02
sz=0.1
'
'
ebene=0    ! Projektionsebene (y-Koordinate)
prozy=-4  ! Projektionszentrum
prozx=0
prozz=0
'
stwink=(5*bildcount)*PI/180
stx=COS(stwink)         ! Beobachterstandpunkt
sty=SIN(stwink)
stz=0.8
'
bbx=SIN(stwink)        ! Beobachterblickrichtung
bby=COS(stwink)
bbz=-1
'
nwink=0       ! Neigungswinkel
zwink=SGN(bby)*SGN(bbx)*ATN(ABS(bbx/bby))+PI*ABS(bby<0)
xwink=-SGN(bbz)*ATN(ABS(bbz/SQR(bbz^2+bby^2)))
'
' Lichteinfallrichtung
lichtx=0
lichty=-0.8
lichtz=SQR(1-lichty^2)
' Lichtfarbe
lichtr=50000
lichtg=65535
lichtb=65535
'
COLOR rot,schwarz

DIM film$(75)
WHILE EXIST(imgpath$+"bild"+STR$(bildcount,3,3,1)+".bmp")
  OPEN "I",#1,imgpath$+"bild"+STR$(bildcount,3,3,1)+".bmp"
  t$=SPACE$(LOF(#1))
  BGET #1,VARPTR(t$),LEN(t$)
  CLOSE #1
  PUT 0,0,t$
  SHOWPAGE
  PAUSE 0.01
  film$(bildcount)=t$
  INC bildcount
WEND

IF bildcount>=72
  film$(0)=film$(1)
  DO
    FOR i=0 TO bildcount-1
      PUT 0,0,film$(i)
      SHOWPAGE
      PAUSE 0.02
    NEXT i
  LOOP
ENDIF

IF NOT EXIST(imgpath$+"welt.xxx")
  t=TIMER
  @calc
  @save
  weltzeit=TIMER-t
  PRINT "WELTZEIT: ",round(weltzeit,3);" s"
ENDIF
@load

DO
  t=TIMER
  stwink=(5*bildcount)*PI/180

  stx=COS(stwink)          ! Beobachterstandpunkt
  sty=SIN(stwink)
  bbx=-COS(stwink)         ! Beobachterblickrichtung
  bby=-SIN(stwink)
  zwink=SGN(bby)*SGN(bbx)*ATN(ABS(bbx/bby))+PI*ABS(bby<0)
  xwink=-SGN(bbz)*ATN(ABS(bbz/SQR(bbz^2+bby^2)))

  @sort
  sortzeit=TIMER-t
  t=TIMER
  @plot
  plotzeit=TIMER-t
  COLOR rot,schwarz

  TEXT 400,130,STR$(sortzeit)
  TEXT 400,150,STR$(plotzeit)
  COLOR schwarz
  PBOX 400,170,400+4*50,178
  PBOX 400,190,400+10*20,198
  COLOR gelb
  PBOX 400,170,400+sortzeit*50,178
  PBOX 400,190,400+plotzeit*20,198
  COLOR weiss
  BOX 400,170,400+4*50,178
  BOX 400,190,400+10*20,198
  SHOWPAGE
  SAVEWINDOW imgpath$+"bild"+STR$(bildcount,3,3,1)+".bmp"
  INC bildcount
  EXIT IF stwink>2*pi
LOOP
PRINT "created ";bildcount;" pictures."
ALERT 0,"Done !|Now restart this program|to watch the animation.",1," OK ",balert
QUIT
PROCEDURE addfl(x1,y1,z1,x2,y2,z2,x3,y3,z3,x4,y4,z4)
  welt(0+13*anzfl)=x1
  welt(3+13*anzfl)=x2
  welt(6+13*anzfl)=x3
  welt(9+13*anzfl)=x4
  welt(1+13*anzfl)=y1
  welt(4+13*anzfl)=y2
  welt(7+13*anzfl)=y3
  welt(10+13*anzfl)=y4
  welt(2+13*anzfl)=z1
  welt(5+13*anzfl)=z2
  welt(8+13*anzfl)=z3
  welt(11+13*anzfl)=z4
  INC anzfl
RETURN
PROCEDURE polar1(r,th,ph)
  x1=r*COS(ph)*SIN(th)
  y1=r*SIN(ph)*SIN(th)
  z1=r*COS(th)
RETURN
PROCEDURE polar2(r,th,ph)
  x2=r*COS(ph)*SIN(th)
  y2=r*SIN(ph)*SIN(th)
  z2=r*COS(th)
RETURN
PROCEDURE polar3(r,th,ph)
  x3=r*COS(ph)*SIN(th)
  y3=r*SIN(ph)*SIN(th)
  z3=r*COS(th)
RETURN
PROCEDURE polar4(r,th,ph)
  x4=r*COS(ph)*SIN(th)
  y4=r*SIN(ph)*SIN(th)
  z4=r*COS(th)
RETURN

PROCEDURE calc
  LOCAL z,zz,zzz,x,y,x,yy,t
  PRINT "calculate surfaces ..."
  SHOWPAGE
  t=CTIMER
  ' goto kug
  FOR y=y2 DOWNTO y1 STEP sy
    yy=y
    FOR x=x1 TO x2 STEP sx
      z=@f(x,y)
      zz=@f(x+sx,y)
      zzz=@f(x,y+sy)
      zzzz=@f(x+sx,y+sy)
      @addfl(x,y,z,x+sx,y,zz,x+sx,y+sy,zzzz,x,y+sy,zzz)
    NEXT x
  NEXT y
  PRINT anzfl;" elements in ";CTIMER-t;" seconds."
  SHOWPAGE
  kug:

  LOCAL r,spsp,st,phi,theta,x1,x2,x3,x4,y1,y2,y3,y4,z1,z2,z3,z4

  ' Innenkugel: Solid

  r=0.3
  st=PI/30
  FOR theta=0.05 TO PI STEP st
    spsp=PI/30*(1+COS(theta)^2)
    FOR phi=0 TO 2*PI STEP spsp
      @polar1(r,theta,phi)
      @polar2(r,theta+st*1.05,phi)
      @polar3(r,theta+st*1.05,phi+spsp*1.05)
      @polar4(r,theta,phi+spsp*1.05)
      @addfl(x1+0.3,y1,z1+0.4,x2+0.3,y2,z2+0.4,x3+0.3,y3,z3+0.4,x4+0.3,y4,z4+0.4)
    NEXT phi
  NEXT theta

  torus:

  GPRINT anzfl;" pieces in ";CTIMER-t;" seconds."
  SHOWPAGE
RETURN

PROCEDURE sort
  LOCAL a,i,mx,my,mz,flb,t,adr,badr,ad,j
  PRINT "calculate distances ... ";
  FLUSH
  FOR i=0 TO anzfl-1
    mx=(welt(6+13*i)+welt(13*i))/2
    my=(welt(7+13*i)+welt(1+13*i))/2
    mz=(welt(8+13*i)+welt(2+13*i))/2
    welt(12+13*i)=(mx-stx)^2+(my-sty)^2+(mz-stz)^2
    dist(i)=-welt(12+13*i)
    index%(i)=i
  NEXT i
  PRINT "SORT ... ";
  FLUSH
  SORT dist(),anzfl,index%()
  PRINT "SWAPPING ... "
  adr=VARPTR(welt(0))
  ad=8*13
  t=CTIMER
  welt2()=welt()
  adr2=VARPTR(welt2(0))
  FOR i=0 TO anzfl-1
    j=index%(i)
    BMOVE adr2+ad*j,adr+ad*i,ad
    flb=TRUE
    IF CTIMER-s>0.5
      @progress(anzfl,i)
      FLUSH
      s=CTIMER
    ENDIF
  NEXT i
  @progress(anzfl,i)
  PRINT CHR$(13);"in ";ctimer-t;" seconds."
  SHOWPAGE
RETURN
PROCEDURE save
  PRINT "--> "+imgpath$+"welt.xxx"
  OPEN "O",#1,imgpath$+"welt.xxx"
  IF LEN(meldung$)<64
    meldung$=meldung$+STRING$(64,".")
  ENDIF
  BPUT #1,VARPTR(meldung$),64
  BPUT #1,VARPTR(anzfl),8
  BPUT #1,VARPTR(sortstart),8
  BPUT #1,VARPTR(welt(0)),anzfl*13*8
  CLOSE #1
RETURN
PROCEDURE load
  PRINT "<-- "+imgpath$+"welt.xxx ";
  OPEN "I",#1,imgpath$+"welt.xxx"
  meldung$=INPUT$(#1,64)
  PRINT "["+meldung$+"] ";
  BGET #1,VARPTR(anzfl),8
  PRINT anzfl;" surfaces."
  BGET #1,VARPTR(sortstart),8
  BGET #1,VARPTR(welt(0)),anzfl*8*13
  CLOSE #1
  SHOWPAGE
RETURN
PROCEDURE plot
  plottime=TIMER
  COLOR grau
  PBOX 0,0,640,400
  COLOR rot,grau
  TEXT 400,100,"Bild "+STR$(bildcount)
  COLOR weiss
  pxU(0)=@kx(x1,y2,z1/2)
  pyU(0)=@ky(x1,y2,z1/2)
  pxU(1)=@kx(x2,y2,z1/2)
  pyU(1)=@ky(x2,y2,z1/2)
  pxU(2)=@kx(x2,y2,z2/2)
  pyU(2)=@ky(x2,y2,z2/2)
  pxU(3)=@kx(x1,y2,z2/2)
  pyU(3)=@ky(x1,y2,z2/2)
  POLYFILL 4,pxU(),pyU()
  COLOR schwarz
  POLYLINE 4,pxU(),pyU()
  pxU(0)=@kx(x1,y2,z1/2)
  pyU(0)=@ky(x1,y2,z1/2)
  pxU(1)=@kx(x1,y2,z2/2)
  pyU(1)=@ky(x1,y2,z2/2)
  pxU(2)=@kx(x1,y1,z2/2)
  pyU(2)=@ky(x1,y1,z2/2)
  pxU(3)=@kx(x1,y1,z1/2)
  pyU(3)=@ky(x1,y1,z1/2)
  COLOR weiss
  POLYFILL 4,pxU(),pyU()
  COLOR blau
  FOR i=z1/2 TO z2/2 STEP (z2-z1)/12
    LINE @kx(x1,y1,i),@ky(x1,y1,i),@kx(x1,y2,i),@ky(x1,y2,i)
    LINE @kx(x1,y2,i),@ky(x1,y2,i),@kx(x2,y2,i),@ky(x2,y2,i)
  NEXT i
  COLOR gelb,grau
  TEXT 20,20,"3D - Flaechengrafik mit X11-Basic   (c) Markus Hoffmann"
  COLOR schwarz
  LINE @kx(x1,y1,0),@ky(x1,y1,0),@kx(x1,y2,0),@ky(x1,y2,0)
  LINE @kx(x1,y2,0),@ky(x1,y2,0),@kx(x2,y2,0),@ky(x2,y2,0)
  POLYLINE 4,pxU(),pyU()

  FOR i=0 TO anzfl-1
    '
    ' brechne Flaechen-Normale
    nx=(welt(4+13*i)-welt(1+13*i))*(welt(11+13*i)-welt(2+13*i))-(welt(5+13*i)-welt(2+13*i))*(welt(10+13*i)-welt(1+13*i))
    ny=(welt(5+13*i)-welt(2+13*i))*(welt(9+13*i)-welt(0+13*i))-(welt(3+13*i)-welt(0+13*i))*(welt(11+13*i)-welt(2+13*i))
    nz=(welt(3+13*i)-welt(0+13*i))*(welt(10+13*i)-welt(1+13*i))-(welt(4+13*i)-welt(1+13*i))*(welt(9+13*i)-welt(0+13*i))
    nl=SQR(nx^2+ny^2+nz^2)

    ' Zeige Normalenvektoren
    nnx=nx/nl
    nny=ny/nl
    nnz=nz/nl
    '   DEFLINE ,1,2,1
    '    LINE @kx(x+sx/2,y+sy/2,z),@ky(x+sx/2,y+sy/2,z),@kx(x+sx/2+nnx,y+sy/2+nny,z+nnz),@ky(x+sx/2+nnx,y+sy/2+nny,z+nnz)
    '   DEFLINE ,0,0
    colorwink=nnx*lichtx+nny*lichty+nnz*lichtz
    blickwink=nnx*bbx+nny*bby+nnz*bbz
    IF blickwink<0
      IF colorwink<0
        COLOR schwarz
      ELSE
        COLOR GET_COLOR(MIN(65535,1.4*colorwink*lichtr),MIN(65535,1.4*colorwink*lichtg),MIN(65535,1.4*colorwink*lichtb))
      ENDIF
      pxU(0)=@kx(welt(13*i),welt(1+13*i),welt(2+13*i))
      pyU(0)=@ky(welt(13*i),welt(1+13*i),welt(2+13*i))
      pxU(1)=@kx(welt(3+13*i),welt(4+13*i),welt(5+13*i))
      pyU(1)=@ky(welt(3+13*i),welt(4+13*i),welt(5+13*i))
      pxU(2)=@kx(welt(6+13*i),welt(7+13*i),welt(8+13*i))
      pyU(2)=@ky(welt(6+13*i),welt(7+13*i),welt(8+13*i))
      pxU(3)=@kx(welt(9+13*i),welt(10+13*i),welt(11+13*i))
      pyU(3)=@ky(welt(9+13*i),welt(10+13*i),welt(11+13*i))
      POLYFILL 4,pxU(),pyU()
      IF TIMER-ptimer>1
        SHOWPAGE
        @progress(anzfl,i)
        ptimer=TIMER
      ENDIF
    ENDIF
  NEXT i
  @progress(anzfl,i)
  SHOWPAGE
  PRINT CHR$(13);"Plotted in ";ROUND(TIMER-plottime);" sec."
RETURN
'
DEFFN f(x,y)=0.8*EXP(-2*(x^2+y^2))*COS((x^2+y^2)*10)

' Koordinatentransformationen mit Perspektive:
FUNCTION kx(x,y,z)
  LOCAL xx
  '  Verschiebe an den Beobachterstandpunkt
  x=x-stx
  y=y-sty
  z=z-stz
  '
  ' Rotation um die Z-Achse
  xx=x
  x=COS(zwink)*x-SIN(zwink)*y
  y=SIN(zwink)*xx+COS(zwink)*y
  '
  ' Rotation um die X-Achse
  xx=z
  z=COS(xwink)*z+SIN(xwink)*y
  y=-SIN(xwink)*xx+COS(xwink)*y
  '
  '
  ' Neigung:
  '  xx=x
  x=COS(nwink)*x+SIN(nwink)*z
  '  z=-SIN(nwink)*xx+COS(nwink)*z
  '
  ' Perspektive:
  '  py=(z-prozz)*(ebene-prozy)/(y-prozy)
  px=(x-prozx)*(ebene-prozy)/(y-prozy)
  '
  RETURN bx%+bw%/2+px*bw%/(x2-x1)
ENDFUNC
FUNCTION ky(x,y,z)
  LOCAL xx
  '  Verschiebe an den Beobachterstandpunkt
  x=x-stx
  y=y-sty
  z=z-stz
  ' Rotation um die Z-Achse
  xx=x
  x=COS(zwink)*x-SIN(zwink)*y
  y=SIN(zwink)*xx+COS(zwink)*y
  ' Rotation um die X-Achse
  xx=z
  z=COS(xwink)*z+SIN(xwink)*y
  y=-SIN(xwink)*xx+COS(xwink)*y
  '
  ' Neigung:
  xx=x
  '  x=COS(nwink)*x+SIN(nwink)*z
  z=-SIN(nwink)*xx+COS(nwink)*z
  ' persp
  py=(z-prozz)*(ebene-prozy)/(y-prozy)
  '  px=(x-prozx)*(ebene-prozy)/(y-prozy)
  RETURN by%+bh%/2-py*bh%/(z2-z1)
ENDFUNC
PROCEDURE progress(a,b)
  PRINT chr$(13);"[";STRING$(b/a*32,"-");">";STRING$((1.03-b/a)*32,"-");"| ";STR$(INT(b/a*100),3,3);"% ]";
  FLUSH
RETURN
