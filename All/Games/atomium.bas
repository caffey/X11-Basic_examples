' ######################################################################
' ##                  ATOMIUM                              V. 1.01    ##
' ##                                                                  ##
' ##                  begonnen am 15.6.1992    Vorversion: 03.10.1982 ##
' ##   Markus Hoffmann                   letzte Bearbeitung: 03.08.11 ##
' ######################################################################
'
GOTO skip
DATA (c) Markus Hoffmann ATOMIUM.PRG erstellt 20.11.1992 V. 1.02
skip:
bx=0
by=20
bw=640
bh=380         ! Bildschirmdaten
ADD by,1
SUB bw,1
ADD bh,by-2
lc=0

rot=GET_COLOR(65530,0,0)
gelb=GET_COLOR(65530,40000,0)
grau=GET_COLOR(65530/2,65530/2,65530/2)
weiss=GET_COLOR(65530,65530,65530)
schwarz=GET_COLOR(0,0,0)
lila=GET_COLOR(65530,0,65530)
gruen=GET_COLOR(0,65530,0)
DEFMOUSE 2

@init
LET sound=TRUE
@l__i
DIM stack(1001)
stackp=1000
stackend=1000

s$(1)="Computer"
s$(2)="Spieler"
@spielfeld
@spieler
spieler=-RANDOM(2)
' print "Spieler: "+str$(spieler)
LINE 0,303,639,303
LINE 0,322,639,322
spiel=TRUE
' ON MENU BUTTON 1,1,1 GOSUB button
' every 1,lauf
CLR cnt
DEFMOUSE 0
DO
  COLOR gruen
  PCIRCLE 10,360,4
  COLOR schwarz
  CIRCLE 10,360,4
  VSYNC
  REPEAT
    @arbeit
    @lauf
  UNTIL in_arbeit=0
  COLOR weiss
  PCIRCLE 10,360,4
  COLOR schwarz
  CIRCLE 10,360,4
  spieler=NOT spieler
  @check_win
  DEFMOUSE 2
  IF s$(ABS(spieler)+1)="Computer" AND spiel
    timevalue=ctimer
    @computer
    ' print "Zugzeit: ";ctimer-timevalue
    ' Zug ausfuehren
    stack(stackp-4)=256*255
    stack(stackp-3)=0
    stack(stackp-2)=x
    stack(stackp-1)=y
    SUB stackp,4
  ELSE IF s$(ABS(spieler)+1)="Thinkhalf" AND spiel

    timevalue=ctimer
    @thinkhalf(spieler)
    ' print "Zugzeit: ";ctimer-timevalue
    ' Zug ausfuehren
    stack(stackp-4)=256*255
    stack(stackp-3)=0
    stack(stackp-2)=x
    stack(stackp-1)=y
    SUB stackp,4
  ELSE IF s$(ABS(spieler)+1)="Thinklocal" AND spiel

    @local
    ' Zug ausfuehren
    stack(stackp-4)=256*255
    stack(stackp-3)=0
    stack(stackp-2)=x
    stack(stackp-1)=y
    SUB stackp,4
  ELSE IF s$(ABS(spieler)+1)="Think1" AND spiel

    @think1
    ' Zug ausfuehren
    stack(stackp-4)=256*255
    stack(stackp-3)=0
    stack(stackp-2)=x
    stack(stackp-1)=y
    SUB stackp,4
  ELSE
    fertig=0
    DEFMOUSE 0
    REPEAT
      VSYNC

      ' IF INP?(2)
      ' ~INP(2)

      '        FOR mx=0 TO xspielfeld-1
      '          FOR my=0 TO yspielfeld-1
      '            TEXT mx*32,my*32+18,STR$(cfeld(mx,my))
      '          NEXT my
      '        NEXT mx
      '      ENDIF
      MOUSEEVENT x,y,k
      @button(x,y,k)
    UNTIL fertig
  ENDIF
  ' print "Zug: ",x,y
  VSYNC
  INC cnt
LOOP
END
PROCEDURE button(x,y,k)
  '
  ' Hier, falls Mausklick
  '
  LOCAL ob,dummy$
  IF @inbutton(600,380,"QUIT",x,y)
    @mbutton(600,380,"QUIT",1)
    VSYNC
    @ende
  ELSE if @inbutton(300,380,"Zugvorschlag",x,y)
    @mbutton(300,380,"Zugvorschlag",1)
    DEFMOUSE 2
    VSYNC
    @zugvorschlag
    DEFMOUSE 0
    @mbutton(300,380,"Zugvorschlag",0)
    VSYNC
  ELSE if @inbutton(500,380,"Sound",x,y)
    sound=not sound
    @mbutton(500,380,"Sound",abs(sound))
    VSYNC
  ELSE if @inbutton(400,380,"neues Spiel",x,y)
    @mbutton(400,380,"neues Spiel",1)
    DEFMOUSE 2
    VSYNC
    @spieler
    spieler=-RANDOM(2)
    spiel=TRUE
    CLR cnt,feld()
    @show_f
    DEFMOUSE 0
    @mbutton(400,380,"neues Spiel",0)
    VSYNC
  ELSE if @inbutton(200,380,"Farbwechsel",x,y)
    @mbutton(200,380,"Farbwechsel",1)
    VSYNC
    dummy$=s$(1)
    s$(1)=s$(2)
    s$(2)=dummy$
    spieler=NOT spieler
    fertig=TRUE
    @mbutton(200,380,"Farbwechsel",0)
    VSYNC
  ELSE if @inbutton(100,380,"Info",x,y)
    @mbutton(100,380,"Info",1)
    VSYNC
    @info
    @mbutton(100,380,"Info",0)
    VSYNC
  ELSE
    IF spiel
      x=x DIV 32
      y=y DIV 32
      IF NOT (x>=xspielfeld OR y>=yspielfeld)
        IF (feld(x,y) AND -256)/256=ABS(spieler)+1 OR (feld(x,y) AND -256)/256=0
          stack(stackp-4)=256*255
          stack(stackp-3)=0
          stack(stackp-2)=x
          stack(stackp-1)=y
          SUB stackp,4
          fertig=TRUE
        ELSE
          BEEP
        ENDIF
      ENDIF
    ENDIF
  ENDIF
RETURN
PROCEDURE l__i
  lt$=SPACE$(80)
  lt$=lt$+"* * * * * * * *   Das Programm, auf das die Welt gewartet hat! Es ist ein weiteres Spiel von RF Soft!    Es heißt     ---> UND ... UND ... <---              Das große Spiel "
  lt$=lt$+"um kleine Teilchen (Atome)  !   Wir wünschen Ihnen mit diesem Programm viel Spaß! "
  lt$=lt$+"--> --> --> --> -->  UND ... UND ... wurde programmiert von Thorsten "
  lt$=lt$+" Frei und Michael Rennekamp. (ATARI-VERSION und X11-BASIC-Version Markus Hoffmann) Eventuelle Geld- und Sachspenden bitte an eine der folgenden Adressen:     Michael Rennekamp , Oststraße 55 (Am Ostbunker) , 4500 Osnabrück "
  lt$=lt$+"   oder      Thorsten Frei  , Max-Reger-Straße 26 , 4500 Osnabrück      Wir bedanken uns ganz herzlich bei unserem Informatiklehrer für die Flasche Sekt (Alles Gute für Moritz auf seinem weiteren Leidensweg). Wir grüßen Onkel Thomas. Es "
  lt$=lt$+"folgen ein paar Worte in seiner Muttersprache: Öhä Bööhrrr! Mageritte, kernig! Ihr fliegenden Sackratten! NEIN! Ihr Schrumpfhirne!    "
  lt$=lt$+"Wenn Ihnen UND ... UND ... gefällt (oder auch nicht), dann sollten Sie es auf keinen Fall versäumen, uns eine eine Geld- oder Sachspende zukommen zu lassen. RF Soft bedankt sich schon mal im voraus. HALT!!! Wir grüßen auch Günther und "
  lt$=lt$+"Carsten und Markus und Hendrik und Hartmut und Olav und Detlef und Gott und die Welt und natürlich LUTZ !      "
  lt$=lt$+"                                                                                               "
  lt$=lt$+"Hier noch ein paar Tricks zu dieser Programmversion:   Anstelle des Standardalgorithmus 'Computer' gibt es noch einige Varianten: 'Thinklocal' 'Thinkhalf' und 'Think1'. Viel Spass beim Ausprobieren ..."
  lt$=lt$+"                                                                                               "
  lt$=lt$+"Es ist übrigens auch eine IBM-Version dieses Spiels erhältlich ! Wir verschicken sie ggf. auf Anfrage gegen eine Sharewaregeb�hr von DM 23,- ...      "
  lt$=lt$+"                                                                                               "
  lt$=lt$+"Verbesserungsvorschläge und Fehlerberichte in der ATARI-Version bitte an UZS0E7@IBM.RHRZ.UNI-BONN.DE .     "
RETURN
PROCEDURE lauf
  INC lc
  IF lc/2>=LEN(lt$)
    lc=0
  ENDIF
  COLOR weiss
  PBOX 0,308,640,321
  COLOR rot,weiss
  DEFTEXT 0,0.064,0.1,0
  SETFONT "BIG"
  TEXT 0,318,MID$(lt$,lc/2+1,100)
  VSYNC
RETURN
PROCEDURE setzte(x,y)
  feld(x,y)=((feld(x,y) AND 255)+1) or (ABS(spieler)+1)*256
  @make(x,y)
RETURN
PROCEDURE arbeit
  IF stackp<>stackend
    @do_hscaler(500,330,100,1-stackp/stackend)
    ' print stackp,hex$(stack(stackp))
    WHILE stack(stackp)=32*256+32
      ADD stackp,4
    WEND
    stackp=MIN(stackp,stackend)
    IF stackp<>stackend
      ss=stackp
      WHILE ss<>stackend
        c=stack(ss) AND 255
        chi=(stack(ss) AND -256) DIV 256
        cr=stack(ss+1)
        cx=stack(ss+2)
        cy=stack(ss+3)
        IF c=0

          @setzte(cx,cy)
          stack(ss)=32*256+32
          stack(ss+1)=32*256+32
          stack(ss+2)=32*256+32
          stack(ss+3)=32*256+32
        ELSE if c<10 AND c>0
          stack(ss)=(stack(ss) AND -256) OR (c-1)
          IF (feld(cx,cy) AND -256)/256=1
            COLOR weiss
          ELSE
            COLOR schwarz
          ENDIF
          cx2=cx*32+16
          cy2=cy*32+16
          IF cr=1
            cx1=cx*32+16-32
            cy1=cy*32+16
          ELSE if cr=2
            cx1=cx*32+16+32
            cy1=cy*32+16
          ELSE if cr=3
            cx1=cx*32+16
            cy1=cy*32+16-32
          ELSE if cr=4
            cx1=cx*32+16
            cy1=cy*32+16+32
          ENDIF
          PCIRCLE cx2+(c)*(cx1-cx2)/chi,cy2+(c)*(cy1-cy2)/chi,4
          IF c<>chi
            COLOR weiss
            PCIRCLE cx2+(c+1)*(cx1-cx2)/chi,cy2+(c+1)*(cy1-cy2)/chi,4
          ENDIF
          COLOR schwarz
          CIRCLE cx2+(c)*(cx1-cx2)/chi,cy2+(c)*(cy1-cy2)/chi,3
          VSYNC
          PAUSE 0.02
        ENDIF
        ADD ss,4
      WEND
      in_arbeit=TRUE
    ELSE
      CLR in_arbeit
    ENDIF
  ELSE
    CLR in_arbeit
  ENDIF
RETURN
PROCEDURE init
  xspielfeld=8
  yspielfeld=8
  DIM s$(3)
  DIM cfeld(xspielfeld,yspielfeld)  ! Hier werden die Bewertungen abgespeichert
  DIM cfeld2(xspielfeld,yspielfeld) ! buffer
  DIM feld(xspielfeld,yspielfeld)   ! Das ist das Spielfeld: im oberen Byte ist der Spieler codiert
  DIM feld2(xspielfeld,yspielfeld)  ! Das ist ein buffer
  DIM feld3(xspielfeld,yspielfeld)  ! Das ist ein buffer
  DIM i1$(5),i2$(5)
  ARRAYFILL feld(),0
  RANDOMIZE
RETURN
FUNCTION anzsteine(s)
  LOCAL x,y,a
  COLOR gelb
  PCIRCLE 10,350,4
  COLOR schwarz
  CIRCLE 10,350,4
  VSYNC
  CLR a
  FOR x=0 TO xspielfeld-1
    FOR y=0 TO yspielfeld-1
      IF (feld(x,y) AND -256)/256=s OR s=-1
        ADD a,feld(x,y) AND 255
      ENDIF
    NEXT y
  NEXT x

  COLOR weiss
  PCIRCLE 10,350,4
  COLOR schwarz
  CIRCLE 10,350,4
  RETURN a
ENDFUNC
FUNCTION anzfelder(s)
  LOCAL x,y,a

  IF s=-1
    RETURN xspielfeld*yspielfeld
  ELSE
    COLOR gelb
    PCIRCLE 20,350,4
    COLOR schwarz
    CIRCLE 20,350,4
    VSYNC
    CLR a
    FOR x=0 TO xspielfeld-1
      FOR y=0 TO yspielfeld-1
        IF (feld(x,y) AND -256)/256=s
          INC a
        ENDIF
      NEXT y
    NEXT x
    COLOR weiss
    PCIRCLE 20,350,4
    COLOR schwarz
    CIRCLE 20,350,4
    RETURN a
  ENDIF
ENDFUNC
FUNCTION anzdreier(s)
  LOCAL x,y,a
  CLR a
  COLOR rot
  PCIRCLE 30,350,4
  COLOR schwarz
  CIRCLE 30,350,4
  VSYNC
  FOR x=0 TO xspielfeld-1
    FOR y=0 TO yspielfeld-1
      IF ((feld(x,y) AND -256)/256=s OR s=-1) AND (feld(x,y) AND 255)=3
        INC a
      ENDIF
    NEXT y
  NEXT x
  COLOR weiss
  PCIRCLE 30,350,4
  COLOR schwarz
  CIRCLE 30,350,4
  RETURN a
ENDFUNC
PROCEDURE check_win
  LOCAL x,y,a,b
  CLR a,b
  FOR x=0 TO xspielfeld-1
    FOR y=0 TO yspielfeld-1
      IF (feld(x,y) AND -256)/256=1
        ADD a,feld(x,y) AND 255
      ENDIF
      IF (feld(x,y) AND -256)/256=2
        ADD b,feld(x,y) AND 255
      ENDIF
    NEXT y
  NEXT x
  COLOR weiss
  PBOX 320,130,520,300
  COLOR schwarz
  BOX 320,130,520,300
  SETFONT "*-courier-bold-r-*-14-*"
  TEXT 350,160,"Spielsteine:"
  TEXT 340,260,s$(abs(spieler)+1)+" ist dran !"

  SETFONT "*-courier-medium-r-*-12-*"

  TEXT 350,220,"Gesamt: "
  TEXT 350,180,s$(1)+" (weiß):    "
  TEXT 350,200,s$(2)+" (schwarz): "
  TEXT 480,180,STR$(a,4)
  TEXT 480,200,STR$(b,4)
  TEXT 480,220,STR$(a+b,4)

  IF a=0 AND b>1
    COLOR gelb
    PBOX 320,240,520,300
    COLOR schwarz
    BOX 320,240,520,300
    TEXT 350,260,s$(2)+" hat gewonnen !"
    CLR spiel
  ENDIF
  IF b=0 AND a>1
    COLOR gelb
    PBOX 320,240,520,300
    COLOR schwarz
    BOX 320,240,520,300
    TEXT 350,260,s$(1)+" hat gewonnen !"
    CLR spiel
  ENDIF
  VSYNC
RETURN
PROCEDURE show_f
  LOCAL x,y
  COLOR weiss
  PBOX 0,0,8*32,8*32
  COLOR schwarz
  PBOX 16,16,7*32+16,7*32+16
  COLOR weiss
  FOR x=0 TO xspielfeld-1
    FOR y=0 TO yspielfeld-1
      @make(x,y)
    NEXT y
  NEXT x
  BOX 0,0,8*32,8*32
  BOX 0,0,8*32+2,8*32+2
  BOX 0,0,8*32+3,8*32+3
  VSYNC
RETURN
PROCEDURE make(x,y)
  @draw_platz(x,y,(feld(x,y) and -256)/256,feld(x,y) and 255)
  IF (feld(x,y) AND 255)=4
    @explode((feld(x,y) and -256)/256,x,y)
  ENDIF
RETURN
PROCEDURE explode(a,x,y)
  IF sound
    ' wave 1,5
    ' SOUND 1,100
    @dosound("explosion.ogg")
    ' SOUND 1,0
  ENDIF
  feld(x,y)=0
  @make(x,y)
  arandom=RANDOM(3)+4
  IF x<xspielfeld-1
    feld(x+1,y)=a*256 or (feld(x+1,y) and 255)
    stack(stackp-4)=arandom*256+arandom
    stack(stackp-3)=1
    stack(stackp-2)=x+1
    stack(stackp-1)=y
    SUB stackp,4
  ENDIF
  IF x>0
    feld(x-1,y)=a*256 or (feld(x-1,y) and 255)
    stack(stackp-4)=arandom*256+arandom
    stack(stackp-3)=2
    stack(stackp-2)=x-1
    stack(stackp-1)=y
    SUB stackp,4
  ENDIF
  IF y<yspielfeld-1
    feld(x,y+1)=a*256 or (feld(x,y+1) and 255)
    stack(stackp-4)=arandom*256+arandom
    stack(stackp-3)=3
    stack(stackp-2)=x
    stack(stackp-1)=y+1
    SUB stackp,4
  ENDIF
  IF y>0
    feld(x,y-1)=a*256 or (feld(x,y-1) and 255)
    stack(stackp-4)=arandom*256+arandom
    stack(stackp-3)=4
    stack(stackp-2)=x
    stack(stackp-1)=y-1
    SUB stackp,4
  ENDIF
RETURN
PROCEDURE zugvorschlag
  LOCAL i
  @computer
  FOR i=0 TO 10
    @draw_platz(x,y,spieler,(feld(x,y) and 255)+1)
    VSYNC
    PAUSE 0.3
    @make(x,y)
    VSYNC
    PAUSE 0.3
  NEXT i
RETURN
PROCEDURE draw_platz(x,y,g,a)
  COLOR weiss
  PCIRCLE x*32+16,y*32+16,16
  COLOR schwarz
  CIRCLE x*32+16,y*32+16,16
  IF a>0
    IF g=1
      COLOR weiss
    ELSE
      COLOR schwarz
    ENDIF
    IF a=1
      PCIRCLE x*32+16,y*32+16,4
    ELSE if a=2
      PCIRCLE x*32+16-7,y*32+16,4
      PCIRCLE x*32+16+7,y*32+16,4
    ELSE if a=3
      PCIRCLE x*32+16-7,y*32+16+7,4
      PCIRCLE x*32+16+7,y*32+16+7,4
      PCIRCLE x*32+16,y*32+16-7,4
    ELSE if a=4
      PCIRCLE x*32+16-7,y*32+16+7,4
      PCIRCLE x*32+16+7,y*32+16+7,4
      PCIRCLE x*32+16-7,y*32+16-7,4
      PCIRCLE x*32+16+7,y*32+16-7,4
    ENDIF
    COLOR schwarz
    IF a=1
      CIRCLE x*32+16,y*32+16,4
    ELSE if a=2
      CIRCLE x*32+16-7,y*32+16,4
      CIRCLE x*32+16+7,y*32+16,4
    ELSE if a=3
      CIRCLE x*32+16-7,y*32+16+7,4
      CIRCLE x*32+16+7,y*32+16+7,4
      CIRCLE x*32+16,y*32+16-7,4
    ELSE if a=4
      CIRCLE x*32+16-7,y*32+16+7,4
      CIRCLE x*32+16+7,y*32+16+7,4
      CIRCLE x*32+16-7,y*32+16-7,4
      CIRCLE x*32+16+7,y*32+16-7,4
    ENDIF
  ENDIF
RETURN

PROCEDURE computer
  ' Dies ist der zusammengestellte Algorithmus
  '
  '
  LOCAL a,g

  g=ABS(NOT spieler)+1 ! Gegner
  a=@anzdreier(-1)
  ' print "Dreier: ";a

  IF a
    @think1
  ELSE
    @local
  ENDIF
RETURN
PROCEDURE local
  ' Dies ist der lokale Algorithmus
  '
  LOCAL s,g,muh
  '
  '
  PRINT at(18,1);"THINKLOCAL...";
  s=ABS(spieler)+1     ! Spieler
  g=ABS(NOT spieler)+1 ! Gegner
  IF cnt=0             ! Computer fängt an
    x=RANDOM(xspielfeld)
    y=RANDOM(yspielfeld)
  ELSE
    CLR cfeld()
    FOR x=0 TO xspielfeld-1
      FOR y=0 TO yspielfeld-1

        IF (feld(x,y) AND -256)/256=g   ! Auf gegnerische Felder nicht setzten
          cfeld(x,y)=-32000
        ELSE
          ' Eigene Atome auf diagonalenm feldern als +Punkte
          ADD cfeld(x,y),@inh(x-1,y-1,s)+@inh(x+1,y+1,s)+@inh(x+1,y-1,s)+@inh(x-1,y+1,s)
          ' Gegnerische Atome auf diagonalen feldern als +Punkte
          ADD cfeld(x,y),@inh(x-1,y-1,g)+@inh(x+1,y+1,g)+@inh(x+1,y-1,g)+@inh(x-1,y+1,g)
          muh=@feld(x,y)
          IF muh=0   ! Leeres feld
            ' Benachbarte Felder als +Punkte (max. 12)
            cfeld(x,y)=cfeld(x,y)+@inh(x-1,y,-1)+@inh(x+1,y,-1)+@inh(x,y-1,-1)+@inh(x,y+1,-1)
            ' Diagonale Felder als +Punkte (max. 12)
            ADD cfeld(x,y),@inh(x-1,y-1,-1)+@inh(x+1,y+1,-1)+@inh(x+1,y-1,-1)+@inh(x-1,y+1,-1)

            ' Benachbarte gegnerische 3er als -Punkte (max. 4*12)
            a=ABS(@inh(x-1,y,g)=3)+ABS(@inh(x+1,y,g)=3)+ABS(@inh(x,y-1,g)=3)+ABS(@inh(x,y+1,g)=3)
            SUB cfeld(x,y),a*12

          ELSE IF muh=2
            ' "Gegnerischer 3er ?
            a=ABS(@inh(x-1,y,g)=3)+ABS(@inh(x+1,y,g)=3)+ABS(@inh(x,y-1,g)=3)+ABS(@inh(x,y+1,g)=3)
            IF a=0
              ADD cfeld(x,y),4
              ' Benachbarte Felder als +Punkte (max. 12)
              ADD cfeld(x,y),@inh(x-1,y,-1)+@inh(x+1,y,-1)+@inh(x,y-1,-1)+@inh(x,y+1,-1)
            ENDIF
          ELSE IF muh=3   ! 3er ...
            ' anzahl der benachbarten gegnerischen 3er
            ' (max. 4*10 Pkt)
            a=ABS(@inh(x-1,y,g)=3)+ABS(@inh(x+1,y,g)=3)+ABS(@inh(x,y-1,g)=3)+ABS(@inh(x,y+1,g)=3)
            ADD cfeld(x,y),a*10
            ' bei explosion einnehmbare gegnerische Felder
            a=@inh(x-1,y,g)+@inh(x+1,y,g)+@inh(x,y-1,g)+@inh(x,y+1,g)
            ADD cfeld(x,y),a*3
            ' Verluste am Rand vermeiden

            IF y=0 OR y=yspielfeld-1 OR x=0 OR x=xspielfeld-1
              cfeld(x,y)=cfeld(x,y)-2
            ENDIF
            ' Bei explosion generell vorsicht
            cfeld(x,y)=cfeld(x,y)-2
          ELSE ! 1 Atom schon im feld
            ' Gegnerische einnehmbare Felder als -Punkte
            ADD cfeld(x,y),@inh(x-1,y,g)+@inh(x+1,y,g)+@inh(x,y-1,g)+@inh(x,y+1,g)
            a=ABS(@inh(x-1,y,g)=3)+ABS(@inh(x+1,y,g)=3)+ABS(@inh(x,y-1,g)=3)+ABS(@inh(x,y+1,g)=3)
            IF a>0
              SUB cfeld(x,y),9
            ENDIF
            ADD cfeld(x,y),3
          ENDIF
        ENDIF
      NEXT y
    NEXT x
    '

    ' Suche jetzt das beste Feld heraus
    '
    @getbest
  ENDIF
  PRINT "Zug --> (";x;",";y;")";CHR$(13)
RETURN
PROCEDURE getbest
  ' sucht aus cfeld() das beste Feld heraus
  '
  LOCAL a,b,exitf
  CLR a
  m=-32000
  FOR mx=0 TO xspielfeld-1
    FOR my=0 TO yspielfeld-1
      @led(mx,my,cfeld(mx,my))
      IF cfeld(mx,my)>m
        m=cfeld(mx,my)
        CLR a
      ELSE IF cfeld(mx,my)=m
        INC a
      ENDIF
    NEXT my
  NEXT mx
  b=RANDOM(a+1)
  CLR a,exitf
  FOR mx=0 TO xspielfeld-1
    FOR my=0 TO yspielfeld-1
      IF cfeld(mx,my)=m
        INC a
        x=mx
        y=my
        IF a>=b
          exitf=TRUE
        ENDIF
      ENDIF
      EXIT IF exitf
    NEXT my
    EXIT IF exitf
  NEXT mx
  IF m=-32000
    STOP ! irgendwas stimmt nicht !
    CLR x,y
  ENDIF
RETURN
PROCEDURE thinkhalf(spieler)
  ' Computeralgorithmus
  ' Muss die Variablen x y und m fuer den besten Zug hinterlassen
  ' wird rekursiv aufgerufen
  '
  LOCAL as,af,s,g,i,j,ii,jj
  s=ABS(spieler)+1     ! Spieler
  g=ABS(NOT spieler)+1 ! Gegner

  IF cnt=0             ! Computer fängt an
    x=RANDOM(xspielfeld)
    y=RANDOM(yspielfeld)
  ELSE
    as=@anzsteine(s)
    af=@anzfelder(s)
    FOR i=0 TO xspielfeld-1
      FOR j=0 TO yspielfeld-1
        feld2(i,j)=feld(i,j)
      NEXT j
    NEXT i
    CLR cfeld()
    FOR ii=0 TO xspielfeld-1
      FOR jj=0 TO yspielfeld-1
        IF (feld2(ii,jj) AND -256)/256<>g
          @dozug(ii,jj,s)
          cfeld(ii,jj)=@anzfelder(s)+@anzsteine(s)-af-as
          FOR i=0 TO xspielfeld-1
            FOR j=0 TO yspielfeld-1
              feld(i,j)=feld2(i,j)
            NEXT j
          NEXT i
        ELSE
          cfeld(ii,jj)=-32000
        ENDIF
      NEXT jj
    NEXT ii
    @getbest
  ENDIF
RETURN
PROCEDURE think1
  ' Computeralgorithmus
  ' Muss die Variablen x y und m fuer den besten Zug hinterlassen
  LOCAL as,af,s,g,i,j,ii,jj
  PRINT "THINK1...";
  s=ABS(spieler)+1     ! Spieler
  g=ABS(NOT spieler)+1 ! Gegner

  IF cnt=0             ! Computer fängt an
    x=RANDOM(6)+1
    y=RANDOM(6)+1
  ELSE
    as=@anzsteine(s)
    af=@anzfelder(s)
    FOR i=0 TO xspielfeld-1
      FOR j=0 TO yspielfeld-1
        feld3(i,j)=feld(i,j)
      NEXT j
    NEXT i
    CLR cfeld()

    FOR ii=0 TO xspielfeld-1
      FOR jj=0 TO yspielfeld-1
        PLOT ii,jj+305
        VSYNC
        IF (feld3(ii,jj) AND -256)/256<>g
          @dozug(ii,jj,s)
          IF @anzfelder(g)
            @thinkhalf(NOT spieler)
            @dozug(x,y,g)
            cfeld2(ii,jj)=@anzfelder(s)+@anzsteine(s)-af-as
          ELSE
            ' Hat er gewonnen
            cfeld2(ii,jj)=999
          ENDIF
          FOR i=0 TO xspielfeld-1
            FOR j=0 TO yspielfeld-1
              feld(i,j)=feld3(i,j)
            NEXT j
          NEXT i
        ELSE
          cfeld2(ii,jj)=-32000
        ENDIF
      NEXT jj
    NEXT ii
    FOR i=0 TO xspielfeld-1
      FOR j=0 TO yspielfeld-1
        cfeld(i,j)=cfeld2(i,j)
      NEXT j
    NEXT i
    @getbest
  ENDIF
  PRINT "Zug --> (";x;",";y;")";CHR$(13)
RETURN
PROCEDURE dozug(x,y,g)
  ' Fuehrt einen Zug aus, kann rekursiv aufgerufen werden
  ' nur interne Berechnung des neuen feld()
  '
  '
  feld(x,y)=(g*256 OR (feld(x,y) AND 255))
  IF (feld(x,y) AND 255)=3
    feld(x,y)=feld(x,y) and -256
    IF x>0
      @dozug(x-1,y,g)
    ENDIF
    IF y>0
      @dozug(x,y-1,g)
    ENDIF
    IF x<xspielfeld-1
      @dozug(x+1,y,g)
    ENDIF
    IF y<yspielfeld-1
      @dozug(x,y+1,g)
    ENDIF
  ELSE
    feld(x,y)=(feld(x,y) AND -256) or ((feld(x,y) and 255)+1)
  ENDIF
RETURN
FUNCTION inh(ina,inb,inc)
  IF ina<0 OR inb<0 OR ina>=xspielfeld OR inb>=yspielfeld
    RETURN 0
  ELSE
    IF (feld(ina,inb) AND -256)/256=inc OR inc=-1
      RETURN feld(ina,inb) AND 255
    ELSE
      RETURN 0
    ENDIF
  ENDIF
ENDFUNC
FUNCTION feld(ina,inb)
  IF ina<0 OR inb<0 OR ina>=xspielfeld OR inb>=yspielfeld
    RETURN 0
  ELSE
    RETURN (feld(a,b) AND 255)
  ENDIF
ENDFUNC

PROCEDURE ende
  QUIT
RETURN

PROCEDURE spieler
  '
  DEFTEXT 1,0.07,0.15,0
  COLOR weiss
  PBOX 320,130,520,300
  COLOR schwarz
  BOX 320,130,520,300

  LTEXT 330,140,"Spielernamen eingeben:"
  LTEXT 330,160,"weiss:"
  LTEXT 330,180,"schwarz:"

  ledit_curlen=25
  REPEAT
    s$(1)=@ledit$(s$(1),400,160)
    IF ledit_status=0 OR ledit_status=2
      s$(2)=@ledit$(s$(2),400,180)
    ENDIF
  UNTIL ledit_status=0
  ' input "Spieler 2 :",s$(2)
RETURN

PROCEDURE mbutton(button_x,button_y,button_t$,sel)
  LOCAL x,y,w,h
  DEFLINE ,1
  DEFTEXT 1,0.05,0.1,0
  button_l=ltextlen(button_t$)

  x=button_x-button_l/2-10
  y=button_y-10
  w=button_l+20
  h=20
  COLOR grau
  PBOX x+5,y+5,x+w+5,y+h+5
  COLOR abs(sel=0)*weiss
  PBOX x,y,x+w,y+h
  IF sel
    COLOR weiss
  ELSE
    COLOR schwarz
  ENDIF
  BOX x,y,x+w,y+h
  LTEXT button_x-button_l/2,button_y-5,button_t$
RETURN

FUNCTION inbutton(button_x,button_y,button_t$,mx,my)
  LOCAL ax,ay,aw,ah,button_l

  DEFTEXT 1,0.05,0.1,0
  button_l=ltextlen(button_t$)

  ax=button_x-button_l/2-10
  ay=button_y-10
  aw=button_l+20
  ah=20
  IF mx>=ax AND my>=ay AND mx<=ax+aw AND my<=ay+ah
    RETURN true
  ELSE
    RETURN false
  ENDIF
ENDFUNC
PROCEDURE spielfeld
  COLOR weiss
  PBOX 0,0,640,400
  COLOR lila,weiss
  DEFTEXT 1,0.2,0.2,0
  LTEXT 450-ltextlen("ATOMIUM")/2,0,"ATOMIUM"
  @show_f
  GRAPHMODE 2
  COLOR lila,weiss
  DEFTEXT 0
  SETFONT "SMALL"
  TEXT 262,25,"SPIELREGELN:"
  TEXT 262,35,"Die Spieler setzten nacheinander die Steine (Neutronen) in die"
  TEXT 262,45,"Felder (Kristallgitter). Mit vier Neutronen wird ein Atom instabil,"
  TEXT 262,55,"und es zerfällt, wobei seine Neutronen von den Nachbaratomen"
  TEXT 262,65,"absorbiert werden, wodurch diese zu eigenen Atomen werden."
  TEXT 262,75,"Gewonnen hat, wer alle Atome des Gegners zu eigenen gemacht hat."
  DEFTEXT 1,0.2,0.2,0
  GRAPHMODE 1
  @mbutton(600,380,"QUIT",0)
  @mbutton(500,380,"Sound",0)
  @mbutton(400,380,"neues Spiel",0)
  @mbutton(300,380,"Zugvorschlag",0)
  @mbutton(200,380,"Farbwechsel",0)
  @mbutton(100,380,"Info",0)
  @hscalerbar(500,330,100)
  DEFTEXT 1,0.05,0.05,0
  LTEXT 10,266,"(c) MARKUS HOFFMANN 1992"
  VSYNC
RETURN
PROCEDURE hscalerbar(scaler_x,scaler_y,scaler_w)
  LOCAL i,k

  COLOR weiss
  PBOX scaler_x,scaler_y,scaler_x+scaler_w,scaler_y+20
  COLOR schwarz
  BOX scaler_x,scaler_y,scaler_x+scaler_w,scaler_y+20
  FOR i=0 TO 100 STEP 5
    IF (i MOD 50)=0
      k=7
      DEFTEXT 0
      TEXT scaler_x+i/100*scaler_w-len(STR$(i))*2.5,scaler_y+37,STR$(i)
    ELSE if (i MOD 10)=0
      k=5
    ELSE
      k=3
    ENDIF
    LINE scaler_x+i/100*scaler_w,scaler_y+20,scaler_x+i/100*scaler_w,scaler_y+20+k
  NEXT i
RETURN

PROCEDURE do_hscaler(scaler_x,scaler_y,scaler_w,wert)
  COLOR weiss
  PBOX scaler_x+1,scaler_y+1,scaler_x+scaler_w,scaler_y+20
  COLOR gelb
  PBOX scaler_x+1,scaler_y+1,scaler_x+(scaler_w-2)*wert+1,scaler_y+20
RETURN

FUNCTION ledit$(t$,x,y)
  LOCAL t2$,cc,curpos,a,b,c$,curlen

  t2$=t$

  cc=LEN(t$)
  curpos=x+ltextlen(LEFT$(t$,cc))
  curlen=MAX(ledit_curlen,20)

  DO
    COLOR weiss
    LTEXT x,y,t2$
    t2$=t$
    LINE curpos,y,curpos,y+curlen
    curpos=x+ltextlen(LEFT$(t$,cc))
    COLOR schwarz
    LTEXT x,y,t2$
    COLOR rot
    LINE curpos,y,curpos,y+curlen
    VSYNC
    KEYEVENT a,b,c$
    COLOR weiss
    IF b AND -256
      ' print ".";
      b=b and 255
      IF b=8 ! Bsp
        IF len(t2$)
          t$=LEFT$(t2$,cc-1)+RIGHT$(t2$,LEN(t2$)-cc)
          DEC cc
        ELSE
          BELL
        ENDIF
      ELSE if b=13 ! Ret
        ledit_status=0
        LINE curpos,y,curpos,y+curlen
        RETURN t2$
      ELSE if b=10 ! Ret
        ledit_status=0
        LINE curpos,y,curpos,y+curlen
        RETURN t2$
      ELSE if b=82
        ledit_status=1
        LINE curpos,y,curpos,y+curlen
        RETURN t2$
      ELSE if b=84
        ledit_status=2
        LINE curpos,y,curpos,y+curlen
        RETURN t2$
      ELSE if b=255 ! Del
        IF cc<len(t2$)
          t$=LEFT$(t2$,cc)+RIGHT$(t2$,LEN(t2$)-cc-1)
        ENDIF
      ELSE if b=81
        cc=MAX(cc-1,0)
      ELSE if b=83
        cc=MIN(cc+1,LEN(t2$))
      ELSE
        PRINT a,"$"+HEX$(b),b;CHR$(13)
      ENDIF
    ELSE
      t$=LEFT$(t2$,cc)+CHR$(b)+RIGHT$(t2$,LEN(t2$)-cc)
      INC cc
    ENDIF
    ' print a,"$"+hex$(b),b
  LOOP
ENDFUNCTION

PROCEDURE info
  ALERT 1,"      A T O M I U M|| (c) Markus Hoffmann 1991| |Dieses Programm darf nur zusammen |mit dem Paket X11-BASIC weiter-|gegeben werden.",1," OK ",dummy
RETURN

PROCEDURE led(x,y,c)
  LOCAL r,g,b,col
  IF c=0
    col=weiss
  ELSE if c<0
    col=schwarz
  ELSE if c>10
    col=lila
  ELSE
    g=0.1*c
    b=0.1*(10-c)
    r=sqrt(abs(1-r*r-g*g))
    col=GET_COLOR(r*65535,g*65535,b*65535)
  ENDIF
  COLOR col
  PCIRCLE x*10+550,100+y*10,4
  COLOR schwarz
  CIRCLE x*10+550,100+y*10,4
RETURN
PROCEDURE dosound(f$)
  IF sound
    IF EXIST("bas/sound/"+f$)
      PLAYSOUNDFILE "bas/sound/"+f$
    ENDIF
  ENDIF
RETURN
