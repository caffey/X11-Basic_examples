ECHO OFF
CLS
PRINT TAB(12);"*=*=*=*>  Zahlenraten  <*=*=*=*     (c) Markus Hoffmann"
PRINT tab(40);"Version 1.03"
PRINT
PRINT "Ziel des Spieles :"
PRINT "In m�glichst wenigen Versuchen      "
PRINT "eine Zahl zwischen 1 und 1000 zu erraten."
PRINT
' varload best.user.zahlenr$,best.vers.zahlenr,zahlenrdate$
RANDOMIZE
DO
  PRINT "Der Bisherig beste User ist ";

  IF best.vers.zahlenr
    PRINT best.user.zahlenr$'"mit"'best.vers.zahlenr'"Versuchen am"'zahlenrdate$'"."
  ELSE
    PRINT " bisher niemand."
  ENDIF
  '
  PRINT string$(79,"-")
  PRINT "Neues Spiel:"
  PRINT
  k=0
  z=RANDOM(1000)
  DO
    INPUT "Die Ratezahl eingeben:",r
    INC k
    EXIT IF r=z
    IF r<0 OR r>1000
      PRINT "Es hat wenig Sinn, negative Zahlen oder Zahlen groesser 1000 einzugeben ! "
      INPUT "Wollen Sie das Spiel beenden ? 1=ja ",yn
      IF yn=1
        END
      ENDIF
    ELSE IF r>z
      PRINT "Die Zahl"'r'"ist zu gro� !"
    ELSE
      PRINT "Die Zahl"'r'"ist zu klein !"
    ENDIF
  LOOP
  PRINT "Richtig geraten, die Zahl ist"'z;"."
  PRINT "Es waren"'k'"Versuche noetig."
  IF k<best.vers.zahlenr OR best.vers.zahlenr=0
    PRINT "Du bist der Beste bisher !"
    best.user.zahlenr$=env$("USER")
    best.vers.zahlenr=k
    zahlenrdate$=date$+" "+time$
    ' varsave best.user.zahlenr$,best.vers.zahlenr,zahlenrdate$
  ENDIF
  INPUT "Noch ein Spiel 1=Ja";r
  EXIT IF r<>1
  k=0
LOOP
END
REM Verfasser  ***** Markus Hoffmann ******
REM            *****     26.4.1986   ******
