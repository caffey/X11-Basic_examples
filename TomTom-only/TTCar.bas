' 2010 Luis Alberto Aldea (nick SkyNet)

' ************************************************
' **************** Variables *********************
' ************************************************

'variables globales
g_gpsfeed$=""
g_gpsGPGGA$=""
g_gpsGPRMC$=""
g_gpsGPGSA=""

'variables del display
g_gpsstatus=0
g_speed$="000.0"
g_speed0$="0"
g_acelera=0
g_tiempo$="0"
g_tiempo0$="0"
'variables auxiliares
g_dir=0
g_alt=0
g_sats=0
g_NSind$=""
g_EWind$=""
g_Dir$=""
'variables de logger
last_Dato$=""
last_DatoD$=""
leidos=0

'Variables de optimizacion
g_chng_status=2

'Tama�o del LED de la speedBar
LED_SPEEDBAR_SIZE=5

'Matrices de leds [x,y,estado]
leds_speedBar()=[20,100;29,92;38,83;47,73;56,64;65,56;74,49;83,42;92,37;101,31;110,26;119,23;128,21;137,20;146,19;155,18;164,18;173,19;182,20;191,21;200,22;209,23;218,25;227,27;236,30;245,33;254,36;263,39;272,43;281,47;290,51;299,55]

'Variable de punto de scan
scan=0

'Recursos graficos
rojo=GET_COLOR(65535,0,0)
rojoOscuro=GET_COLOR(35535,0,0)
rojoOscurete=GET_COLOR(9535,0,0)
rojoOscurote=GET_COLOR(20535,0,0)
verde=GET_COLOR(0,65535,0)
amarillo=GET_COLOR(65535,65535,0)
blanco=GET_COLOR(65535,65535,65535)
negro=GET_COLOR(0,0,0)
azul=GET_COLOR(0,0,65535)
lightgreen=GET_COLOR(37008,61166,37008)
darkgreen=GET_COLOR(0,43908,0)
gris=GET_COLOR(20000,20000,20000)
fuentepeq$="-*-helvetica-bold-r-normal-*-10-*-iso8859-*"
fuentemedia$="-*-helvetica-bold-r-normal-*-12-*-iso8859-*"

' ************************************************
' **************** /Variables ********************
' ************************************************

' ************************************************
' *************** Inicializacion  ****************
' ************************************************

'Dimension del display
SIZEW ,320,240

'Dibujamos la pantalla inicial
@PintarDisplay
@AbrirDatosGPS

' ************************************************
' *************** /Inicializacion  ***************
' ************************************************

' ************************************************
' *********** Bucle principal de programa ********
' ************************************************
DO

  @LeerDatosGPS
  IF (scan=9)
    @ActualizarPantalla
  ENDIF
  scan=(scan+1) mod 10

LOOP
QUIT
' ************************************************
' ********** /Bucle principal de programa ********
' ************************************************

' ************************************************
' ************** Procedimientos ******************
' ************************************************

PROCEDURE PintarDisplay
  'Fondo negro
  COLOR negro
  PBOX 0,0,320,240
  'Lineas verticales
  @PintarLineas()
  'Barra de leds progresivos
  @PintarProgress(0)
  'Numeros asociados a los leds
  @PintarProgressNum()
  'Velocimetro
  COLOR rojoOscurote
  PBOX 135,70,250,145
  @PintarVelocidad(A,A,A)
  'La se�al de satelite
  @PintarGanancia(0)
  'La se�al de satelite (texto)
  @PintarGananciaText()
  'La altitud
  @PintarAltitud(0)
  @PintarAltitudText()
  'La brujula
  @PintarDireccion1LED(0)
  @PintarBrujulaText()
  'La aceleracion
  @PintarAceleracion("0","0")
  @PintarAceleracionText()
  @PintarKmh
  ' Royalties
  SETFONT fuentepeq$
  COLOR azul
  TEXT 267,232,"by Aldea"
RETURN

'
' Dibuja la barra de progreso de leds encendida hasta valor
'
PROCEDURE PintarProgress(valor)

  FOR i=0 TO 31
    IF (i>=valor)
      @PintarLed(i,0)
    ELSE
      @PintarLed(i,1)
    ENDIF
  NEXT i
RETURN

'
' 'Dibuja los numeros asociados a la barra de progreso
'
PROCEDURE PintarProgressNum()
  SETFONT fuentepeq$
  COLOR blanco
  TEXT 45,95,"20"
  TEXT 77,66,"40"
  TEXT 115,45,"60"
  TEXT 150,40,"80"
  TEXT 183,42,"100"
  TEXT 220,48,"120"
  TEXT 252,61,"140"
  TEXT 290,74,"160"
  TEXT 15,80,"25"
  TEXT 15,60,"50"
  TEXT 15,40,"75"
  TEXT 15,20,"100"
RETURN

'
' Dibuja un display LCD de 7 segmentos ajustado a offsetX y offsetY, con valor luminoso, siendo valor=[0|1|2|3|4|5|6|7|8|9|A]
'
PROCEDURE PintarLCD7(offsetX,offsetY,valor)

  'Grafico poligonal
  x1()=[23,2,7,16,16,16]
  y1()=[2,2,7,7,7,7]
  x2()=[21,24,24,18,18,18]
  y2()=[26,21,2,7,21,21]
  x3()=[24,24,21,18,18,18]
  y3()=[51,30,25,29,45,45]
  x4()=[2,22,18,7,7,7]
  y4()=[51,51,46,46,46,46]
  x5()=[3,0,0,6,5,5]
  y5()=[25,29,51,46,29,29]
  x6()=[0,0,3,6,5,5]
  y6()=[2,22,26,21,6,6]
  x7()=[5,8,16,20,16,8]
  y7()=[25,28,28,25,22,22]

  'Reajustes con offset
  FOR i=0 TO 5
    x1(i)=x1(i)+offsetX
    x2(i)=x2(i)+offsetX
    x3(i)=x3(i)+offsetX
    x4(i)=x4(i)+offsetX
    x5(i)=x5(i)+offsetX
    x6(i)=x6(i)+offsetX
    x7(i)=x7(i)+offsetX
    y1(i)=y1(i)+offsetY
    y2(i)=y2(i)+offsetY
    y3(i)=y3(i)+offsetY
    y4(i)=y4(i)+offsetY
    y5(i)=y5(i)+offsetY
    y6(i)=y6(i)+offsetY
    y7(i)=y7(i)+offsetY
  NEXT i

  'Coloreamos el display segun valor
  IF (valor=1)
    COLOR rojo
    DEFFILL rojo
    POLYFILL 5,x3(),y3()
    POLYFILL 5,x2(),y2()
    COLOR rojoOscurete
    DEFFILL rojoOscurete
    POLYFILL 6,x7(),y7()
    POLYFILL 5,x6(),y6()
    POLYFILL 5,x5(),y5()
    POLYFILL 4,x4(),y4()
    POLYFILL 4,x1(),y1()
  ELSE if (valor=2)
    COLOR rojo
    DEFFILL rojo
    POLYFILL 4,x1(),y1()
    POLYFILL 5,x2(),y2()
    POLYFILL 6,x7(),y7()
    POLYFILL 5,x5(),y5()
    POLYFILL 4,x4(),y4()
    COLOR rojoOscurete
    DEFFILL rojoOscurete
    POLYFILL 5,x6(),y6()
    POLYFILL 5,x3(),y3()
  ELSE if (valor=3)
    COLOR rojo
    DEFFILL rojo
    POLYFILL 4,x1(),y1()
    POLYFILL 5,x2(),y2()
    POLYFILL 6,x7(),y7()
    POLYFILL 5,x3(),y3()
    POLYFILL 4,x4(),y4()
    COLOR rojoOscurete
    DEFFILL rojoOscurete
    POLYFILL 5,x6(),y6()
    POLYFILL 5,x5(),y5()
  ELSE if (valor=4)
    COLOR rojo
    DEFFILL rojo
    POLYFILL 5,x2(),y2()
    POLYFILL 5,x3(),y3()
    POLYFILL 5,x6(),y6()
    POLYFILL 6,x7(),y7()
    COLOR rojoOscurete
    DEFFILL rojoOscurete
    POLYFILL 4,x1(),y1()
    POLYFILL 4,x4(),y4()
    POLYFILL 5,x5(),y5()
  ELSE if (valor=5)
    COLOR rojo
    DEFFILL rojo
    POLYFILL 4,x1(),y1()
    POLYFILL 5,x3(),y3()
    POLYFILL 4,x4(),y4()
    POLYFILL 5,x6(),y6()
    POLYFILL 6,x7(),y7()
    COLOR rojoOscurete
    DEFFILL rojoOscurete
    POLYFILL 5,x2(),y2()
    POLYFILL 5,x5(),y5()
  ELSE if (valor=6)
    COLOR rojo
    DEFFILL rojo
    POLYFILL 4,x1(),y1()
    POLYFILL 5,x3(),y3()
    POLYFILL 4,x4(),y4()
    POLYFILL 5,x5(),y5()
    POLYFILL 5,x6(),y6()
    POLYFILL 6,x7(),y7()
    COLOR rojoOscurete
    DEFFILL rojoOscurete
    POLYFILL 5,x2(),y2()
  ELSE if (valor=7)
    COLOR rojo
    DEFFILL rojo
    POLYFILL 5,x3(),y3()
    POLYFILL 5,x2(),y2()
    POLYFILL 4,x1(),y1()
    COLOR rojoOscurete
    DEFFILL rojoOscurete
    POLYFILL 6,x7(),y7()
    POLYFILL 5,x6(),y6()
    POLYFILL 5,x5(),y5()
    POLYFILL 4,x4(),y4()
  ELSE if (valor=8)
    COLOR rojo
    DEFFILL rojo
    POLYFILL 6,x7(),y7()
    POLYFILL 5,x6(),y6()
    POLYFILL 5,x5(),y5()
    POLYFILL 4,x4(),y4()
    POLYFILL 5,x3(),y3()
    POLYFILL 5,x2(),y2()
    POLYFILL 4,x1(),y1()
  ELSE if (valor=9)
    COLOR rojo
    DEFFILL rojo
    POLYFILL 6,x7(),y7()
    POLYFILL 5,x6(),y6()
    POLYFILL 4,x4(),y4()
    POLYFILL 5,x3(),y3()
    POLYFILL 5,x2(),y2()
    POLYFILL 4,x1(),y1()
    COLOR rojoOscurete
    DEFFILL rojoOscurete
    POLYFILL 5,x5(),y5()
  ELSE if (valor=0)
    COLOR rojo
    DEFFILL rojo
    POLYFILL 5,x6(),y6()
    POLYFILL 5,x5(),y5()
    POLYFILL 4,x4(),y4()
    POLYFILL 5,x3(),y3()
    POLYFILL 5,x2(),y2()
    POLYFILL 4,x1(),y1()
    COLOR rojoOscurete
    DEFFILL rojoOscurete
    POLYFILL 6,x7(),y7()
  ELSE
    COLOR rojoOscurete
    DEFFILL rojoOscurete
    POLYFILL 6,x7(),y7()
    POLYFILL 5,x6(),y6()
    POLYFILL 5,x5(),y5()
    POLYFILL 4,x4(),y4()
    POLYFILL 5,x3(),y3()
    POLYFILL 5,x2(),y2()
    POLYFILL 4,x1(),y1()
  ENDIF

RETURN

'
' Escribe la velocidad en el LCD de 7 segmentos
'
PROCEDURE PintarVelocidad(x,y,z)
  @PintarLCD7(150,80,x)
  @PintarLCD7(180,80,y)
  @PintarLCD7(210,80,z)
RETURN

'
' Dibuja el Km/h
'
PROCEDURE PintarKmh
  'El Km/h
  IF (g_chng_status<>g_gpsstatus)

    g_chng_status=g_gpsstatus

    IF (g_gpsstatus=1)
      COLOR verde
      DEFFILL verde
    ELSE
      COLOR rojo
      DEFFILL rojo
    ENDIF
    x1()=[260,262,262,260]
    y1()=[112,110,118,120]
    POLYFILL 4,x1(),y1()
    x1()=[260,262,262,260]
    y1()=[120,121,128,130]
    POLYFILL 4,x1(),y1()
    x1()=[262,260,266,264]
    y1()=[119,121,121,119]
    POLYFILL 4,x1(),y1()
    x1()=[265,264,272,270]
    y1()=[118,121,112,112]
    POLYFILL 4,x1(),y1()
    x1()=[265,267,272,270]
    y1()=[120,121,129,129]
    POLYFILL 4,x1(),y1()
    x1()=[276,278,278,276]
    y1()=[112,111,130,129]
    POLYFILL 4,x1(),y1()
    x1()=[278,278,282,281]
    y1()=[112,110,120,122]
    POLYFILL 4,x1(),y1()
    x1()=[281,281,285,285]
    y1()=[122,120,110,112]
    POLYFILL 4,x1(),y1()
    x1()=[285,287,287,285]
    y1()=[112,110,130,128]
    POLYFILL 4,x1(),y1()
    x1()=[281,283,283,281]
    y1()=[122,121,130,129]
    POLYFILL 4,x1(),y1()
    x1()=[289,291,298,296]
    y1()=[130,130,112,112]
    POLYFILL 4,x1(),y1()
    x1()=[301,303,303,301]
    y1()=[112,110,130,128]
    POLYFILL 4,x1(),y1()
    x1()=[303,301,308,306]
    y1()=[119,121,121,119]
    POLYFILL 4,x1(),y1()
    x1()=[307,309,309,307]
    y1()=[112,110,130,128]
    POLYFILL 4,x1(),y1()
    'setfont fuentegrande$
    'text 260,140,"Km/h"
  ENDIF
RETURN

'
' Pinta las lineas verticales del cuadro
'
PROCEDURE PintarLineas()

  COLOR gris

  BOX 257,108,311,132
  '20
  x1()=[48,50,50,48]
  y1()=[10,10,83,83]
  POLYFILL 4,x1(),y1()
  y1()=[100,100,155,155]
  POLYFILL 4,x1(),y1()
  y1()=[220,220,235,235]
  POLYFILL 4,x1(),y1()
  '40
  x1()=[82,84,84,82]
  y1()=[10,10,53,53]
  POLYFILL 4,x1(),y1()
  y1()=[71,71,155,155]
  POLYFILL 4,x1(),y1()
  y1()=[220,220,235,235]
  POLYFILL 4,x1(),y1()
  '60
  x1()=[118,120,120,118]
  y1()=[10,10,33,33]
  POLYFILL 4,x1(),y1()
  y1()=[50,50,155,155]
  POLYFILL 4,x1(),y1()
  y1()=[190,190,205,205]
  POLYFILL 4,x1(),y1()
  y1()=[220,220,235,235]
  POLYFILL 4,x1(),y1()
  '80
  x1()=[154,156,156,154]
  y1()=[10,10,28,28]
  POLYFILL 4,x1(),y1()
  y1()=[45,45,69,69]
  POLYFILL 4,x1(),y1()
  y1()=[146,146,235,235]
  POLYFILL 4,x1(),y1()
  '100
  x1()=[190,192,192,190]
  y1()=[10,10,30,30]
  POLYFILL 4,x1(),y1()
  y1()=[47,47,69,69]
  POLYFILL 4,x1(),y1()
  y1()=[146,146,155,155]
  POLYFILL 4,x1(),y1()
  y1()=[220,220,235,235]
  POLYFILL 4,x1(),y1()
  '120
  x1()=[226,228,228,226]
  y1()=[10,10,36,36]
  POLYFILL 4,x1(),y1()
  y1()=[53,53,69,69]
  POLYFILL 4,x1(),y1()
  y1()=[146,146,155,155]
  POLYFILL 4,x1(),y1()
  y1()=[220,220,235,235]
  POLYFILL 4,x1(),y1()
  '140
  x1()=[262,264,264,262]
  y1()=[10,10,49,49]
  POLYFILL 4,x1(),y1()
  y1()=[66,66,108,108]
  POLYFILL 4,x1(),y1()
  y1()=[132,132,155,155]
  POLYFILL 4,x1(),y1()
  y1()=[220,220,235,235]
  POLYFILL 4,x1(),y1()
  '160
  x1()=[300,302,302,300]
  y1()=[10,10,62,62]
  POLYFILL 4,x1(),y1()
  y1()=[79,79,108,108]
  POLYFILL 4,x1(),y1()
  y1()=[132,132,175,175]
  POLYFILL 4,x1(),y1()
  y1()=[190,190,205,205]
  POLYFILL 4,x1(),y1()
  'y1()=[220,220,235,235]
  'polyfill 4,x1(),y1()
RETURN

'
' Pinta la se�al de ganancia con el valor indicado encendido
'
PROCEDURE PintarGanancia(valor)
  inicio=20
  FOR i=1 TO 12
    IF (valor>=i)
      COLOR rojo
    ELSE
      COLOR rojoOscurete
    ENDIF
    PBOX inicio,180,inicio+8,185
    inicio=inicio+10
  NEXT i
RETURN

PROCEDURE PintarGananciaText()
  SETFONT fuentepeq$
  COLOR blanco
  TEXT 18,170,"S A T S   A C T I V O S"
RETURN

'
' Pinta la se�al de altitud con el valor indicado encendido
'
PROCEDURE PintarAltitud(valor)
  inicio=20
  FOR i=1 TO 12
    IF (valor>=i)
      COLOR rojo
    ELSE
      COLOR rojoOscurete
    ENDIF
    PBOX inicio,210,inicio+8,215
    inicio=inicio+10
  NEXT i
RETURN

PROCEDURE PintarAltitudText()
  SETFONT fuentepeq$
  COLOR blanco
  TEXT 18,201,"A L T I T U D"
RETURN

PROCEDURE PintarAceleracion(t$,v$)
  LOCAL varT
  LOCAL varV
  LOCAL segs
  LOCAL segs0
  'Pasamos el UTC a segundos
  segs=(VAL(MID$(t$,3,2))*60)+(VAL(MID$(t$,5,2)))
  segs0=(VAL(MID$(g_tiempo0$,3,2))*60)+(VAL(MID$(g_tiempo0$,5,2)))
  varT=segs-segs0
  varV=VAL(v$)-val(g_speed0$)
  IF varT>0
    g_acelera=varV/varT
  ELSE
    g_acelera=0
  ENDIF

  'Pintamos graficamente segun el valor de la aceleracion
  inicio=185

  FOR i=1 TO 12
    IF (g_acelera>(i-0)*0.5)
      COLOR rojo
    ELSE
      COLOR rojoOscurete
    ENDIF
    PBOX inicio,180,inicio+8,185
    inicio=inicio+10
  NEXT i
  'Actualizacion de v como v0
  g_speed0$=v$
  g_tiempo0$=t$
RETURN

PROCEDURE PintarAceleracionText()

  SETFONT fuentepeq$
  COLOR blanco
  TEXT 183,170,"A C E L E R A C I O N"

RETURN

'Pinta la direccion con 1 LED (track course)
PROCEDURE PintarDireccion1LED(course)
  inicio=185
  IF (g_gpsstatus=1)
    FOR i=1 TO 12
      IF ((course>=((i-1)*30)) AND (course<(i*30)))
        COLOR rojo
      ELSE
        COLOR rojoOscurete
      ENDIF
      PBOX inicio,210,inicio+8,215
      inicio=inicio+10
    NEXT i
  ELSE
    IF (g_chng_status<>g_gpsstatus)
      COLOR rojoOscurete
      FOR i=1 TO 12
        PBOX inicio,210,inicio+8,215
        inicio=inicio+10
      NEXT i
    ENDIF
  ENDIF
RETURN

PROCEDURE PintarBrujulaText()
  SETFONT fuentepeq$
  COLOR blanco
  TEXT 183,201,"N     E     S     W"
RETURN

'
' Pinta el led argumentado como 'cual' con el color correspondiente
'
PROCEDURE PintarLed(cual,estado)
  IF (estado=1)
    COLOR rojo
  ELSE
    COLOR rojoOscurete
  ENDIF
  'Lo pintamos
  PCIRCLE leds_speedBar(cual,0), leds_speedBar(cual,1), LED_SPEEDBAR_SIZE
RETURN

'
' Actualiza los datos de la pantala con los recibidos del modem
'
PROCEDURE ActualizarPantalla()

  veloX=VAL(MID$(g_speed$,1,1))
  veloY=VAL(MID$(g_speed$,2,1))
  veloZ=VAL(MID$(g_speed$,3,1))
  velocidad=(veloX*100)+(veloY*10)+(veloZ)

  'La barra de progreso (vel)
  @PintarProgress((velocidad*32)/160)
  'La se�al de satelite
  @PintarGanancia(g_sats)
  'La altitud
  @PintarAltitud(g_alt/100)
  'La brujula
  @PintarDireccion1LED(val(g_Dir$))
  'La velocidad en leds
  @PintarVelocidad(veloX,veloY,veloZ)
  'La aceleracion
  @PintarAceleracion(g_tiempo$,g_speed$)
  'El color de Km/h
  @PintarKmh
  'Refresco veraniego
  SHOWPAGE
  PAUSE 0.1
RETURN

'
' Abre el fichero de datos GPS
'
PROCEDURE AbrirDatosGPS()
  LOCAL aux=0
  tomtom=false
  IF GPS?=0
    IF exist("/var/run/gpsfeed")
      OPEN "I",#1,"/var/run/gpsfeed"
      tomtom=true
    ELSE
      COLOR red
      ALERT 1,"Error|GPS",1,"Ok",a
    ENDIF
  ELSE
    GPS ON
  ENDIF
RETURN

'
' Lee los datos recogidos por el GPS y actualiza las variables globales de datos GPS
'
PROCEDURE LeerDatosGPS()
  IF tomtom
    LINEINPUT #1,g_gpsfeed$

    IF left$(g_gpsfeed$,6)="$GPGGA"
      'parseo GPGGA - para la altitud
      @parseGPGGA(g_gpsfeed$)
    ELSE if LEFT$(g_gpsfeed$,6)="$GPGSA"
      'parseo GPGSA - para los satelites activos
      @parseGPGSA(g_gpsfeed$)
    ELSE if LEFT$(g_gpsfeed$,6)="$GPRMC"
      'parseo  GPRMC - para el resto
      @ParseGPRMC(g_gpsfeed$)
    ENDIF
  ELSE
    GET_LOCATION g_lat,g_lon,g_alt,b,a,speed,t,p$
    g_speed$=USING$(speed/3.6,"000.#")
  ENDIF
RETURN

'
' Parseo de datos de tipo GPGGA
'

PROCEDURE ParseGPGGA(gpsfeed$)
  '
  ' De este parametro del nmea sacamos la altitud

  '	$GPGGA,hhmmss.ss,llll.ll,a,yyyyy.yy,a,x,xx,x.x,x.x,M,x.x,M,x.x,xxxx*hh
  '	1    = UTC of Position
  '	2    = Latitude
  '	3    = N or S
  '	4    = Longitude
  '	5    = E or W
  '	6    = GPS quality indicator (0=invalid; 1=GPS fix; 2=Diff. GPS fix)
  '	7    = Number of satellites in use [not those in view]
  '	8    = Horizontal dilution of position
  '	9    = Antenna altitude above/below mean sea level (geoid)
  '	10   = Meters  (Antenna height unit)
  '	11   = Geoidal separation (Diff. between WGS-84 earth ellipsoid and
  '	       mean sea level.  -=geoid is below WGS-84 ellipsoid)
  '	12   = Meters  (Units of geoidal separation)
  '	13   = Age in seconds since last update from diff. reference station
  '	14   = Diff. reference station ID#
  '	15   = Checksum

  g_gpsGPGGA$=gpsfeed$

  LOCAL feed$
  LOCAL i
  LOCAL altitud$

  FOR i=0 TO 9
    SPLIT gpsfeed$,",",0,feed$,gpsfeed$
    IF i=9
      altitud$=feed$
    ENDIF
  NEXT i
  IF altitud$=""
    g_alt=0
  ELSE
    g_alt=VAL(altitud$)
  ENDIF
RETURN

PROCEDURE ParseGPGSA(gpsfeed$)

  'GPGSA= De este otro parametro nmea sacamos la info. de satelites activos

  '	$GPGSA,A,3,,,,,,16,18,,22,24,,,3.6,2.1,2.2*3C

  '	1    = Mode:
  '       M=Manual, forced to operate in 2D or 3D
  '       A=Automatic, 3D/2D
  '	2    = Mode:
  '       1=Fix not available
  '       2=2D
  '       3=3D
  '	3-14 = IDs of SVs used in position fix (null for unused fields)
  '	15   = PDOP
  '	16   = HDOP
  '	17   = VDOP

  g_gpsGPGSA$=gpsfeed$

  LOCAL satelites
  LOCAL i
  LOCAL feed$

  satelites=0
  FOR i=0 TO 12
    SPLIT gpsfeed$,",",0,feed$,gpsfeed$
    IF i>3 AND i<12
      IF len(feed$)>0
        satelites=satelites+1
      ENDIF
    ENDIF
  NEXT i
  g_sats=satelites
RETURN

PROCEDURE ParseGPRMC(gpsfeed$)

  ' De este otro sacamos el estado, latitud, longitud, velocidad y direccion

  '	$GPRMC,hhmmss.ss,A,llll.ll,a,yyyyy.yy,a,x.x,x.x,ddmmyy,x.x,a*hh
  '	1    = UTC of position fix
  '	2    = Data status (V=navigation receiver warning)
  '	3    = Latitude of fix
  '	4    = N or S
  '	5    = Longitude of fix
  '	6    = E or W
  '	7    = Speed over ground in knots
  '	8    = Track made good in degrees True
  '	9    = UT date
  '	10   = Magnetic variation degrees (Easterly var. subtracts from true course)
  '	11   = E or W
  '	12   = Checksum

  LOCAL feed$
  LOCAL i
  LOCAL estado$
  LOCAL lat$
  LOCAL lon$
  LOCAL knots$
  LOCAL cog$
  LOCAL kph$
  LOCAL tiempo$

  g_gpsGPRMC$=gpsfeed$

  FOR i=0 TO 12
    SPLIT gpsfeed$,",",0,feed$,gpsfeed$
    IF i=2
      '	Estado : [A::dato activo y valido | V::dato no valido]
      estado$=feed$
    ELSE if i=1
      'tiempo formato UTC
      tiempo$=feed$
    ELSE if i=3
      'latitud (ddmm.mmmm)
      lat$=feed$
    ELSE if i=4
      'indicator N/S : [N::norte | S::sur]
      g_NSind$=feed$
    ELSE if i=5
      'longitud (dddmm.mmmm)
      lon$=feed$
    ELSE if i=6
      'indicator E/O : [E::este | W::oeste]
      g_EWind$=feed$
    ELSE if i=7
      '	velocidad en knots (1 knot = 1.852 Km/h )
      knots$=feed$
    ELSE if i=8
      ' 	Direccion (en grados)
      g_Dir$=feed$
    ENDIF
  NEXT i

  'global de estado
  IF left$(estado$,1)="A"
    g_gpsstatus=1
  ELSE
    g_gpsstatus=0
  ENDIF

  'conversion de knots a km/h
  kph$=STR$(VAL(knots$)*1.852)
  IF instr(kph$,".")=0
    kph$=kph$+".0"
  ELSE
    kph$=LEFT$(kph$,instr(kph$,".")+1)
  ENDIF
  kph$=RIGHT$("000"+kph$,5)
  g_speed$=kph$
  ' global de tiempo
  g_tiempo$=tiempo$
RETURN

' ************************************************
' ************** /Procedimientos *****************
' ************************************************
