' Sonnen und Monddaten Berechnung. Siehe auch den Kommentar am Ende
' des Programms.

' Adaptiert fuer X11-Basic  (c)  2008 Markus Hoffmann

wochentage$()=["Sun","Mon","Tue","Wed","Thu","Fri","Sat","Glückstag"]
tage()=[0,31,29,31,30,31,30,31,31,30,31,30,31]
signs$()=["Widder","Stier","Zwillinge","Krebs","Löwe","Jungfrau","Waage","Skorpion","Schütze","Steinbock","Wassermann","Fische","ERROR"]
phases$()=["Neumond","Zunehmende Sichel","Erstes Viertel","zunnehmend","Vollmond","abnehmend","Letztes Viertel","Abnehmende Sichel","Neumond"]

' Eingangsdaten sind:
lat=RAD(53+30/60)
lon=RAD(9+50/60)
height=30/1000 ! in km

GPS ON
IF GPS_LAT<>-1
  lat=RAD(gps_lat)
  lon=RAD(gps_lon)
  height=gps_alt/1000
ENDIF

zone=1    ! Difference to Weltzeit in hours
deltat=65 ! deltaT - difference among 'earth center' versus 'observered' time (TDT-UT), in seconds
verbose=0
saves=0

SIZEW ,320,240
rot=GET_COLOR(65535,0,0)
gelb=GET_COLOR(65535,65535,0)
orange=GET_COLOR(65535,34000,0)
weiss=GET_COLOR(65535,65535,65535)
schwarz=GET_COLOR(0,0,0)
blau=GET_COLOR(0,0,65535)
gruen=GET_COLOR(0,65535,0)
grau=GET_COLOR(20000,20000,20000)
magenta=GET_COLOR(65535,0,65535)
lila=GET_COLOR(20000,0,20000)
helveticanormfont$="-*-helvetica-bold-r-normal-*-14-*-iso8859-*"
helveticasmallfont$="-*-helvetica-medium-r-normal-*-10-*-iso8859-*"
pid=815
COLOR schwarz
PBOX 0,0,320,240
@send_request
@init
@init_stars

' Draw the sky
COLOR grau
CIRCLE 260,180,50*ABS(90-80)/90
CIRCLE 260,180,50*ABS(90-45)/90
CIRCLE 260,180,50*ABS(90-10)/90
CIRCLE 260,180,50*ABS(90-0)/90
PLOT 260,180

DEFTEXT 0
COLOR blau
TEXT 0,240-6,"V.1.03 (c) Markus Hoffmann 2008"
COLOR grau
LINE 0,120,320,120
LINE 200,0,200,240
COLOR rot
DEFTEXT 1
TEXT 260-50-4,180,"W"
TEXT 260+50-4,180,"E"
TEXT 260-4,180-50,"N"
TEXT 260-4,180+50,"S"
TEXT 200+16,1*16,"SUN:"
PUT 200+16,2*16,sun_bmp$
TEXT 0+16,120+1*16,"MOON:"
PUT 200-40,120+32,plune15_bmp$

GET 201,121,119,119,sky$
DO
  year=VAL(RIGHT$(date$,4))
  month=VAL(MID$(date$,4,2))
  day=VAL(LEFT$(date$,2))
  hour=VAL(LEFT$(time$,2))
  minute=VAL(MID$(time$,4,2))
  second=VAL(RIGHT$(time$,2))

  @get_position

  @doit

  IF verbose
    CLS

    PRINT "Oestl. geografische Länge: ";deg(lon);" Grad."
    PRINT "Geografische Breite      : ";deg(lat);" Grad."
    PRINT "Hoehe ueber Meeresspiegel: ";height*1000;" m"
    PRINT "Tag.Monat.Jahr	    : ";day;".";month;".";year;"."
    PRINT "Stunde:Minute:Sekunde    : ";hour;":";minute;":";second;" "
    PRINT "Zeitdifferenz zu Weltzeit: ";zone;" h (1 h =Winterzeit, 2 h = Sommerzeit)"
    PRINT "deltaT		    : ";deltat;" sek"

    PRINT "Julianisches Datum	: ";round(JD,2);" Tage"
    PRINT "Greenwich Sternzeit GMST : ";round(GMST,4);" h ("+GMSTvalue$+")"
    PRINT "Lokale Sternzeit LMST	: ";round(LMST,4);" h ("+LMSTvalue$+")"
    PRINT "Entfernung der Sonne (Erdmittelpunkt): ";round(SunDistance);" km"
    PRINT "Entfernung der Sonne (vom Beobachter): ";round(SunDistanceObserver);" km"
    PRINT "Eklipt. Länge der Sonne  : ";round(SunLon,4);" Grad"
    PRINT "Rektaszension der Sonne  : ";round(SunRA,4);" h"
    PRINT "Deklination der Sonne	: ";round(SunDec,4);" Grad"
    PRINT "Durchmesser der Sonne	: ";round(SunDiameter,1);" '"
    PRINT "Sonnenaufgang		: ";round(SunRise,4);" h ("+@HHMM$(SunRise)+")"
    PRINT "Sonnenkulmination	: ";round(SunTransit,4);" h ("+@HHMM$(SunTransit)+")"
    PRINT "Sonnenuntergang  	: ";round(SunSet,4);" h ("+@HHMM$(SunSet)+")"
    PRINT "Tierkreiszeichen 	: ";SunSign$;" "

    PRINT "Entfernung des Mondes (Erdmittelpunkt): ";round(MoonDistance);" km"
    PRINT "Entfernung des Mondes (vom Beobachter): ";round(MoonDistanceObserver);" km"
    PRINT "Eklipt. Länge des Mondes : ";MoonLon;" Grad"
    PRINT "Eklipt. Breite des Mondes: ";MoonLat;" Grad"
    PRINT "Rektaszension des Mondes : ";MoonRA;" h"
    PRINT "Deklination des Mondes	: ";MoonDec;" Grad"
    PRINT "Durchmesser des Mondes	: ";round(MoonDiameter,1);" '"

    PRINT "Mondaufgang              : ";MoonRise;" h ("+@HHMM$(MoonRise)+")"
    PRINT "Mondkulmination          : ";MoonTransit;" h ("+@HHMM$(MoonTransit)+")"
    PRINT "Monduntergang            : ";MoonSet;" h ("+@HHMM$(MoonSet)+")"
    PRINT "Mondphase                : ";round(100*MoonPhaseNumber,4);" %"
    PRINT "Mondalter                : ";MoonAge;" Grad"
    PRINT "Mondphase                : ";MoonPhase$;" "
    PRINT "Mondzeichen              : ";MoonSign$;" "
  ENDIF
  DEFTEXT 1
  COLOR gelb
  SETFONT helveticanormfont$

  TEXT 0,16,STR$(hour,2,2,1)+":"+STR$(minute,2,2,1)+":"+STR$(second,2,2,1)+" +"+STR$(zone)+"h  "+STR$(day,2,2,1)+"."+STR$(month,2,2,1)+"."+STR$(year,4,4,1)
  COLOR gruen
  TEXT 0,32,LMSTvalue$+" "+STR$(round(JD,2))+" "+wochentage$((JD+1) MOD 7)+" "
  COLOR weiss
  TEXT 0,49,@breite$(deg(lat))+" "+@laenge$(deg(lon))
  COLOR magenta
  TEXT 0,64,"alt: "+STR$(height*1000)+" m "
  COLOR gelb
  TEXT 320-8*8,3*16,"^ "+@HHMM$(Sunrise)
  TEXT 320-8*8,4*16,"- "+@HHMM$(suntransit)
  TEXT 320-8*8,5*16,"v "+@HHMM$(SunSet)
  TEXT 200-6*8,120+16,STR$(round(100*MoonPhaseNumber,1))+" %"

  TEXT 200-18*8,120+3*16,"^ "+@HHMM$(moonrise)
  TEXT 200-18*8,120+4*16,"- "+@HHMM$(moontransit)
  TEXT 200-18*8,120+5*16,"v "+@HHMM$(moonSet)

  DEFTEXT 0
  SETFONT helveticasmallfont$
  COLOR magenta
  TEXT 200+48,2*16,STR$(INT(SunDistanceObserver))+" km"
  TEXT 80,120+1*16+4,STR$(INT(MoonDistanceObserver))+" km"

  IF abs(lmst-oldlmst)>1/120    ! every 2 minutes
    DEFTEXT 1
    SETFONT helveticanormfont$
    COLOR lila
    TEXT 200+16,6.5*16,SunSign$

    t$="a$=plune"+STR$(round(moonage/360*28)+1)+"_bmp$"
    &t$
    PUT 200-40,120+32,a$
    COLOR weiss
    SETFONT helveticasmall$
    DEFTEXT 0
    TEXT 200-len(moonphase$)*5,240-24,moonphase$
    TEXT 200-40,120+70,STR$(round(MoonDiameter,1))+" '"
    TEXT 200+16,74,STR$(round(SunDiameter,1))+" '"
    @update_skyview
    oldlmst=lmst
  ENDIF
  SHOWPAGE
  @send_request
  PAUSE 1
  IF saves
    GET 0,0,320,240,a$
    PAUSE 1
    BSAVE "sunmoon.bmp",VARPTR(a$),LEN(a$)
    QUIT
  ENDIF
LOOP

QUIT

PROCEDURE update_skyview
  LOCAL i,c
  sonnentau=(lmst-sunra)/24*2*pi
  mondtau=(lmst-moonra)/24*2*pi
  sonnenzenitdistanz=acos(cos(RAD(sundec))*cos(sonnentau)*cos(lat)+sin(RAD(sundec))*sin(lat))
  mondzenitdistanz=acos(cos(RAD(moondec))*cos(mondtau)*cos(lat)+sin(rad(moondec))*sin(lat))

  IF sonnenzenitdistanz<0 OR sonnenzenitdistanz>pi/2
    sonnenzenitdistanz=abs(sonnenzenitdistanz)
    IF sonnenzenitdistanz>pi/2
      sonnenzenitdistanz=pi-sonnenzenitdistanz
    ENDIF
    COLOR grau
  ELSE
    COLOR gelb
  ENDIF
  IF sonnenzenitdistanz=0
    sonnenazimut=0
    sinsonnenazimut=0
  ELSE
    sonnenazimut=asin(cos(RAD(sundec))*sin(sonnentau)/sin(sonnenzenitdistanz))
    sinsonnenazimut=(cos(RAD(sundec))*sin(sonnentau)/sin(sonnenzenitdistanz))
    cossonnenazimut=cos(sonnenazimut)
    cossonnenazimut=sgn(cos(sonnentau))*cossonnenazimut
  ENDIF
  IF mondzenitdistanz=0
    mondazimut=0
  ELSE
    mondazimut=asin(cos(RAD(moondec))*sin(mondtau)/sin(mondzenitdistanz))
  ENDIF
  PUT 201,121,sky$
  PCIRCLE 260-50*sonnenzenitdistanz/pi*2*sinsonnenazimut,180+50*sonnenzenitdistanz/pi*2*cossonnenazimut,4
  IF mondzenitdistanz<0 OR mondzenitdistanz>pi/2
    IF mondzenitdistanz>pi/2
      mondzenitdistanz=pi-mondzenitdistanz
    ENDIF
    COLOR lila
  ELSE
    COLOR weiss
  ENDIF
  PCIRCLE 260-50*mondzenitdistanz/pi*2*sin(mondazimut),180+50*mondzenitdistanz/pi*2*sgn(cos(mondtau))*cos(mondazimut),4
  FOR i=0 TO anzstars-1
    @dostar(i)
    IF starzenitdistanz<0 OR starzenitdistanz>pi/2
      ' starzenitdistanz=abs(starzenitdistanz)
      ' if starzenitdistanz>pi/2
      ' starzenitdistanz=pi-starzenitdistanz
      ' endif
      ' color schwarz
    ELSE
      c=(4.5-starmagnitude)*65535/6
      COLOR get_color(c,c,c)
      PLOT 260-50*starzenitdistanz/pi*2*sin(starazimut),180+50*starzenitdistanz/pi*2*sgn(cos(startau))*cos(starazimut)
    ENDIF
  NEXT i
RETURN

PROCEDURE dostar(n)
  startau=(lmst-starra(n))/24*2*pi
  starzenitdistanz=acos(cos(RAD(starde(n)))*cos(startau)*cos(lat)+sin(rad(starde(n)))*sin(lat))
  IF sonnenzenitdistanz=0
    starazimut=0
    sinstarazimut=0
  ELSE
    starazimut=asin(cos(RAD(starde(n)))*sin(startau)/sin(starzenitdistanz))
    ainstarazimut=(cos(RAD(starde(n)))*sin(startau)/sin(starzenitdistanz))
    cosstarazimut=cos(starazimut)
    cosstarazimut=sgn(cos(startau))*cosstarazimut
  ENDIF
  starmagnitude=starmag(n)
RETURN

PROCEDURE init_stars
  LOCAL i,t$,a
  DIM starmag(100)
  DIM starra(100)
  DIM starde(100)
  CLR anzstars
  READ i
  WHILE i>=0
    READ t$
    READ a
    starmag(anzstars)=a
    READ a
    starra(anzstars)=a
    READ a
    starde(anzstars)=a
    READ t$
    INC anzstars
    READ i
  WEND
RETURN

PROCEDURE send_request
  LOCAL f1$,f2$
  IF exist("/var/run/ttpnpd.pid")
    ON error gosub derr
    f1$="/var/run/SDK.TomTomNavigationServer."+STR$(pid)+"."+STR$(request)+".message"
    OPEN "O",#1,f1$
    IF err=0
      PRINT #1,"GetCurrentPosition|";CHR$(0);
      CLOSE #1
      f2$="/var/run/SDK.TomTomNavigationServer."+STR$(pid)+"."+STR$(request)+".finished"
      OPEN "O",#1,f2$
      PRINT #1,"finish"
      CLOSE #1
      INC request
    ENDIF
  ENDIF
RETURN

PROCEDURE derr
  '
RETURN

PROCEDURE get_position
  LOCAL t$,a$,g$
  IF exist("/var/run/ttpnpd.pid")
    t$=system$("ls /var/run/TomTomNavigationServer.SDK.*.finished 2>/dev/null")
    WHILE len(t$)
      SPLIT t$,CHR$(10),0,a$,t$
      IF exist(a$)
        SYSTEM "rm -f "+a$
        a$=REPLACE$(a$,".finished",".message")
        IF exist(a$)
          OPEN "I",#1,a$
          g$=input$(#1,lof(#1))
          CLOSE #1
          SYSTEM "rm -f "+a$
          COLOR orange
          DEFTEXT 0
          TEXT 0,120-4,g$
          SPLIT g$,"|",0,a$,g$
          IF val(a$)=0
            SPLIT g$,"|",0,a$,g$
            valid=VAL(a$)
            IF valid
              COLOR gruen
            ELSE
              COLOR rot
            ENDIF
            PCIRCLE 8,120-32,7
            SPLIT g$,"|",0,a$,g$
            a=VAL(a$)
            SPLIT g$,"|",0,a$,g$
            b=VAL(a$)
            SPLIT g$,"|",0,a$,g$
            speed=VAL(a$)
            SPLIT g$,"|",0,a$,g$
            direction=VAL(a$)
            DEFTEXT 1
            COLOR orange
            TEXT 24,120-16,"Speed: "+STR$(speed)+" km/h "
            TEXT 24,120-32,"Dir:   "+STR$(direction)+" "

            lon=RAD(a/100000)
            lat=RAD(b/100000)
          ENDIF
        ENDIF
      ENDIF
    WEND
  ENDIF
RETURN

PROCEDURE init
  ' output of inline.bas for X11-Basic 21.01.2008
  ' plune10.bmp 542 Bytes.
  plune10$=""
  plune10$=plune10$+":H8DDZ:.`0&MC`>\=3Kc3U]&b^8X/?[,VE;b3:D9KKR+8O4+`5bK=CWL3)J6:7DG"
  plune10$=plune10$+"6c/R5@H$2VKaK^]9$9$:X'<@?DNYaCTR*'X?bT)]V%7?P,c(Q(A5H2(^CA4-=b7P"
  plune10$=plune10$+"Z8_cUKM',Q)36'Y:`09&4FK7RDSR4=4B$6_`+UQM>YbI'I_$M^CRA;FGcQUGBcBE"
  plune10$=plune10$+"UT*RN@63SY-.ODS:N)%ABPB/V+*c_.CKF9QL:=J4=Na1.5SN,X/c+ZW$'Ba7:aV6"
  plune10$=plune10$+"3C+Z'HVC+%Z/:C%)-a2/>N;G^1`9):-MIKP<>;Z<$%7b9D)$J3R55><UU[K+]9YX"
  plune10$=plune10$+"UbICL4U`W*7Rc(P[Z.;H%T)b.TGVO,(M;43:MK\X6CA4Hb>_`VT_/7@4W*EP(-^G"
  plune10$=plune10$+"cLR6<LZI$;8[VTYZ/80Lc52H@;T(O-,MYWXL*?-@ZQb_[HO5D=RQB@LA2H?:8^OU"
  plune10$=plune10$+"&$H2DC5Y3\CE<`UFH4OOC=C@IN5H6T_NKO(C&\I2c?^_XXXN2SD[+2+16W(b-F>/"
  plune10$=plune10$+"M`:X7)\`=aIX&5'02)`<`B2H96O5MAQba'+8cC_H2G1Cbb>G]/S@'S)WS35W2M.6"
  plune10$=plune10$+"T5N@@TFT3'B=I2cc>E>-b^5F)@^0WJ\Ub6R.:YY2CT)NY_?CW)CcW<R=@+6NF3Q?"
  plune10$=plune10$+"`2VO:WFRIV9CY8MH7G?V4`A15WKQD\3XN;9IAF:Y/<WQCTPaEEE@bZ])123;:;C,"
  plune10$=plune10$+"+:2OD)3c+OJ\5M>8@(,$"
  plune10_bmp$=UNCOMPRESS$(INLINE$(plune10$))
  ' output of inline.bas for X11-Basic 21.01.2008
  ' plune11.bmp 604 Bytes.
  plune11$=""
  plune11$=plune11$+".%@D;+^.@/2aN[:.)W&XWb=b\8]c*5P$c@(aGBXCYFQC-ZB4YTD>a=cP<GPZHR4?"
  plune11$=plune11$+"[?<cJ?6J3Q\&JAAA4]A1/V/,%&aSRR$]VbKG=WD6%V:HBSV$$YJV;2UB6bM?H>IY"
  plune11$=plune11$+"=,7Y%FU0WROLC-N7F52Z-GXB;W*&[3/:ZOLAAHcVIEF+\H2[0ZLI\6^T=^P]7a9Z"
  plune11$=plune11$+"&<6W<E7$`O?]^_3'WbN->B<5K/,X:$463U:&1MU-ATPH02(cDGOWV`]_@A30QZE6"
  plune11$=plune11$+"a0aN:==@K,7=K=-SKPHVNL,&4(J^92XY@(7@.W)I263%7)?7Z@($8EZC3.WY)>N\"
  plune11$=plune11$+"X&7$'%J^&N]&\A^F;<MFB*S')bbLM:G(B.-b9`0T>O+6KL0&3YC.H0aL$HA$T\P%"
  plune11$=plune11$+"X6]*=];-SbY`-:<OB'Y*C,C^K=:cZ5:9IUK?2bIc$6P?4\,0K=b-E+QO)&'U:.\U"
  plune11$=plune11$+"9RTX%R9VH?0`TJHGS^X)J81ZCD&69L>1I4S[O0U0I)B[HaAR+50Y_LE&$+I?ZSEZ"
  plune11$=plune11$+">7_<:$=QCP:K\V9:5=]A?%+?3L'XE7a;PG_bJP<&FQC:F@'BZT?6QE3A3\,6X^$("
  plune11$=plune11$+"^FYM*W\5QV@4S6`bN_UKX>=YbNLFN*O,(>/G;Q\c0c7DP8cRFX3BTacKS=CV$+VA"
  plune11$=plune11$+"1aHL8<II<,4B6LRI3\\U9*952JQ_3b[.CO`E[S72;R1-=)-@V5TXGbCON_2S1-GV"
  plune11$=plune11$+"SHM=V9=PIaN+WIECWa0J-.JWRaWL].&C]INTJ6U4^='+X*aJ]%T8b;85S/J,EcPc"
  plune11$=plune11$+"7M-.P3=D(ET1[[O59'0;<?LCR*6',UE<b5_4(T$$"
  plune11_bmp$=UNCOMPRESS$(INLINE$(plune11$))
  ' output of inline.bas for X11-Basic 21.01.2008
  ' plune12.bmp 628 Bytes.
  plune12$=""
  plune12$=plune12$+"X,`DI+F.@/2a7;8SXAK^8L;cca0(cG1TNO-G]@Fb?^C5_7H)>+'+PS_SYJ8>B<Ba"
  plune12$=plune12$+"7W'0N<-([VH]$A^CMN6X\=Q`'MGB>^8FHa'ZGT.K28U\-_`])c,M&36aPBa-%J'I"
  plune12$=plune12$+"N[=>)B[6X51Q1`(T?:>QY.MCRA07,L,JA3MWE_B+?2b%Y[X6/V]a1I[L+XD-(BK0"
  plune12$=plune12$+"..@^`&%B6Z2('EX@Y[YH<b<R6\FXX0OZ)WKJ_^<`N^b_X)ZWO?QbYKP0`W:R[W\I"
  plune12$=plune12$+"C169,([Daa=RD4@-FW0b'.N'GD\H&[T^?+X/PX9C'-0)(7Uc2ZC19UIH9J1>=_c+"
  plune12$=plune12$+"%J`)U^TAV==2W&ELHYHbS1.5FO=J`cJbbbLV&NbO?TX\K49_NC@NT\/-:XF5,(OD"
  plune12$=plune12$+"*`BQM(W\bH>>-]?^SIHY([7O2//E$b^;S5SW/Q9)bF98b5'bGA+OI-ObaNK?,7'Q"
  plune12$=plune12$+"c[12\;(cH3Q?F7UKF=1\c^'O+D0F>;VP$a`72/`\9Y5V:*NK5(ECB[B*)[^LT_K3"
  plune12$=plune12$+"5Q`K4UN2VJ,`:*V&;c6*4'*`[3(U;[*:GR,1K,J&X_?:7C&=(A)-MHbX>P+J19DD"
  plune12$=plune12$+"Y:LZ\DaZ7\;Q-C5]:=XTM3@G\30MZLOY6]<-4]^TC^K8$+,\%M@D1/(8.:c,`UA."
  plune12$=plune12$+";?YJ^YU/2O[4=`9G5QT':V*I[T;c6G4c:U:%.I&/ONE=ZSa<5[13&V464A<(;(_0"
  plune12$=plune12$+"0U2)X?b+P`1/0,,W^$a@3L0_80_+^G[aR%0)Q-@F7695FBP+5=H>[IabB<;G?J.*"
  plune12$=plune12$+"*8<H`Z'LNC[-\BP.ZUb6]G0QWPU]>:O9]C0bV^^8+`$C3B]N]_Z\3KPHWJ^(8=QT"
  plune12$=plune12$+"U;R8%$$$"
  plune12_bmp$=UNCOMPRESS$(INLINE$(plune12$))
  ' output of inline.bas for X11-Basic 21.01.2008
  ' plune13.bmp 657 Bytes.
  plune13$=""
  plune13$=plune13$+"_V%D@/_+D+['YVRM@3_aNW-6aCV9$>_aN,)bN0%C&6$O[c$1%G=S9YYDR0(;\F@1"
  plune13$=plune13$+"Z]9Q6Y08?K&QZA>,XTLAc;I<T1W=`E>2KWX8_X/K/[]M8.FOK''Pc$X-4P(8c1R_"
  plune13$=plune13$+"MT[;6@Xc3+9V+9H90K_^JP60BR&OBQY..F(YA:P)6'*&O.S,VU+B4')5:G4`5UOa"
  plune13$=plune13$+":3'?DH-:Q.3bNH2QFC;0FD1=WLG:I(Lb`DR+DD*N6A7]'[-0DGa*c:78]%^9[X%0"
  plune13$=plune13$+"%>A7X$>*;-LZLC?C_65D4J'Y-,0?,XMJC8&E0_a;3P6.E8[P(02J?%M^4@AZ,]\]"
  plune13$=plune13$+"C1K[AQ^YW&a.^YGb1GKA_*EALY\H57>-_HF;3/(@>NX1HE'X+U+[E-_TT;NL/?<0"
  plune13$=plune13$+")\\E<<?$0%X?]:,O&D%8LE/WU_>)7LK8Pca^.SD]'B9*b:Y/Y9`P\LP.0B/]K_bb"
  plune13$=plune13$+"Y-\1,1EQ[1&C?D*C\3Dc1(,KAP*)`%`c3H)L8-Ec`))])-\:JJ9K'58<5WRHbG`Q"
  plune13$=plune13$+"a)\ZT6Pa&5$<a4(PF)K2'K'bQZC)P\MJ;6Kc*>O8Q8VO-M+'^6-@Y^^,SUR(MREB"
  plune13$=plune13$+".35.:%V@3]:ZO+aZFVG;c=<K7Ra^[`b_K+A%S8;5)X+JFY\(2CK_Dba<*)^O;J*;"
  plune13$=plune13$+"OXTTA-1/0**-+`9<%[Y%Z]@F971Q/FP..UaP7cC,I<P9<F3GL)J*80)OAQD2a*HR"
  plune13$=plune13$+"JFG$W/,6VUFG5`PG-H'VY1NL2b`?JSU>4[L%@ZM\3NEEU`WPEL[a>HY30:BPH6L4"
  plune13$=plune13$+"*YQ2]B8Ca59EIJbLRA%P.%A7I<C1<BHV$;6.1>Z^I=PNCQGS6G;ZT]b+ME%:_8W@"
  plune13$=plune13$+"3S*<Y_LK7)P7)N[1&OObBP5Q*K/C^S*;JHF(Y6*&`?$$"
  plune13_bmp$=UNCOMPRESS$(INLINE$(plune13$))
  ' output of inline.bas for X11-Basic 21.01.2008
  ' plune14.bmp 671 Bytes.
  plune14$=""
  plune14$=plune14$+"<*`Db*^.@/2aG@`QZ5[b2XY5[T;$*4Lc6R_27^9E4^D_Z=IID&7`FWcX7D=)]>CZ"
  plune14$=plune14$+"=3ZV[TW[0F-T7L*IRa]]:Z,*HPA`+S.^$cK_:>HcJ[aFG,-bG^K]F0Fb:%?Z76Y,"
  plune14$=plune14$+",]GcUZ0F?'&8?7OX,:K(%IJ:_*Vb6D=5)A,?1M&B77A^2E>2MPYGAO:<L[CO*L?D"
  plune14$=plune14$+">)(5aNC?\8bH[F9*N^H_?]FLS?`M%LTJ)BHZZ\@RXSZQNU(,OZ;)$I=)$_*[J=BU"
  plune14$=plune14$+"0[0?*%J(N4\:6ZTHE?\?=^`<^I6[b]@*AM'A[)BWHEP:b1)QQU1F0[`QVX.4XDFD"
  plune14$=plune14$+"cR?KKUR794DL\Tcb7P5GSA*JHA.?T;H&&6%32/[L6KW7_A=,Q^Yb25_MK2P)@U(,"
  plune14$=plune14$+"A3:`<MSEY_AT678MBCYM]='M2EGZ)V5ICXBT3ECFO/a^,L$\NM7%AU8:1+<U6XW]"
  plune14$=plune14$+"&EA=^IW5@'H<I23]%Y7N_T@88:HLZ:?]?WV\H/&Y>PB&L7aAZJ6%B28T2JD[*];^"
  plune14$=plune14$+"/]8KT(B&?&NQ-%.8ET;N;<=H\IU`V;LC9Z[aIGFP2^G:^Y>YH=S@5.V@Tc97-3O5"
  plune14$=plune14$+"A;.*<:)>C)E\1c),?SK52b?\Q]cOL[?b7-a-*8`>'<:[Bb8];=2JY&CO(2aaHPYP"
  plune14$=plune14$+"=[\Q0U<F^A40-_K:?P$_O]&2SG?%0?c4+VcE[J2b%SVT;%=$ENOKB&^_:=%Z?`EG"
  plune14$=plune14$+"&+60?T0<8B1T23aC>R0GPO3G9Nc8MO_1OM[abB]bKQE:?_)TNW+R__5)\/&--8FQ"
  plune14$=plune14$+"SC6)%.'0TS&=HGCULCE0KA.[FU;3:+.X&62$1L56Y@-QUP7%TW`^74NK-U9OZ]RJ"
  plune14$=plune14$+"_9KaN`]GSEQZJ=A;HG][]LCI>3@EZY?7JUK=8^TWD,26^F@6]5_1_1.%S9B3\&\$"
  plune14_bmp$=UNCOMPRESS$(INLINE$(plune14$))
  ' output of inline.bas for X11-Basic 21.01.2008
  ' plune15.bmp 677 Bytes.
  plune15$=""
  plune15$=plune15$+"OZ%D0$8L70ESF&Z`\R1'V1bEG'SaXKFMCZG(]XGaA=-9D.EXVGJ5<`/P%V>_BC$'"
  plune15$=plune15$+"N>'//'XE09XU%+Y+T5N.?2A+BQc(($4J]+D1A.8$bL@F>.'N%M+).P;ZU[P9B]52"
  plune15$=plune15$+"[Zc0^F25M%4+:AZ(RC((F2O*TZNU3]a=3[+a(VR$V'`BB`IKV5,;65-46;4T$%F,"
  plune15$=plune15$+"+\U5D))^HUU[3TJ@,7@S-b,E]cWDJ_*/RR(.O5[c'&4)T+7HF?];&cE/;c5`PH^%"
  plune15$=plune15$+"1[[6XQa];)]0Q@I73Ja4Z6CS2P8DRc/TS&,:VFR&@$b\aGOLS_)M+*;H`U2SIW99"
  plune15$=plune15$+"5KLCFO@`.G;,E[9>FC`M4`'(*.DIWX@G$*VV%M>Wb?TbH$2Q-OSa[=R4$KAYb;7$"
  plune15$=plune15$+"aF3$N>PP)S[3M%Q:+;LX_;V'[/EPC)+,VK+BZW-3H>^9TBS9U,OY+::;B<L/N+GF"
  plune15$=plune15$+"&B2Q(<baE'A9[]1<C++@V7OITb_SWG`/:[15EIQ9(W4UX`71$?OVR2(C1P((H.?("
  plune15$=plune15$+"1)BOV1:.0H(8/$SZK^95Y:,XFX*IN:W/LY-08Rc,XY5c[LNO<$:/HCT-c5O@'F7@"
  plune15$=plune15$+"+@':bG,'\:WST5Y2/F@baW._aBI2a.bA%8[Z??+bP[&]D/(-cY&]3`^$cZ;4=/YS"
  plune15$=plune15$+"TC'=M7OE3S2\:UBHTSQ?NXTZKK_&CC?HK7;9QY9\2JJCC)7<Ib_N;Q@c^c$B)cA2"
  plune15$=plune15$+"N.@SNAZ<Z(=7N$0B%^>-1/X@[V5.c/9VC-9FS(NK>S>^VUG8c`a?2,NbPC?_?<^Y"
  plune15$=plune15$+"U**X:??9WO0QFcNN5(*7`T\3L]L*MA%%\H/2=,D6\XJ@GZ?.`SZO.&:PTA+@YX/@"
  plune15$=plune15$+"7OUP?32F<B*,)@A*.MPLH^Ka_'Q:<DR:J=S+U%JY4A?,/MO;UR<MA$RBED;a)b6?"
  plune15$=plune15$+"SKWE3%$$"
  plune15_bmp$=UNCOMPRESS$(INLINE$(plune15$))
  ' output of inline.bas for X11-Basic 21.01.2008
  ' plune16.bmp 654 Bytes.
  plune16$=""
  plune16$=plune16$+"?*0D3J4`<7SZ==\O(W1T0cBGM9[T5`VP;6M/?c&BI.7`C4cUJ*WI/Ga&5bX8@`9W"
  plune16$=plune16$+"IG5ERc:;O'c-O5CY]M^%4_6;9C.;OSHcY[cMCA,O`[VU$$K25.X*W>PcK155V?'5"
  plune16$=plune16$+"F.PHM[]+=G4`3NC-8-`\1V)8%[UX/ILS75G9MKa](:cZ$AY2[b&7IFC0]4BX\W.>"
  plune16$=plune16$+"_,D-3]O%*WXc,A8ID--P\IH6=C@<9_7I1`;K'8.9`L@6M7)U6_+'HKM\RH?V>)3\"
  plune16$=plune16$+"W*\bZ0=)_b]Z:^-0<_DB/(JA)JJaFOQ9Y:Y\B+=;V3ML_LS1R8,2QK):7^D86*`W"
  plune16$=plune16$+"G.)3-Ra3'P8HWKB8A?IM[PP<,-\(A4\0W\E6C*=+$6*L.;FF]/@,aA&5;O0@E^42"
  plune16$=plune16$+"\B7^J=H\U^N%._XJ:/[Zba>_R6$A_DYB7BRS.;K-;@'Q62HLCK'^KL\@:T3$@-X:"
  plune16$=plune16$+"E(.HD<ETE[K'IS`?4@b@G_L(O03&I=XPZ]?-A*MQ6$0N5@BbRVUO;%LO.\1O4J<3"
  plune16$=plune16$+"`8?.B-?`%8Z338..3$=1TA,>_0S[?aaH1_?5O`H+:52Ma*L4]3[AX`0(,/bNJ3Pc"
  plune16$=plune16$+"E*S3M=P-M6'DT,I6NU%]M^6)bGP1(79V@%;7(U;]60_SE((D?M+&I^7([c0NFL3X"
  plune16$=plune16$+"A0%MB^I\:+(9\M5+3O;T@$E01O5;ZS7%Q:P4XF/[1:9CB[&N%?;),X`c1b[J3'ZP"
  plune16$=plune16$+"6c6AUb[\7b.X:c2b`/D+'^/?\cR2S6XL.P;I@C<LWYN(2?IOPK$FR4O(SQH4)?/E"
  plune16$=plune16$+"31Q?/JR;Q)G'/P4TY2;5[[E81W^U\6FZ9,:ILI+68BMR\JBZ>b)MO-G7b7)'b/]:"
  plune16$=plune16$+"_F.[E@S.=5D$,];8W<UB4SD[9)'5H4[E=5+OH,0$"
  plune16_bmp$=UNCOMPRESS$(INLINE$(plune16$))
  ' output of inline.bas for X11-Basic 21.01.2008
  ' plune17.bmp 668 Bytes.
  plune17$=""
  plune17$=plune17$+"X,`DI+F.@/2a>]P0FY,(cW\KK[a-Q-L+*4c^7@HTF[`HY2IHP?G:L*Q[9RO4Q_,J"
  plune17$=plune17$+"G9/MKE9X[Z(1WDQ$[KU9%UFC>&81K6,S?.c\=9'b0cG'..F_V^BCSK35?1T<5()5"
  plune17$=plune17$+"KW3L7<7*+.&W<P%F48O$_H$9'&]%:6G>EM`KGGSM$*D76W-9a<II(U8_2E;?F'-5"
  plune17$=plune17$+";Lc2\>Q@9VFKQ3UZS49(?ab.&6;*-S&1D,+C7H6*Z'38`b-X3E]&]4%5<+[J=8,0"
  plune17$=plune17$+"<Ob)R*D`_&R[%*=ZYYc*AII-=*)KF(C>DBX3,&7Q-L(-?Y=:%a>>KZHNbBW-J)(H"
  plune17$=plune17$+"WTHW4H6CRJSQ1VSLcT=8?B2F$Ya'.*(D>&\X/IYJ'M@IT-C3^@\7T[9EX^8^?O6a"
  plune17$=plune17$+"P,Q$/P$`P'&5V*H2X21BW[aJB%Z,`1W'_OO</%I9MJ;FQ'1PUa;?UR6X1cH'b$+U"
  plune17$=plune17$+"VK/_F2'0G%V$2543[VOB=96%\'(ZM'`%*b+R,7HEOQB73@31_U@%b(?DT]c/cB`b"
  plune17$=plune17$+"RN@Y,cM3FB^O$/KFIR`VTI5[]SN@E?5<>S7>'GG//b=Q-a6*RR&Z\(1CcNJ^HGGL"
  plune17$=plune17$+"_(VZCWSK<4Oc=$P8VDF152NR@&<>X,?%9@H^c3%+A4&;HK$cbB0,Fc`+>>KaSKF@"
  plune17$=plune17$+"c%31DL&,?J7U_SK*YTZ*T7bR1=-[;`(JSH\OL=XI`I/5YP;Uc<B;SbbKAV1*.]c2"
  plune17$=plune17$+";]'U).,5?_6\/:WAYQW\cc[(PY=<VY.`5(VN/^*A'+<a%J^+])X:@TL/Q]IFM_5V"
  plune17$=plune17$+"*@2]F8PZ(`C%=(DaN>HAb%G%Y3<Y\]R.%H@&;F*UTMI;+@[.:'.=H]aH&D]PXQWO"
  plune17$=plune17$+"5F&2P:@)4P'.QcY?.-a*La(?IA4,)YC;?(a8aLb>53@[N[=<?7(_X?AUY4D$"
  plune17_bmp$=UNCOMPRESS$(INLINE$(plune17$))
  ' output of inline.bas for X11-Basic 21.01.2008
  ' plune18.bmp 643 Bytes.
  plune18$=""
  plune18$=plune18$+"&'@D7*2.@/0Q*D$K7S7;O6+%K`OD[-..CBAFJD8:=>B\=7abA$aCG0,`bT1'Y.+S"
  plune18$=plune18$+"<K;Hc1Hc.K(''RQQ$P4a0K+7.-;RRHW<H/=`MYO(RT%6YV_R,U%X=0Q?_J3Va&%U"
  plune18$=plune18$+"?-WBZYGA4?^+]-6HM`?G'7&TV.5/(11X:]M@@]<S\0EUK^YA4MJ@EZ<?c::ESV;("
  plune18$=plune18$+">Q;S:7O1?-IZ>2E<+S=E[LV_7*YE+K]J.N[M)F_*Z^90^$cIR/ECP<?G50]*:GS%"
  plune18$=plune18$+"+(7DI<3_,$&FRJQ'X,%CZM:D2Q/Z]@44c4(U6=EH4a/72`9O,Q.X2J_1D14\aN\."
  plune18$=plune18$+"=9IaA[.*S]c-\D$;\H2[EZ+PMUJJFQZ*]];L<8IFZOXB;2NW9I4B$Q^56MSY?5@8"
  plune18$=plune18$+";;]%*`1b_GPX96S?D?1aB%[*X%c9(3=UI*02ScL;*UZ([112^O7a;K?N1BVD:[N`"
  plune18$=plune18$+"=P4>K?86L4@SIG3QF*GECQFMDTELP/D%'@D/Wc.1I/35:a(F/-@/,-LK(8^V,)+9"
  plune18$=plune18$+"07FIa[RTcT(;BX=+@P(S_4%'<D\[VMIM;*_[\QG^,a]4JaCbc.a_%0_-2H[.\72&"
  plune18$=plune18$+"&]N+%V[1MJcO%c%3QB?V7W?<aERGDID8IHI8^`5492JW[XKQ?'c]P4=KY<YQP?-C"
  plune18$=plune18$+"&^]'2DP1\bMXFP8T%?,V0&0.aZM%'1,J0Sa&*DQP-N]-V*L$ZMWC<\-N(@Z2,LD6"
  plune18$=plune18$+".WX8^H@:_WOS(;V2Z6N'29>+@1)E`,]aQV\<ZYA_N9R2cV26<7aTWK^7[`S+WSO3"
  plune18$=plune18$+"8?)6.HJ8K%\X=HPJU:_L@F\$W).`JRacY4X8I1K(4U*-F`]-P;QW:`=C?,.Fa\Y@"
  plune18$=plune18$+"\CJ)(L(b/:4DH.C6cGR?O1&,$$$$"
  plune18_bmp$=UNCOMPRESS$(INLINE$(plune18$))
  ' output of inline.bas for X11-Basic 21.01.2008
  ' plune19.bmp 621 Bytes.
  plune19$=""
  plune19$=plune19$+"+&0D$8$`<A04..'0AZUY.3.aH%FC_`MU[bcI(0Ka,a03]@.ZOU=7N$5OO^87(J&2"
  plune19$=plune19$+"W>c-3cLaN4&G%*b/P%'^M[;<YZ6%bK]-5IJ*L+]TDT0VVYFXE6,T+HRA=QbEXEFV"
  plune19$=plune19$+"7U+N%N,A0;7RUN$Kc`09:A08Q8=E/)OI7FZ=R?cXD\59=YPG+-JI=0:&&\FcCS_["
  plune19$=plune19$+"/N=Ab]NQR>BQ4M4PEGC2_^4E^%JL:REBLO(&W4XR?D>>@^V&@Y<AF:@BFJ^6$c)]"
  plune19$=plune19$+"EcO?bSD=WF1DJF[*9P'XG15'Q%;3'&2U>+X%9XG$IaO4(1FSA@J/2T_:\EQ`0b[V"
  plune19$=plune19$+"1ST\<__5JV^L>\V1&,K>Z^0:&CXQ2A;>M*D>`-c0b8+S3V@^RG&F+&0&aW8Kc.X2"
  plune19$=plune19$+"4TF9B=<XHb&T]_`ADW%CIa^.8b(_UEY1ZOT>9J77Ec?H]:GaY*HD.?QD-Q:ECUDb"
  plune19$=plune19$+"NPIG,a5_[5Q+1`R)6Y2EQ16Q>F@^50[bDB]:=:3.EOJGT,Ca'b0^*7-@*_1AU_5,"
  plune19$=plune19$+"ZFWbU7](HaW5;\MSc?K6BTA:B'KS640$2'O[b>3^;K\bc6<;PB<H$,YR=c`;+BS%"
  plune19$=plune19$+"Z.`ZY+.cY_:9*A5c(X9(F0NAL/b'``D-YV%55&X>UUV*]-CS\3P&R81UUJH&E;@E"
  plune19$=plune19$+";'G*'TO0IO^DQ<)'US7`K+Nb;_5(GBO-6]<C2'c`3a^\D?C4M3=?aKZ\Y>@TLOZc"
  plune19$=plune19$+"0T0+9aO*+Z0L4L*TbI3>_5Ia-ME2FM@O7X[2UTVYN]>7X\BQSX@)VU+D@>7I2XS)"
  plune19$=plune19$+"F2-bHKKK?cY0Hb4G@HESbCUa;-M,I)W?5QA0\9Y*2P@-(ScaME?Z'WLC5TH%"
  plune19_bmp$=UNCOMPRESS$(INLINE$(plune19$))
  ' output of inline.bas for X11-Basic 21.01.2008
  ' plune1.bmp 45 Bytes.
  plune1$=""
  plune1$=plune1$+"*V7?B.6)?KJJYI,)1E52IY`D$8/bR7$9WR]+`M8F3<FTY-']'.QE0__A6L($"
  plune1_bmp$=UNCOMPRESS$(INLINE$(plune1$))
  ' output of inline.bas for X11-Basic 21.01.2008
  ' plune20.bmp 599 Bytes.
  plune20$=""
  plune20$=plune20$+"J0@D,J6.@/0Qa4P?5I@-;*bc(I9`B*BX_;SR>EIc:P'N^3cX@W9<&6A_][\@+$NG"
  plune20$=plune20$+"JV3=/+8,=6A$<9QKF.[D.\8%D%%'7(^S/_Y[244C&MH.%GW&?X^1_-=PG+\UCA9A"
  plune20$=plune20$+"^(Rc66ZP/?DL'SJOBTSSF,;M`T(HYC`2\_M8$K9/4;-8V9\UH^S(+=4](41P9]O*"
  plune20$=plune20$+"HKJ`K/N*Z[H$GZWb,9'&E\U3A<X&G,='>UU`,ST=:H.0*Y*P-4\WG-LHZaa:ac(,"
  plune20$=plune20$+"^5??MWY181=Y$`8Z%@/51MaDKH&??9:^&DLS_&X<S3-^B5aHK1^-592-?2$AF2Wa"
  plune20$=plune20$+"X=LK>8b;(I,^MHZUM14D27)%*']9TaGc<V3P>ZH0\%M@636NB+/H1R5Q@3SO5',S"
  plune20$=plune20$+"LN9Z>>5]=VK?R[CF`@@?>F4/,,Bb9S^`;D^(b?KG&6)GYYBRC8EX*S6[^5Q,X1AC"
  plune20$=plune20$+"(OL@BTCO@*+c1*X$CAT1C)Q?*Tc]2R&&:cI4>EBY)SI%.]'W`_CCENT5aLT3?a/F"
  plune20$=plune20$+"CN_4V>93O2*T1'.O<:DV9-+&-c;.M:Ba1SQ>=Y81'470Z](5(O.C4H'D8;`\D`T1"
  plune20$=plune20$+"AN>cNH_0%:`>&/:L;X/FW+AQ+3b2Ya_MQ14/cS:/);H6UR5,'L,G_/Y[C)/9K5)/"
  plune20$=plune20$+"/?/NIV63E5Y/7,7?)G^+HS_%BU/FOc<9(F*[*9LT@YSSMLIbO-,.\TCFX:TB&:aH"
  plune20$=plune20$+"EE''PC?;0][QL=2OH.10?SN>@W:BB%8/MGN+^1)DH2I5`X+XE;Xc-52.GNR,-&<O"
  plune20$=plune20$+"5\[33+T(MT^5c`://aK:=NGJbUDVFT0$"
  plune20_bmp$=UNCOMPRESS$(INLINE$(plune20$))
  ' output of inline.bas for X11-Basic 21.01.2008
  ' plune21.bmp 554 Bytes.
  plune21$=""
  plune21$=plune21$+"X,`DI+F.@/2aN]UK1;4LOY*\T^9O\P7)AcWWION'G-[c^NJBIAD&'bCcBE5NME(/"
  plune21$=plune21$+"HI`C*HA24+M46'T<M2OO?N$:*2XQKX1GR6&>cZ(JF1M'[?4@^<?NP@%VR,-/<XF@"
  plune21$=plune21$+"$_<W;Y%Q7][KB$:[%T`%Y[)B4PG_O;NXQV.>H5ZZ[=E[TPbOMTD_T*'R.4YD9NN3"
  plune21$=plune21$+"&*3FIB^aV-4ICJ]<@S)J_`'R%[5FMOa[DYN'U@B>D27A%H0$IXFX5\U$Q.F5&N6L"
  plune21$=plune21$+"SI>aYQP0B$N[<^HS[c%^I@P[3bRR]26A8YNTS_Q)C;4BKKD433c*cY1J5=%3*G.c"
  plune21$=plune21$+"*1FMYG-W^6X9T4:-6+)HK_BY9K5WBWc9bN[1PIC9%J2PH*-PYVGE;O^(_(@6]I9Q"
  plune21$=plune21$+"3*.FSW*OVU<VQ1&F)4SL:]QNXXXM2.;a(&b`-)9I$W-Tc*]/:-J.I(XZ(*P*BHX)"
  plune21$=plune21$+"*$I=P6E_L%<2DAC]4XDO+>US:ZN[S3I`\07`AV6\\^TEbV*H9>[)`KM[J<&T4'TH"
  plune21$=plune21$+"I;^/%ZD601MJK]9N[]@5K>:3$B$(P)QcUF.S8]T7^Z<B))`)0(U8KF?.NaGRK^[9"
  plune21$=plune21$+"K^07^aB4W(J7DO*bS9+GM8J60UD([\+]>\>[P-C7VZ1,+/I^/^4BR25MW1R([>@G"
  plune21$=plune21$+"KEMa1.;M[EDBYU4KWLB</Rb`ac>2%4XAOWZO`6.@ND'PMX2Y%*C=/1>JQZR8I;ZK"
  plune21$=plune21$+"B6b2/8(%bB/,b[U>3O7C/)[FQBA0_A5CH4$$"
  plune21_bmp$=UNCOMPRESS$(INLINE$(plune21$))
  ' output of inline.bas for X11-Basic 21.01.2008
  ' plune22.bmp 496 Bytes.
  plune22$=""
  plune22$=plune22$+"LRXDJ9F.@+QW<WV],JE+T>Fc2)LVTSbFHOMF,a2.1/I?JXa`BR,?[+8IC-82;YKU"
  plune22$=plune22$+"XT^[?0%1/J6@/AC+;VK6cP`;aMNP,P+_-YQ]ZcS5M_H?9@%51@Y@B^5E.P[Q$MH6"
  plune22$=plune22$+"/:cGFM,cU7(&G56T78FX?L5C.X<bRLV<E90XTZ^_=\&^+*bR;QJFZDKSU,Z=DMGV"
  plune22$=plune22$+",6cN\K7W++/0)VSXW@K:V`5(>FR`C2F:5?J5N]89'HQSW44]W]UP/&0$0B^P]aP+"
  plune22$=plune22$+"4RR_.H'EQN''S^EB[W$\<C.W]QaRYHTH%UB.$:.\.\B1AU@3J?Z5D-Q`D^R@H'A>"
  plune22$=plune22$+"2=-TQ]b2*=Y(cGW7)N.)KR__3F)<-)5]1-4P):\;3&a*<@@Uc16<3D<DP4/T]4BU"
  plune22$=plune22$+"(\,HM_4UV&FH0OH;R@<]&?CK(%,LN''a^Q@P)KF5FZ(O)72/a92P?ZZVB/D:*+XH"
  plune22$=plune22$+"*0$$HHWOSF8GOUMM2A*`+G-c>Z5NX8BR+=5LF(:Kb'4>7:X+0MN5T.3V389(T.$2"
  plune22$=plune22$+"M9.;,YIcB8$@2%F):X%TJaDIFFD58ZEPMVWX-SO9PTS6XAL>?D&b7,=[P`-O5L-\"
  plune22$=plune22$+"B/21HG/(Z>'?OU;3]0=a-A%;5<-VHL`0L'V@\_X&4]&=_H_BQ:c3WB.SL2/+K)X^"
  plune22$=plune22$+"c[aC%]b^14A`_-HCKL1,,$$$"
  plune22_bmp$=UNCOMPRESS$(INLINE$(plune22$))
  ' output of inline.bas for X11-Basic 21.01.2008
  ' plune23.bmp 432 Bytes.
  plune23$=""
  plune23$=plune23$+"[L*DD+N.@/2aT\DQXAK^8QLSV[`1JE2bC[_S\3+%Q%F]U=VC)>]V9(H,A5HC$M*'"
  plune23$=plune23$+"BG,3^HcDNQ/]?_Y&ZM3B',X9/:2;SHG$3^U5ANJ\8LT?[F\,cM]R0_'`Z>]cQ.J3"
  plune23$=plune23$+"((>>5XDDI@_(b+HA/VP6CS%71`3Vb?(M(HZc-@WR9Z5+H<&4E:PcHX[M?Fb),$N@"
  plune23$=plune23$+"*0W5a$':3>SZ?F&AO.<WWFQ>LYaPIYHRC)</GU05:9ZW5Q'?McU4B2JNFZU+b-,O"
  plune23$=plune23$+"]>-WF'8)'O:+R8-%%B\EH6:P56*H:]5A@G0-5-4YAO;U1\9'A_J'_7NHCYC&_AXX"
  plune23$=plune23$+"^)S>3A)CU?4%X&2UH<YN1`;;B8$bH/^U;I9TKD7b$I9I*BTLK'?(H-MTbO(YE9G-"
  plune23$=plune23$+"1@;^1(I2=,O*XGASEY66<XBJY*:RBIE?)V1c.'-NNPS@E_a5*WS7B)LK+4G=S9CP"
  plune23$=plune23$+"`@8/U`?6ET07'c4;M6GYTZT7LPR;U>@EK+4I_Y?$Y\K.3UM@HDYHI+S1@%X79IU;"
  plune23$=plune23$+"UW5AP,0V>J739cb'Z&6Z`WbETX<]IVU+YRS'2@:[1B4$EFJL/LR7'I3MM\HA[F$&"
  plune23_bmp$=UNCOMPRESS$(INLINE$(plune23$))
  ' output of inline.bas for X11-Basic 21.01.2008
  ' plune24.bmp 380 Bytes.
  plune24$=""
  plune24$=plune24$+"3D*DT+,`<?2aTbTPZ:*-^^DZa-a+K]>-`_U=$4KH<.@4$3`'D:5CE->.:X`[&L12"
  plune24$=plune24$+"?U]A%KB-:\/;UU.^(:WJ'8W$HX6**LZO/SG?WS>AC4=I-A*Y+9>QO6M0.V/IOH3]"
  plune24$=plune24$+"=G`A&?>Y=A)cF,0;88A+>Y`\SHT`N8N[a;\E\\&,7^,WR2R(ZY@6`V31:7?MI&%?"
  plune24$=plune24$+"(]R(1LPVbXKF+aFY]6=0O]&5SVIJ=C*/bNV/'4S@1ZL0Ba]6T.%DDA>BWSRc3YYC"
  plune24$=plune24$+"RQM\RZ9%c;B-)H]YW1H1QDZbVNY6I&1aR\8I.cbTbZ3&7=$QAN$AIaBHR0O@L^K]"
  plune24$=plune24$+"<aa%+*Y,8[PJKP6$5/WF-$b=8NYG-18_/>W<Lc-^RG9P615BFSK+LZ@JR<-LQGDc"
  plune24$=plune24$+"@-REA=R%[b5U*C</]9ZC0)<:\bAWQ5D3_4OR]CSAb0bUMAC9K:=I8,I7GJ=^UF_5"
  plune24$=plune24$+"6SE,;R`TUN=%]TEE$C54A1P+D&I4;X-FR+BPG6J*8,0W-J6:0ZK0P7RP>&($"
  plune24_bmp$=UNCOMPRESS$(INLINE$(plune24$))
  ' output of inline.bas for X11-Basic 21.01.2008
  ' plune25.bmp 320 Bytes.
  plune25$=""
  plune25$=plune25$+"A;.DUJR.@/2a^[[7&>7CJ$$,?*b=N)O&2^IZFWUUJTV7SERO4+\M67+=9,8?8%X^"
  plune25$=plune25$+"RW=+S;V+%R(VG5'7?(V.BB>27AEW`E7'PA9YSJR:\&J,QPOQ5):JYK0K9O.*KD*7"
  plune25$=plune25$+"=<<INHJJ\S;cI?3,<E326-N&UIN5KZ2Y:_:E,O\=<Xa?H?GUCR:/?P43NY]NO]E0"
  plune25$=plune25$+"^3B33'?ZH`=-=X8Z])\[4+*E5+=<J7-T^3S8*`S[-;X^'/L^5?6W/Z?&AJ,*KV;V"
  plune25$=plune25$+"'0VE;VUDV0B)5,I;E)76)6GC.5='`*^0K//aJ\0.0;bC@CRP*-Z:HYW6?,cP=3P^"
  plune25$=plune25$+"7&.(+MO\YZa\Z9FVRR9_cQ3SHK<=],;-.5'?A_RK%8&A;.^*&a*'1Y<55aOVSZ)P"
  plune25$=plune25$+"88JVS-5,5[+D,>BD*'Kb1T_ZK-+6H@6?L-5.RVQ2,$D$"
  plune25_bmp$=UNCOMPRESS$(INLINE$(plune25$))
  ' output of inline.bas for X11-Basic 21.01.2008
  ' plune26.bmp 255 Bytes.
  plune26$=""
  plune26$=plune26$+"B[FD$X*.@/2aF_(Q&YFb(398AR2VWcNF_DS5>+c?U'Ja3+WcGS&(,1XQ_X8;,^?H"
  plune26$=plune26$+"UK?$aCM/R9`KXWA*L8258_SUKGCWBKV%Q1(GO)O?SM,`Sc?$W]&S?,33N1-F`9<V"
  plune26$=plune26$+"6,RFJ53Y^(-AN'*a:%09;)(9R;Q@.-=UCFA9JC^b+>=)bO\H)2%.@I`cRE/.U&,T"
  plune26$=plune26$+"-:VFMac[L`B[BFY@`Q<R65J$B+_R&^H2.V_W$]W1-%S%(U04c@_/VYV>/@2<SNF3"
  plune26$=plune26$+"W*BWF5?@=.FNO-H+FAWC6&Qb$*$E[.b5bZP)P;;7YG_UI-VBQ_=%L9TP+A$[86<S"
  plune26$=plune26$+"cD@;T/aYW)4VFY\+,7T("
  plune26_bmp$=UNCOMPRESS$(INLINE$(plune26$))
  ' output of inline.bas for X11-Basic 21.01.2008
  ' plune27.bmp 212 Bytes.
  plune27$=""
  plune27$=plune27$+";X&D?XHN4]>PaV`,1R3D%S[_12b)AQ0,a6W*'c^)\)5b+1GG>D=X*%2$IbbP**FA"
  plune27$=plune27$+"VYO:VUB;*,+M:31SR<(HL`b\\EN;PO3;.@VcSW5IR@&@W^^/**VcX]<-?P->c?MZ"
  plune27$=plune27$+",TJ?Rb(>6cM:\,1K$\aH8Ja'6(XE3]M'@&%16%V)%_3b5-7AEYXB>5')-X5$`_Z&"
  plune27$=plune27$+"WYR<-R/@>W:a3\`DR.(OCM`Q&X.W\$PIX5_*B69*>0N^+(EWK+S*0'9W06F$6A_b"
  plune27$=plune27$+";5F+YW)@4A^_I*%=K*^Q;U4)'$,$"
  plune27_bmp$=UNCOMPRESS$(INLINE$(plune27$))
  ' output of inline.bas for X11-Basic 21.01.2008
  ' plune28.bmp 176 Bytes.
  plune28$=""
  plune28$=plune28$+"W]&DI[&.6.>a;%cGQ(Sac/`5\?KLAQX(_V_-BcQ0c.B]2]8_8):@)L+c?I]cZKD<"
  plune28$=plune28$+"HFUVYF93G%%=L@1HQ_DGEZ[;2V@%+I/&,1):2FDZV>RI,IJV05KDSOa'Ma6PaR(_"
  plune28$=plune28$+"])Qa_UV*9VI8V)Kb/2*[cEYI0K30E54KGR3MBT/Q:6&;0@?U(\KJ;>R=UF;A32Uc"
  plune28$=plune28$+"-E)/[>5+.+>8YUO%4.@?Z,'F>Y9E8H[a.SC53FT)6TD$"
  plune28_bmp$=UNCOMPRESS$(INLINE$(plune28$))
  ' output of inline.bas for X11-Basic 21.01.2008
  ' plune29.bmp 45 Bytes.
  plune29$=""
  plune29$=plune29$+"*V7?B.6)?KJJYI,)1E52IY`D$8/bR7$9WR]+`M8F3<FTY-']'.QE0__A6L($"
  plune29_bmp$=UNCOMPRESS$(INLINE$(plune29$))
  ' output of inline.bas for X11-Basic 21.01.2008
  ' plune2.bmp 45 Bytes.
  plune2$=""
  plune2$=plune2$+"*V7?B.6)?KJJYI,)1E52IY`D$8/bR7$9WR]+`M8F3<FTY-']'.QE0__A6L($"
  plune2_bmp$=UNCOMPRESS$(INLINE$(plune2$))
  ' output of inline.bas for X11-Basic 21.01.2008
  ' plune3.bmp 125 Bytes.
  plune3$=""
  plune3$=plune3$+"JP;^0?5)c'<H=WcNGSKc3C]TV?>[AK2;O`X.$CJC</2K$*_\44K\:;G6'A%%]YUW"
  plune3$=plune3$+"6b6LE=DMD\V-M>38J-(5MYJ%$2<9D1=Xb_P_b/.K$F*\PHBc,B@ZND/ZMP'122LZ"
  plune3$=plune3$+"_;?b^C%U@6,NT=WS=B+`=(EE^b1Z]^V7S'9X.4$$"
  plune3_bmp$=UNCOMPRESS$(INLINE$(plune3$))
  ' output of inline.bas for X11-Basic 21.01.2008
  ' plune4.bmp 209 Bytes.
  plune4$=""
  plune4$=plune4$+"Y\FDJ[H`Fa@^-QJbS[W17NC$]cUK_^K_3K<T)3OZHK.XW.<:>?HFQ)<+F]^W-'2]"
  plune4$=plune4$+"Y._A>QWOK%^O4.YD/55<IX99UU_-E&'T;;3&J;G%8)5:YHN25LaY._0(V;]N,*O^"
  plune4$=plune4$+"^O?E=a-Kbc.&?TL=/H,9Kc?[`%[8RV^T7`@OF[bQ8'`S_Z_;4C`Q^]/'NJ[Z=?'Q"
  plune4$=plune4$+"EB8+60]b/^6LO^=$M.bY0.(DA.'UW5=\J=ESH`1,XH^9FEH>KG]V)@8TKY\_HIQ/"
  plune4$=plune4$+"Y_cM3&7)F,H&/]BL>J1`$D($"
  plune4_bmp$=UNCOMPRESS$(INLINE$(plune4$))
  ' output of inline.bas for X11-Basic 21.01.2008
  ' plune5.bmp 289 Bytes.
  plune5$=""
  plune5$=plune5$+"YLHD5Y\`<?2aNW>O]F.I6C\``B8K^Q`V6/QGFXPB*77\YPZ%JM$;]YD%AZ-aL&P3"
  plune5$=plune5$+"JW2T<1a$'0$b*.5I`N7B^]7M+cF8ON\[/1^E>R.\JXS=;@MU[FK?Z2RX*B<,.1CV"
  plune5$=plune5$+"NL1D[34%'7?=@NR$@Y-.[L'%*(\&NAN\H(/F%bHIXY+&BXPSW98>Q0F`SK<7'A)["
  plune5$=plune5$+"LS\NZ[Z)$c4M_a5?X^DR=:EaWW2N,,F$CI*U0^OLS=E+FES$ZT*MbNF$_4LZS'ZL"
  plune5$=plune5$+".%H)CaBHXB<+?)8?=ZB[$b>G'+1:[Pb\PD/+@*T6Y=\Fc]T,OM]T0;?/MQ;MG;'R"
  plune5$=plune5$+"(U:,L\F+PL(4c=EI7^EOR/cV%8+:,OFMPS>.V_B$]/]\2cL0HW92^9`>P91DPDTB"
  plune5$=plune5$+",4$$"
  plune5_bmp$=UNCOMPRESS$(INLINE$(plune5$))
  ' output of inline.bas for X11-Basic 21.01.2008
  ' plune6.bmp 322 Bytes.
  plune6$=""
  plune6$=plune6$+"(6^D:*^.@+QW<_5EU]C`-X'A)_,KcZcBV+0c=b9XZJBWQ'9_Bc49Z3))]L%+BO3_"
  plune6$=plune6$+"57OGcO@L7=IIXO^M,EJ]@;DGE58INEZ'E%Dc+LD<W'7aTF$[V>K;]4X$;?Oc:*6L"
  plune6$=plune6$+"OO;\J,1MMc?C%XCQ7(WI>cID+=aZE0EU8H'VR8JXJ8QY);X)?;\6C$?QSSF+Y4$9"
  plune6$=plune6$+"TN]2WCN5&VVI)IK1M*:>5\]G<?Tbc2$6;b_&YQ\O.\1=0+'Q=DY\TZBR3]1R=4FT"
  plune6$=plune6$+"_,a]aS8T4SZ=-0cY?OV3$,AA=8II?F=/IA3b\9Y;]P.c$J9LVEXHEWDXC[D-V7$>"
  plune6$=plune6$+"(/b,7&H0\?bM>0I?$?]C&P\bOIM\'B/V3M1_a$&5.[&>C_<9T(N,O%[SZEB>.->-"
  plune6$=plune6$+"%C'G]T39YRcF(MJNa]?B+*Q9K.();0/+bOI2N42&X?P,%$$$"
  plune6_bmp$=UNCOMPRESS$(INLINE$(plune6$))
  ' output of inline.bas for X11-Basic 21.01.2008
  ' plune7.bmp 363 Bytes.
  plune7$=""
  plune7$=plune7$+"69>DQ*2.@/2A0OX/7S7;O>,%)AP;5@G_cW+K_.Y+CXb0_GM8%;,+*BG0T(@A6943"
  plune7$=plune7$+"PP&,$P6bV.K;R9a$4BF$;X&Q(@bV?PA\G^/)`1,\7'+Z-(:c=$25ZI:.QcH-V]]W"
  plune7$=plune7$+"E^6-49M^_*26E'*:c(X1RZ_/C1BO\Z\F^9<BVW<c<*HCN6@KF@<$MA&N,;EW452_"
  plune7$=plune7$+"V<I([`1/bFJB;K1PL8F^=+T\$`ZGIS'7?UM1VKSS/Y];FPWS9UTaa\VS`+]SY9A6"
  plune7$=plune7$+"/bS[^]^_(%O1B&EL^,_[Z0D>U%[6YDM7OG*XFT'5,\.&*M@CEG-2UUEPAY87)*G/"
  plune7$=plune7$+"7@**3V:H0+:Q'>7';1H.(.a-9J_[P$3:BC2KI01+L]2R]&)BK3(\<1^Y@459\&F,"
  plune7$=plune7$+"O`T[Z>0E^?:)aQE22YT5SQ/WU.>U@3;E-`HS,DE4+L/D]]Gb,:A+L1.*KB/MTV\W"
  plune7$=plune7$+",\WMcaJ?E,Hb\T;LP+<ZLLIHM&'2+?Q/9L`D"
  plune7_bmp$=UNCOMPRESS$(INLINE$(plune7$))
  ' output of inline.bas for X11-Basic 21.01.2008
  ' plune8.bmp 429 Bytes.
  plune8$=""
  plune8$=plune8$+"_N(DKZ8`<A2_*-'6/?OZJAbXM$&TG3OcB?V/CF_cS<*'(MFQ?C>6;)TK94;+aNU7"
  plune8$=plune8$+"9ZX`Z=0ZbE@)S?_6bZ=/4(RD,XCaDHC9++^=MAO._D@X32NR*V<C,^N&cR8[a:/["
  plune8$=plune8$+"CI\;&Q;2=EE753'Z-8/^,0X1O00OO//JGUPDYM>1:-JbCHV_=BSBY1C2Q?K:+6^c"
  plune8$=plune8$+"c57371)9c.,NX2<SUY?`K,`c)^I%M,/X8_9D7H=FS^('*U*S@4\J<Y*:S>M4T,9&"
  plune8$=plune8$+"]S7'R/B%cE0+STWa>3a$Z9Lc+0G^3C]<HN>IO];MQ+B^cBcOQUOL-O7)I8C&:YH5"
  plune8$=plune8$+"a4[)]B@GD:W9DCb;[LE>*=&9c%@J-&WVJR?IE2M=JVZBXCA=2/[GaGN)?&UF$?3G"
  plune8$=plune8$+"@\a8Z%`[NN5W9*1cT:FY).ab:,WA/[aT2W&PES6Q7F'a,&9TT?+2WBS1&OY*$$VU"
  plune8$=plune8$+"cG^65P8$K]7/8*S8II(%[;@AKFQXIGH+0_AKHL[*,RF3VBN09VL7.[:P5/:3BbT$"
  plune8$=plune8$+"MQCbWRM(@U\9cZ1YD(>PY(6cbH@K[.9<[_cB[)UHaM(H]Ab<9MEYS+G+E,X0"
  plune8_bmp$=UNCOMPRESS$(INLINE$(plune8$))
  ' output of inline.bas for X11-Basic 21.01.2008
  ' plune9.bmp 475 Bytes.
  plune9$=""
  plune9$=plune9$+"DA^D0*a`<A1c)]'0Q`PcBHX5cO8A[T;]-$*%cYSZ2-[$LDVY^+)<8]A]:?<./A&\"
  plune9$=plune9$+"'U;N*2W8RG-a]:HY(JM62(*XYIJE'-9>0;79<^-T=O+LL+&G`,RG=1)UINJc3KA,"
  plune9$=plune9$+"/D`Z,TJ+S:WJ,N+VFaYI0<0>[9FJXCF/\&/8Q$E&?Q:PY=Z641J^M:6&8;>)[R'c"
  plune9$=plune9$+"UC\+9>,EN67&.J>?+C=S^Q>U`O>6SDZ)K5L@R1_Z64O&H`TbUFH$'YNFB71)BX01"
  plune9$=plune9$+"c.cGZ-O^1$3cW&-C,NZ4E-N)SY+CL@.SF](H_T5P4CcJ'>YMKJHXK`K`\DP/DW_>"
  plune9$=plune9$+"92@58OT:b\KU*[ORX[=M9_$VLE(b&YT-P^*J\=.`XW0:M00S5\12b:(FSI[=]<Y["
  plune9$=plune9$+"&.+R;V.5A:[\IV_T0QB?F9FXc27'+VA6D%*7YQNP^]*N&>>*MS18L,<G45^WAN$N"
  plune9$=plune9$+"IYI``+C-T=@cU6TZJD]EKc/4JFOP]YY>FX_bSVZ(@[4/7VTK2-%.U-<@^B3%5M=4"
  plune9$=plune9$+"@P^U0LPcP:5TW3()AO*$6[<MGWTaX&<X@\N9Eb,c]X_:*EVN`2Z52,>H8X/FNUC:"
  plune9$=plune9$+"CDa.%aA;573ZCO7VDG9>\/7C/RK4;P;,6Ib$0)T;KB:*c*RKSMM[-E4N$4$$"
  plune9_bmp$=UNCOMPRESS$(INLINE$(plune9$))

  ' output of inline.bas for X11-Basic 23.01.2008
  ' sun.bmp 1298 Bytes.
  sun$=""
  sun$=sun$+"P+BDa)GJbXKLS.X?1QN\bE*&*LV]DG&6[D'cW\]].@R<^71^_W`'SQaHW&GFBD;4"
  sun$=sun$+"<][C94_C4+X@$55K?\(ESMccT`TUH8Y;XTK\T/7D<N;)J-^5.6<9I&8EU]+FTSP]"
  sun$=sun$+"84bLaGb-.A?1,XYE*aF;W[Y,80*I;3;9.YaQ-[5A=Z_QC%M.;ADV64B:(K@0EH@%"
  sun$=sun$+"OBIHY`FAB?b0J&MF((B`HJBE2cVR4_?QJY&*_S$\U0AT.N0HI\1>N`:>KX>$_b.7"
  sun$=sun$+"R1XA=4>%%.,M99=b@<9&c/acT2&_Y0a5/?<HONH-*<a]cO-U[>7EA1AQ]IPaWZT="
  sun$=sun$+"Z^=44+P_'JRQD83GI9(S_JL[7%YG]0Z7T>C`ET?:-`^@L2$RV-?DTaM,7JDI.=(U"
  sun$=sun$+"8X/`(O/bPEc6^S.(-0Y1R]cAM$D<;UPM+D&ZLSF7C^WOVH`.76`?-S*_JP;*a,LF"
  sun$=sun$+"5]=J+NOL\XX^?DDDF7CI5+`J(IL3B_:3KM/6U+Y>GJ6P@cI%P1EB3V[M:'_?bY0E"
  sun$=sun$+"K]K1&S8R9.@b[cASDO<<L9\@+JOB-]8%06L_UVO<>S_DA>@>.KRG\-Q*W*F^c`N_"
  sun$=sun$+"JQKL$^,$[a-GU*%IQA`aCQI1O:[RHEA2.X6AR&4H5\MPc*YA+AQPC>9N)$GWQ:ZQ"
  sun$=sun$+"A17FEbaN7\';E5<(6b[1&&^4Q)>7'A].R=7;cD,N:N(L-c0H?.JbTX2GZD&X]5:5"
  sun$=sun$+"QTG0KH?_E]@N>WQZ^EP^/V(F^=G(0WM]$^CI7.[A\(FR965[]S2./.>M*=`./A'X"
  sun$=sun$+"[Ha=<F7B.Z6K3HIPT<V7^.VB=LMX)M_Qa&S<*R6FF_5OD<SF9B/_^ZCBH1N-:KD("
  sun$=sun$+"7;;?OQ3AK*[UaUK3&)CG*X')'Y\+13_F))+=K*A`M=[Y&)6-Na_>(Qa_$bP71WM1"
  sun$=sun$+".79_K+'Q(^2:>([_5`(4L(.9A%79QQW?%.-BLJa1[3A3*5WYU$RMFFQ-<GN%Z:H."
  sun$=sun$+"*9:>'DW5[[@HI9KU%G*Z0Ba%S&J1_GAJJ:1,a.GN/aN9Ma4JN=2VN7J=E]90Q9\K"
  sun$=sun$+"7YDX(5MLW'(SbbN;-'^1*>M%A*`F*3D8Wc22R2]^.0BQ-STEDLE+6BOFMXO^X7/_"
  sun$=sun$+"\G:,3FZ)4:=F6;T0SD>$1Z-+=%TFDLUS%[+_?`<1/*bDK.0`B8<(\P&)*=(A7TK6"
  sun$=sun$+";JP99P@McI=:RH>;b.aB%S.H_bCE6[ENQc3UZ)HZNJL+P+%bU7\WI7ZM[;%4@VX]"
  sun$=sun$+"/)4>;aX\2Y\[^7R(.6+58<]1^AW0Q7;5E+TX3\?W[P[P0C<=c.b%<M4<I\?^1L[Y"
  sun$=sun$+"4D-;J9N(LQKP,U18=-&J5U1bcT6K58?05PDA;%D5H4J_RTQ3^T%F0OQ;_]64%B(A"
  sun$=sun$+"JZ3<>56%.LAM?5X/-M`E97L2A<?HZ'(=\]5[E6@COHH*2W^_Q8W'?bA]09K*YH8W"
  sun$=sun$+"TH&cSRJ^HSQa)$3C2F18HD-`T$JIJYGK3\MA?[?NW30.%T=/U6-RYUMJNJ_:@$XG"
  sun$=sun$+")1^)PD\BY/S8(\X*9AOI\G1.,Y&SBC%?K['FBTKSD9</J.ELV^HOG)]Z%8/<VS?T"
  sun$=sun$+"+?O`QUX\ZMOWEF6XQbbMNE:J5D\6Hbb)(b(HUE;S+^Ca%?0W`GNP`T2+b[&V\5IC"
  sun$=sun$+"/b*E5T['AU@S&*\R?Q*Jc[$YI7KGI3bO3_@>?WBO.<8MRB>A>H?,?Q;`;05X%=82"
  sun$=sun$+",1*\J/CT0+'OP4.TZ@:WYOD418S$Q6.b'aQ&+_8B%NS%(9``c>NGSL&H>]$B<O.?"
  sun$=sun$+"54,$"
  sun_bmp$=UNCOMPRESS$(INLINE$(sun$))

RETURN

FUNCTION breite$(x)
  LOCAL posx$
  IF x>0
    posx$="N"
  ELSE
    posx$="S"
  ENDIF
  x=ABS(x)
  posx$=posx$+RIGHT$("00"+STR$(INT(x)),2)+":"
  x=x-INT(x)
  x=x*60
  posx$=posx$+RIGHT$("00"+STR$(INT(x)),2)+":"
  x=x-INT(x)
  x=x*60
  posx$=posx$+RIGHT$("00"+STR$(INT(x)),2)+"."
  x=x-INT(x)
  x=x*10
  posx$=posx$+STR$(INT(x))
  RETURN posx$
ENDFUNC
FUNCTION laenge$(x)
  LOCAL posx$
  IF x>0
    posx$="E"
  ELSE
    posx$="W"
  ENDIF
  x=ABS(x)
  posx$=posx$+RIGHT$("000"+STR$(INT(x)),3)+":"
  x=x-INT(x)
  x=x*60
  posx$=posx$+RIGHT$("00"+STR$(INT(x)),2)+":"
  x=x-INT(x)
  x=x*60
  posx$=posx$+RIGHT$("00"+STR$(INT(x)),2)+"."
  x=x-INT(x)
  x=x*10
  posx$=posx$+STR$(INT(x))
  RETURN posx$
ENDFUNC

PROCEDURE doit

  IF year<=1900 OR year>=2100
    PRINT "Dies Script erlaubt nur Berechnungen"
    PRINT "in der Zeitperiode 1901-2099. Angezeigte Resultat sind ungültig."
  ENDIF
  ' Julian date at 0:00 h
  JD0=JULIAN(STR$(day,2,2,1)+"."+STR$(month,2,2,1)+"."+STR$(year,4,4,1))-0.5
  JD=JD0+((hour-zone)+minute/60+second/3600)/24
  TDT=JD+deltat/24/3600

  gmst=@GMST(JD)
  GMSTvalue$=@HHMMSS$(gmst)
  LMST=@GMST2LMST(gmst,lon)
  LMSTvalue$=@HHMMSS$(lmst)

  @SunPosition(TDT)             ! Calculate data for the Sun at given time

  sunsign$=sunCoor_sign$
  sunlon=deg(sunCoor_lon)
  sunlat=deg(sunCoor_lat)
  sundistance=sunCoor_distance
  sunra=deg(suncoor_ra)/15
  sundec=deg(suncoor_dec)
  sundiameter=deg(sunCoor_diameter)*60

  @MoonPosition(TDT)   ! Calculate data for the Moon at given time

  MoonLon=DEG(moonCoor_lon)
  MoonLat=DEG(moonCoor_lat)
  MoonRA=DEG(moonCoor_ra)/15
  MoonDec=DEG(moonCoor_dec)
  MoonAge=DEG(moonCoor_moonAge)
  MoonPhaseNumber=moonCoor_phase
  MoonPhase$=moonCoor_moonPhase$

  MoonSign$=moonCoor_sign$
  MoonDistance=moonCoor_distance
  MoonDiameter=DEG(moonCoor_diameter)*60   !  angular diameter in arc seconds

  ' Calculate distance from the observer (on the surface of earth) to the center of the sun

  @sunEquPolar2Cart(sunCoor_ra,sunCoor_dec,sunCoor_distance)

  @Observer2EquCart(lon,lat,height,gmst)
  SunDistanceObserver=(sqrt((sunCart_x-observerCart_x)^2+(sunCart_y-observerCart_y)^2+(sunCart_z-observerCart_z)^2))

  ' JD0: JD of 0h UTC time
  @SunRise(JD0,DeltaT,lon,lat,Zone,0)

  SunTransit=sunRise_transit
  SunRise=sunRise_rise
  SunSet=sunRise_set

  ' Calculate distance from the observer (on the surface of earth) to the center of the moon

  @moonEquPolar2Cart(moonCoor_ra,moonCoor_dec,moonCoor_distance)
  MoonDistanceObserver=(sqrt((moonCart_x-observerCart_x)^2+(moonCart_y-observerCart_y)^2+(moonCart_z-observerCart_z)^2))

  @MoonRise(JD0,DeltaT,lon,lat,Zone,0)

  MoonTransit=moonRise_transit
  MoonRise=moonRise_rise
  MoonSet=moonRise_set
RETURN

' Julian Date to Greenwich Mean Sidereal Time
FUNCTION GMST(JD)
  UT=frac(JD-0.5)*24    ! UT in hours
  JD=floor(JD-0.5)+0.5  ! JD at 0 hours UT
  T=(JD-2451545.0)/36525.0
  T0=6.697374558+T*(2400.051336+T*0.000025862)
  RETURN mod(T0+UT*1.002737909,24)
ENDFUNCTION

' Local Mean Sidereal Time, geographical longitude in radians, East is positive
FUNCTION GMST2LMST(gmst,lon)
  lmst=mod(gmst+deg(lon)/15,24)
  RETURN lmst
ENDFUNCTION

FUNCTION HHMM$(hh)
  LOCAL m,h
  IF hh=0
    RETURN ""
  ENDIF

  m=frac(hh)*60
  h=Int(hh)
  IF m>=59.5
    INC h
    SUB m,60
  ENDIF
  m=round(m)
  RETURN str$(h,2,2,1)+":"+STR$(m,2,2,1)
ENDFUNCTION
FUNCTION HHMMSS$(hh)
  LOCAL m,s,h
  IF hh=0
    RETURN ""
  ENDIF
  m=frac(hh)*60
  h=INT(hh)
  s=frac(m)*60
  m=INT(m)
  IF s>=59.5
    INC m
    SUB s,60
  ENDIF
  IF m>=60
    INC h
    SUB m,60
  ENDIF
  s=round(s)
  RETURN str$(h,2,2,1)+":"+STR$(m,2,2,1)+":"+STR$(s,2,2,1)
ENDFUNCTION
' Calculate coordinates for Sun
' Coordinates are accurate to about 10s (right ascension)
' and a few minutes of arc (declination)
PROCEDURE SunPosition(TDT)
  LOCAL d,eg,wg,e,a,diameter0,msun,nu
  D=TDT-2447891.5

  eg=RAD(279.403303)
  wg=rad(282.768422)
  e=0.016713
  a=149598500   ! km
  diameter0=RAD(0.533128)    ! angular diameter of Moon at a distance

  MSun=RAD(360)/365.242191*D+eg-wg
  nu=MSun+ RAD(360)/pi*e*sin(MSun)

  sunCoor_lon=@Mod2Pi(nu+wg)
  sunCoor_lat=0
  sunCoor_anomalyMean=MSun

  sunCoor_distance=(1-e*e)/(1+e*cos(nu))        ! distance in astronomical units
  sunCoor_diameter=diameter0/sunCoor_distance   ! angular diameter in radians
  sunCoor_distance=sunCoor_distance*a            ! distance in km
  sunCoor_parallax=6378.137/sunCoor_distance    ! horizonal parallax

  @Ecl2Equ(sunCoor,TDT)
  suncoor_sign$=@Sign$(suncoor_lon)
RETURN

' Calculate data and coordinates for the Moon
' Coordinates are accurate to about 1/5 degree (in ecliptic coordinates)
PROCEDURE MoonPosition(TDT)
  LOCAL d,l0,p0,n0,i,e,a,diameter0,parallax0,l,MMoon,n,c,ev,ae,a3,mmoon2,ec,a4,l2,v,l3,n2
  D=TDT-2447891.5

  ' Mean Moon orbit elements as of 1990.0
  l0=RAD(318.351648)
  P0=RAD(36.340410)
  N0=RAD(318.510107)
  i=RAD(5.145396)
  e=0.054900
  a=384401          !km
  diameter0=RAD(0.5181)  ! angular diameter of Moon at a distance
  parallax0=RAD(0.9507)  !  parallax at distance a

  l=RAD(13.1763966)*D+l0
  MMoon=l-RAD(0.1114041)*D-P0  ! Moon's mean anomaly M
  N=N0-RAD(0.0529539)*D       ! Moon's mean ascending node longitude
  C=l-sunCoor_lon
  Ev=RAD(1.2739)*sin(2*C-MMoon)
  Ae=RAD(0.1858)*sin(sunCoor_anomalyMean)
  A3=RAD(0.37)*sin(sunCoor_anomalyMean)
  MMoon2=MMoon+Ev-Ae-A3              ! corrected Moon anomaly
  Ec=RAD(6.2886)*sin(MMoon2)         ! equation of centre
  A4=RAD(0.214)*sin(2*MMoon2)
  l2=l+Ev+Ec-Ae+A4                   ! corrected Moon's longitude
  V=RAD(0.6583)*sin(2*(l2-sunCoor_lon))
  l3=l2+V                ! true orbital longitude;

  N2=N-RAD(0.16)*sin(sunCoor_anomalyMean)

  moonCoor_lon=@Mod2Pi(N2+atan2(sin(l3-N2)*cos(i),cos(l3-N2)))
  moonCoor_lat=asin(sin(l3-N2)*sin(i))
  moonCoor_orbitLon=l3

  @moonEcl2Equ(moonCoor,TDT)

  ' relative distance to semi mayor axis of lunar oribt
  moonCoor_distance=(1-sqr(e))/(1+e*cos(MMoon2+Ec))
  moonCoor_diameter=diameter0/moonCoor_distance     ! angular diameter in radians
  moonCoor_parallax=parallax0/moonCoor_distance     ! horizontal parallax in radians
  moonCoor_distance=moonCoor_distance*a             ! distance in km

  ' Age of Moon in radians since New Moon (0) - Full Moon (pi)
  moonCoor_moonAge=@Mod2Pi(l3-sunCoor_lon)
  moonCoor_phase=0.5*(1-cos(moonCoor_moonAge))     ! Moon phase, 0-1

  mainPhase=1/29.53*RAD(360)         ! show 'Newmoon, 'Quarter' for +/-1 day arond the actual event
  p=mod(moonCoor_moonAge,RAD(90))
  IF p<mainPhase OR p>RAD(90)-mainPhase
    p=2*round(moonCoor_moonAge/RAD(90))
  ELSE
    p=2*floor(moonCoor_moonAge/RAD(90))+1
  ENDIF
  moonCoor_moonPhase$=phases$(p)

  moonCoor_sign$=@Sign$(moonCoor_lon)
RETURN

' Modulo PI
FUNCTION Mod2Pi(x)
  x=mod(x,2*pi)
  RETURN x
ENDFUNCTION
' Transform ecliptical coordinates (lon/lat) to equatorial coordinates (RA/dec)
PROCEDURE Ecl2Equ(coor,TDT)
  T=(TDT-2451545.0)/36525       ! Epoch 2000 January 1.5
  eps=RAD(23+(26+21.45/60)/60+T*(-46.815+T*(-0.0006+T*0.00181))/3600)
  coseps=cos(eps)
  sineps=sin(eps)

  sinlon=sin(suncoor_lon)
  suncoor_ra=@Mod2Pi(atan2((sinlon*coseps-tan(suncoor_lat)*sineps),cos(suncoor_lon)))
  suncoor_dec=asin(sin(suncoor_lat)*coseps+cos(suncoor_lat)*sineps*sinlon)
RETURN
PROCEDURE moonEcl2Equ(coor,TDT)
  T=(TDT-2451545.0)/36525       ! Epoch 2000 January 1.5
  eps=RAD(23+(26+21.45/60)/60+T*(-46.815+T*(-0.0006+T*0.00181))/3600)
  coseps=cos(eps)
  sineps=sin(eps)

  sinlon=sin(mooncoor_lon)
  mooncoor_ra=@Mod2Pi(atan2((sinlon*coseps-tan(mooncoor_lat)*sineps),cos(mooncoor_lon)))
  mooncoor_dec=asin(sin(mooncoor_lat)*coseps+cos(mooncoor_lat)*sineps*sinlon)
RETURN
FUNCTION Sign$(lon)
  IF lon<0
    ADD lon,2*pi
  ENDIF
  RETURN signs$(floor(DEG(lon)/30))
ENDFUNCTION

' Calculate observers cartesian equatorial coordinates (x,y,z in celestial frame)
' from geodetic coordinates (longitude, latitude, height above WGS84 ellipsoid)
' Currently only used to calculate distance of a body from the observer

PROCEDURE Observer2EquCart(lon,lat,height,gmst)
  flat=298.257223563      ! WGS84 flatening of earth
  aearth=6378.137           ! GRS80/WGS84 semi major axis of earth ellipsoid
  ' Calculate geocentric latitude from geodetic latitude
  co=cos(lat)
  si=sin(lat)
  fl=1.0-1.0/flat
  fl=fl*fl
  si=si*si
  u=1/sqrt(co*co+fl*si)
  a=aearth*u+height
  b=aearth*fl*u+height
  observercart_z=sqrt(a*a*co*co+b*b*si)   ! geocentric distance from earth center
  observercart_y=acos(a*co/observercart_z)       ! geocentric latitude, rad
  observercart_x=lon          ! longitude stays the same
  IF lat<0.0 ! adjust sign
    observercart_y=-observercart_y
  ENDIF
  @observerEquPolar2Cart(observercart_x,observercart_y,observercart_z)  ! convert from geocentric polar to geocentric cartesian, with regard to Greenwich

  '  rotate around earth's polar axis to align coordinate system from Greenwich to vernal equinox
  x=observercart_x
  y=observercart_y
  rotangle=gmst/24*2*pi          ! sideral time gmst given in hours. Convert to radians
  observercart_x=x*cos(rotangle)-y*sin(rotangle)
  observercart_y=x*sin(rotangle)+y*cos(rotangle)
RETURN

PROCEDURE observerEquPolar2Cart(lon,lat,distance)
  rcd=cos(lat)*distance
  observercart_x=rcd*cos(lon)
  observercart_y=rcd*sin(lon)
  observercart_z=distance*sin(lat)
RETURN
PROCEDURE sunEquPolar2Cart(lon,lat,distance)
  rcd=cos(lat)*distance
  suncart_x=rcd*cos(lon)
  suncart_y=rcd*sin(lon)
  suncart_z=distance*sin(lat)
RETURN
PROCEDURE moonEquPolar2Cart(lon,lat,distance)
  rcd=cos(lat)*distance
  mooncart_x=rcd*cos(lon)
  mooncart_y=rcd*sin(lon)
  mooncart_z=distance*sin(lat)
RETURN

' Find (local) time of sunrise and sunset
' JD is the Julian Date of 0h local time (midnight)
' Accurate to about 1-2 minutes
' recursive: 1 - calculate rise/set in UTC
' recursive: 0 - find rise/set on the current local day (set could also be first)
PROCEDURE SunRise(JD,deltaT,lon,lat,zone,recursive)
  LOCAL jd0UT
  jd0UT=floor(JD-0.5)+0.5            ! JD at 0 hours UT
  @SunPosition(jd0UT+0*deltaT/24/3600)
  coor1_ra=suncoor_ra
  coor1_dec=suncoor_dec

  @SunPosition(jd0UT+1+0*deltaT/24/3600)   ! calculations for next day's UTC midnight
  coor2_ra=suncoor_ra
  coor2_dec=suncoor_dec

  ' rise/set time in UTC.

  @RiseSet(jd0UT,coor1,coor2,lon,lat,1)

  IF recursive=0  ! check and adjust to have rise/set time on local calendar day
    IF zone>0
      ' rise time was yesterday local time -> calculate rise time for next UTC day
      IF rise_rise>=24-zone OR rise_transit>=24-zone OR rise_set>=24-zone
        risetemp_transit=rise_transit
        risetemp_rise=rise_rise
        risetemp_set=rise_set
        @SunRise(JD+1,deltaT,lon,lat,zone,1)

        IF risetemp_rise>=24-zone
          risetemp_rise=rise_rise
        ENDIF
        IF risetemp_transit>=24-zone
          risetemp_transit=rise_transit
        ENDIF
        IF risetemp_set>=24-zone
          risetemp_set=rise.set
        ENDIF
        rise_transit=risetemp_transit
        rise_rise=risetemp_rise
        rise_set=risetemp_set
      ENDIF

    ELSE if (zone<0)
      ' rise time was yesterday local time -> calculate rise time for next UTC day
      IF rise_rise<-zone OR rise_transit<-zone OR rise_set<-zone
        risetemp_transit=rise_transit
        risetemp_rise=rise_rise
        risetemp_set=rise_set

        @SunRise(JD-1,deltaT,lon,lat,zone,1)

        IF risetemp_rise<-zone
          risetemp_rise=rise_rise
        ENDIF
        IF risetemp_transit<-zone
          risetemp_transit=rise_transit
        ENDIF
        IF risetemp_set<-zone
          risetemp_set=rise_set
        ENDIF
        rise_transit=risetemp_transit
        rise_rise=risetemp_rise
        rise_set=risetemp_set
      ENDIF
    ENDIF
    sunrise_transit=mod(rise_transit+zone,24)
    sunrise_rise=mod(rise_rise+zone,24)
    sunrise_set=mod(rise_set+zone,24)
  ENDIF
RETURN

' JD is the Julian Date of 0h UTC time (midnight)
PROCEDURE RiseSet(jd0UT,coor1,coor2,lon,lat,timeinterval)
  LOCAL t0,psi,y,alt,dt

  coor_ra=coor1_ra
  coor_dec=coor1_dec
  @GMSTRiseSet(coor1,lon,lat)
  rise1_rise=riseset_rise
  rise1_set=riseset_set
  rise1_transit=riseset_transit

  coor_ra=coor2_ra
  coor_dec=coor2_dec
  @GMSTRiseSet(coor2,lon,lat)
  rise2_rise=riseset_rise
  rise2_set=riseset_set
  rise2_transit=riseset_transit

  ' unwrap GMST in case we move across 24h -> 0h
  IF rise1_transit>rise2_transit AND abs(rise1_transit-rise2_transit)>18
    ADD rise2_transit,24
  ENDIF
  IF rise1_rise>rise2_rise AND abs(rise1_rise-rise2_rise)>18
    ADD rise2_rise,24
  ENDIF
  IF rise1_set>rise2_set AND abs(rise1_set-rise2_set)>18
    ADD rise2_set,24
  ENDIF

  T0=@GMST(jd0UT)

  '  var T02 = T0-zone*1.002738; // Greenwich sidereal time at 0h time zone (zone: hours)

  ' Greenwich sidereal time for 0h at selected longitude
  T02=T0-DEG(lon)/15*1.002738
  IF T02<0
    ADD T02,24
  ENDIF

  IF rise1_transit<T02
    ADD rise1_transit,24
    ADD rise2_transit,24
  ENDIF
  IF rise1_rise<T02
    ADD rise1_rise,24
    ADD rise2_rise,24
  ENDIF
  IF rise1_set<T02
    ADD rise1_set,24
    ADD rise2_set,24
  ENDIF

  ' Refraction and Parallax correction

  decMean=0.5*(coor1_dec+coor2_dec)
  psi=acos(sin(lat)/cos(decMean))

  ' altitude of sun center: semi-diameter, horizontal parallax and (standard) refraction of 34'
  alt=0.5*coor1_diameter-coor1_parallax+RAD(34)/60
  y=asin(sin(alt)/sin(psi))
  dt=240*DEG(y)/cos(decMean)/3600                 ! time correction due to refraction, parallax

  rise_transit=@GMST2UT(jd0UT,@InterpolateGMST(T0,rise1_transit,rise2_transit,timeinterval))
  rise_rise=@GMST2UT(jd0UT,@InterpolateGMST(T0,rise1_rise,rise2_rise,timeinterval)-dt)
  rise_set=@GMST2UT(jd0UT,@InterpolateGMST(T0,rise1_set,rise2_set,timeinterval)+dt)

  ' rise.transit = Mod(rise.transit, 24.);
  ' rise.rise    = Mod(rise.rise, 24.);
  ' rise.set     = Mod(rise.set,  24.);
RETURN
'  Convert Greenweek mean sidereal time to UT
FUNCTION GMST2UT(JDd,gmst)
  LOCAL t,t0,ut
  JDd=floor(JDd-0.5)+0.5   ! JD at 0 hours UT
  T=(JDd-2451545.0)/36525.0
  T0=mod(6.697374558+T*(2400.051336+T*0.000025862),24)
  ' var UT = 0.9972695663*Mod((gmst-T0), 24.);
  UT=0.9972695663*((gmst-T0))
  RETURN UT
ENDFUNCTION
' Find GMST of rise/set of object from the two calculates
' (start)points (day 1 and 2) and at midnight UT(0)
FUNCTION InterpolateGMST(gmst0,gmst1,gmst2,timefactor)
  RETURN (timefactor*24.07*gmst1-gmst0*(gmst2-gmst1))/(timefactor*24.07+gmst1-gmst2)
ENDFUNCTION
' returns Greenwich sidereal time (hours) of time of rise
' and set of object with coordinates coor.ra/coor.dec
' at geographic position lon/lat (all values in radians)
' Correction for refraction and semi-diameter/parallax of body is taken care of in function RiseSet
PROCEDURE GMSTRiseSet(coor,lon,lat)
  tagbogen=acos(-tan(lat)*tan(coor_dec))

  riseset_transit=DEG(1)/15*(coor_ra-lon)
  riseset_rise=24+DEG(1)/15*(-tagbogen+coor_ra-lon) ! calculate GMST of rise of object
  riseset_set=DEG(1)/15*(+tagbogen+coor_ra-lon) ! calculate GMST of set of object

  ' using the modulo function Mod, the day number goes missing. This may get a problem for the moon
  riseset_transit=mod(riseset_transit,24)
  riseset_rise=mod(riseset_rise,24)
  riseset_set=mod(riseset_set,24)
RETURN

' Find local time of moonrise and moonset
' JD is the Julian Date of 0h local time (midnight)
' Accurate to about 5 minutes or better
' recursive: 1 - calculate rise/set in UTC
' recursive: 0 - find rise/set on the current local day (set could also be first)
' returns '' for moonrise/set does not occur on selected day

PROCEDURE MoonRise(JD,deltaT,lon,lat,zone,recursive)
  LOCAL timeinterval,jd0UT

  timeinterval=0.5
  jd0UT=floor(JD-0.5)+0.5         !JD at 0 hours UT

  @SunPosition(jd0UT+0*deltaT/24/3600)
  suncoor1_ra=suncoor_ra
  suncoor1_dec=suncoor_dec
  suncoor1_anomalyMean=sunCoor_anomalyMean
  suncoor1_lon=suncoor_lon
  @MoonPosition(jd0UT+0*deltaT/24/3600)
  coor1_ra=mooncoor_ra
  coor1_dec=mooncoor_dec

  @SunPosition(jd0UT+timeinterval+0*deltaT/24/3600)  ! calculations for noon
  suncoor2_ra=suncoor_ra
  suncoor2_dec=suncoor_dec
  suncoor2_anomalyMean=sunCoor_anomalyMean
  suncoor2_lon=suncoor_lon

  ' calculations for next day's midnight
  @MoonPosition(jd0UT+timeinterval+0*deltaT/24/3600)
  coor2_ra=mooncoor_ra
  coor2_dec=mooncoor_dec

  ' rise/set time in UTC, time zone corrected later.
  ' Taking into account refraction, semi-diameter and parallax
  @RiseSet(jd0UT,coor1,coor2,lon,lat,timeinterval)

  IF recursive=0  ! check and adjust to have rise/set time on local calendar day
    IF zone>0
      ' recursive call to MoonRise returns events in UTC
      risetemp_transit=rise_transit
      risetemp_rise=rise_rise
      risetemp_set=rise_set
      @MoonRise(JD-1,deltaT,lon,lat,zone,1)

      ' recursive call to MoonRise returns events in UTC
      ' risenext = MoonRise(JD+1, deltaT, lon, lat, zone, 1);
      ' alert("yesterday="+riseprev.transit+"  today="+rise.transit+" tomorrow="+risenext.transit);
      ' alert("yesterday="+riseprev.rise+"  today="+rise.rise+" tomorrow="+risenext.rise);
      ' alert("yesterday="+riseprev.set+"  today="+rise.set+" tomorrow="+risenext.set);

      IF risetemp_transit>=24-zone OR risetemp_transit<-zone  ! transit time is tomorrow local time
        IF rise_transit<24-zone
          risetemp_transit=0          ! there is no moontransit today
        ELSE
          risetemp_transit=rise_transit
        ENDIF
      ENDIF
      IF risetemp_rise>=24-zone OR risetemp_rise<-zone  ! transit time is tomorrow local time
        IF rise_rise<24-zone
          risetemp_rise=0          ! there is no moonrise today
        ELSE
          risetemp_rise=rise_rise
        ENDIF
      ENDIF
      IF risetemp_set>=24-zone OR risetemp_set<-zone  ! transit time is tomorrow local time
        IF rise_set<24-zone
          risetemp_set=0          ! there is no moonset today
        ELSE
          risetemp_set=rise_set
        ENDIF
      ENDIF
      rise_transit=risetemp_transit
      rise_rise=risetemp_rise
      rise_set=risetemp_set
    ELSE if zone<0
      ' rise/set time was tomorrow local time -> calculate rise time for former UTC day
      IF rise_rise<-zone OR rise_set<-zone OR rise_transit<-zone
        risetemp_transit=rise_transit
        risetemp_rise=rise_rise
        risetemp_set=rise_set
        @MoonRise(JD+1,deltaT,lon,lat,zone,1)

        IF risetemp_rise<-zone
          IF rise_rise>-zone
            risetemp_rise=0     ! there is no moonrise today
          ELSE
            risetemp_rise=rise_rise
          ENDIF
        ENDIF
        IF risetemp_transit<-zone
          IF rise_transit>-zone
            risetemp_transit=0     ! there is no moontransit today
          ELSE
            risetemp_transit=rise_transit
          ENDIF
        ENDIF
        IF risetemp_set<-zone
          IF rise_set>-zone
            risetemp_set=0     ! there is no moonset today
          ELSE
            risetemp_set=rise_set
          ENDIF
        ENDIF
        rise_transit=risetemp_transit
        rise_rise=risetemp_rise
        rise_set=risetemp_set
      ENDIF
    ENDIF
    ' correct for time zone, if time is valid
    CLR moonrise_transit,moonrise_rise,moonrise_set
    IF rise_transit
      moonrise_transit=mod(rise_transit+zone,24)
    ENDIF
    IF rise_rise
      moonrise_rise=mod(rise_rise+zone,24)
    ENDIF
    IF rise_set
      moonrise_set=mod(rise_set+zone,24)
    ENDIF
  ENDIF
RETURN

stardata:
DATA 048915,"ALPHA CMA SIRIUS",-1.46,6.752472222222,-15.28388888889,"A1 V* 8.6 1.7"
DATA 045348,"ALPHA CAR CANOPUS",-0.72,6.399222222222,-51.30444444444,"F0II 74 ?"
DATA 128620,"ALPHA CEN RIGIL",0,0.2341666666667,35.21388888889,"07 G2V+K1V 4.3 1.18"
DATA 124897,"ALPHA BOO ARCTURUS",-0.04,14.261,19.1825,"K1IIIBCN-1 34 25.1"
DATA 172167,"ALPHA LYR VEGA",0.03,18.61561111111,38.78361111111,"A0V 25.3 2"
DATA 034029,"ALPHA AUR CAPELLA",0.08,5.278138888889,45.99805555556,"G8 41 13"
DATA 034085,"BETA ORI RIGEL",0.12,5.242277777778,-7.798333333333,"B8 I* 815 63"
DATA 061421,"ALPHA CMI PROCYON",0.38,7.655027777778,5.225,"F5 IV 11.4 2"
DATA 010144,"ALPHA ERI ARCHENAR",0.46,1.628583333333,-56.76333333333,"B3VPE 69 5.0"
DATA 039801,"ALPHA ORI BETELGEUSE",0.5,5.919527777778,7.406944444444,"M2 I 425 226"
DATA 122451,"BETA CEN HADAR",0.61,14.06372222222,-59.62722222222,"B1III 320"
DATA 187642,"ALPHA AQL ALTAIR",0.77,19.84636111111,8.876666666667,"16.8 1.6"
DATA 029139,"ALPHA TAU ALDEBARAN",0.85,4.598666666667,16.50916666667,"K5 III 60 46"
DATA 116658,"ALPHA VIR SPICA",0.98,13.41986111111,-10.83861111111,"B1 III + B2 V 220 6.6"
DATA 148478,"ALPHA SCO ANTARES",0.96,16.49011111111,-25.57638888889,"M1.5I* 425 510"
DATA 062509,"BETA GEM POLLUX",1.14,7.75525,28.02611111111,"K0 III 40 10"
DATA 216956,"ALPHA PSA FOMALHAUT",1.16,22.96083333333,-28.37777777778,"A3VA 22 1.5"
DATA 197345,"ALPHA CYG DENEB",1.25,20.6905,45.28027777778,"A2IAE 1630 ?"
DATA 111123,"BETA CRU MIMOSA",1.25,12.79536111111,-58.31138888889,"B0.5III 460"
DATA 087901,"ALPHA LEO REGULUS",1.35,10.13952777778,11.96722222222,"B7V* 69 3.5"
DATA 052089,"EPSILON CMA ADHARA",1.5,6.977083333333,-27.02777777778,"B2II 570 ?"
DATA 108248,"ALPHA CRU ACRUX",1.58,12.44330555556,-62.90111111111,"B1* 510"
DATA 060178,"ALPHA GEM CASTOR",1.58,7.576638888889,31.88833333333,"A1V* 46 1.7"
DATA 108903,"GAMMA CRU GACRUX",1.63,12.51941666667,-56.88694444444,"M3.5III 120"
DATA 158926,"LAMBDA SCO SHAULA",1.63,17.56011111111,-36.89638888889,"B1.5IV 325 6.6"
DATA 035468,"GAMMA ORI BELLATRIX",1.64,5.418833333333,6.349722222222,"B2III 303 8.1"
DATA 035497,"BETA TAU EL",0,1.740555555556,17.97666666667,"27 B7 III 130 5.2"
DATA 080007,"BETA CAR MIAPLACIDUS",1.68,9.220027777778,-68.28277777778,"A2IV 2.6"
DATA 037128,"EPSILON ORI ALNILAM",1.7,5.603527777778,-0.7980555555556,"B0IAE 31"
DATA 209952,"ALPHA GRU AL",0,2.108888888889,13.14916666667,"40 B7IV 91 3.6"
DATA 112185,"EPSILON UMA ALIOTH",1.77,12.90047222222,55.95972222222,"A0PCR 49 3"
DATA 068273,"GAMMA VEL REGOR",1.78,8.158861111111,-46.66333333333,"WC8+O7.5E 17"
DATA 020902,"ALPHA PER MARFAK",1.79,3.405361111111,49.86138888889,"F5IB 270 55"
DATA 095689,"ALPHA UMA DUBHE",1.79,11.06211111111,61.75083333333,"K0IIIA 105 ?"
DATA 054605,"DELTA CMA AL",0,1.958888888889,22.97305555556,"35 F8IA 650 300 (?)"
DATA 169022,"EPSILON SGR KAUS",0,2.156666666667,9.739722222222,"05 B9.5III 160"
DATA 071129,"EPSILON CAR SHE/(AVIOR)",1.86,8.375222222222,-58.49055555556,"K0II* 330 70"
DATA 120315,"ETA UMA BENETNASCH",1.86,13.79230555556,49.31333333333,"B3V 3.9"
DATA 159532,"THETA SCO SARGAS",1.87,17.62194444444,-41.00222222222,"F1II 140 40"
DATA 040183,"BETA AUR MENKALINAM",1.9,5.992138888889,44.9475,"A2IV 84 2.5"
DATA 150708,"ALPHA TRA RASALMUTHALLATH",1.92,16.81108333333,-68.97222222222,"K2 II 130 37"
DATA 047105,"GAMMA GEM ALMISAN",1.93,6.628527777778,16.39916666667,"A0IV 78 3"
DATA 193924,"ALPHA PAV JOO",0,2.280277777778,37.87888888889,"07 B2IV 160 5"
DATA 074956,"DELTA VEL KOO",0,2.105555555556,41.31166666667,"30 A1 V 70 1.89"
DATA 044743,"BETA CMA MURZIM",1.98,6.378305555556,-16.04388888889,"B1II-II 300 9"
DATA 081797,"ALPHA HYA ALPHARD",1.98,9.459777777778,-7.341388888889,"K3III 200 37"
DATA 012929,"ALPHA ARI HAMAL",2,2.119527777778,23.4625,"K2IIIABCA-I 74 21.4"
DATA 000000,"CRB 2.00 15",59,30.53194444444,13,"K2IIIABCA-I 74 21.4"
DATA 008890,"ALPHA UMI POLARIS",2.02,2.530694444444,89.26416666667,"F7:IB-IIV 470 19.5"
DATA 175224,"SIGMA SGR NUNKI",2.02,18.92105555556,-25.70333333333,"B2.5V 160 4.5"
DATA 004128,"BETA CET DENEB",0,2.051944444444,35.03305555556,"12 K0III 57 14"
DATA 037742,"ZETA ORI ALNITAK",2.05,5.679305555556,-0.05722222222222,"O9.5IBE* 400 20"
DATA 000358,"ALPHA AND ALPHERATZ",2.06,0.1397777777778,29.09055555556,"B8IVPMNHG 120 3.6"
DATA 006860,"BETA AND MIRACH",2.06,1.162194444444,35.62055555556,"M0IIIA 76 21.8"
DATA 012533,"GAMMA AND ALAMACH",2.06,2.064972222222,42.32972222222,"K3-IIB* 400 83.2"
DATA 123139,"THETA CEN HARATAN",2.06,14.11133333333,-35.63,"K0IIIB 56 8.9"
DATA 038771,"KAPPA ORI SAIPH",2.06,5.795916666667,-8.330277777778,"B0.5IAV 550 38"
DATA 159561,"ALPHA OPH RAS",0,2.372777777778,56.20916666667,"36 A5III 67 3.15"
DATA 131873,"BETA UMI KOCHAB",2.08,14.84505555556,74.15555555556,"K4 III 120 37"
DATA 214952,"BETA GRU AL",0,2.478333333333,39.24805555556,"05 M5III 270"
DATA 019356,"BETA PER ALGOL",2.12,3.136138888889,40.95583333333,"B8V 100 3.16"
DATA 102647,"BETA LEO DENEBOLA",2.14,11.81763888889,14.57194444444,"A3V 42 1.8"
DATA 110304,"GAMMA CEN KOO",0,2.381388888889,30.11583333333,"34 A1IV 130"
DATA 194093,"GAMMA CYG SADR",2.2,20.37044444444,40.25666666667,"F8IB 470 30.9"
DATA 078647,"LAMBDA VEL SUHAIL",2.21,9.13325,-42.58,"K4 IB-II 220"
DATA 003712,"ALPHA CAS SCHEDIR",2.23,0.6751111111111,56.5375,"K0IIIA 230 40.7"
DATA 139006,"ALPHA CRB ALPHECCA",2.23,15.57811111111,26.71472222222,"A0V 67 2.7"
DATA 164058,"GAMMA DRA ETAMIN",2.23,17.94341666667,51.48888888889,"K5III 148 23.6"
DATA 036486,"DELTA ORI MINTAKA",2.23,5.533416666667,0.2991666666667,"B0* 600 16"
DATA 000432,"BETA CAS CAPH",2.25,0.1529444444444,59.14972222222,"F2III 45 2.0"
DATA 080404,"IOTA CAR TUREIS",2.25,9.284833333333,-58.72472222222,"A8IB ??? 192"
DATA 066811,"ZETA PUP SUHAIL",0,2.384166666667,34.33333333333,"11 O5IAF 800 16"
DATA 116656,"ZETA UMA MIZAR",2.27,13.39875,54.92527777778,"A2VPSRSI* 190 1.6"
DATA 151680,"EPSILON SCO WEI",2.29,16.83602777778,-33.70666666667,"K2.5III 69 16"
DATA 129078,"ALPHA LUP 2.30",14,41.91527777778,23.28333333333,"130 "
DATA 118716,"EPSILON CEN 2.30",13,39.87194444444,27.98333333333,"130 "
DATA 000000,"CEN 2.31 14",35,29.6025,28,"130 "
DATA 143275,"DELTA SCO DSCHUBBA",2.32,16.00552777778,-21.37833333333,"B0.3IV ?"
DATA 095418,"BETA UMA MERAK",2.37,11.03066666667,56.38222222222,"A0V 76 2.5"
DATA 002261,"ALPHA PHE ANKAA",2.39,0.4380555555556,-41.69388888889,"K0III 76 10.2"
DATA 206778,"EPSILON PEG ENIF",2.39,21.73641666667,9.875,"K2IB 26.5"
DATA 160613,"KAPPA SCO 2.41",17,42.47416666667,1.8,"6.9 "
DATA 217906,"BETA PEG SCHEAT",2.42,23.06288888889,28.08277777778,"M2.5II-III 38.7"
DATA 203280,"ALPHA CEP ALDERAMIN",2.44,21.30963888889,62.58555555556,"A7V 49 2"
DATA 103287,"GAMMA UMA PHECDA",2.44,11.89716666667,53.69472222222,"A0VE 88 2.4"
DATA 058350,"ETA CMA ALUDRA",2.45,7.401555555556,-28.69694444444,"B5IA 3200 60"
DATA 197989,"EPSILON CYG 2.46",20,46.21916666667,58.21666666667,"13.2 "
DATA 005394,"GAMMA CAS CIH",0,2.485555555556,43.41194444444,"00 B0IVE 200 23"
DATA 218045,"ALPHA PEG MARKAB",2.49,23.07933333333,15.20527777778,"B9V 2"
DATA 081188,"KAPPA VEL CIH",2.5,9.368555555556,55.01055555556,"B2 IV 6.9"
DATA -1

' Kommentar zum Orginal-Script (Java-Script):

'<!--
'     ------------------ Das Script beginnt hier ---------------
'     KOMMENTAR ZUM SCRIPT
'     Wir freuen uns selbstverstaendlich wenn Sie sich
'     fuer die Details unseres kleinen Skripts interessieren.
'     Es bringt jedoch nichts, wenn Sie dieses Script auf Ihre Seite kopieren.
'     Ein einfacher Link beweist genau so gut, dass Sie das Skript gefunden haben.
'     Kopieren Sie deshalb diese Skript nicht auf Ihre 'private' Hompage.
'     Arnold Barmettler, astro!nfo

'     Source code by Arnold Barmettler, www.astronomie.info / www.CalSky.com
'     based on algorithms by Peter Duffett-Smith's great and easy book
'     'Practical Astronomy with your Calculator'.
'-->

'Ich gehe davon aus, dass Sie den Programmcode für eigene Projekte verwenden
'können. Es wäre dann aber schön, wenn Sie das gute Werk von Peter Duffett-Smith
'kaufen würden, z.B. gleich bei uns: Practical Astronomy with your Calculator,
'und einen Hinweis mit Link auf die Programmcode-Quelle (A.
'Barmettler/www.astronomie.info) erstellen könnten.
