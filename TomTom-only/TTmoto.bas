' TTmoto for TomTom (c)  2009 Adam Bunter
' Adaptiert für X11-Basic
'
' 2013-08-08 adapted for Android

g_backgroundcycle=0
g_debugdisplay=0
g_simulatefeed=0
g_utcoffset=10
g_debugmsg=""

g_gpsfeed$=""
g_gpsGPGGA$=""
g_gpsGPRMC$=""
g_gpsGPGSA=""

'display variables
g_gpsstatus=0
g_speed$="000.0"

g_datetime$="00/00/00 00:00:00"
g_gpsstatus$="Inactive"
'g_ttd$="TTD: 00:00"

g_dir$="DIR: 000.0'"
g_alt$="ALT: 000.0 m"
g_sats$="SATS: 00"

g_lat$="S 000"+CHR$(034)+" 00.000"+CHR$(039)
g_lon$="E 000"+CHR$(034)+" 00.000"+CHR$(039)

'load resources
red=GET_COLOR(65535,0,0)
yellow=GET_COLOR(65535,65535,0)
orange=GET_COLOR(65535,34000,0)
white=GET_COLOR(65535,65535,65535)
black=GET_COLOR(0,0,0)
blue=GET_COLOR(0,0,65535)
green=GET_COLOR(0,65535,0)
'lightgreen=get_color(26214,65535,26214)
lightgreen=GET_COLOR(37008,61166,37008)
darkgreen=GET_COLOR(0,43908,0)
gray=GET_COLOR(20000,20000,20000)
bluz1=GET_COLOR(0,65535,65535)
bluz2=GET_COLOR(0,32768,65535)
bluz3=GET_COLOR(0,0,65535)
bluz4=GET_COLOR(0,0,32768)
bluz5=GET_COLOR(0,32768,32768)
fontsmall$="-'-helvetica-small-r-normal-'-10-'-iso8859-'"
fontmedium$="-*-helvetica-bold-r-normal-*-14-*-iso8859-*"

'set window size
SIZEW ,320,240

'draw initial screen with fixed elements
@DrawStatic

tomtom=0

IF GPS?=0
  'read from tom tom
  IF exist("/var/run/gpsfeed")
    OPEN "I",#1,"/var/run/gpsfeed"
    tomtom=1
  ELSE
    'open "I",#1,"/home/hoffmann/bas/gps/log/nmea-11.08.2008.log"
    COLOR red
    TEXT 10,220,"Unable to open gpsfeed!"
  ELSE
    GPS ON
  ENDIF
ENDIF
'main program loop
DO
  IF GPS?=0 AND tomtom=1
    ' get feeds
    ' see comments at end of file for list of all sentences

    ' parse GPGGA - altitude
    WHILE j<5
      LINEINPUT #1,g_gpsfeed$
      IF left$(g_gpsfeed$,6)="$GPGGA"
        @parseGPGGA(g_gpsfeed$)
        j=5
      ENDIF
      INC j
    WEND

    ' parse GPGSA - no of sats
    j=0
    WHILE j<5
      LINEINPUT #1,g_gpsfeed$
      IF left$(g_gpsfeed$,6)="$GPGSA"
        @parseGPGSA(g_gpsfeed$)
        j=5
      ENDIF
      INC j
    WEND

    ' parse GPRMC - status, lat, lon, speed, dir
    j=0
    WHILE j<5
      LINEINPUT #1,g_gpsfeed$
      IF left$(g_gpsfeed$,6)="$GPRMC"
        @ParseGPRMC(g_gpsfeed$)
        j=5
      ENDIF
      INC j
    WEND
  ELSE
    GET_LOCATION lat,lon,alt
    g_alt$="ALT: "+using$(alt,"###.#")+"m"
    satcount=0
    g_sats$="SATS: "+RIGHT$("0"+STR$(satcount),2)
  ENDIF
  IF g_backgroundcycle>12
    g_backgroundcycle=0
    @DrawStatic
  ELSE
    INC g_backgroundcycle
  ENDIF

  ' speed
  IF g_debugdisplay=1 then
    COLOR white
  ELSE
    COLOR black
  ENDIF
  PBOX 2,2,310,130
  @DrawSpeed
  g_datetime$=@GetDateTime$()

  ' info
  IF g_debugdisplay=1 then
    COLOR white
  ELSE
    COLOR black
  ENDIF
  PBOX 2,130,318,180
  @DrawInfo(g_datetime$,10,140,white)
  '@DrawInfo(g_ttd$,230,140,gray)
  @DrawInfo(g_gpsstatus$,245,140,white)
  @DrawInfo(g_dir$,10,155,bluz1)
  @DrawInfo(g_alt$,130,155,bluz1)
  @DrawInfo(g_sats$,245,155,bluz1)
  @DrawInfo(g_lat$,10,170,bluz2)
  @DrawInfo(g_lon$,200,170,bluz2)
  '@DrawFeed(left$(g_gpsGPGGA$,60), 10, 190,green)
  '@DrawFeed(left$(g_gpsGPRMC$,60), 10, 200,green)
  '@DrawFeed(right$(g_gpsGPRMC$,len(g_gpsGPRMC$)-60), 10, 210,green)
  @DrawFeeds(g_gpsGPRMC$+g_gpsGPGSA$+g_gpsGPGGA$)

  'refresh display
  '@DrawDebug("vsync")
  SHOWPAGE
  PAUSE 0.1
LOOP
QUIT

PROCEDURE ParseGPGGA(gpsfeed$)

  ' used for altitude

  ' GPGGA= Global Positioning System Fix Data
  ' $GPGGA,074347.000,4338.1504,N,00658.6430,E,1,05,2.8,263.5,M,48.5,M,,0000*5D
  ' $GPGGA,123519,4807.038,N,01131.000,E,1,08,0.9,545.4,M,46.9,M,,*47

  '	1. Message ID = $GPGGA
  '	2. UTC Time (hhmmss)
  '	3. Latitude
  '	4.  N/S Indicator = N=north or S=south
  '	5. Longitude
  ' 	6. E/W Indicator = E=east or W=west
  '	7. Fix Quality: 0 = invalid,  1 = GPS fix (SPS), 2 = DGPS fix, 3 = PPS fix, 4 = Real Time Kinematic, 5 = Float RTK, 6 = estimated (dead reckoning) (2.3 feature), 7 = Manual input mode, 8 = Simulation mode
  '	8. No of satellites being tracked
  ' 	9. Horizontal dilution of position (accuracy)
  '	10. Altitude,  above mean sea level
  '	11. M=meters
  '	12. Height of geoid (mean sea level) above WGS84
  '	13. M=meters
  '	14. empty
  '	15. stationID
  '	16. checksum = *47

  @DrawDebug("ParseGPGGA")
  g_gpsGPGGA$=gpsfeed$

  LOCAL feed$
  LOCAL i
  LOCAL alt$
  FOR i=0 TO 9
    SPLIT gpsfeed$,",",0,feed$,gpsfeed$
    IF i=9 then
      alt$=feed$
    ENDIF
  NEXT i
  IF alt$=""
    alt$="000.0"
  ENDIF
  g_alt$="ALT: "+alt$+"m"
RETURN

PROCEDURE ParseGPGSA(gpsfeed$)

  ' GPGSA= GPS DOP and Active Satellites
  '$GPGSA,A,3,04,05,,09,12,,,24,,,,,2.5,1.3,2.1*39

  'GSA      Satellite status
  'A        Auto selection of 2D or 3D fix (M = manual)
  '3        3D fix - values include: 1 = no fix
  '                                  2 = 2D fix
  '                                  3 = 3D fix
  '04,05... PRNs of satellites used for fix (space for 12)
  '2.5      PDOP (dilution of precision)
  '1.3      Horizontal dilution of precision (HDOP)
  '2.1      Vertical dilution of precision (VDOP)
  '*39      the checksum data, always begins with *

  @DrawDebug("ParseGpsGPGSA")
  g_gpsGPGSA$=gpsfeed$

  LOCAL satcount
  LOCAL i
  LOCAL feed$

  satcount=0
  FOR i=0 TO 12
    SPLIT gpsfeed$,",",0,feed$,gpsfeed$
    IF i>3 AND i<12 then
      IF len(feed$)>0 then
        satcount=satcount+1
      ENDIF
    ENDIF
  NEXT i

  g_sats$="SATS: "+RIGHT$("0"+STR$(satcount),2)
RETURN

PROCEDURE ParseGPGSV()
  @DrawDebug("ParseGPGSV")
RETURN

PROCEDURE ParseGPRMC(gpsfeed$)
  ' used for status, lat, lon, speed, dir

  ' GPRMC = Recommended Minimum Specific
  ' $GPRMC,161229.487,A,3723.2475,N,12158.3416,W,0.13,309.62,120598, ,'10
  '	1. Message ID = $GPRMC
  '	2. UTC Time (hhmmss.sss)= 161229.487
  '	3. Status: A=data valid or V=data not valid
  '	4. Latitude (ddmm.mmmm) = 3723.2475
  '	5.  N/S Indicator = N=north or S=south
  '	6. Longitude (dddmm.mmmm) = 12158.3416
  ' 	7. E/W Indicator = E=east or W=west
  '	8. Speed Over Ground (knots) = 0.13 (1 knot = 1.852 KPH )
  ' 	9. Course Over Ground (degrees) = 309.62 TRACK ANGLE
  '	10. Date (ddmmyy) = 120598
  '	11. Magnetic Variation (degrees) = E=east or W=west
  '	12. Mode = A=Autonomous, D=DGPS, E=DR
  '	13. Checksum = '10

  LOCAL feed$
  LOCAL i
  LOCAL status$
  LOCAL lat$
  LOCAL nsind$
  LOCAL lon$
  LOCAL ewind%
  LOCAL knots$
  LOCAL cog$
  LOCAL kph$

  @DrawDebug("ParseGPRMC")
  g_gpsGPRMC$=gpsfeed$

  FOR i=0 TO 12
    SPLIT gpsfeed$,",",0,feed$,gpsfeed$
    IF i=2
      '	Status: A=data active and valid or V=data not valid
      status$=feed$
    ELSE if i=3
      '	Latitude (ddmm.mmmm) = 3723.2475
      lat$=feed$
    ELSE if i=4
      '	N/S Indicator = N=north or S=south
      nsind$=feed$
    ELSE if i=5
      '	 Longitude (dddmm.mmmm) = 12158.3416
      lon$=feed$
    ELSE if i=6 then
      '	E/W Indicator = E=eastor W=west
      ewind$=feed$
    ELSE if i=7 then
      '	Speed Over Ground (knots) = 0.13 (1 knot = 1.852 KPH )
      knots$=feed$
    ELSE if i=8 then
      ' 	Course Over Ground (degrees) = 309.62
      cog$=feed$
    ENDIF
  NEXT i

  'set global display strings
  'status
  IF left$(status$,1)="A"
    g_gpsstatus=1
    g_gpsstatus$="  Active"
  ELSE
    g_gpsstatus=0
    g_gpsstatus$="Inactive"
  ENDIF

  'convert knots to kilmeters per hour
  kph$=STR$(VAL(knots$)*1.852)
  IF instr(kph$,".")=0
    kph$=kph$+".0"
  ELSE
    kph$=LEFT$(kph$,instr(kph$,".")+1)
  ENDIF
  kph$=RIGHT$("000"+kph$,5)
  g_speed$=kph$

  'position
  'Latitude (ddmm.mmmm) = 3723.2475
  g_lat$=nsind$+" "+RIGHT$("0"+LEFT$(lat$,LEN(lat$)-9),3)+CHR$(034)+" "+MID$(lat$,LEN(lat$)-8,6)+CHR$(039)
  g_lon$=ewind$+" "+RIGHT$("0"+LEFT$(lon$,LEN(lon$)-9),3)+CHR$(034)+" "+MID$(lon$,LEN(lon$)-8,6)+CHR$(039)
  g_dir$="DIR: "+cog$+CHR$(034)

RETURN

FUNCTION GetDateTime$()

  @DrawDebug("GetDateTime")

  'sys date time variables
  LOCAL dt_date$
  LOCAL dt_time$

  'return value
  LOCAL dt$

  'utc offset variables
  LOCAL utcoffset
  LOCAL inc_day
  LOCAL inc_mth
  LOCAL inc_yr

  LOCAL dt_sec$
  LOCAL dt_min$
  LOCAL dt_hr$
  LOCAL dt_day$
  LOCAL dt_mth$
  LOCAL dt_yr$

  LOCAL dt_sec
  LOCAL dt_min
  LOCAL dt_hr
  LOCAL dt_day
  LOCAL dt_mth
  LOCAL dt_yr

  'set initial return value
  dt$="00/00/00 00:00:00"
  'return dt$

  'set utc offset (hours) and set inital values of flags for utc offset calc
  utcoffset=10
  inc_day=0
  inc_mth=0
  inc_yr=0

  'get date and time from system
  dt_date$=date$
  dt_time$=time$

  'dt$=dt_date$+" " +dt_time$
  'return dt$
  '@DrawDebug(dt$)

  'seperate date time parts
  dt_sec$=RIGHT$(dt_time$,2)
  dt_sec=VAL(dt_sec$)

  dt_min$=MID$(dt_time$,4,2)
  dt_min=VAL(dt_min$)

  dt_hr$=LEFT$(dt_time$,2)
  dt_hr=VAL(dt_hr$)

  dt_day$=LEFT$(dt_date$,2)
  dt_day=VAL(dt_day$)

  dt_mth$=MID$(dt_date$,4,2)
  dt_mth=VAL(dt_mth$)

  dt_yr$=RIGHT$(dt_date$,4)
  dt_yr=VAL(dt_yr$)

  'increment hours
  IF dt_hr<(24-utcoffset)
    dt_hr=dt_hr+utcoffset
  ELSE
    dt_hr=dt_hr+utcoffset-24
    inc_day=1
  ENDIF
  dt_hr$=RIGHT$("00"+STR$(dt_hr),2)

  'increment day
  IF inc_day=1
    dt_day=dt_day+2
    IF dt_mth=2
      IF dt_day>29
        dt_day=1
        inc_mth=1
      ENDIF
    ELSE if dt_mth=4 OR dt_mth=6 OR dt_mth=9 OR dt_mth=11 then
      IF dt_day>30
        dt_day=1
        inc_mth=1
      ENDIF
    ELSE
      IF dt_day>31
        dt_day=1
        inc_mth=1
      ENDIF
    ENDIF
  ENDIF
  dt_day$=RIGHT$("00"+STR$(dt_day),2)

  ' increment month
  IF inc_mth=1
    IF dt_mth<12
      dt_mth=dt_mth+1
    ELSE
      dt_mth=1
      inc_yr=1
    ENDIF
  ENDIF
  dt_mth$=RIGHT$("00"+STR$(dt_mth),2)
  ' incrment year
  IF inc_yr=1
    dt_yr=dt_yr+1
  ENDIF
  dt_yr$=RIGHT$(STR$(dt_yr),2)
  dt$=dt_day$+"/"+dt_mth$+"/"+dt_yr$+" "+dt_hr$+":"+dt_min$+":"+dt_sec$
  RETURN dt$
ENDFUNCTION

PROCEDURE DrawSpeed()
  @DrawDebug("DrawSpeed")
  LOCAL speed$
  ' kph - red if inactive, green if active
  DEFTEXT 2,0.05,0.1,0
  IF g_gpsstatus=1
    COLOR green
  ELSE
    COLOR red
  ENDIF
  FOR cnta=1 TO 1
    FOR cntb=1 TO 1
      LTEXT 290+cnta,107+cntb,"kph"
      LTEXT 290+cntb,108+cnta,"kph"
    NEXT cntb
  NEXT cnta

  ' speed
  speed$=g_speed$
  DEFTEXT 3,0.5,1,0
  COLOR yellow
  FOR cnta=1 TO 4
    FOR cntb=1 TO 5
      LTEXT 14+cnta,10+cntb,speed$
      LTEXT 14+cntb,10+cnta,speed$
    NEXT cntb
  NEXT cnta
RETURN

PROCEDURE DrawInfo(info$,x,y,textcolour)
  ' @DrawDebug("DrawInfo")
  SETFONT fontmedium$
  DEFTEXT 1
  IF g_gpsstatus=1
    COLOR textcolour
  ELSE
    COLOR red
  ENDIF
  ' color white
  TEXT x,y,info$
RETURN

PROCEDURE DrawFeed(feed$,x,y,textcolour)
  DEFTEXT 0
  COLOR textcolour
  TEXT x,y,feed$
RETURN

PROCEDURE DrawFeeds(feeds$)
  LOCAL feed$
  feed$=feeds$+feeds$+feeds$
  DEFTEXT 0
  COLOR lightgreen
  TEXT 10,190,LEFT$(feed$,60)
  COLOR green
  TEXT 10,200,MID$(feed$,61,60)
  COLOR darkgreen
  TEXT 10,210,MID$(feed$,121,60)
RETURN

PROCEDURE DrawDebug(debugmsg$)
  DEFTEXT 0
  COLOR gray
  g_debugmsg$=LEFT$(debugmsg$+" "+g_debugmsg$,60)
  TEXT 10,220,g_debugmsg$
  ' vsync
RETURN

PROCEDURE DrawStatic
  ' draw background with boarder
  COLOR orange
  PBOX 0,0,320,240
  COLOR black
  PBOX 2,2,318,238
  ' draw copyright notice
  SETFONT fontsmall$
  COLOR blue
  TEXT 10,230,"V.1.01 (c) Adam Bunter 2009"
RETURN
