' measures the speed of typing
' originally written in GFA-Basic 1995
'
' ported to X11-Basic by Markus Hoffmann 2000-01-12
'
' works only under UNIX/linux
'
' demonstrates INP(-1), FLUSH, TIMER
'
PRINT "ESC=ABBRUCH"
' Print out the text to be typed:
SYSTEM "cat vorgabe.dat"
OPEN "O",#1,"tippdata.dat"
PRINT "Los gehts... (wenn los, mit SPACE Starten...)"
a=INP(-2)
PRINT "LOS:"
t=TIMER
CLR c
a=1
WHILE a<>27
  a=INP(-2)
  PRINT CHR$(a);
  PRINT #1,CHR$(a);
  FLUSH
  IF a=8
    DEC c
  ELSE
    INC c
  ENDIF
  IF (TIMER-t)>=15
    PRINT
    PRINT "Anschlaege: "+STR$(INT(c/(TIMER-t)*60))+" pro Min."
    CLR c
    t=TIMER
  ENDIF
WEND
CLOSE
' Check if there are any typing errors...
SYSTEM "wdiff tippdata.dat vorgabe.dat"
QUIT
