' Printout the free memory (RAM) of the computer
' This program demonstrates how to do this in X11-Basic under linux, since
' There is no FRE(0) function built in like in GFA-Basic
'
PRINT "Total Memory  : ";@fre(0);" kBytes."
PRINT "Free Memory   : ";@fre(1);" kBytes."
PRINT "Swapped Memory: ";@fre(4);" kBytes."
END
' Get the free memory available
' n=0 physical memory (total)
' n=1 free memory
' n=4 Swapped
FUNCTION fre(n)
  LOCAL i,a,t$,a$
  a=FREEFILE()
  OPEN "I",#a,"/proc/meminfo"
  WHILE n>0
    LINEINPUT #a,t$
    DEC n
  WEND
  LINEINPUT #a,t$
  t$=TRIM$(t$)
  ' PRINT t$
  FOR i=0 TO 1
    SPLIT t$," ",0,a$,t$
  NEXT i
  CLOSE #a
  RETURN VAL(a$)
ENDFUNCTION
