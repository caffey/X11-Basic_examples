' This simple program reads the data from a Voltcraft DL-141 Datalogger connected to USB
'
' It relies on the cp210x driver, works only under linux
' (c) Markus Hoffmann  2014-05-25
'

' 00 --> 68 5e
'

' config:
' dd 00 50 00 00 60 0a ce 14 06 06 15 17 24 46 d8 64 00 23 78 aa
' dd 00 30 00 00 60 0a ce 14 06 06 16 21 57 46 d8 64 00 23 78 aa
' dd -sam-        r i  ce --date-- --time-- --minmax---       aa
'

' write config
' 18 41             0a                      46 d8 64 00
' Set time
' 58 14 06 14 15 24 52
'    --date-- --time--
'
' Antwort: 1e

' Set flags:
' m8  rr xt xt xh xh
'        --minmax---
' m=  mode:
'     xxxx 1000
'     xxxx----- FLASH mode (1=10s, 2=20s, 3=30s, 4=none)
'     1001 1000 set date & time
'     0000 1000 ???
' rr= xxxx xxxx
'     |||    ++- 1= resolution->sec, 2=resolution->min, 3=resolution->hours
'     ||+------ 1=ALERT mode on, 0=ALert mode off
'     |+------- 1=linear buffer, 0=circular buffer
'     +-------- 1= PUSH Button to start recording, 0=record immediately
'
' Antwort: 1e

' data:
'  00 e7 01 be
'  00 e7 01 dc
'  00 e8 01 e0
'  23.2 C 48.0 %

IF WIN32? OR ANDROID?
  PRINT "Sorry, this program works only with linux."
  END
ENDIF

OPEN "UX",#1,"/dev/ttyUSB0"
PRINT #1,CHR$(0);   ! say helo
FLUSH #1
PAUSE 0.4

a$=@get_message$(2)
IF LEN(a$)=2
  PRINT "Logger Version: "+HEX$(DPEEK(VARPTR(a$)))
ENDIF

PAUSE 0.3

PRINT #1,"hhhhhhh";    ! get config und PC mode on
' print #1,"hhhhhh"+chr$(0);  ! get data
' print #1,chr$(0x80)+chr$(0x25)+chr$(0)+chr$(0);

' print #1,chr$(0xb0)+chr$(4)+chr$(0)+chr$(0);
' print #1,chr$(0);   ! say helo

' print #1,chr$(0x18)+chr$(0x41)+chr$(0x0a)+chr$(0x46)+chr$(0xd8)+chr$(0x64)+chr$(0); ! write config + PC mode off
FLUSH #1
PAUSE 0.4
a$=@get_message$(21)
PRINT LEN(a$)
WHILE LEFT$(a$)=CHR$(0)
  a$=RIGHT$(a$,LEN(a$)-1)
WEND
IF LEFT$(a$)=CHR$(0xdd) AND RIGHT$(a$)=CHR$(0xaa)
  PRINT "Configuration: "
  anzdata=(PEEK(VARPTR(a$)+1) AND 255)*256+(PEEK(VARPTR(a$)+2) AND 255)
  PRINT " Data Mem: ";anzdata;" Bytes"
  PRINT " Anzahl Samples: ";anzdata/4
  data_rec=anzdata/4
  PRINT " 3: ";PEEK(VARPTR(a$)+3)
  PRINT " 4: ";PEEK(VARPTR(a$)+4)
  ' Byte 5: Resolution: 0x20 -> hour, 0x30 -> min, 0x60 -> sec
  PRINT " 5: ";PEEK(VARPTR(a$)+5)
  interval=PEEK(VARPTR(a$)+6) AND 255
  PRINT "Sample time: ";interval;" Seconds."
  PRINT " 7: ";PEEK(VARPTR(a$)+7)
  starttime$=HEX$(PEEK(VARPTR(a$)+11),2)+":"+HEX$(PEEK(VARPTR(a$)+12),2)+":"+HEX$(PEEK(VARPTR(a$)+13),2)
  startdate$=HEX$(PEEK(VARPTR(a$)+10),2)+"."+HEX$(PEEK(VARPTR(a$)+9),2)+".20"+HEX$(PEEK(VARPTR(a$)+8),2)
  PRINT "Start Date/Time: "+startdate$+" "+starttime$
  PRINT "Year:  20";HEX$(PEEK(VARPTR(a$)+8),2)
  PRINT "Month: ";HEX$(PEEK(VARPTR(a$)+9))
  PRINT "Day  : ";HEX$(PEEK(VARPTR(a$)+10))
  PRINT "hour:  ";HEX$(PEEK(VARPTR(a$)+11))
  PRINT "minute:";HEX$(PEEK(VARPTR(a$)+12))
  PRINT "second:";HEX$(PEEK(VARPTR(a$)+13))
  PRINT "temp_max: ";PEEK(VARPTR(a$)+14)
  temp_max=PEEK(VARPTR(a$)+14)
  PRINT "temp_min: ";PEEK(VARPTR(a$)+15)
  temp_min=PEEK(VARPTR(a$)+15)
  PRINT "hum_max: ";PEEK(VARPTR(a$)+16)
  hum_max=PEEK(VARPTR(a$)+16)
  PRINT "hum_min: ";PEEK(VARPTR(a$)+17)
  hum_min=PEEK(VARPTR(a$)+17)
  PRINT "18: ";PEEK(VARPTR(a$)+18)
  PRINT "19: ";PEEK(VARPTR(a$)+19)
  timestamp=(JULIAN(startdate$)-JULIAN("01.01.1970"))*24*60*60+VAL(HEX$(PEEK(VARPTR(a$)+13)))+60*VAL(HEX$(PEEK(VARPTR(a$)+12)))+3600*VAL(HEX$(PEEK(VARPTR(a$)+11)))-3600
  PRINT UNIXTIME$(timestamp)
  PRINT UNIXDATE$(timestamp)
ELSE
  PRINT "BAD configuration."
  QUIT
ENDIF
a$=@get_membank$(0)
i=1
WHILE NOT RIGHT$(a$)=CHR$(0)
  EXIT IF i>10
  a$=a$+@get_membank$(i)
  INC i
WEND
OPEN "O",#2,"VDL-141-"+UNIXDATE$(timestamp+i*interval)+"-"+UNIXTIME$(timestamp+i*interval)+".txt"
PRINT #2,"# Anzahl Samples: ";data_rec
PRINT #2,"# Start Date/Time: "+startdate$+" "+starttime$

FOR i=0 TO LEN(a$)/4-1
  temp=((PEEK(VARPTR(a$)+i*4) AND 255)*256+(PEEK(VARPTR(a$)+i*4+1) AND 255))/10
  hum=((PEEK(VARPTR(a$)+i*4+2) AND 255)*256+(PEEK(VARPTR(a$)+i*4+3) AND 255))/10
  PRINT #2,i,timestamp+i*interval,temp,hum,UNIXDATE$(timestamp+i*interval),UNIXTIME$(timestamp+i*interval)
NEXT i
CLOSE #2
' stop

' Set time:
PRINT "Set time:"
PRINT #1,CHR$(0x58)+CHR$(VAL("0x"+RIGHT$(date$,2)))+CHR$(VAL("0x"+MID$(DATE$,4,2)))+CHR$(VAL("0x"+LEFT$(DATE$,2)));
PRINT #1,CHR$(VAL("0x"+LEFT$(TIME$,2)))+CHR$(VAL("0x"+MID$(TIME$,4,2)))+CHR$(VAL("0x"+RIGHT$(TIME$,2)));
FLUSH #1
a$=@get_message$(1)
PRINT LEN(a$)
IF (ASC(a$) AND 255)=0xee
  PRINT "Time set."
ELSE
  PRINT ASC(a$)
ENDIF
PAUSE 0.1

PRINT "Start logging:"
interval=5
sek10=0x10
sek20=0x20
sek30=0x30
sek00=0x40

nowrap=0x40
noalm=0x20
noauto=0x80
unitsek=1
unitmin=2
unithour=3

PRINT #1,CHR$(sek10 OR 8)+CHR$(nowrap+noauto+unitsek)+CHR$(interval)+CHR$(temp_max)+CHR$(temp_min)+CHR$(hum_max)+CHR$(hum_min); ! write config + PC mode off
FLUSH #1
PAUSE 0.1
a$=@get_message$(1)
PRINT LEN(a$)
IF ASC(a$)=30
  PRINT "Logging started."
ENDIF
QUIT

FUNCTION get_membank$(n)
  PRINT #1,"hhhhhh"+CHR$(n); ! get data
  FLUSH #1
  PAUSE 0.3
  RETURN @get_message$(162*4)
ENDFUNCTION

FUNCTION get_message$(l)
  LOCAL t$,a,retry
  PRINT "receiving data ";
  FLUSH
  t$=""
  retry=0
  again:
  WHILE INP?(#1)
    a=INP(#1) AND 255
    IF 0
      PRINT a,HEX$(a),
    ENDIF
    IF a>=32 AND a<=ASC("z")
      PRINT CHR$(a);
    ELSE IF a>0
      PRINT "x";
    ELSE
      PRINT ".";
    ENDIF
    t$=t$+CHR$(a)
    FLUSH
    PAUSE 0.03
  WEND
  PRINT
  IF l>=0 AND l>LEN(t$)
    PRINT "Warning: only ";LEN(t$);" Bytes received."
    INC retry
    PAUSE 0.3
    IF retry<5
      PRINT "retry..."
      GOTO again
    ENDIF
  ENDIF
  RETURN t$
ENDFUNCTION
