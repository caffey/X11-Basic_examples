'
' Cut a (large) File in two parts (useful for cutting mpeg streams)
' written in X11-Basic
'
i=0
bs=1024*1024
WHILE LEN(PARAM$(i))
  a$=PARAM$(i)
  IF a$="-b"
    INC i
    b=VAL(PARAM$(i))
  ENDIF
  INC i
WEND
PRINT "Filename=";a$
IF EXIST(a$)
  OPEN "I",#1,a$
  l=LOF(#1)
  PRINT "Length: ",l,"Bytes"
  IF b>l
    PRINT a$+" is small enough. Nothing to do. quit."
    QUIT
  ENDIF
  OPEN "O",#2,a$+".1.mpg"
  OPEN "O",#3,a$+".2.mpg"
  PRINT b'"Bytes."
  PRINT a$
  WHILE b>bs
    PRINT #2,INPUT$(#1,bs);
    SUB b,bs
    SUB l,bs
    PRINT b,l
  WEND
  PRINT #2,INPUT$(#1,b);
  SUB l,b
  CLOSE #2
  WHILE l>bs
    PRINT #3,INPUT$(#1,bs);
    SUB l,bs
    PRINT l
  WEND
  PRINT #3,INPUT$(#1,l);
  CLOSE #3
ENDIF
CLOSE
QUIT
