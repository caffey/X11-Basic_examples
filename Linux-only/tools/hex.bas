#!/usr/bin/xbasic
' DUST (Portierung von ATARI ST GFA-Basic Orginalversion 1989)
' Markus Hoffmann (Modifikation Weihnachten 2004 --> HEX)
'
' should be called with one parameter, namly the the file to be
' dumped as hex...
'
i=0
WHILE LEN(PARAM$(i))
  f$=PARAM$(i)
  INC i
WEND
IF EXIST(f$)
  OPEN "I",#1,f$
  t$=INPUT$(#1,LOF(#1))
  CLOSE #1
ELSE
  PRINT "Usage:"
  PRINT PARAM$(0);" filename"
  QUIT
ENDIF
i=VARPTR(t$)
i0=i
DO
  PRINT "$";HEX$(i-i0,8)'
  FOR j%=0 TO 15
    PRINT HEX$(PEEK(i+j%) AND 255,2)'
  NEXT j%
  PRINT '
  FOR j%=0 TO 15
    a=PEEK(i+j%)
    IF a>31
      PRINT CHR$(a);
    ELSE
      PRINT ".";
    ENDIF
  NEXT j%
  PRINT
  ADD i,16
  EXIT IF i-VARPTR(t$)>LEN(t$)
LOOP
QUIT
