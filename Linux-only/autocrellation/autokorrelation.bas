' finds autocorrelation in files. It uses a very fast routine
' written in C. needs autokorrel.c and gcc
' Autokorrelationen in Files finden (z.B. Segmentgroesse)
' written in X11-Basic (c) Markus Hoffmann 2010-01-25
'
' demonstrates the use of LINK, CALL, SYM_ADR
'
f$="/path/to/a/really/big/file.dat"

OPEN "I",#1,f$
t$=INPUT$(#1,LOF(#1))
CLOSE #1

ci$=SPACE$(LEN(t$)*4)
n=LEN(t$)
n=50000
PRINT "% n=";n

' compile a shared library file from C source
IF NOT EXIST("./autokorrel.so")
  SYSTEM "gcc -O3 -shared -o autokorrel.so autokorrel.c"
ENDIF
' Link the shared library
LINK #11,"./autokorrel.so"
' Use the c routine
ret=CALL(SYM_ADR(#11,"autokorrel"),L:VARPTR(t$),L:VARPTR(ci$),L:n)

FOR i=0 TO n-1
  PRINT i,LPEEK(VARPTR(ci$)+4*i)/n
NEXT i
PRINT "% max=";ret
QUIT
