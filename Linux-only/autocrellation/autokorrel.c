
/* Autokorrelationsfinktion fuer Speicherbereiche. Es wirg gezaehlt, wieviele 
Bytes uebereinstimmen. (c) Markus Hoffman 2010 
Rueckgabe ist das Interval mit den meisten Uebereinstimmungen.*/


int autokorrel(char *a, int *b, int n) {
  int j,i;
  int m=0;
  int mm=0;
  for(i=0;i<n;i++) {
    b[i]=0;
    for(j=i;j<n;j++) {
      if(a[j]==a[j-i]) b[i]++;
    }
  }
  for(i=1;i<n;i++) {
    if(b[i]>m) {m=b[i];mm=i;}
  }
  return(mm);
}
