' rip.bas   V.1.02     (c) Markus Hoffmann
'
' Letzte Bearbeitung 2006
'
' needs: cdparanoia, mp3info, lame, mp3cover
'

PRINT "Label: ";
LINEINPUT l$
labeldir$=TRIM$(l$)
labeldir$=REPLACE$(labeldir$," ","_")
labeldir$=REPLACE$(labeldir$,"(","_")
labeldir$=REPLACE$(labeldir$,")","_")
labeldir$=REPLACE$(labeldir$,"/","_")
labeldir$=REPLACE$(labeldir$,"?","_")
labeldir$=REPLACE$(labeldir$,"*","_")
labeldir$=REPLACE$(labeldir$,"[","_")
labeldir$=REPLACE$(labeldir$,"]","_")
IF LEN(labeldir$)=0
  labeldir$="New-Label"
ENDIF
PRINT "Autor: ";
LINEINPUT a$
DIM titel$(99)

PRINT "Eingabe beenden mit '.'"
FOR i=1 TO 99
  PRINT STR$(i);": ";
  INPUT t$
  titel$(i)=t$
  EXIT IF t$="" OR t$="."
NEXT i
anz=i-1

PRINT anz;" Stuecke."

SYSTEM "mkdir "+labeldir$

FOR i=1 TO anz
  PRINT "Hole: ";titel$(i)
  SYSTEM "cdparanoia -w "+STR$(i)
  SYSTEM "lame cdda.wav"
  SYSTEM "rm -f cdda.wav"
  SYSTEM "mv cdda.wav.mp3 "+STR$(i,2,2,1)+".mp3"
  SYSTEM "mp3info -l "+CHR$(34)+l$+CHR$(34)+" -a "+CHR$(34)+a$+CHR$(34)+" -n "+STR$(i)+" "+STR$(i,2,2,1)+".mp3"
  SYSTEM "mp3info -t "+CHR$(34)+titel$(i)+CHR$(34)+" "+STR$(i,2,2,1)+".mp3"
  SYSTEM "mv "+STR$(i,2,2,1)+".mp3 "+CHR$(34)+"`mp3info -p "+CHR$(34)+"%n_%a-%t.mp3"+CHR$(34)+" "+STR$(i,2,2,1)+".mp3`"+CHR$(34)
NEXT i
SYSTEM "mp3cover"
SYSTEM "eject"
QUIT
