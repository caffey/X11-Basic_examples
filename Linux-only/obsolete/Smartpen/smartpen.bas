'
' smartpen.bas (c) Markus Hoffmann 2011
'
' demonstrated how one can use the data, which comes from a livescrive Smartpen
' Export a page as pdf. Then this program can read it.
' needs debian package: pstoedit
'

bw=930
bh=1080
top=850
SIZEW ,bw,bh
SHOWPAGE
PAUSE 0.1
CLEARW
SHOWPAGE

' Fileneame to read :

in$="Smartpen-Demo.pdf"

IF NOT EXIST(in$)
  in$=PARAM$(2)
ENDIF

tmp$="/tmp/a.svg"

SYSTEM "pstoedit "+in$+" "+tmp$

fluent=0

scale=1.5

OPEN "I",#1,tmp$

entry:
bbcount=0
WHILE NOT EOF(#1)
  LINEINPUT #1,t$
  IF GLOB(t$,"*<polyline*")
    t$=TRIM$(t$)
    a$=WORD$(t$,2)
    b$=WORD$(t$,3)
    ' print t$
    SPLIT a$,"=",0,dummy$,a$
    @drawline(a$)
    PAUSE 0.1
  ENDIF
WEND

SEEK #1,0
INC fluent
IF fluent>1
  DEFLINE ,3
ENDIF
COLOR GET_COLOR(65535,30000,0)
IF fluent<3
  GOTO entry
ENDIF
' system "convert 'bb*.bmp' a.mpg"
' system "rm -f bb*.bmp"
SYSTEM "rm -f "+tmp$
PAUSE 10
QUIT

PROCEDURE drawline(l$)
  count=0
  WHILE len(l$)
    SPLIT l$," ",0,p$,l$
    # print p$
    SPLIT p$,",",0,x$,y$
    x=VAL(x$)*scale
    y=(top-val(y$))*scale
    IF count=0
      ox=x
      oy=y
    ENDIF
    LINE ox,oy,x,y
    ox=x
    oy=y
    IF fluent
      SHOWPAGE
      ' pause 0.01
    ENDIF
    IF fluent=0 AND 0
      GET 100,200,320,200,bb$
      INC bbcount
      BSAVE "bb"+STR$(bbcount,4,4,1)+".bmp",VARPTR(bb$),LEN(bb$)
    ENDIF
    INC count
  WEND
  SHOWPAGE
RETURN
