' TV-Aufnahme.bas  (c) Markus Hoffmann 2006
'
' schedules a TV-recording (with DVB-T) by Time or by PID
'
' needs:  replex, tzap, dvb-get-epg
'
startzeit$="20:00:00"
startdate$=DATE$
endzeit$="23:45:00"
enddate$=startdate$
devicename$="/dev/dvb/adapter0/dvr0"
outputfilename$=""
rdir$="/work/tmp/"
pid=-1
now=0
i=1
zappid=-1
format$="MPEG"
@loadsettings(ENV$("HOME")+"/.DVB-T.rc")
WHILE LEN(PARAM$(i))
  IF LEFT$(PARAM$(i))="-"
    IF PARAM$(i)="--help" OR PARAM$(i)="-h"
      @intro
      @using
    ELSE IF PARAM$(i)="--version"
      @intro
      QUIT
    ELSE IF PARAM$(i)="--channel" OR PARAM$(i)="-c"
      INC i
      IF LEN(PARAM$(i))
        sender$=PARAM$(i)
      ENDIF
    ELSE IF PARAM$(i)="--device" OR PARAM$(i)="-d"
      INC i
      IF LEN(PARAM$(i))
        devicename$=PARAM$(i)
      ENDIF
    ELSE IF PARAM$(i)="--pid" OR PARAM$(i)="-p"
      INC i
      IF LEN(PARAM$(i))
        pid=VAL(PARAM$(i))
      ENDIF
    ELSE IF PARAM$(i)="--start" OR PARAM$(i)="-s"
      INC i
      IF LEN(PARAM$(i))
        startzeit$=PARAM$(i)
      ENDIF
    ELSE IF PARAM$(i)="--end" OR PARAM$(i)="-e"
      INC i
      IF LEN(PARAM$(i))
        endzeit$=PARAM$(i)
      ENDIF
    ELSE IF PARAM$(i)="--len" OR PARAM$(i)="-l"
      INC i
      IF LEN(PARAM$(i))
        starttime=@getunixtime(startdate$,startzeit$)
        endtime=starttime+@value(PARAM$(i))
        endzeit$=UNIXTIME$(endtime)
        enddate$=UNIXDATE$(endtime)
      ENDIF
    ELSE IF PARAM$(i)="--format" OR PARAM$(i)="-f"
      INC i
      IF LEN(PARAM$(i))
        format$=UPPER$(PARAM$(i))
        IF format$="MPEG2"
          format$="MPEG"
        ELSE IF format$="MPEG"
        ELSE IF format$="TS"
        ELSE
          PRINT "Unknown file format "+format$
        ENDIF
      ENDIF
    ELSE IF PARAM$(i)="--now" OR PARAM$(i)="-n"
      now=1
    ELSE IF PARAM$(i)="-o"
      INC i
      IF LEN(PARAM$(i))
        outputfilename$=PARAM$(i)
      ENDIF
    ELSE
      collect$=collect$+PARAM$(i)+" "
    ENDIF
  ELSE
    inputfile$=PARAM$(i)
    IF NOT EXIST(inputfile$)
      PRINT PARAM$(0)+": "+inputfile$+": file or path not found"
      CLR inputfile$
    ENDIF
  ENDIF
  INC i
WEND
IF LEN(sender$)=0
  PRINT "ERROR: you have not specified any channel to tune to."
  QUIT
ENDIF
IF format$="MPEG"
  endung$=".mpg"
ELSE
  endung$=".ts"
ENDIF
@tuneto(sender$)
sender$=REPLACE$(sender$," ","_")
sender$=REPLACE$(sender$,"/","_")
sender$=REPLACE$(sender$,"*","_")
IF now=0
  IF pid=-1
    PRINT "Systemzeit setzen..."
    SYSTEM "dvbdate --set"
    IF LEN(outputfile$)=0
      outputfile$=rdir$+"/"+startdate$+"-"+startzeit$+"-"+sender$+endung$
    ENDIF
    PRINT "Aufnahme von "+sender$+" ab "+startzeit$
    runs=0
  ELSE
    PRINT "trying to get epg-Information:"
    epg$=TRIM$(REPLACE$(SYSTEM$("dvb-epg-get --watch "+STR$(pid)),CHR$(10),""))
    IF LEN(epg$)<3
      PRINT "ERROR: could not get information for PID=";pid
      GOTO finishup
    ENDIF
    PRINT
    PRINT "Title:                "+ENCLOSE$(@getval$(epg$,"title"))
    PRINT "                      "+ENCLOSE$(@getval$(epg$,"des"))
    start$=@getval$(epg$,"start")
    SPLIT start$,";",0,startdate$,startzeit$
    startdate$=RIGHT$(startdate$,2)+"."+MID$(startdate$,5,2)+"."+LEFT$(startdate$,4)
    PRINT "scheduled start time: "+startdate$+" "+startzeit$
    stop$=@getval$(epg$,"stop")
    SPLIT stop$,";",0,enddate$,endzeit$
    enddate$=RIGHT$(enddate$,2)+"."+MID$(enddate$,5,2)+"."+LEFT$(enddate$,4)
    PRINT "scheduled end time:   "+enddate$+" "+endzeit$
    runs=VAL(@getval$(epg$,"RunningStatus"))
    PRINT runs
    IF LEN(outputfile$)=0
      outputfile$=rdir$+"/"+sender$+"-"+STR$(pid)+endung$
    ENDIF
  ENDIF
  starttime=@getunixtime(startdate$,startzeit$)
  endtime=@getunixtime(enddate$,endzeit$)
  PRINT "Duration:        ";(endtime-starttime)/60;" minutes."
  duration=endtime-starttime
  IF pid=-1
    WHILE (starttime-TIMER)>60
      PRINT "Warte auf Beginn..."+TIME$+CHR$(13)
      PAUSE 10
    WEND
  ELSE
    pretrigger=0
    WHILE runs<2
      epg$=TRIM$(REPLACE$(SYSTEM$("dvb-epg-get --watch "+STR$(pid)),CHR$(10),""))
      IF INSTR(epg$,"RunningStatus")=0
        PRINT "ERROR: could not get information for PID=";pid
      ELSE
        runs=VAL(@getval$(epg$,"RunningStatus"))
      ENDIF
      PRINT TIME$+" "+STR$(runs)
      IF runs=0 AND pretrigger=0
        PAUSE 30
      ELSE IF runs=1
        pretrigger=1
        PAUSE 5
      ENDIF
    WEND
  ENDIF
ELSE
  IF LEN(outputfile$)=0
    outputfile$=rdir$+"/"+sender$+"-"+STR$(STIMER)+endung$
  ENDIF
ENDIF

PRINT " Start der Aufnahme "+outputfile$
atimer=TIMER
recordcmd$="replex -j -l 1000 -t MPEG2 -o "+outputfile$+" "+devicename$
IF format$="MPEG"
  SYSTEM "xterm -geometry 80x16 -bg \#ffff60 -sb -e '"+recordcmd$+"' &"
ELSE
  SYSTEM "xterm -geometry 40x5 -bg \#ffff60 -sb -e 'cat "+devicename$+" > "+outputfile$+"' &"
ENDIF
PAUSE 8
IF format$="MPEG"
  catpid=@getpid(recordcmd$)
ELSE
  catpid=@getpid("cat "+dvicename$)
ENDIF
PRINT "PID2=";catpid
IF catpid=-1
  PRINT "ERROR: could not start recording ..."
  GOTO finishup
ENDIF
IF now=0
  IF pid=-1
    WHILE (endtime-TIMER)>0
      PRINT "Warte auf Ende der Aufnahme..."+TIME$+CHR$(13)
      PAUSE 10
    WEND
  ELSE
    WHILE runs<>0 OR (TIMER-atimer)<duration
      epg$=TRIM$(REPLACE$(SYSTEM$("dvb-epg-get -s --watch "+STR$(pid)),CHR$(10),""))
      IF INSTR(epg$,"RunningStatus")=0
        PRINT "ERROR: could not get information for PID=";pid
      ELSE
        runs=VAL(@getval$(epg$,"RunningStatus"))
      ENDIF
      PRINT TIME$+" "+STR$(INT((TIMER-atimer)/60))+"/"+STR$(INT(duration/60))+" "+STR$(runs)
      PAUSE 10
    WEND
  ENDIF
  PAUSE 60
ELSE
  g$=""
  WHILE UPPER$(g$)<>"STOP"
    PRINT "type 'stop' to stop recording"
    INPUT g$
  WEND
ENDIF
SYSTEM "kill "+STR$(catpid)
PAUSE 2
finishup:
SYSTEM "kill "+STR$(zappid)
' PRINT "Umwandeln in MPEG: "
' SPLIT outputfile$,".ts",0,rumpf$,a$
' PRINT "replex -t MPEG2 -o "+rumpf$+".mpg "+outputfile$
QUIT
PROCEDURE intro
  PRINT "TV-Aufnahme V.1.00 (c) Markus Hoffmann 2006"
  VERSION
RETURN
PROCEDURE using
  PRINT "Usage: "+PARAM$(0)+" [options] "
  PRINT "Options:"
  PRINT "  -h, --help               Display this information"
  PRINT "  -d, --device  <dev>      use device dev ["+devicename$+"]"
  PRINT "  -c, --channel <ch>       record from channel cd ["+sender$+"]"
  PRINT "  -s, --start <hh:mm:ss>   specify start time"
  PRINT "  -e, --end <hh:mm:ss>     specify end time"
  PRINT "  -n, --now                start recording immediately"
  PRINT "  -p, --pid <pid>          record program #pid"
  PRINT "  -f, --format <fmt>       record as MPEG2 or TS ["+format$+"]"
  PRINT "  -o <file>                Place the output into <file>"
RETURN

PROCEDURE tuneto(sender$)
  IF zappid<>-1
    SYSTEM "kill "+STR$(zappid)
  ENDIF
  SYSTEM "xterm -geometry 80x12 -bg \#a3ff60 -sb -e 'tzap -r "+ENCLOSE$(sender$)+"' &"
  PAUSE 4
  zappid=@getpid("tzap")
  IF zappid=-1
    PRINT "ERROR"
    QUIT
  ENDIF
RETURN
' Zeitwerte
FUNCTION value(v$)
  LOCAL g
  g=1
  IF RIGHT$(v$)="d"
    g=3600*24
    v$=LEFT$(v$,LEN(v$)-1)
  ELSE IF RIGHT$(v$)="h"
    g=3600
    v$=LEFT$(v$,LEN(v$)-1)
  ELSE IF RIGHT$(v$)="m"
    g=60
    v$=LEFT$(v$,LEN(v$)-1)
  ENDIF
  RETURN g*VAL(v$)
ENDFUNCTION

FUNCTION getunixtime(dat$,tim$)
  LOCAL h,m,s
  h=VAL(LEFT$(tim$,2))
  m=VAL(MID$(tim$,4,2))
  s=VAL(RIGHT$(tim$,2))
  RETURN (JULIAN(dat$)-JULIAN("01.01.1970"))*24*60*60+(h-1)*3600+m*60+s
ENDFUNCTION
FUNCTION getpid(f$)
  LOCAL t$,pid,a$
  pid=-1
  t$=SYSTEM$("ps x")
  WHILE LEN(t$)
    SPLIT t$,CHR$(10),0,a$,t$
    IF MID$(a$,28,LEN(f$))=f$
      pid=VAL(LEFT$(a$,6))
    ENDIF
  WEND
  RETURN pid
ENDFUNCTION
FUNCTION getval$(t$,f$)
  LOCAL a$,val$
  val$=""
  SPLIT t$," ",1,a$,t$
  WHILE LEN(a$)
    a$=TRIM$(a$)
    SPLIT a$,"=",1,name$,val$
    EXIT IF UPPER$(name$)=UPPER$(f$)
    val$=""
    SPLIT t$," ",1,a$,t$
  WEND
  IF LEFT$(val$)=CHR$(34)
    val$=DECLOSE$(val$)
  ENDIF
  RETURN val$
ENDFUNCTION
PROCEDURE loadsettings(f$)
  LOCAL t$,a$,b$
  IF EXIST(f$)
    PRINT "load settings "+f$+"[";
    OPEN "I",#9,f$
    WHILE NOT EOF(#9)
      LINEINPUT #9,t$
      t$=TRIM$(t$)
      IF LEFT$(t$)<>"#"
        SPLIT t$,"=",1,a$,b$
        IF UPPER$(a$)="DVR_DEVICE"
          devicename$=b$
        ELSE IF UPPER$(a$)="CHANNEL"
          sender$=b$
        ELSE IF UPPER$(a$)="FORMAT"
          format$=b$
        ELSE IF UPPER$(a$)="RDIR"
          rdir$=b$
        ENDIF
        PRINT ".";
        FLUSH
      ENDIF
    WEND
    CLOSE #9
    PRINT "]"
  ENDIF
RETURN
