' Make a SVCD out of a set of .mpg and other video Files
' (c) Markus Hoffmann 2004
'
' needs ffmpeg vcdxbuild cdrdao
'
CLR anzvideos
DIM vob$(100),in$(100)
CLR anztextpages

volume_id$=LEFT$(UPPER$(ENV$("PWD"))+STRING$(30,"_"),30)
volume_id$=REPLACE$(volume_id$,"/","_")
PRINT "Volume ID: "+volume_id$

title$="Hauptmen�"

'in$(0)="item0001.mpg"
in$(0)="/work/tmp/Filme/Drestner-Fauenkirche-Doku.mpg"
anzvideos=1

@make_main_menu()
@transcode_videos
@make_picture_page("/work/Fotos/2005-07-09/IMG_3251.JPG")
@make_text_page("makesvcd.bas")
@make_xml_file()

GOTO burn

i=1
WHILE LEN(PARAM$(i))
  IF LEFT$(PARAM$(i))="-"
    IF param$(i)="--help" OR PARAM$(i)="-h"
      @intro
      @using
    ELSE IF PARAM$(i)="--version"
      @intro
      QUIT
    ELSE
      PRINT "Unknown Option: ";PARAM$(i)
    ENDIF
  ELSE
    IF @exist(PARAM$(i))
      IF RIGHT$(PARAM$(i),4)<>".bas"
        in$(anzvideos)=PARAM$(i)
        INC anzvideos
      ENDIF
    ELSE
      PRINT PARAM$(i);" does not exist. skipped"
    ENDIF
  ENDIF
  INC i
WEND
PRINT "got ";anzvideos;" Files."
IF anzvideos=0
  QUIT
ENDIF
' force=1
' system "mkdir -v svcd"

' Files convertieren
FOR i=0 TO anzvideos-1
  vob$(i)=@rumpf$(in$(i))+".o.mpg"
  IF NOT @exist(vob$(i))
    t$="nice ffmpeg -i "+in$(i)+"  -target pal-svcd "+vob$(i)
    PRINT t$
    SYSTEM t$
    PAUSE 5
  ENDIF
NEXT i

burn:

' Image creieren

t$="nice vcdxbuild -p -b svcd.bin -c svcd.cue SVCD.xml  "
SYSTEM t$

SYSTEM "cdrdao write --device 0,0,0 svcd.cue"
QUIT

PROCEDURE intro
  PRINT "makesvcd V.1.01 (c) Markus Hoffmann 2005"
  VERSION
RETURN
PROCEDURE using
  PRINT "Usage: makesvcd [options]"
  PRINT "Options:"
  PRINT "  -h, --help               Display this information"
  PRINT "  -q, --quiet              be more quiet"
  PRINT "  -v, --verbose            be more verbose"
  PRINT "  -b <blocksize>           set blocksize to b [";blocksize;"]"
  PRINT "  -c <comment>             Place a comment into file"
  PRINT "  -e <key>                 encrypt file with key"
  PRINT "  -o <file>                Place the output into <file>"
RETURN

PROCEDURE make_xml_file
  LOCAL i
  OPEN "O",#1,"SVCD.xml"
  PRINT #1,"<?xml version="+ENCLOSE$("1.0")+"?>"

  PRINT #1,"<!DOCTYPE videocd PUBLIC "+ENCLOSE$("-//GNU//DTD VideoCD//EN")+" "+ENCLOSE$("http://www.gnu.org/software/vcdimager/videocd.dtd")+">"
  PRINT #1,"<videocd xmlns="+ENCLOSE$("http://www.gnu.org/software/vcdimager/1.0/")+" class="+ENCLOSE$("svcd")+" version="+ENCLOSE$("1.0")+">"
  PRINT #1,"  <option name="+ENCLOSE$("update scan offsets")+" value="+ENCLOSE$("true")+"/>"

  PRINT #1,"  <info>"
  PRINT #1,"	<album-id>"+STR$(crc(volume_id$))+"</album-id>"
  PRINT #1,"	<volume-count>1</volume-count>"
  PRINT #1,"	<volume-number>1</volume-number>"
  PRINT #1,"	<restriction>0</restriction>"
  PRINT #1,"  </info>"

  PRINT #1,"  <pvd>"
  PRINT #1,"	<volume-id>"+volume_id$+"</volume-id>"
  PRINT #1,"	<system-id>CD-RTOS CD-BRIDGE</system-id>"
  PRINT #1,"	<application-id>X11-BASIC</application-id>"
  PRINT #1,"	<preparer-id>"+STR$(timer)+"</preparer-id>"
  PRINT #1,"	<publisher-id></publisher-id>"
  PRINT #1,"  </pvd>"

  ' check for text page files to create the segment part of the
  ' XML file
  IF anztextpages
    i=anztextpages
    PRINT #1,"  <segment-items>"
    WHILE i
      PRINT #1,"    <segment-item src="+ENCLOSE$("summary_"+STR$(i-1)+".mpeg")+" id="+ENCLOSE$("segment"+STR$(i)+"-menu")+"/>"
      DEC i
    WEND
    PRINT #1,"  </segment-items>"
  ENDIF

  IF anzvideos
    i=anzvideos
    PRINT #1,"  <sequence-items>"
    WHILE i
      PRINT #1,"    <sequence-item src="+ENCLOSE$(in$(i-1))+" id="+ENCLOSE$("sequence-"+STR$(i))+"/>"
      DEC i
    WEND
    '   print #1,"<default-entry id="+chr$(34)+"entry-000"+chr$(34)+"/>"
    PRINT #1,"  </sequence-items>"
  ENDIF

  PRINT #1,"  <pbc>"

  PRINT #1,"    <selection id="+CHR$(34)+"menu"+CHR$(34)+">"
  PRINT #1,"      <bsn>1</bsn>"
  PRINT #1,"      <next ref="+CHR$(34)+"lid-1"+CHR$(34)+"/>"
  PRINT #1,"      <default ref="+CHR$(34)+"menu"+CHR$(34)+"/>"
  PRINT #1,"      <timeout ref="+CHR$(34)+"menu"+CHR$(34)+"/>"
  PRINT #1,"      <wait>60</wait>"
  PRINT #1,"      <loop jump-timing="+CHR$(34)+"immediate"+CHR$(34)+">1</loop>"
  PRINT #1,"      <play-item ref="+CHR$(34)+"segment1-menu"+CHR$(34)+"/>"
  PRINT #1,"      <select ref="+CHR$(34)+"lid-1"+CHR$(34)+"/>"
  PRINT #1,"    </selection>"

  IF anzvideos
    i=anzvideos
    WHILE i
      PRINT #1,"    <playlist id="+CHR$(34)+"lid-"+STR$(i)+CHR$(34)+">"
      IF i<anzvideos
        PRINT #1,"      <prev ref="+CHR$(34)+"lid-"+STR$(i+1)+CHR$(34)+"/>"
      ELSE
        PRINT #1,"      <prev ref="+CHR$(34)+"menu"+CHR$(34)+"/>"
      ENDIF
      IF i>1
        PRINT #1,"      <next ref="+CHR$(34)+"lid-"+STR$(i-1)+CHR$(34)+"/>"
      ELSE
        IF anztextpages
          PRINT #1,"      <next ref="+CHR$(34)+"lid-dis"+CHR$(34)+"/>"
        ELSE
          PRINT #1,"      <next ref="+CHR$(34)+"lid-end"+CHR$(34)+"/>"
        ENDIF
      ENDIF
      PRINT #1,"      <return ref="+CHR$(34)+"menu"+CHR$(34)+"/>"
      PRINT #1,"      <wait>5</wait>"
      PRINT #1,"      <autowait>0</autowait>"
      PRINT #1,"      <play-item ref="+CHR$(34)+"sequence-"+STR$(i)+CHR$(34)+"/>"
      PRINT #1,"    </playlist>"
      DEC i
    WEND
  ENDIF
  IF anztextpages
    PRINT #1,"    <playlist id="+CHR$(34)+"lid-dis"+CHR$(34)+">"
    PRINT #1,"      <next ref="+CHR$(34)+"menu"+CHR$(34)+"/>"
    PRINT #1,"      <return ref="+CHR$(34)+"menu"+CHR$(34)+"/>"
    PRINT #1,"      <wait>6</wait>"
    PRINT #1,"      <autowait>0</autowait>"
    i=anztextpages
    WHILE i
      PRINT #1,"      <play-item ref="+CHR$(34)+"segment"+STR$(i)+"-menu"+CHR$(34)+"/>"
      DEC i
    WEND
    PRINT #1,"    </playlist>"
  ENDIF
  PRINT #1,"    <endlist id="+CHR$(34)+"lid-end"+CHR$(34)+" rejected="+CHR$(34)+"true"+CHR$(34)+"/>"
  PRINT #1,"  </pbc>"
  PRINT #1,"</videocd>"
  CLOSE #1
RETURN

PROCEDURE transcode_videos
  LOCAL i
  PRINT "Transcodiere Videos: "
  IF anzvideos
    i=anzvideos
    WHILE i
      DEC i
      PRINT in$(i)
      vob$(i)=@rumpf$(in$(i))+".vob"
      PRINT vob$(i)
      IF NOT EXIST(vob$(i))
        SYSTEM "nice ffmpeg -i "+in$(i)+" -target pal-svcd "+vob$(i)
      ENDIF
    WEND
  ENDIF
RETURN

PROCEDURE make_text_page(file$)
  LOCAL count,ccc,t$
  count=0
  OPEN "I",#1,file$
  WHILE NOT EOF(#1)
    OPEN "O",#2,"ttt"
    ccc=0
    WHILE EOF(#1)=0 AND ccc<18
      LINEINPUT #1,t$
      PRINT #2,LEFT$(t$+SPACE$(50),50)
      INC ccc
    WEND
    PRINT #2,"========================"
    PRINT #2,"Weiter --> NEXT druecken"
    CLOSE #2
    IF NOT EXIST("summary_"+STR$(count+anztextpages)+".mpeg")
      t$=system$("cat ttt | pbmtext | pnmscale --width 704 --height 576 | pgmtoppm black-burlywood | ppmtoy4m -Ip -F25:1 | yuvscaler -O SIZE_704x576 | mpeg2enc -f 7 -T 120 -a 2 -o summary.m2v")
      t$=system$("rm -f ttt")
      t$=system$("mplex -f 7 -o summary_"+STR$(count+anztextpages)+".mpeg summary.m2v")
      SYSTEM "rm -f summary.m2v"
    ENDIF
    INC count
  WEND
  ADD anztextpages,count
  CLOSE #1
RETURN
PROCEDURE make_main_menu()
  LOCAL t$
  PRINT "creating main menu ..."
  IF not EXIST("summary_"+STR$(count+anztextpages)+".mpeg")
    OPEN "O",#2,"ttt"
    PRINT #2,title$
    PRINT #2,"========================"
    FOR i=0 TO anzvideos-1
      PRINT #2,STR$(i,3,3)+" "+in$(i)
    NEXT i
    PRINT #2,"========================"
    PRINT #2,"Weiter --> NEXT druecken"
    CLOSE #2
    t$=SYSTEM$("cat ttt | pbmtext -builtin fixed | pnmscale --width 704 --height 576 | pgmtoppm yellow-black | ppmtoy4m -Ip -F25:1 | yuvscaler -O SIZE_704x576 | mpeg2enc -f 7 -T 120 -a 2 -o summary.m2v")
    t$=SYSTEM$("rm -f ttt")
    t$=SYSTEM$("mplex -f 7 -o summary_"+STR$(count+anztextpages)+".mpeg summary.m2v")
    t$=SYSTEM$("rm -f summary.m2v")
  ENDIF
  ADD anztextpages,1
RETURN
PROCEDURE make_picture_page(file$)
  LOCAL t$
  PRINT "creating picture segment ..."
  IF NOT EXIST("summary_"+STR$(count+anztextpages)+".mpeg")
    SYSTEM "convert "+file$+" ttt.pnm"
    t$=SYSTEM$("pnmscale --width 704 --height 576 ttt.pnm | ppmtoy4m -Ip -F25:1 | yuvscaler -O SIZE_704x576 | mpeg2enc -f 7 -T 120 -a 2 -o summary.m2v")
    t$=SYSTEM$("mplex -f 7 -o summary_"+STR$(count+anztextpages)+".mpeg summary.m2v")
    t$=SYSTEM$("rm -f summary.m2v ttt.pnm")
  ENDIF
  ADD anztextpages,1
RETURN

FUNCTION rumpf$(in$)
  LOCAL rumpf$,a$
  rumpf$=in$
  WHILE LEN(rumpf$)
    SPLIT rumpf$,"/",1,a$,rumpf$
  WEND
  RETURN a$
ENDFUNCTION

FUNCTION exist(f$)
  ' exist does not work for Files larger than 2 GB. Dont know why.
  LOCAL t$
  IF EXIST(f$)
    RETURN TRUE
  ELSE
    t$=SYSTEM$("ls "+f$)
    IF LEN(t$)
      RETURN TRUE
    ENDIF
  ENDIF
  RETURN 0
ENDFUNCTION
