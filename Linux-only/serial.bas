' Demonstration how to fully control the serial RS232 interface
' with X11-Basic     (c) Markus Hoffmann 2007-03-17

device$="/dev/ttyS0"
OPEN "U",#1,device$

' Baud rate, parity etc.

' The hardware modem control lines can be monitored or modified by the
' ioctl() function.
' The form of this call is:

' a%=ioctl(#1,COMMAND, varptr(flags%))

'  The COMMANDS and their action are:

TIOCMBIS=0x5416   ! turn on control lines depending upon which bits are set
'                   in flags.
TIOCMBIC=0x5417   ! turn off control lines depending upon which bits are
'                   unset in flags.
TIOCMGET=0x5415   ! the appropriate bits are set in flags according to the
'                   current status
TIOCMSET=0x5418   ! the state of the UART is changed according to which bits
'                   are set/unset in 'flags'
'
' The bit pattern of flags refer to the following control lines:

'o  TIOCM_LE=  1     Line enable
'o  TIOCM_DTR= 2     Data Terminal Ready
'o  TIOCM_RTS= 4     Request to send
'o  TIOCM_ST=  8     Secondary transmit
'o  TIOCM_SR= 16     Secondary receive
'o  TIOCM_CTS=32     Clear to send
'o  TIOCM_CAR=64     Carrier detect
'o  TIOCM_RNG=128    Ring
'o  TIOCM_DSR=256    Data set ready

bits$()=["LE","DTR","RTS","ST","SR","CTS","CAR","RNG","DSR"]

'  It should be noted that some of these bits are controlled by the modem
'  and the UART cannot change them but their status can be sensed by
'  TIOCMGET.  Also, most Personal Computers do not provide hardware for
'  secondary transmit and receive.

'  There are also a pair of ioctl() to monitor these lines.  They are
'  undocumented as far as I have learned.  The commands are TIOCMIWAIT
'  and TCIOGICOUNT.  They also differ between versions of the Linux
'  kernel.

PRINT device$

' Get the line states
flags%=0
a%=IOCTL(#1,TIOCMGET,VARPTR(flags%))
PRINT BIN$(flags%)
FOR i=0 TO DIM?(bits$())-1
  IF BTST(flags%,i)
    PRINT bits$(i);", ";
  ENDIF
NEXT i
PRINT
CLOSE #1
QUIT
