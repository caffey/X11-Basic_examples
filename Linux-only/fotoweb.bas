#!/usr/bin/xbasic

' This program creates a html-File and thumbnail-pictures of all
' *.jpg, *.gif and *.png files
'                                (c) Markus Hoffmann 2003-2010
'
' Revision 1 ??-2003
' Revision 3 07-2005
' Revision 4 11-2005
' Revision 5 12-2005
' Revision 6 12-2006
' Revision 7 11-2009 (convert angepasst)
' Revision 8 02-2010 README Dateien in Header, wenn vorhanden

tmpfile$="fotoweb"+STR$(TIMER)

' Unterordner herausfinden

ordner$=""
tmp$=SYSTEM$("ls -l")
WHILE LEN(tmp$)
  SPLIT tmp$,CHR$(10),0,t$,tmp$
  PRINT ".";
  FLUSH
  IF LEFT$(t$)="d"
    t$=TRIM$(t$)
    WHILE LEN(t$)
      SPLIT t$,CHR$(32),0,a$,t$
    WEND
    IF a$<>"picturedata" AND a$<>"thumbs"
      ordner$=ordner$+"<li> <a href="+CHR$(34)+a$+"/fotoweb.html"+CHR$(34)+">"+a$+"</a>"+CHR$(13)
    ENDIF
  ENDIF
WEND

tmp$=SYSTEM$("ls *.jpg *.JPG *.gif *.png *.eps *.fig *.bmp *.mp4 *.MP4 *.avi *.AVI *.mpg *.MPG")
IF LEN(tmp$)<2
  PRINT "No pictures found"
  QUIT
ENDIF

OPEN "O",#2,"fotoweb.html"
PRINT #2,"<HTML> <HEAD> <TITLE>fotoweb V.1.03 (c) Markus Hoffmann</TITLE></HEAD>"
PRINT #2,"<BODY bgcolor="+ENCLOSE$("#ffffff")+" link=2200aa vlink=008800>"

' README einbinden, wenn vorhanden
IF EXIST("README")
  OPEN "I",#33,"README"
  rm$=INPUT$(#33,lof(#33))
  CLOSE #33
  PRINT #2,rm$
ELSE
  PRINT #2,"<center><h1>MH picture galery</h1></center>"
ENDIF
PRINT #2,"<h5>"+env$("PWD")+"</h5>"
IF LEN(ordner$)
  PRINT #2,"<h2>Subfolders:</h2><ul>"+ordner$+"</ul><p>"
ENDIF

tmp2$=tmp$
IF NOT EXIST("thumbs")
  PRINT "creating thumbnails ..."
  SYSTEM "mkdir thumbs"
  SYSTEM "chmod 755 thumbs"
ENDIF
IF NOT EXIST("picturedata")
  PRINT "creating picturedata ..."
  SYSTEM "mkdir picturedata"
  SYSTEM "chmod 755 picturedata"
ENDIF

PRINT "creating [";
FLUSH
WHILE LEN(tmp$)
  SPLIT tmp$,CHR$(10),0,t$,tmp$
  FLUSH
  ' Creating Thumbnail
  IF not EXIST("thumbs/"+t$+".png")
    IF upper$(RIGHT$(t$,3))="AVI"
      SYSTEM "lav2yuv +p -f 1 "+CHR$(34)+t$+CHR$(34)+" | y4mtoppm | pnmsplit - "+CHR$(34)+"y-%d.ppm"+CHR$(34)
      SYSTEM "convert -fill black -colors 64 +dither -comment 'fotoweb V.1.03' -geometry x76 -draw 'text 0,10 "+CHR$(34)+"%m:%wx%h"+CHR$(34)+"' -fill red  -font x:fixed -draw 'text 0,74 "+CHR$(34)+"oooooooooooooo"+CHR$(34)+"' -border 2x2 "+CHR$(34)+"y-0.ppm"+CHR$(34)+" "+CHR$(34)+"thumbs/"+t$+".png"+CHR$(34)
      SYSTEM "rm -f y-0.ppm"
    ELSE if UPPER$(RIGHT$(t$,3))="MPG"
      SYSTEM "convert -fill black -colors 64 +dither -comment 'fotoweb V.1.03' -geometry x76 -draw 'text 0,10 "+CHR$(34)+"%m:%wx%h"+CHR$(34)+"' -fill red  -font x:fixed -draw 'text 0,74 "+CHR$(34)+"oooooooooooooo"+CHR$(34)+"' -border 2x2 -quiet "+CHR$(34)+t$+CHR$(34)+"[10] "+CHR$(34)+"thumbs/"+t$+".png"+CHR$(34)
    ELSE
      info1$=system$("echo `identify -format "+CHR$(34)+"%f"+CHR$(34)+" "+CHR$(34)+t$+CHR$(34)+"`")
      info2$=system$("echo `identify -format "+CHR$(34)+"%m:%wx%h"+CHR$(34)+" "+CHR$(34)+t$+CHR$(34)+"`")
      SYSTEM "echo `identify -format "+CHR$(34)+"%m:Name: %f, %b Bytes, %wx%h"+CHR$(34)+" "+CHR$(34)+t$+CHR$(34)+"`"
      SYSTEM "convert -fill black -colors 64 +dither -comment 'fotoweb V.1.03' -geometry x76 -draw 'text 0,10 "+CHR$(34)+info2$+CHR$(34)+"' -fill red  -font x:fixed -draw 'text 0,74 "+CHR$(34)+info1$+CHR$(34)+"' -border 2x2 "+CHR$(34)+t$+CHR$(34)+" "+CHR$(34)+"thumbs/"+t$+".png"+CHR$(34)
    ENDIF
    SYSTEM "chmod 644 "+CHR$(34)+"thumbs/"+t$+".png"+CHR$(34)
    PRINT "T";
  ENDIF
  ' Creating Picturedata
  IF upper$(RIGHT$(t$,3))="AVI"
    ' Hier nicht umwandeln
    PRINT #2,"<a href="+CHR$(34)+""+t$+""+CHR$(34)+"><IMG SRC="+CHR$(34)+"thumbs/"+t$+".png"+CHR$(34)+"></A>"
    SYSTEM "chmod 644 "+CHR$(34)+t$+CHR$(34)
    PRINT ".";
  ELSE
    IF not EXIST("picturedata/"+t$+".jpg")
      ' Finde raus, ob das Bild gross genug ist, um verkleinert zu werden:
      '
      '
      idfy$=system$("identify "+CHR$(34)+t$+CHR$(34))
      SPLIT idfy$,CHR$(10),0,ia$,ib$
      SPLIT ia$," ",0,ia$,ib$
      SPLIT ib$," ",0,ia$,ib$
      SPLIT ib$," ",0,ia$,ib$
      PRINT ia$;
      SPLIT ia$,"x",0,ia$,ib$
      IF val(ia$)>800
        PRINT "C";
        SYSTEM "convert -geometry 760  "+CHR$(34)+t$+CHR$(34)+" "+CHR$(34)+"picturedata/"+t$+".jpg"+CHR$(34)
        PRINT #2,"<a href="+CHR$(34)+"picturedata/"+t$+".jpg"+CHR$(34)+"><IMG SRC="+CHR$(34)+"thumbs/"+t$+".png"+CHR$(34)+"></A>"
        SYSTEM "chmod 644 "+CHR$(34)+"picturedata/"+t$+".jpg"+CHR$(34)
        PRINT ".";
      ELSE
        PRINT "L";
        ' system "ln -s "+chr$(34)+"../"+t$+chr$(34)+" "+chr$(34)+"picturedata/"+t$+".jpg"+chr$(34)
        PRINT #2,"<a href="+CHR$(34)+""+t$+""+CHR$(34)+"><IMG SRC="+CHR$(34)+"thumbs/"+t$+".png"+CHR$(34)+"></A>"
        SYSTEM "chmod 644 "+CHR$(34)+t$+CHR$(34)
        PRINT ".";
      ENDIF
    ELSE
      PRINT #2,"<a href="+CHR$(34)+"picturedata/"+t$+".jpg"+CHR$(34)+"><IMG SRC="+CHR$(34)+"thumbs/"+t$+".png"+CHR$(34)+"></A>"
      PRINT "-";
    ENDIF
  ENDIF
  FLUSH
  FLUSH #2
WEND
PRINT "]"
PRINT #2,"<HR><br>"
PRINT #2,"<I>Kommentare oder Anregungen zu dieser WWW-Seite bitte "
PRINT #2,"<A HREF="+CHR$(34)+"mailto:user@domain.de"+CHR$(34)+">hierhin</A>.</I><P>"
PRINT #2,"<FONT FACE="+CHR$(34)+"ARIAL,HELVETICA"+CHR$(34)+" SIZE=1>"
PRINT #2,"<SCRIPT Language="+CHR$(34)+"JavaScript"+CHR$(34)+">"
PRINT #2," <!--"
PRINT #2,"  document.write('Letzte Bearbeitung: '+document.lastModified);"
PRINT #2," //-->"
PRINT #2,"</SCRIPT></FONT>"
PRINT #2,"</BODY></html>"
CLOSE #2
SYSTEM "chmod 644 fotoweb.html"
QUIT

