' Sucht doppelte identische Dateien
' noch sehr rudimentaer
' (C) Markus Hoffmann    2004

DIM dat$(64)
i=0
WHILE LEN(PARAM$(i))
  such$=PARAM$(i)
  INC i
WEND

t$=SYSTEM$("locate "+such$)

WHILE LEN(t$)
  SPLIT t$,CHR$(10),0,a$,t$
  IF LEFT$(a$,5)="/home"
    @doit(a$)
  ENDIF
WEND
QUIT

PROCEDURE doit(f$)
  LOCAL a$,i
  i=0
  a$=@filen$(f$)
  PRINT a$+": [";
  g$=system$("locate "+CHR$(34)+a$+CHR$(34))
  WHILE len(g$)
    SPLIT g$,CHR$(10),0,b$,g$
    IF @filen$(b$)=a$
      IF exist(b$) AND EXIST(f$)
        IF device(b$)=device(f$)
          dat$(i)=b$
          INC i
          PRINT ".";
          FLUSH
        ENDIF
      ENDIF
    ENDIF
  WEND
  PRINT "]"
  IF i>1
    @combine(i)
  ENDIF
RETURN

PROCEDURE combine(n)
  LOCAL i
  IF n>1
    FOR i=1 TO n-1
      @combine2(0,i)
    NEXT i
  ENDIF
RETURN

PROCEDURE combine2(a1,a2)
  IF dat$(a1)<>dat$(a2)
    IF nlink(dat$(a2))=1
      o$=system$("diff -q -s "+CHR$(34)+dat$(a1)+CHR$(34)+" "+CHR$(34)+dat$(a2)+CHR$(34))
      IF right$(o$,11)="identisch."+CHR$(10) OR RIGHT$(o$,10)="identical"+CHR$(10)
        SYSTEM "ls -l "+CHR$(34)+dat$(a1)+CHR$(34)+" "+CHR$(34)+dat$(a2)+CHR$(34)
        INPUT "Hardlink ",s$
        IF s$="j"
          SYSTEM "ln -f -n "+CHR$(34)+dat$(a1)+CHR$(34)+" "+CHR$(34)+dat$(a2)+CHR$(34)
          SYSTEM "ls -l "+CHR$(34)+dat$(a1)+CHR$(34)+" "+CHR$(34)+dat$(a2)+CHR$(34)
        ENDIF
      ENDIF
    ENDIF
  ENDIF
RETURN

FUNCTION filen$(path$)
  LOCAL a$
  WHILE LEN(path$)
    SPLIT path$,"/",0,a$,path$
  WEND
  RETURN a$
ENDFUNCTION
