'--------------------------------------------------
' Download Program to download the Intel Hex file
' Created by Keil or sdcc into the MSC1210
'
' port to linux (c) Markus Hoffmann 2007
' the original WINDOWS program was written
' by Russell Anderson with
' copyright by Texas Instruments
'--------------------------------------------------  V.1.00
' (letzte Bearbeitung: 13.03.2007)
'
'
' todo: serial port handshaking
'
'
i=1
inputfile$=""
device$="/dev/ttyS1"
verbose=0
frequency=22.1184     ! Core Fequency in MHz
esize=-1
' --------------------- Get Command Line Arguments -------------------
WHILE LEN(PARAM$(i))
  IF LEFT$(PARAM$(i))="-"
    IF PARAM$(i)="--help" OR PARAM$(i)="-h"
      @intro
      @using
    ELSE IF PARAM$(i)="--version"
      @intro
      QUIT
    ELSE IF PARAM$(i)="-d" OR PARAM$(i)="--device"
      INC i
      IF LEN(PARAM$(i))
        device$=PARAM$(i)
      ENDIF
    ELSE IF PARAM$(i)="-e" OR PARAM$(i)="--esize"
      INC i
      IF LEN(PARAM$(i))
        esize=VAL(PARAM$(i))
      ENDIF
    ELSE IF PARAM$(i)="-f" OR PARAM$(i)="--frequency" OR PARAM$(i)="-X"
      INC i
      IF LEN(PARAM$(i))
        frequency=VAL(PARAM$(i))
      ENDIF
    ELSE IF PARAM$(i)="-p" OR PARAM$(i)="--port"
      INC i
      IF LEN(PARAM$(i))
        port=VAL(PARAM$(i))
      ENDIF
    ELSE IF PARAM$(i)="-H" OR PARAM$(i)="--hwconfig"
      hwconfig=true
    ELSE IF PARAM$(i)="-v" OR PARAM$(i)="--verbose"
      INC verbose
    ELSE IF PARAM$(i)="-q" OR PARAM$(i)="--quiet"
      DEC verbose
    ENDIF
  ELSE
    inputfile$=PARAM$(i)
    IF not EXIST(inputfile$)
      PRINT param$(0)+": "+inputfile$+": file or path not found"
      CLR inputfile$
    ENDIF
  ENDIF
  INC i
WEND
IF len(inputfile$)
  IF not EXIST(inputfile$)
    PRINT "File "+inputfile$+" not found. ABORT"
    QUIT
  ENDIF
  IF frequency<0.95 OR frequency>50
    PRINT "Illegal Frequency value, ABORT"
    QUIT
  ENDIF
  freq=frequency
  oneusec=freq-0.5
  Ftpres=(7*freq)/(Oneusec+1)-0.5
  value=1000*freq-0.5
  temp=value/256-0.5
  OneMSH=Temp
  OneMSL=value-(Temp*256)
  Ftscal=(4000*freq)/(value+1)-0.5
  Hundms=(100000*freq)/(value+1)-0.5

  @doit
ELSE
  PRINT "downloader: No input files"
ENDIF
QUIT

PROCEDURE intro
  PRINT "MSC1210 HEX File downloader V.1.00 (c) Markus Hoffmann 2007"
  VERSION
RETURN
PROCEDURE using
  PRINT "Usage: download [options] file..."
  PRINT "Options:"
  PRINT "  -d, --device  <device>   Set serial device ["+device$+"]"
  PRINT "  -e, --esize  <num>       Set esize ["+STR$(esize)+"]"
  PRINT "  -f, --frequency <freq>   Set the core frequency ["+STR$(frequency)+"] MHz"
  PRINT "  -H, --hwconfig           Also erase HWconfig"
  PRINT "  -h, --help               Display this information"
  PRINT "  -v                       be more verbose"
  PRINT "  -q                       be more quiet"
  PRINT "  -X                       Same as -f"
  '  print "  -o <file>                Place the output into <file>"
RETURN

PROCEDURE doit
  ' ---------------------------- Open Comm Port ---------------------------------
  OPEN "UX:9600,N,8,1",#1,device$
  SYSTEM "dtr-init --rts "+device$
  'Reset to Program Load
  ' SaxComm1.DTREnable=True   ! On (HIGH) state
  ' SaxComm1.RTSEnable=True   ! RTS = positive voltage

  ' pause 0.040               ! Delay 40 milliseconds to charge the cap
  ' SaxComm1.RTSEnable=False  ! RTS = negative voltage, PROG LOAD (DMM board)

  ' pause 0.040               ! Charge the cap for EVM reset
  ' SaxComm1.RTSEnable=True   ! RTS = positive voltage, PROG LOAD (EVM board)
  PAUSE 0.040                 ! Wait for Reset to complete

  IF verbose
    PRINT "Reset for Program Load"
  ENDIF
  PRINT #1,CHR$(13);
  FLUSH #1
  PAUSE 0.1
  ' wait for prompt
  IF @timeout(">",1)
    PRINT "timeout, no connection to board."
    STOP
  ENDIF
  '------------- Write Timer Registers ---------------------------------
  IF verbose
    PRINT "Write Timer Registers"
  ENDIF
  @sendmessage("RWFB"+hex$(Oneusec,2,2)) ! Write Reg(FB) = ONEUSEC
  @sendmessage("RWFC"+hex$(OneMSL,2,2))  ! Write Reg(FC) = OneMSL
  @sendmessage("RWFD"+hex$(OneMSH,2,2))  ! Write Reg(FD) = OneMSH
  @sendmessage("RWEF"+hex$(Ftscal,1,1)+hex$(Ftpres,1,1)) ! Write Reg(EF) = Ftpres & Ftscal
  @sendmessage("RWFE"+hex$(Hundms,2,2))  ! Write Reg(FE) = Hundms

  '------------------- Mass or Page Erase -------------------------
  IF verbose
    PRINT "Mass or Page Erase"
  ENDIF
  i=esize*8   ! Number of seqments to erase
  IF esize<0
    @sendmessage("M0000")
  ELSE
    PRINT "  Erasing "+STR$(i)+" Flash Pages (128 bytes)"
    FOR j=0 TO i-1
      @sendmessage("CP"+hex$(j*128,4,4))
    NEXT j
  ENDIF
  '----------------------- If selected, erase HWCONFIG Bytes ------------
  IF hwconfig
    IF verbose
      PRINT "erase HWCONFIG Bytes."
    ENDIF
    @sendmessage("M8000")  ! Erase Configuration Bytes
  ENDIF
  @sendmessage("E") ! Turn off Echo
  '------------------------- Load Memory --------------------------------------
  PRINT "Load memory..."
  PRINT #1,"L";
  FLUSH #1
  OPEN "I",#2,inputfile$
  WHILE not eof(#2)
    LINEINPUT #2,t$
    @sendmessage2(t$+chr$(13)+chr$(10))
  WEND
  CLOSE #2

  ' end progam mode

  SYSTEM "dtr-init "+device$
  '   frmTerminal.SaxComm1.RTSEnable = True  'RTS = positive voltage
  '   frmTerminal.SaxComm1.DTREnable = True  'DTR = positive voltage
  '   Delay (400)                            'Wait for Program Load mode to end
  '   frmTerminal.SaxComm1.DTREnable = False  'DTR = negative voltage, Start Normal Reset
  '   Delay (40)                             'Delay 40 milliseconds
  '   frmTerminal.SaxComm1.DTREnable = True   'DTR = positive voltage, Finish Reset
RETURN
PROCEDURE sendmessage(m$)
  BPUT #1,VARPTR(m$),LEN(m$)
  FLUSH #1
  IF verbose>1
    PRINT "Send: "+m$
  ENDIF
  ' wait for prompt
  IF @timeout(">",1)
    PRINT "Communication timeout, stop"
    STOP
  ENDIF
RETURN
PROCEDURE sendmessage2(m$)
  LOCAL t,t$,sec
  sec=1

  BPUT #1,VARPTR(m$),LEN(m$)
  FLUSH #1
  '  if verbose>1
  PRINT "Send: "+m$
  '  endif
  ' wait for ACK
  t=TIMER
  DO
    IF INP?(#1)
      t$=CHR$(INP(#1))
      PRINT t$;
      IF t$="E"
        RETURN
      ELSE IF t$="."
        RETURN
      ELSE IF t$="T"
        PRINT "Got T, Transmission complete."
        RETURN
      ELSE IF t$="X"
        PRINT "Got X, FLASH Memory not erased."
        STOP
      ENDIF
    ENDIF
    EXIT IF TIMER-t>sec
  LOOP
  PRINT "Timeout."
  STOP
RETURN

FUNCTION timeout(n$,sec)
  LOCAL t,t$
  t=TIMER
  DO
    IF inp?(#1)
      t$=CHR$(INP(#1))
      PRINT t$;
      IF t$=n$
        RETURN 0
      ENDIF
    ENDIF
    EXIT if timer-t>sec
  LOOP
  RETURN -1
ENDFUNCTION
